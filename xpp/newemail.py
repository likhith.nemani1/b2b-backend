import smtplib, ssl
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import requests

from xpp import app

msg = MIMEMultipart('alternative')


def emailViaAPIRequest(subject, from_email, to_email, html_body_req, cc_email=None, bcc_email=None, reply_to = None, from_name = "InterviewBuddy Pro"):
	app.logger.info("emailViaAPIRequest")
	# from_email = "hello@interviewbuddy.app"
	url = "https://api.transmail.co.in/v1.1/email"
	app.logger.info("create payload here")
	html_body = html_body_req
	try:
		app.logger.info("email details")
		app.logger.info(subject)
		app.logger.info(from_email)
		app.logger.info(to_email)
		app.logger.info(html_body)
		payload = '{ "bounce_address":"bounce@bounce.interviewbuddy.app", "from": { "address": "'+from_email+'", "name": "'+from_name+'"}, "to": [{"email_address": {"address": "'+to_email[0]+'"}}], "subject":"'+subject+'", "htmlbody":"'+html_body+'", "track_clicks": true,"track_opens": true'

		if bcc_email:
			payload += ', "bcc": [{"email_address": {"address": "'+''.join(bcc_email)+'"}}]'
		
		if cc_email:
			payload += ', "cc": [{"email_address": {"address": "'+''.join(cc_email)+'"}}]'
		
		if reply_to:
			payload += ', "reply_to": [{"address": "'+''.join(reply_to)+'"}]'
			
		payload += '}'
		
		app.logger.info("*********************")
		app.logger.info(payload)
		app.logger.info("*********************")
		
		app.logger.info("header preparation")
		headers = {
		'accept': "application/json",
		'content-type': "application/json",
		'authorization': "Zoho-enczapikey PHtE6r1eE7/v32Et9BUJ7aPuFM+sZN4s9ek2JFEWudtFW6MAG00A/t4jwTLj/Rl4UvhFFfLInYo+5O6e4eqNJ2rrNm0eWGqyqK3sx/VYSPOZsbq6x00VtFUedUffUoTpddNi0yPWvNbTNA==",
		}
		app.logger.info("calling api request")
		response = requests.request("POST", url, data=payload, headers=headers)
		app.logger.info("response")
		app.logger.info(response)
		app.logger.info(response.text)
		app.logger.info("end of emailViaAPIRequest")
	except Exception as e:
		app.logger.info("error while sending email")
		app.logger.info(e)
		app.logger.info("end of emailViaAPIRequest")
				