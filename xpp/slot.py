# -*- coding: utf-8 -*-
# -*- coding: utf-8 -*-

from datetime import time, datetime, timedelta
from operator import itemgetter
from pytz import timezone
from xpp import app
from xpp import timeszone

import string
# IF slot(x) < slot(y) THEN time(x) < time(y) is MANDATORY
sd = {
	1: (0, 0),
	2: (0, 30),
	3: (1, 0),
	4: (1, 30),
	5: (2, 0),
	6: (2, 30),
	7: (3, 0),
	8: (3, 30),
	9: (4, 0),
	10: (4, 30),
	11: (5, 0),
	12: (5, 30),
	13: (6, 0),
	14: (6, 30),
	15: (7, 0),
	16: (7, 30),
	17: (8, 0),
	18: (8, 30),
	19: (9, 0),
	20: (9, 30),
	21: (10, 0),
	22: (10, 30),
	23: (11, 0),
	24: (11, 30),
	25: (12, 0),
	26: (12, 30),
	27: (13, 0),
	28: (13, 30),
	29: (14, 0),
	30: (14, 30),
	31: (15, 0),
	32: (15, 30),
	33: (16, 0),
	34: (16, 30),
	35: (17, 0),
	36: (17, 30),
	37: (18, 0),
	38: (18, 30),
	39: (19, 0),
	40: (19, 30),
	41: (20, 0),
	42: (20, 30),
	43: (21, 0),
	44: (21, 30),
	45: (22, 0),
	46: (22, 30),
	47: (23, 0),
	48: (23, 30)
}


# def get_date_and_time_format_email(date,slot_id):
# 	app.logger.info("in slot.py: get_date_and_time_format_email")
	
# 	time_slot = sd[slot_id]
# 	tm = time(time_slot[0], time_slot[1])
# 	equivalent_datetime = datetime.combine(date, tm)
# 	date_and_time = equivalent_datetime.strftime('%a %d  %B %Y, at, %I:%M %p')
# 	split_date_time =date_and_time.split(',')
# 	split_date = split_date_time[0]
# 	change_date_format = split_date.split(' ')
# 	format_date = change_date_format[0] +","+" "+ordinal(change_date_format[1])+change_date_format[2]+" "+change_date_format[3]+","+" "+ change_date_format[4]
# 	format_date_and_time = format_date+''+' '+split_date_time[1]+' '+split_date_time[2]+" "+"UTC"
	
# 	app.logger.info("done.. get_date_and_time_format_email")
# 	return str(format_date_and_time)

# Fectch time zone result
def getTimeZone(date_time,zone):
	app.logger.info("Accessing getTimeZone")
	date_str = date_time.strftime("%Y-%m-%d %H:%M:%S")
	app.logger.debug("date_str")
	app.logger.debug(date_str)
	datetime_obj_naive = datetime.strptime(date_str, "%Y-%m-%d %H:%M:%S")
	app.logger.debug("datetime_obj_naive")
	app.logger.debug(datetime_obj_naive)
	datetime_obj_asia = timezone(zone).localize(datetime_obj_naive)
	app.logger.debug("datetime_obj_asia")
	app.logger.debug(datetime_obj_asia)
	final_result = datetime_obj_asia.strftime("%Y-%m-%d %H:%M:%S %Z")
	app.logger.debug("final_result")
	app.logger.debug(final_result)
	split_date_time =final_result.split(' ')
	time_zone = split_date_time[2]
	app.logger.info("End of getTimeZone")
	return time_zone


# def getTimeZoneForAsync(date_and_time,zone):
# 	app.logger.info("Accessing getTimeZone")
# 	date_str = date_and_time.strftime("%Y-%m-%d %H:%M:%S")
# 	app.logger.debug("date_str")
# 	app.logger.debug(date_str)
# 	datetime_obj_naive = datetime.strptime(date_str, "%Y-%m-%d %H:%M:%S")
# 	app.logger.debug("datetime_obj_naive")
# 	app.logger.debug(datetime_obj_naive)
# 	datetime_obj_asia = timezone(zone).localize(datetime_obj_naive)
# 	app.logger.debug("datetime_obj_asia")
# 	app.logger.debug(datetime_obj_asia)
# 	final_result = datetime_obj_asia.strftime("%Y-%m-%d %H:%M:%S %Z")
# 	app.logger.debug("final_result")
# 	app.logger.debug(final_result)
# 	split_date_time =final_result.split(' ')
# 	time_zone = split_date_time[2]
# 	app.logger.info("End of getTimeZone")
# 	return time_zone


# To change the date format to 'Tue , 01st Jan , 2017 at 00:00 AM'	
def get_date_and_time_format_email(date,slot_id,zone):
	app.logger.info("in slot.py: get_date_and_time_format_email")
	app.logger.info("date")
	app.logger.info(date)
	#check Dst
	yr,mn,dy=str(date).split("-")
	dst_status=timeszone.is_dst(datetime(int(yr),int(mn),int(dy)), timezone=zone)
	app.logger.info("dst_status")
	app.logger.info(dst_status)

	try:
		time_slot = sd[slot_id]
		tm = time(time_slot[0], time_slot[1])
		app.logger.info("process timezone")
		zone_hours = timeszone.Timezones.timezones[zone]
		app.logger.debug("zone_hours")
		app.logger.debug(zone_hours)
		hour = zone_hours[0]
		minute = zone_hours[1]
		equivalent_datetime = datetime.combine(date, tm) + timedelta(hours=hour , minutes=minute)
		app.logger.debug("equivalent_datetime before DST check")
		app.logger.debug(equivalent_datetime)
		if dst_status==True :
			app.logger.info("Currently this timezone have DST")
			equivalent_datetime = equivalent_datetime + timedelta(hours=1 ,minutes = 0)
			app.logger.info(equivalent_datetime)
		else:
			app.logger.info("Currently this timezone doesn't have any DST")
		app.logger.debug("equivalent_datetime after DST check")
		app.logger.debug(equivalent_datetime)
		date_and_time = equivalent_datetime.strftime('%a %d  %B %Y, at, %I:%M %p')
		app.logger.info("date_and_time")
		app.logger.info(date_and_time)
		time_zone = getTimeZone(datetime.combine(date, tm),zone)
		app.logger.info("time_zone")
		app.logger.info(time_zone)
		split_date_time =date_and_time.split(',')
		split_date = split_date_time[0]
		change_date_format = split_date.split(' ')
		format_date = change_date_format[0] +","+" "+ordinal(change_date_format[1])+change_date_format[2]+" "+change_date_format[3]+","+" "+ change_date_format[4]
		format_date_and_time = format_date+''+' '+split_date_time[1]+' '+split_date_time[2]+' '+time_zone
		app.logger.info("format_date_and_time")
		app.logger.info(format_date_and_time)
		app.logger.info("End of get_date_and_time_format_email")
	except Exception as e:
		app.logger.info("error while converting timezone")
		app.logger.info(e)
		app.logger.info("now generate it using UTC timezone")
		time_slot = sd[slot_id]
		tm = time(time_slot[0], time_slot[1])
		equivalent_datetime = datetime.combine(date, tm)
		date_and_time = equivalent_datetime.strftime('%a %d  %B %Y, at, %I:%M %p')
		split_date_time =date_and_time.split(',')
		split_date = split_date_time[0]
		change_date_format = split_date.split(' ')
		format_date = change_date_format[0] +","+" "+ordinal(change_date_format[1])+change_date_format[2]+" "+change_date_format[3]+","+" "+ change_date_format[4]
		format_date_and_time = format_date+''+' '+split_date_time[1]+' '+split_date_time[2]+' UTC '		
		app.logger.info("format_date_and_time")
		app.logger.info(format_date_and_time)
		app.logger.info("done.. get_date_and_time_format_email")
	app.logger.info("format_date_and_time")
	app.logger.info(format_date_and_time)
	dt_num = format_date_and_time.split(",")[1].split(" ")[1]
	new_format = format_date_and_time.split(",")[0] + " " + dt_num[:len(dt_num) - 2] +' '+ format_date_and_time.split(",")[1].split(" ")[2][0:3] + ", " + format_date_and_time.split(",")[2].split("at  ")[1]
	app.logger.info("new_format")
	app.logger.info(new_format)
	return str(new_format)
	

# To get suffix for date like 1st,2nd,3rd ...	
def ordinal( n ):

    suffix = ['th', 'st', 'nd', 'rd', 'th', 'th', 'th', 'th', 'th', 'th']

    if n < 0:
        n *= -1

    n = int(n)

    if n % 100 in (11,12,13):
        s = 'th'
    else:
        s = suffix[n % 10]

    return str(n) + s

	
# To change the date format to '01 Jan' 2017'		
def get_date_format(date):
	app.logger.info("in slot.py: get_date_format()")
	date_format= date.strftime('%d-%B-%Y')
	split_date =date_format.split('-')
	format_date = split_date[0] +" "+ split_date[1] +" "+split_date[2]
	app.logger.info("done .. get_date_format")
	return format_date

def upcoming_slot(at_time=None):
	app.logger.info("in upcoming_slot()")
	if not at_time:
		at_time = datetime.now().time()
		pr_time = datetime.now()- timedelta(minutes=30)
		
	slots_with_time = [(x[0], time(x[1][0], x[1][1])) for x in sd.iteritems()]
	slots_asc_by_time = sorted(slots_with_time, key=itemgetter(1), reverse=False)  # slots_asc_by_time = slots_with_time  # as sd is initialized sorted on slot
	us = None
	td = timedelta()
	
	for s, tm in slots_asc_by_time:
		if at_time < tm:
			us = s
			break
	if us is None:
		us = slots_asc_by_time[0][0]
		td = timedelta(days=1)
		
	ret = (us, td)
	
	app.logger.info("done.. upcoming_slot()")
	return ret


def add_secs(tm, secs):
	dummy_dt = datetime.combine(datetime(5000, 1, 1), tm)
	dummy_dt = dummy_dt + timedelta(seconds=secs)
	return dummy_dt.time()


def completed_slot(at_time=None):
	app.logger.info("in completed_slot()")
	if not at_time:
		at_time = datetime.now().time()
	slots_with_end_time = [(x[0], add_secs(time(x[1][0], x[1][1]), app.config['SESSION_LENGTH'])) for x in sd.iteritems()]
	slots_desc_by_end_time = sorted(slots_with_end_time, key=itemgetter(1), reverse=True)
	cs = None
	td = timedelta()
	for s, tm in slots_desc_by_end_time:
		if at_time > tm:
			cs = s
			break
		elif s == 1:
			td = timedelta(days=-1)
	if cs is None:
		cs = slots_desc_by_end_time[0][0]
	ret = (cs, td)
	return ret

	
def get_remaining_time(sl):
	app.logger.info("in slot.py: get_remaining_time()")
	current_time = datetime.now().time()
	time_slot_cal = sd[sl]
	slot_time = time(time_slot_cal[0],time_slot_cal[1]) 
	diff_sec=current_time.second- slot_time.second;
	time_difference = current_time.minute- slot_time.minute
	if diff_sec>=0:
		remaining_time= 30- time_difference-1
		current_time_sec =60-(current_time.second- slot_time.second)
		ret= {'remaining_time':remaining_time , 'current_time_sec': current_time_sec}
	
	app.logger.info("return from get_remaining_time()")
	return ret

	#this method will return how many seconds left for received date and slot
def get_now_delta_seconds(dt, sl):
	app.logger.info("in get_now_delta_seconds()")
	app.logger.info("this method will return how many seconds left for received date and slot")
	time_slot = sd[sl]
	tm = time(time_slot[0], time_slot[1])
	equivalent_datetime = datetime.combine(dt, tm)
	now_delta_seconds = int((equivalent_datetime - datetime.utcnow()).total_seconds())
	app.logger.info("return from get_now_delta_seconds()")
	return now_delta_seconds
	
	
# To convert date and slots to  time	
def get_time_by_slotid(sl):
	app.logger.info('in slot.py get_time_by_slotid()')
	time_slot = sd[sl]
	tm = time(time_slot[0], time_slot[1])
	app.logger.info('return from get_time_by_slotid()')
	return tm
	
	
#To convert date,slots to eqivalent datetime
def get_now_delta_date(dt, sl):
	app.logger.info("in slot.py: get_now_delta_date()")
	time_slot = sd[sl]
	tm = time(time_slot[0], time_slot[1])
	equivalent_datetime = datetime.combine(dt, tm)
	nextday_datetime = equivalent_datetime+timedelta(days=2)
	
	current_datetime = datetime.now()
	if nextday_datetime < current_datetime:
		datetime_check_flag =True
	else:
		datetime_check_flag =False
		
		
	app.logger.info("return from get_now_delta_date()")
	return datetime_check_flag
	
	
def get_date_and_time(dt, sl):
	app.logger.info("in slot.py: get_date_and_time()")
	time_slot = sd[sl]
	tm = time(time_slot[0], time_slot[1])
	equivalent_datetime = datetime.combine(dt, tm)
	return equivalent_datetime


def get_local_date_and_time(dt,zone):
	app.logger.info("get_local_date_and_time")
	try:
		zone_hours = timeszone.Timezones.timezones[zone]	
		app.logger.debug("zone_hours")
		app.logger.debug(zone_hours)
		hour = zone_hours[0]
		minute = zone_hours[1]
		equivalent_datetime = dt + timedelta(hours=hour , minutes=minute)
		app.logger.debug("equivalent_datetime")
		app.logger.debug(equivalent_datetime)
		date_and_time = equivalent_datetime.strftime('%a %d  %B %Y, at, %I:%M %p')
		app.logger.info("date_and_time")
		app.logger.info(date_and_time)
		time_zone = getTimeZone(equivalent_datetime,zone)
		app.logger.info("time_zone")
		app.logger.info(time_zone)
		split_date_time =date_and_time.split(',')
		split_date = split_date_time[0]
		change_date_format = split_date.split(' ')
		format_date = change_date_format[0] +","+" "+ordinal(change_date_format[1])+change_date_format[2]+" "+change_date_format[3]+","+" "+ change_date_format[4]
		format_date_and_time = format_date+''+' '+split_date_time[1]+' '+split_date_time[2]+' '+time_zone
		app.logger.info("format_date_and_time")
		app.logger.info(format_date_and_time)
		app.logger.info("End of get_local_date_and_time")
		return str(format_date_and_time)
	except KeyError as ke:
		equivalent_datetime = dt
		app.logger.debug("equivalent_datetime")
		app.logger.debug(equivalent_datetime)
		date_and_time = equivalent_datetime.strftime('%a %d  %B %Y, at, %I:%M %p')
		app.logger.info("date_and_time")
		app.logger.info(date_and_time)
		split_date_time =date_and_time.split(',')
		split_date = split_date_time[0]
		change_date_format = split_date.split(' ')
		format_date = change_date_format[0] +","+" "+ordinal(change_date_format[1])+change_date_format[2]+" "+change_date_format[3]+","+" "+ change_date_format[4]
		format_date_and_time = format_date+''+' '+split_date_time[1]+' '+split_date_time[2]+' UTC '
		app.logger.info("format_date_and_time")
		app.logger.info(format_date_and_time)
		app.logger.info("End of get_local_date_and_time")
		return str(format_date_and_time)
