# -*- coding: utf-8 -*-

from xpp import app
import lxml.builder as lb
from lxml import etree
import requests
import json
from xpp.cst import C
from flask.ext.restful import abort

MSG91_XML_URL = 'https://control.msg91.com/api/postsms.php'

def send_sms(mobile_msg_tuple_list, common_message=False, DLT_TE_ID = False):
    app.logger.info("In send_sms")
    app.logger.info(mobile_msg_tuple_list)
    app.logger.info(common_message)
    app.logger.info(DLT_TE_ID[0])
    sms = lb.E.MESSAGE(
        lb.E.AUTHKEY(app.config['MSG91_KEY']),
        lb.E.ROUTE('4'),
        lb.E.COUNTRY('91'),
        lb.E.SENDER(app.config['MSG91_SENDER']),
        lb.E.UNICODE('1'),
        lb.E.DLT_TE_ID(DLT_TE_ID[1])
    )
    if common_message:
        msg = lb.E.SMS(TEXT=common_message)
        for i in mobile_msg_tuple_list:
            app.logger.info(i)
            etree.SubElement(msg, 'ADDRESS').attrib['TO'] = i
            sms.append(msg)
    else:
        for i in mobile_msg_tuple_list:
            msg = lb.E.SMS(lb.E.ADDRESS(TO=i[0]), TEXT=i[1])
            sms.append(msg)
    app.logger.info(etree.tostring(sms, pretty_print=True))
    r = requests.post(MSG91_XML_URL, data={'data': etree.tostring(sms)}, verify=False)
    app.logger.info(r.text)

