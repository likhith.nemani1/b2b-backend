# -*- coding: utf-8 -*-

from xhtml2pdf import pisa
import cStringIO as StringIO
import shutil
from xpp import app



def htmltopdf(html):
	try:
		fo = StringIO.StringIO()
	# app.logger.info('In html to pdf')
	# app.logger.info('fo')
	# app.logger.info(fo)
	# app.logger.info('html')
	# app.logger.info(html)
		pisastatus = pisa.CreatePDF(html, dest=fo)
		return fo
	# app.logger.info('pisastatus')
	# app.logger.info(pisastatus)
	except:
		e = sys.exc_info()[0]
		app.logger.info("in pdf.py: error while generating pdf")
		app.logger.info(e)
	
def writefile(sio, fn):
	with open(fn, 'wb') as fo:
		sio.seek(0)
		shutil.copyfileobj(sio, fo)

