# -*- coding: utf-8 -*-

import logging
from logging.handlers import RotatingFileHandler

def logg(file_name):
	# log_formatter = logging.Formatter("%(asctime)s [%(name)s] [%(levelname)s] [%(pathname)s:%(funcName)s:%(lineno)d]\n%(message)s\n")
	# log_formatter = logging.Formatter("%(asctime)s - %(clientip)s - %(levelname)s - %(lineno)d - %(module)s - %(pathname)s - %(process)d - %(processName)s - %(thread)d - %(threadName)s - %(message)s \n")
	log_formatter = logging.Formatter("[ %(asctime)s : %(levelname)s/%(processName)s ] - %(module)s - [%(pathname)s:%(funcName)s:%(lineno)d] - %(message)s \n")
	root_logger = logging.getLogger()

	#FOR INFO LOGS (ONLY INFO)
	#root_logger.setLevel(logging.INFO)

	#FOR DEBUG LOGS (ALL LOGS)
	root_logger.setLevel(logging.DEBUG)

	#FOR WARNING LOGS (WARNING, ERROR and CRITICAL)
	#root_logger.setLevel(logging.WARNING)

	#FOR ERROR LOGS (ONLY ERROR and CRITICAL)
	#root_logger.setLevel(logging.ERROR)

	#FOR CRITICAL LOGS (ONLY CRITICAL)
	# root_logger.setLevel(logging.CRITICAL)

	#file_handler = logging.FileHandler(file_name)
	# make 10 MB log size and every 10MB, it'll create new one and old one recent shold be .1, .2 ...
	file_handler = RotatingFileHandler(file_name, mode='a', maxBytes=10*1024*1024, 
                                 backupCount=5, encoding=None, delay=0)
	file_handler.setFormatter(log_formatter)
	root_logger.addHandler(file_handler)

	console_handler = logging.StreamHandler()
	console_handler.setFormatter(log_formatter)
	root_logger.addHandler(console_handler)
	
	