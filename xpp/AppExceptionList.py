class RequiredFieldsMissing(Exception):
	pass


class ValueMissing(Exception):
	pass

class invalidEmailError(Exception):
	pass

class emptyStringError(Exception):
	pass

class TempException(Exception):
	def __init__(self, value):
		self.value = value
	def __str__(self):
		return repr(self.value)