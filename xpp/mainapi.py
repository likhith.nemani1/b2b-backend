from __future__ import division
from datetime import time, datetime, timedelta,date
import sys
import binascii
from flask import json
from flask import render_template
from flask import request, send_file
from flask import redirect, url_for
from flask.ext.restful import Api, abort
from flask.ext.restful import Resource as RestResource
from sqlalchemy import exc, func, or_, and_ ,MetaData, Table, distinct
from sqlalchemy.sql import table, column, select, update, insert
from sqlalchemy.dialects import postgresql
from sqlalchemy.exc import *
from simplejson import JSONDecodeError

from webargs.flaskparser import parser

from xpp import app
from xpp.dbtables import *
from xpp.mainschemas import *
from xpp.cst import C
from xpp.url import L
from xpp import db
from xpp import slot
from xpp import pdf
import numpy as np
import numpy.ma as ma
from collections import OrderedDict
from xpp.act import organizationSignatureAndLogo,notify_interviewer_for_feedback, notify_schedule, update_interviews, update_async_interviewstatus, subscribed_minutes, automateTestMySetupOtTokensRenewal
from xpp.tq import notify_schedule_task
from xpp.AppExceptionList import RequiredFieldsMissing,ValueMissing, TempException,invalidEmailError,emptyStringError
from sqlalchemy.exc import IntegrityError
from sqlalchemy.exc import InvalidRequestError
import sqlalchemy
import re
from xpp import ot

import calendar
import math
from xpp import archive
import os
from pytz import timezone

from xpp.em import Email

import random
import string

import cStringIO as StringIO
import csv

from msg import send_email

import requests, json

api = Api(app, prefix='/api')

request_locations = ('json', 'form')
reload(sys)
sys.setdefaultencoding('utf8')

def verify_user():
	app.logger.info("verify_user")
	app.logger.info(request.authorization)
	if request.authorization:
		if request.authorization.username:
			ua = User.verify_t(request.authorization.username if request.authorization else '')
			app.logger.info(ua)
			if not ua:
				return None, 401		
			app.logger.info("verify_user successfull")
			return ua, 200
		else:
			return None, 401

	else:
		return None, 401

def user_auth(userid=None, admin_access=C.NORMAL, return_requestor=False):
	ur,code = verify_user()
	if ur:
		if admin_access == C.NORMAL:
			app.logger.info("C.NORMAL")
			return True, ur
		elif admin_access == C.SUPERADMIN:
			if user_is_super_admin(ur):
				return True, ur
			else:
				return False, ur
		elif admin_access == C.ADMIN:
			if user_is_admin(ur):
				return True, ur
			else:
				return False, ur
		elif admin_access == C.INTERVIEWER:
			if user_is_interviewer(ur):
				return True, ur
			else:
				return False, ur
		elif admin_access == C.INTERVIEWEE:
			if user_is_candidate(ur):
				return True, ur
			else:
				return False, ur
		else:
			return False, ur
	else:
		return False, None
				

def user_is_disabled(s):
	if s >= C.ENABLED:
		return False
	else:
		return True



def user_is_super_admin(s):
	if s.role == C.SUPERADMIN:
		return True
	else:
		return False



def user_is_admin(s):
	if s.role == C.ADMIN:
		return True
	else:
		return False


# Check user authorization here
def user_is_interviewer(s):
	if s.role == C.INTERVIEWER:
		return True
	else:
		return False


def user_is_candidate(s):
	if s.role == C.INTERVIEWEE:
		return True
	else:
		return False


def getOrgLogo(org_id):
	app.logger.info("find logo of the organization "+ str(org_id))
	
	org = Organization().query.filter(Organization.orgid==org_id).first()
	if org.logo is not None:
		app.logger.debug("organization logo exist")
		logo = org.logo
		app.logger.debug(logo)
	else:
		app.logger.debug("organization logo not exist")
		app.logger.debug("return default organization logo")
		logo = "IBU&U.png"
		app.logger.debug(logo)
	# get absolute path of logo 
	server_add = app.config['APP_URL']
	logo_full_path = server_add+'/logos/'+logo

	app.logger.info("logo full path")
	app.logger.info(logo_full_path)
	return logo_full_path

# Authentication Service
class ApiAuth(RestResource):
	def get(self):
		clientip = request.remote_addr
		app.logger.info("Accessing ApiAuth - "+str(clientip))
		user_data = user_auth()
		user = user_data[1]
		if user:
			app.logger.info(user)
			user_orgid = user.orgid
			organization = Organization().query.get(user_orgid)
			if organization.status:

				token = user.generate_t()
				app.logger.info("Token generated successfully - "+str(clientip)+"- [ "+str(user.userid)+" ] ")
				# ret = {'userid': user.userid, 'token': token}
				logo = getOrgLogo(user.orgid)
				app.logger.info("End of ApiAuth by - "+str(clientip)+" - [ "+str(user.userid)+" ] ")
				return {'status':True, 'message':'APIAUTH_TOKEN_VALIDATED', 'status_code':200,'userid':user.userid,'token':token,'logo':logo}
	
			else:
				app.logger.info("Token not generated organzation status is inactive  - "+str(clientip))
				app.logger.info("End of ApiAuth by - "+str(clientip)+" - [ "+str(user.userid)+" ] ")
				return {'status':False, 'message':'APIAUTH_ORGANIZATION_IS_INACTIVE', 'status_code':405}
		else:
			app.logger.info("Token not generated  - "+str(clientip))
			app.logger.info("End of ApiAuth by - "+str(clientip))
			return {'status':True, 'message':'APIAUTH_TOKEN_NOT_GENERATED', 'status_code':405}
	
	def post(self):
		clientip = request.remote_addr
		app.logger.info("Accessing ApiAuth - " + str(clientip))
		app.logger.debug("User trying to login - " + str(clientip))
		try:
			args = parser.parse(AuthSchema(), request, locations=request_locations)
			app.logger.info("User EMAIL: "+ args['email'] + " - " + str(clientip))
			app.logger.debug("Check email exist or not - " + str(clientip))
			user = User.query.filter(and_(func.lower(User.email) == func.lower(args['email'])),or_(User.role==C.ADMIN,User.role==C.SUPERADMIN)).first()
			if user:
				app.logger.info("user "+args['email']+" exist  - " + str(clientip))
				app.logger.debug("check password")
				if not user.verify_p(args['password']):
					app.logger.info("invalid credentails  - " + str(clientip))

					return {'status':False, 'message':'APIAUTH_INVALID_CRED', 'status_code':403}
				if user_is_disabled(user.status):
					app.logger.info("Account blocked  - " + str(clientip))
					return {'status':False, 'message':'APIAUTH_ERROR_ACCOUNT_BLOCK', 'status_code':401}
			else:
				app.logger.info("THIS EMAIL ID NOT REGISTERED WITH US  - " + str(clientip))
				return {'status':False, 'message':'APIAUTH_ERROR_UNREGISTERED_EMAIL', 'status_code':403}

				
			ret={}
			app.logger.info("login successfull - " + str(clientip))
			user.last_login_time = datetime.utcnow()
			app.logger.debug("update last_login_time time - " + str(clientip))
			db.session.commit()
			app.logger.info("generat token  - " + str(clientip))
			user_orgid = user.orgid
			organization = Organization().query.get(user_orgid)
			if organization.status:
				token = user.generate_t()
				logo = getOrgLogo(user.orgid)
				app.logger.debug("End user details along with generated token  - " + str(clientip))
				ret = {'status':True, 'message':'APIAUTH_SUCCESS_AUTHENTICATION', 'status_code':201, 'userid': user.userid, 'token': token, 'mobile': user.mobile, 'email': user.email, 'role':user.role, 'orgid': user.orgid, 'firstname':user.firstname, 'lastname':user.lastname,'logo':logo}
				app.logger.info("End of login  - " + str(clientip))
				return ret, 200
			else:
				app.logger.info("Token not generated organzation status is inactive  - "+str(clientip))
				app.logger.info("End of ApiAuth by - "+str(clientip)+" - [ "+str(user.userid)+" ] ")
				return {'status':False, 'message':'APIAUTH_ORGANIZATION_IS_INACTIVE', 'status_code':405}

		except Exception as e:
			app.logger.error("error while authentication  - " + str(clientip))
			app.logger.error(e)
			return {'status':False, 'message':'COMMON_ERROR_IN_AUTHENTICATION', 'status_code':422}



# Organization Creation
class ApiCreateOrganization(RestResource):
	def post(self):
		app.logger.info("Accessing ApiCreateOrganization")
		clientip = request.remote_addr
		app.logger.debug("Check user token - " + str(clientip))
		try:
			status, user = user_auth(admin_access=C.SUPERADMIN)
			if status and user:
				app.logger.debug("User is authorized to access ApiCreateOrganization service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")

				# read parameters
				try:
					args = parser.parse(OrganizationCreateSchema(), request, locations=request_locations)
				except Exception as e:
					app.logger.info("error while parsing organization details - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.exception(e)
					app.logger.info("End of ApiCreateOrganization - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {'status': False, 'message': 'APICREATEORG_FIELDS_MISSING', 'code':422}
				else:
					app.logger.info("args")
					app.logger.info(args)
					app.logger.debug("request fileds received - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					try:
						organization = Organization()
						name = args['name']
						isNamevalid = validateString(name)
						if isNamevalid:
							app.logger.debug("Name is not Empty - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							organization.name = name
						else:
							app.logger.debug("Name empty "+str(name)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							raise emptyStringError()

						preferredname = args['preferredname']
						isPreferredNamevalid = validateString(preferredname)
						if isPreferredNamevalid:
							app.logger.debug("Preferred name is not Empty - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							organization.preferredname = preferredname
						else:
							app.logger.debug("Preferred name empty "+str(preferredname)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							raise emptyStringError()

						url = args['url']
						isURLValid = validateString(url)
						if isURLValid:
							app.logger.debug("Url is not Empty - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							organization.url = url
						else:
							app.logger.debug("Url empty "+str(url)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							raise emptyStringError()


						address = args['address']
						isAddressValid = validateString(address)
						if isAddressValid:
							app.logger.debug("Url is not Empty - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							organization.address = address
						else:
							app.logger.debug("Url empty "+str(address)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							raise emptyStringError()


						location = args['location']
						isLocationValid = validateString(location)
						if isLocationValid:
							app.logger.debug("Location is not Empty - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							organization.location = location
						else:
							app.logger.debug("Location empty "+str(address)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							raise emptyStringError()

							
						country = args['country']
						isCountryValid = validateString(country)
						if isCountryValid:
							app.logger.debug("Country is not Empty - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							organization.country = country
						else:
							app.logger.debug("Country empty "+str(location)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							raise emptyStringError()

						contactperson = args['contactperson']
						isContactPersonValid = validateString(contactperson)
						if isContactPersonValid:
							app.logger.debug("ContactPerson is not Empty - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							organization.contactperson = contactperson
						else:
							app.logger.debug("ContactPerson empty "+str(address)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							raise emptyStringError()						
						
						
						contactemail = args['contactemail']
						isContactPersonEmailValid = validateEmail(contactemail)
						if isContactPersonEmailValid:
							app.logger.debug("ContactPersonEmail is not Empty - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							organization.contactemail = contactemail
						else:
							app.logger.debug("ContactPersonEmail is empty "+str(address)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							raise invalidEmailError()						

						#signature
						signature = args['signature']
						isSignaturevalid = validateString(signature)
						if isSignaturevalid:
							app.logger.debug("Signature is not Empty - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							organization.signature = args['signature']
						else:
							app.logger.debug("Signature empty "+str(signature)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							raise emptyStringError()

						#mobile
						mobile = args['mobile']
						isMobile = validateString(mobile)
						if isMobile:
							app.logger.debug("Mobile not empty "+str(mobile)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							if mobile.isdigit():
								mobile = long(mobile)
								if mobile > 0:
									organization.mobile = args['mobile'] 
									app.logger.debug("Mobile is exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
								else:
									app.logger.debug("Mobile number is invalid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
									app.logger.info("End of ApiCreateOrganization - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
									return {'status': False, 'message': 'COMMON_MOBILE_NUMBER_INVALID', 'code':422}	
							else:
								app.logger.debug("Mobile number is not digit - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
								app.logger.info("End of ApiCreateOrganization - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
								return {'status': False, 'message': 'COMMON_MOBILE_NUMBER_NOT_DIGIT', 'code':422}	
						else:
							app.logger.debug("Mobile not empty "+str(args['mobile'])+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							raise emptyStringError()

						
						phone = args['phone']
						isPhoneMobile = validateString(phone)
						if isPhoneMobile:
							app.logger.debug("Phone number not empty "+str(phone)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							if phone.isdigit():
								phone = long(phone)
								if phone > 0:
									organization.phone = args['phone'] 
									app.logger.debug("Phone number is exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
								else:
									app.logger.debug("Phone number is invalid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
									app.logger.info("End of ApiCreateOrganization - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
									return {'status': False, 'message': 'COMMON_MOBILE_NUMBER_INVALID', 'code':422}	
							else:
								app.logger.debug("Phone number is not digit - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
								app.logger.info("End of ApiCreateOrganization - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
								return {'status': False, 'message': 'COMMON_MOBILE_NUMBER_NOT_DIGIT', 'code':422}	
						else:
							app.logger.debug("Phone number not empty "+str(args['phone'])+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							raise emptyStringError()


						alternative = args['alternative']
						isAlternativeMobile = validateString(alternative)
						if isAlternativeMobile:
							app.logger.debug("Alternate Mobile not empty "+str(alternative)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							if alternative.isdigit():
								alternative = long(alternative)
								if phone > 0:
									organization.alternative = args['alternative'] 
									app.logger.debug("Alternate Mobile is exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
								else:
									app.logger.debug("Alternate Mobile number is invalid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
									app.logger.info("End of ApiCreateOrganization - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
									return {'status': False, 'message': 'COMMON_MOBILE_NUMBER_INVALID', 'code':422}	
							else:
								app.logger.debug("Alternate Mobile number is not digit - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
								app.logger.info("End of ApiCreateOrganization - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
								return {'status': False, 'message': 'COMMON_MOBILE_NUMBER_NOT_DIGIT', 'code':422}	
						else:
							app.logger.debug("Alternate Mobile not empty "+str(args['alternative'])+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							raise emptyStringError()

						#status
						if args['status'] == 1:
							app.logger.debug("Status is True - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							organization.status = True
						elif args['status'] == 0:
							app.logger.debug("Status is False - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							organization.status = False
						else:
							raise TempException()
					
						
						organization.createdby = user.userid
						organization.createdtime = datetime.utcnow()

						app.logger.debug("add organization to database - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						db.session.add(organization)
				
						app.logger.debug("Update database - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						db.session.commit()
						app.logger.info("End of ApiCreateOrganization - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						orgId = organization.orgid
						return {'status': True, 'message': 'APICREATEORG_SUCCESSFULLY_CREATE', 'orgid':orgId, 'code':201}
					except TempException as te:
						app.logger.info(te.message +" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						app.logger.exception(te)
						app.logger.info("End of ApiCreateOrganization - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						db.session.rollback()
						return {'status': False, 'message': te.message , 'code':422}

					except IntegrityError as ie:
						app.logger.info("Error: DB Exceeding limit Error - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						app.logger.error(ie.message)
						app.logger.info("End of ApiCreateOrganization - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						db.session.rollback()
						return {'status': False, 'message': 'COMMON_ERROR_CONTACT_SUPPORT', 'code':409}
					except DataError as de:
						app.logger.info("DataBase field value out of range - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						app.logger.error(de.message)
						app.logger.info("End of ApiCreateOrganization - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						db.session.rollback()
						return {'status': False, 'message': 'COMMON_ERROR_INVALID_DATA', 'code':422}
					except InvalidRequestError as ir:
						app.logger.info("received invalid request - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						app.logger.exception(ir)
						app.logger.error(ir.message)
						app.logger.info("End of ApiCreateOrganization - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						db.session.rollback()
						return {'status': False, 'message': 'COMMON_ERROR_INVALID_REQUSET', 'code':422}
					except ValueError as ve:
						app.logger.info("Date Format is invalid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						app.logger.error(ve.message)
						app.logger.info("End of ApiCreateOrganization - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						db.session.rollback()
						return {'status': False, 'message': str(ve.message), 'code':422}
					except invalidEmailError as iee:
						app.logger.info("Email ID is not valid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						app.logger.exception(iee)
						app.logger.error(iee.message)
						app.logger.info("End of ApiCreateOrganization - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						db.session.rollback()
						return {'status': False, 'message': 'COMMON_ERROR_EMAIL_NOT_VALID', 'code':422}
					except emptyStringError as ese:
						app.logger.info("One of the field is Empty - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						app.logger.exception(ese)
						app.logger.error(ese.message)
						app.logger.info("End of ApiCreateOrganization - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						db.session.rollback()
						return {'status': False, 'message': 'COMMON_ERROR_ONE_OF_EMPTY_FIELD', 'code':422}						
					except Exception as orgerror:
						app.logger.info("Error while Accessing values to org object - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						app.logger.exception(orgerror)
						app.logger.info("End of ApiCreateOrganization - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						return {'status': False, 'message': 'COMMON_ERROR_REQ_MORE_FIELD', 'code':422}
			elif user:
				app.logger.debug("User is not authorized to access ApiCreateOrganization service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.info("End of ApiCreateOrganization - " + str(clientip))
				return {'status': False,  'message': 'COMMON_UNAUTHORIZED', 'code':403}
			else:
				app.logger.info("Invalid token or Session expired - " + str(clientip))
				app.logger.info("End of ApiCreateOrganization - " + str(clientip))
				return {'status': False, 'message': 'COMMON_INVALID_TOKEN', 'code':401}
		except Exception as e:
			app.logger.info("Error while checking authentication - " + str(clientip))
			app.logger.exception(e)
			app.logger.info("End of ApiCreateOrganization - " + str(clientip))
			return {'status': False, 'message': 'COMMON_ERROR_IN_AUTHENTICATION', 'code':422}
		

# Organization Update
class ApiUpdateOrganization(RestResource):
	def post(self, orgid):
		app.logger.info("Accessing ApiUpdateOrganization")
		clientip = request.remote_addr

		app.logger.debug("Check user token - " + str(clientip))
		app.logger.debug("request.args")
		app.logger.debug(request.args)
		app.logger.debug("request.data")
		app.logger.debug(request.data)
		
		app.logger.debug("Check user token - " + str(clientip))
		
		try:
			status, user = user_auth(admin_access=C.SUPERADMIN)
			if status and user:
				app.logger.debug("User is authorized to access ApiUpdateOrganization service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.debug("Check orgid is received or not - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")

				if orgid:
					app.logger.debug("Organization id "+str(orgid)+" received - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.debug("check Organization "+str(orgid)+" exit or not - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					
					organization = Organization().query.get(orgid)
					if organization:
						app.logger.debug("Organization "+str(orgid)+" found in database - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						pass
					else:
						app.logger.info("Organization id "+str(orgid)+" not found - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						app.logger.info("End of ApiUpdateOrganization - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						return {'status': False, 'message': 'APIORGUPDATE_ORG_NOT_FOUND', 'code':404}
				else:
					app.logger.info("Organization id "+str(orgid)+" not received - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.info("End of ApiUpdateOrganization - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {'status': False, 'message': 'COMMON_ERROR_REQ_MORE_FIELD', 'code':422}
				
				# read parameters
				try:
					# organization = Organization(**args)
					app.logger.debug("Parse organization input - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					args = parser.parse(OrganizationUpdateSchema(partial=('name','preferredname','url','address','location','country','contactperson','contactemail','mobile','phone','alternative','status')), request, locations=request_locations)
					app.logger.debug("Parsed succussfully - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.info("Request args")
					app.logger.info(args)
					name = args['name']
					isNamevalid = validateString(name)
					if isNamevalid:
						app.logger.debug("Name is not Empty - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						organization.name = name
					else:
						app.logger.debug("Name empty "+str(name)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						raise emptyStringError()

					preferredname = args['preferredname']
					isPreferredNamevalid = validateString(preferredname)
					if isPreferredNamevalid:
						app.logger.debug("Preferred name is not Empty - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						organization.preferredname = preferredname
					else:
						app.logger.debug("Preferred name empty "+str(preferredname)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						raise emptyStringError()

					url = args['url']
					isURLValid = validateString(url)
					if isURLValid:
						app.logger.debug("Url is not Empty - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						organization.url = url
					else:
						app.logger.debug("Url empty "+str(url)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						raise emptyStringError()


					address = args['address']
					isAddressValid = validateString(address)
					if isAddressValid:
						app.logger.debug("Url is not Empty - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						organization.address = address
					else:
						app.logger.debug("Url empty "+str(address)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						raise emptyStringError()


					location = args['location']
					isLocationValid = validateString(location)
					if isLocationValid:
						app.logger.debug("Location is not Empty - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						organization.location = location
					else:
						app.logger.debug("Location empty "+str(address)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						raise emptyStringError()

						
					country = args['country']
					isCountryValid = validateString(country)
					if isCountryValid:
						app.logger.debug("Country is not Empty - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						organization.country = country
					else:
						app.logger.debug("Country empty "+str(location)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						raise emptyStringError()

					contactperson = args['contactperson']
					isContactPersonValid = validateString(contactperson)
					if isContactPersonValid:
						app.logger.debug("ContactPerson is not Empty - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						organization.contactperson = contactperson
					else:
						app.logger.debug("ContactPerson empty "+str(address)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						raise emptyStringError()						
					
					
					contactemail = args['contactemail']
					isContactPersonEmailValid = validateEmail(contactemail)
					if isContactPersonEmailValid:
						app.logger.debug("ContactPersonEmail is not Empty - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						organization.contactemail = contactemail
					else:
						app.logger.debug("ContactPersonEmail is empty "+str(address)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						raise invalidEmailError()						

					
					#mobile
					mobile = args['mobile']
					isMobile = validateString(mobile)
					if isMobile:
						app.logger.debug("Mobile not empty "+str(mobile)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						if mobile.isdigit():
							mobile = long(mobile)
							if mobile > 0:
								organization.mobile = args['mobile'] 
								app.logger.debug("Mobile is exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							else:
								app.logger.debug("Mobile number is invalid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
								app.logger.info("End of ApiUpdateOrganization - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
								return {'status': False, 'message': 'COMMON_MOBILE_NUMBER_INVALID', 'code':422}	
						else:
							app.logger.debug("Mobile number is not digit - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							app.logger.info("End of ApiUpdateOrganization - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							return {'status': False, 'message': 'COMMON_MOBILE_NUMBER_NOT_DIGIT', 'code':422}	
					else:
						app.logger.debug("Mobile not empty "+str(args['mobile'])+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						raise emptyStringError()

					
					phone = args['phone']
					isPhoneMobile = validateString(phone)
					if isPhoneMobile:
						app.logger.debug("Phone number not empty "+str(phone)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						if phone.isdigit():
							phone = long(phone)
							if phone > 0:
								organization.phone = args['phone'] 
								app.logger.debug("Phone number is exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							else:
								app.logger.debug("Phone number is invalid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
								app.logger.info("End of ApiUpdateOrganization - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
								return {'status': False, 'message': 'COMMON_MOBILE_NUMBER_INVALID', 'code':422}	
						else:
							app.logger.debug("Phone number is not digit - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							app.logger.info("End of ApiUpdateOrganization - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							return {'status': False, 'message': 'COMMON_MOBILE_NUMBER_NOT_DIGIT', 'code':422}	
					else:
						app.logger.debug("Phone number not empty "+str(args['phone'])+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						raise emptyStringError()


					alternative = args['alternative']
					isAlternativeMobile = validateString(alternative)
					if isAlternativeMobile:
						app.logger.debug("Alternate Mobile not empty "+str(alternative)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						if alternative.isdigit():
							alternative = long(alternative)
							if phone > 0:
								organization.alternative = args['alternative'] 
								app.logger.debug("Alternate Mobile is exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							else:
								app.logger.debug("Alternate Mobile number is invalid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
								app.logger.info("End of ApiUpdateOrganization - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
								return {'status': False, 'message': 'COMMON_MOBILE_NUMBER_INVALID', 'code':422}	
						else:
							app.logger.debug("Alternate Mobile number is not digit - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							app.logger.info("End of ApiUpdateOrganization - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							return {'status': False, 'message': 'COMMON_MOBILE_NUMBER_NOT_DIGIT', 'code':422}	
					else:
						app.logger.debug("Alternate Mobile not empty "+str(args['alternative'])+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						raise emptyStringError()

					#signature
					signature = args['signature']
					isSignaturevalid = validateString(signature)
					if isSignaturevalid:
						app.logger.debug("Signature is not Empty - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						organization.signature = args['signature']
					else:
						app.logger.debug("Signature empty "+str(signature)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						raise emptyStringError()
						
					#status
					if args['status'] == True:
						app.logger.debug("Status is True - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						organization.status = True
					elif args['status'] == False:
						app.logger.debug("Status is False - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						organization.status = False
					else:
						raise TempException()

					
					organization.updatedby = user.userid
					organization.updatedtime = datetime.utcnow()

					app.logger.debug("Update database - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					db.session.commit()
					app.logger.info("End of ApiUpdateOrganization - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {'status': True, 'message': 'APIORGUPDATE_ORG_UPDATE_SUCCESSFUL', 'code':201}
				
				except TempException as te:
					app.logger.info(te.message +" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.exception(te)
					app.logger.info("End of ApiUpdateOrganization - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					db.session.rollback()
					return {'status': False, 'message': te.message , 'code':422}

				except IntegrityError as ie:
					app.logger.info("Error: DB Exceeding limit Error - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.error(ie.message)
					app.logger.info("End of ApiUpdateOrganization - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					db.session.rollback()
					return {'status': False, 'message': 'COMMON_ERROR_CONTACT_SUPPORT', 'code':409}
				except DataError as de:
					app.logger.info("DataBase field value out of range - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.error(de.message)
					app.logger.info("End of ApiUpdateOrganization - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					db.session.rollback()
					return {'status': False, 'message': 'COMMON_ERROR_INVALID_DATA', 'code':422}
				except InvalidRequestError as ir:
					app.logger.info("received invalid request - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.exception(ir)
					app.logger.error(ir.message)
					app.logger.info("End of ApiUpdateOrganization - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					db.session.rollback()
					return {'status': False, 'message': 'COMMON_ERROR_INVALID_REQUSET', 'code':422}
				except ValueError as ve:
					app.logger.info("Date Format is invalid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.error(ve.message)
					app.logger.info("End of ApiUpdateOrganization - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					db.session.rollback()
					return {'status': False, 'message': str(ve.message), 'code':422}
				except invalidEmailError as iee:
					app.logger.info("Email ID is not valid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.exception(iee)
					app.logger.error(iee.message)
					app.logger.info("End of ApiUpdateOrganization - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					db.session.rollback()
					return {'status': False, 'message': 'COMMON_ERROR_EMAIL_NOT_VALID', 'code':422}
				except emptyStringError as ese:
					app.logger.info("One of the field is Empty - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.exception(ese)
					app.logger.error(ese.message)
					app.logger.info("End of ApiUpdateOrganization - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					db.session.rollback()
					return {'status': False, 'message': 'COMMON_ERROR_ONE_OF_EMPTY_FIELD', 'code':422}						
				except Exception as orgerror:
					app.logger.info("Error while Accessing values to org object - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.exception(orgerror)
					app.logger.info("End of ApiUpdateOrganization - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {'status': False, 'message': 'COMMON_ERROR_REQ_MORE_FIELD', 'code':422}
			
			elif user:
				app.logger.debug("User is not authorized to access ApiUpdateOrganization service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.info("End of ApiUpdateOrganization - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': False,  'message': 'COMMON_UNAUTHORIZED', 'code':403}
			
			else:
				app.logger.info("Invalid token or Session expired - " + str(clientip))
				app.logger.info("End of ApiUpdateOrganization - " + str(clientip))
				return {'status': False, 'message': 'COMMON_INVALID_TOKEN', 'code':401}
		
		except Exception as e:
			app.logger.info("error while checking authentication - " + str(clientip))
			app.logger.exception(e)
			app.logger.info("End of ApiUpdateOrganization - " + str(clientip))
			return {'status': False, 'message': 'COMMON_ERROR_IN_AUTHENTICATION', 'code':422}

	
# list of organization
class ApiOrganizationList(RestResource):
	def get(self):
		app.logger.info("Accessing ApiOrganizationList")
		clientip = request.remote_addr
		
		app.logger.debug("Check user token - " + str(clientip))
		
		try:
			status, user = user_auth(admin_access=C.SUPERADMIN)
			if status and user:
				app.logger.debug("User is authorized to access ApiOrganizationList service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				try:
					app.logger.info(request.args)
					if 'orgname' not in request.args:
						orgname = None
					else:
						isNamevalid = validateString(request.args['orgname'])
						if isNamevalid:
							orgname = request.args['orgname']
							app.logger.debug("orgname " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							app.logger.debug(orgname)
						else:
							orgname = None	
					if 'status' not in request.args:
						status = None
					else:
						status = request.args['status']
						app.logger.info("status " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						app.logger.info(status)
						if status == 'true':
							status = 'true'
						elif status == 'false':
							status = 'false'
						else:
							status = None

					if orgname and status:
						query = "select * from organization where (lower(name) like lower('%%"+orgname+"%%') or lower(preferredname) like lower('%%"+orgname+"%%')) and status="+status+" ;"
					elif orgname:
						query = "select * from organization where lower(name) like lower('%%"+orgname+"%%') or  lower(preferredname) like lower('%%"+orgname+"%%');"
					elif status:
						query = "select * from organization where status="+status+" ;"
					else:
						query = "select * from organization;"

					app.logger.debug("final query")
					app.logger.debug(query)
					result = db.engine.execute(query)
					listOfOrgs = OrganizationSchema().dump(result, many=True).data
					app.logger.info("End of ApiOrganizationList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {'status': True, 'message': 'APIORGLIST_SUCCESS', 'organization_list':listOfOrgs, 'code':201}
				
				except Exception as e:
					app.logger.info("error while querying db - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.exception(e)
					app.logger.info("End of ApiOrganizationList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {'status': False, 'message': 'APIORGLIST_ERROR', 'code':422}


			elif user:
				app.logger.debug("User is not authorized to access ApiOrganizationList service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.info("End of ApiOrganizationList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': False,  'message': 'COMMON_UNAUTHORIZED', 'code':403}
			
			else:
				app.logger.info("Invalid token or Session expired - " + str(clientip))
				app.logger.info("End of ApiOrganizationList - " + str(clientip))
				return {'status': False, 'message': 'COMMON_INVALID_TOKEN', 'code':401}
		
		except Exception as e:
			app.logger.info("error while checking authentication - " + str(clientip))
			app.logger.exception(e)
			app.logger.info("End of ApiOrganizationList - " + str(clientip))
			return {'status': False, 'message': 'COMMON_ERROR_IN_AUTHENTICATION', 'code':422}	
				



def validateString(input):
	app.logger.info("Accessing validateString ")
	app.logger.info("String for validateString "+str(input))

	if input:
		if input.strip() == '':
			app.logger.info("String is empty "+str(input))
			return False
		else:
			app.logger.info("String is not empty "+str(input))
			return True
	else:
		return False

def validateEmail(email):
	app.logger.info("Accessing validateEmail")
	app.logger.info("Email for validateEmail "+str(email))
	if not re.match(r"[^@]+@[^@]+\.[^@]+", email):
		app.logger.info("Email  empty "+str(email))
		return False
	else:
		app.logger.info("Email is not empty "+str(email))
		return True

# Create user
# {firstname, lastname, email, mobile, role, orgid, status}
class ApiCreateUser(RestResource):
	def post(self):
		app.logger.info("Accessing ApiCreateUser")
		clientip = request.remote_addr
		app.logger.debug("Check user token - " + str(clientip))
		# try:
		status, user = user_auth(admin_access=C.SUPERADMIN)
		if status and user:
			app.logger.debug("User is authorized to access ApiCreateUser service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			# read parameters
			try:
				args = parser.parse(UserCreateSchema(), request, locations=request_locations)
			except Exception as e:
				app.logger.info("Error while parsing user details - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.exception(e)
				app.logger.info("End of ApiCreateUser - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': False, 'message': 'APICREATEUSER_FIELDS_MISSING', 'code':422}
			
			app.logger.debug(args)
			app.logger.debug("Required fileds received - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			try:

				app.logger.info("first check email is already REGISTERED or not")


				#email
				if 'email' not in  args:
					app.logger.debug("Email not found - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					raise RequiredFieldsMissing()
				else:
					app.logger.debug("Email found - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					if not args['email']:
						app.logger.debug("Email value not found - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						raise ValueMissing()
					else:
						isValidEmail = validateEmail(args['email'])
						if isValidEmail:
							app.logger.debug("Email is valid"+str(args['email'])+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")			
						else:
							app.logger.debug("Email is not valid"+str(args['email'])+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							raise invalidEmailError()
						app.logger.debug("Email value found "+str(args['email'])+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						

				app.logger.info("check email already REGISTERED or not as Organization Admin")	
				user_availability = User().query.filter(func.lower(User.email) == func.lower(args['email'])).first()
				if user_availability:
					app.logger.info("Email already exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.info("End of ApiCreateUser - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {'status': False, 'message': 'APICREATEUSER_EMAIL_ERROR_PART1 '+ args['email'] +' APICREATEUSER_EMAIL_ERROR_PART2', 'code':409,'messageFlag':True}
				else:
					app.logger.debug("Email id not exists in Database "+str(args['email'])+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				
				newuser = User()
				newuser.email = args['email']

				app.logger.debug("Access orgid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				
				#orgid
				if 'orgid' not in  args:
					app.logger.debug("Orgid not found - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					raise RequiredFieldsMissing()
				else:
					orgid = args['orgid']
					if not args['orgid']:
						app.logger.debug("Orgid value not found - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						raise ValueMissing()
					else:
						app.logger.debug("Orgid found "+str(args['orgid'])+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")

					# if isinstance(orgid,int) and orgid > 0:
					if orgid > 0 :
						app.logger.debug("Orgid is digit "+str(args['orgid'])+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						newuser.orgid = args['orgid']
						pass
					else:
						app.logger.info("orgid not a digit - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						app.logger.info("End of ApiupdateOrganizationUser - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						return {'status': False, 'message': 'APICREATEUSER_ORGID_INVALID', 'code':422}


				app.logger.debug("Access email - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				
				app.logger.debug("Access firstname - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				#firstname
				if 'firstname' not in  args:
					app.logger.debug("Firstname not found - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					raise RequiredFieldsMissing()
				else:
					app.logger.debug("Firstname found - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					if not args['firstname']:
						app.logger.debug("Firstname  value not found - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						raise ValueMissing()
					else:
						isFirstName = validateString(args['firstname'])
						if isFirstName:
							app.logger.debug("Firstname not empty "+str(args['firstname'])+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							newuser.firstname = args['firstname']
						else:
							app.logger.debug("Firstname empty "+str(args['firstname'])+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							raise emptyStringError()
				
				app.logger.debug("Access lastname - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				#lastname
				if 'lastname' not in  args:
					app.logger.debug("Lastname not found - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					pass
				else:
					app.logger.debug("Lastname found - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					if not args['lastname']:
						app.logger.debug("Lastname value not found - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						raise ValueMissing()
					else:
						
						isLastName = validateString(args['lastname'])
						if isLastName:
							app.logger.debug("Lastname not empty "+str(args['lastname'])+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							newuser.lastname = args['lastname']
						else:
							app.logger.debug("Lastname empty "+str(args['lastname'])+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")		
							raise emptyStringError()

				app.logger.debug("Access mobile - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				#mobile
				if 'mobile' not in  args:
					app.logger.debug("Mobile not found - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					raise RequiredFieldsMissing()
				else:
					app.logger.debug("Mobile found - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					mobile = args['mobile']
					if not args['mobile']:
						app.logger.debug("Mobile value not found - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						raise ValueMissing()
					else:
						pass
					if mobile:
						isMobile = validateString(args['mobile'])
						if isMobile:
							app.logger.debug("Mobile not empty "+str(args['mobile'])+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							if mobile.isdigit():
								mobile_number = long(mobile)
								if mobile_number > 0:
									app.logger.debug("Mobile is valid "+str(args['mobile'])+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
									newuser.mobile = args['mobile']
								else:
									app.logger.info("Mobile is invalid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
									app.logger.info("End of ApiCreateUser - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
									return {'status': False, 'message': 'COMMON_MOBILE_NUMBER_INVALID', 'code':422}
							else:
								app.logger.info("Mobile is not Digit - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
								app.logger.info("End of ApiCreateUser - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
								return {'status': False, 'message': 'COMMON_MOBILE_NUMBER_NOT_DIGIT', 'code':422}
						else:
							app.logger.debug("Mobile is empty "+str(args['email'])+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							raise emptyStringError()
				app.logger.debug("Access role - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				#role
				if 'role' not in  args:
					app.logger.debug("Role not found - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					raise RequiredFieldsMissing()
				else:
					if not args['role']:
						app.logger.debug("Role found - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						raise ValueMissing()
					else:
						app.logger.debug("Role value found "+str(args['role'])+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						role = args['role']
						if role in C.GROUP_TYPES :
							app.logger.debug("Role is valid "+str(args['role'])+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							newuser.role = role
						else:
							app.logger.debug("Role is invalid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							app.logger.debug("End of ApiCreateUser - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							return {'status': False, 'message': 'APICREATEUSER_ROLE_INVALID', 'code':422}


				app.logger.debug("Access status - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				#status
				if 'status' not in  args:
					app.logger.debug("Status not found - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					newuser.status = True
				else:
					app.logger.debug("Status found "+str(args['status'])+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					newuser.status = args['status']

				newuser.createdby = user.userid
				newuser.createdtime = datetime.utcnow()

				db.session.add(newuser)
				
				app.logger.debug("Update database - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.commit()
			except RequiredFieldsMissing as re:
				app.logger.info("One or more required fileds(email, firstname, mobile, role, orgid) missing - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.exception(re)
				app.logger.info("End of ApiCreateUser - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'APICREATEUSER_FIELDS_MISSING', 'code':422}
			except IntegrityError as ie:
				app.logger.info("Error: Database related - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				# app.logger.exception(ie)
				app.logger.error(ie)
				app.logger.error(ie.message)
				app.logger.info("End of ApiCreateUser - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_CONTACT_SUPPORT', 'code':409}
			except InvalidRequestError as ir:
				
				app.logger.info("received invalid request - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.exception(ir)
				app.logger.error(ir.message)
				app.logger.info("End of ApiCreateUser - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_INVALID_REQUSET', 'code':422}
			except DataError as de:
				
				app.logger.info("DataBase field value out of range - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.error(de.message)
				app.logger.info("End of ApiCreateUser - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_INVALID_DATA', 'code':422}
			except ValueMissing as vm:
				
				app.logger.info("Received invalid input - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.exception(vm)
				app.logger.error(vm.message)
				app.logger.info("End of ApiCreateUser - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_INVALID_INPUT', 'code':422}
			except ValueError as ve:
				
				app.logger.info("Date Format is invalid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.error(ve.message)
				app.logger.info("End of ApiCreateUser - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': str(ve.message), 'code':422}
			except invalidEmailError as iee:
				
				app.logger.info("Email ID is not valid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.exception(iee)
				app.logger.error(iee.message)
				app.logger.info("End of ApiCreateUser - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_EMAIL_NOT_VALID', 'code':422}
			except emptyStringError as ese:
				
				app.logger.info("One of the field is Empty - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.exception(ese)
				app.logger.error(ese.message)
				app.logger.info("End of ApiCreateUser - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_ONE_OF_EMPTY_FIELD', 'code':422}
			except Exception as e:				
				app.logger.exception(e)
				app.logger.info("error while adding user - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.info("End of ApiCreateUser - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'APICREATE_ERROR_IN_USER_CREATION', 'code':422}

			# send set password link to user
			try:
				app.logger.info("Sending Email to user for setup password "+str(newuser.email))
				generate_send_reset_password(newuser)
			except Exception as e:
				app.logger.error("unable to send reset password link to user "+str(newuser.email))
				app.logger.error(e)
			try:
				app.logger.info("End of ApiCreateUser - " + str(clientip))
			except Exception as errrrr:
				app.logger.error(errrrr)
			return {'status': True, 'message': 'APICREATEUSER_CREATION_SUCCESS', 'code':201}
		elif user:
			app.logger.debug("User is not authorized to access ApiCreateUser service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.info("End of ApiCreateUser - " + str(clientip))
			return {'status': False,  'message': 'COMMON_UNAUTHORIZED', 'code':403}
		else:
			app.logger.info("Invalid token or Session expired - " + str(clientip))
			app.logger.info("End of ApiCreateUser - " + str(clientip))
			return {'status': False, 'message': 'COMMON_INVALID_TOKEN', 'code':401}


class ApiUpdateOrganizationUser(RestResource):
	def post(self,uid):
		app.logger.info("Accessing ApiupdateOrganizationUser")
		clientip = request.remote_addr
		status, user = user_auth(admin_access=C.SUPERADMIN)
		if status and user:
			app.logger.debug("User is authorized to access ApiupdateOrganizationUser service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			#check whether the user is valid or not based on uid
			user_details = User().query.get(uid)
			if user_details:
				# read parameters
				try:
					app.logger.debug("Parsing User Update Details fields - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					args = parser.parse(OrganizationUserUpdateSchema(), request, locations=request_locations)
				except Exception as e:
					app.logger.debug("Error while parsing organization user - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.exception(e.message)
					app.logger.info("End of ApiupdateOrganizationUser - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {'status': False, 'message': 'APICREATEUSER_FIELDS_MISSING', 'code':422}

				try:
					app.logger.info("Create user object and assign values to new object - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					
					app.logger.debug("Access email - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					#email
					if 'email' not in  args:
						app.logger.debug("Email not found - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						raise RequiredFieldsMissing()
					else:
						app.logger.debug("Email found - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						if not args['email']:
							app.logger.debug("Email value not found - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							raise ValueMissing()
						else:
							app.logger.debug("Email value found "+str(args['email'])+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							isValidEmail = validateEmail(args['email'])
							if isValidEmail:
								user_availability = User().query.filter(and_(func.lower(User.email) == func.lower(args['email'])),User.orgid == args['orgid'],User.userid==uid).first()
								if user_availability:
									app.logger.debug("Email is Available"+str(args['email'])+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")			
									user_details.email = args['email']
								else:
									otheruser_availability = User().query.filter(func.lower(User.email) == func.lower(args['email'])).first()
									if otheruser_availability:
										app.logger.info("Email already exists as other user- " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
										app.logger.info("End of ApiCreateUser - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
										return {'status': False, 'message': 'APICREATEUSER_EMAIL_ERROR_PART1'+ args['email'] +'APICREATEUSER_EMAIL_ERROR_PART2', 'code':409,'messageFlag':True}
									else:
										app.logger.info("Email not exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
										user_details.email = args['email']
							else:
								app.logger.debug("Email is not valid"+str(args['email'])+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
								raise invalidEmailError()
					
					app.logger.debug("Access firstname - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					#firstname
					if 'firstname' not in  args:
						app.logger.debug("Firstname not found - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						raise RequiredFieldsMissing()
					else:
						if not args['firstname']:
							app.logger.debug("Firstname not found - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							raise ValueMissing()
						else:
							isFirstName = validateString(args['firstname'])
							if isFirstName:
								app.logger.debug("Firstname not empty "+str(args['firstname'])+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
								user_details.firstname = args['firstname']
							else:
								app.logger.debug("Firstname empty "+str(args['firstname'])+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
								raise emptyStringError()
							
							
					app.logger.debug("Access lastname - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					#lastname
					if 'lastname' not in  args:
						app.logger.debug("Lastname not found - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						raise RequiredFieldsMissing()
					else:
						app.logger.debug("Lastname not found - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						if not args['lastname']:
							app.logger.debug("Lastname value not found - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							raise ValueMissing()
						else:
							app.logger.debug("Lastname value found "+str(args['lastname'])+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							isLastName = validateString(args['lastname'])
							if isLastName:
								app.logger.debug("Lastname not empty "+str(args['lastname'])+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
								user_details.lastname = args['lastname']
							else:
								app.logger.debug("Lastname empty "+str(args['lastname'])+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
								raise emptyStringError()

					app.logger.debug("Access mobile - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					#mobile
					if 'mobile' not in  args:
						app.logger.debug("Mobile not found - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						raise RequiredFieldsMissing()
					else:
						app.logger.debug("Mobile found - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						mobile = args['mobile']
						if not args['mobile']:
							app.logger.debug("Mobile value not found - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							raise ValueMissing()
						else:
							pass

						if mobile:
							isMobile = validateString(mobile)
							if isMobile:
								if mobile.isdigit():
									app.logger.debug("Mobile not empty "+str(args['mobile'])+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
									mobile_number = long(mobile)
									if mobile_number > 0:
										app.logger.debug("Mobile number is valid "+str(args['mobile'])+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
										user_details.mobile = args['mobile']
									else:
										app.logger.debug("Mobile Number is invalid  "+str(args['mobile'])+" - "+ str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
										app.logger.info("End of ApiupdateOrganizationUser - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
										db.session.rollback()
										return {'status': False, 'message': 'COMMON_MOBILE_NUMBER_INVALID', 'code':422}
								else:
									app.logger.debug("Mobile Number is not digit  "+str(args['mobile'])+" - "+ str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
									app.logger.info("End of ApiupdateOrganizationUser - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
									db.session.rollback()
									return {'status': False, 'message': 'COMMON_MOBILE_NUMBER_NOT_DIGIT', 'code':422}
							else:
								app.logger.debug("Mobile empty "+str(args['mobile'])+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
								raise emptyStringError()

					app.logger.debug("Access role - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					#role
					if 'role' not in  args:
						app.logger.debug("Role not found - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						raise RequiredFieldsMissing()
					else:
						app.logger.debug("Role  found - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						role = args['role']
						if not args['role']:
							app.logger.debug("Role value not found - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							raise ValueMissing()
						else:
							pass
						
						if role in C.GROUP_TYPES :
							app.logger.debug("Role value found "+str(args['role'])+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							user_details.role = args['role']
						else:
							app.logger.debug("User Role is invalid "+str(args['role'])+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							app.logger.info("End of ApiupdateOrganizationUser - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							db.session.rollback()
							return {'status': False, 'message': 'APIUPDATEUSER_ROLE_INVALID', 'code':422}
					app.logger.debug("Access orgid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					
					#orgid
					if 'orgid' not in  args:
						app.logger.debug("Orgid not found - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						raise RequiredFieldsMissing()
					else:
						
						orgid= args['orgid']

						if not args['orgid']:
							raise ValueMissing()
						else:
							pass

						if orgid > 0 :
							app.logger.debug("Orgid value found "+str(args['orgid'])+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							user_details.orgid = args['orgid']
							pass
						else:
							app.logger.debug("Organization id is invalid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							app.logger.info("End of ApiupdateOrganizationUser - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							db.session.rollback()
							return {'status': False, 'message': 'APICREATEUSER_ORGID_INVALID', 'code':422}

					app.logger.debug("Access status - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					#status
					if 'status' not in args:
						raise RequiredFieldsMissing()
					else:
						if args['status'] == False:
							user_details.status = args['status']
						elif args['status'] == True:
							user_details.status = args['status']
						else:
							app.logger.debug("Status is invalid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							app.logger.info("End of ApiupdateOrganizationUser - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							db.session.rollback()
							return {'status': False, 'message': 'APIUPDATEUSER_STATUS_INVALID', 'code':422}

					user_details.updatedby = user.userid
					user_details.updatedtime = datetime.utcnow()

					app.logger.debug("Update database - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					db.session.commit()
					
					app.logger.info("End of ApiupdateOrganizationUser - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {'status': True, 'message': 'User Updated succussfully', 'code':201}
				except RequiredFieldsMissing as re:
					app.logger.info("One or more required fileds(email, firstname, mobile, role, orgid) missing - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.exception(re)
					app.logger.info("End of ApiupdateOrganizationUser - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					db.session.rollback()
					return {'status': False, 'message': 'APICREATEUSER_FIELDS_MISSING', 'code':422}
				except InvalidRequestError as ir:
					app.logger.info("Received invalid request - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.error(ir.message)
					app.logger.info("End of ApiupdateOrganizationUser - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					db.session.rollback()
					return {'status': False, 'message': 'COMMON_ERROR_INVALID_REQUSET', 'code':422}
				except DataError as de:
					app.logger.info("DataBase field value out of range - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.error(de.message)
					app.logger.info("End of ApiupdateOrganizationUser - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					db.session.rollback()
					return {'status': False, 'message': 'COMMON_ERROR_INVALID_DATA', 'code':422}
				except IntegrityError as ie:
					app.logger.info("Error: DB exceeding limit  - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.error(ie.message)
					app.logger.info("End of ApiupdateOrganizationUser - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					db.session.rollback()
					return {'status': False, 'message': 'COMMON_ERROR_CONTACT_SUPPORT', 'code':409}
				except ValueMissing as vm:
					app.logger.info("Received invalid input - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.error(vm.message)
					app.logger.info("End of ApiupdateOrganizationUser - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					db.session.rollback()
					return {'status': False, 'message': 'COMMON_ERROR_INVALID_INPUT', 'code':422}
				except ValueError as ve:
					app.logger.info("Date Format is invalid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.error(ve.message)
					app.logger.info("End of ApiupdateOrganizationUser - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					db.session.rollback()
					return {'status': False, 'message': str(ve.message), 'code':422}
				except invalidEmailError as iee:
					app.logger.info("Email ID is not valid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.exception(iee)
					app.logger.error(iee.message)
					app.logger.info("End of ApiupdateOrganizationUser - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					db.session.rollback()
					return {'status': False, 'message': 'COMMON_ERROR_EMAIL_NOT_VALID', 'code':422}
				except emptyStringError as ese:
					app.logger.info("One of the field is Empty - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.exception(ese)
					app.logger.error(ese.message)
					app.logger.info("End of ApiupdateOrganizationUser - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					db.session.rollback()
					return {'status': False, 'message': 'COMMON_ERROR_ONE_OF_EMPTY_FIELD', 'code':422}
				except Exception as e:
					app.logger.info("Error while adding user - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.exception(e.message)
					app.logger.info("End of ApiupdateOrganizationUser - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					db.session.rollback()
					return {'status': False, 'message': 'APIUPDATEUSER_ERROR_IN_DETAILS_UPDATE', 'code':422}

			else:
				app.logger.info("User details not found to update with given "+str(uid)+" - " + str(clientip))
				app.logger.info("End of ApiupdateOrganizationUser - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'APIUPDATEUSER_DETAILS_NOT_FOUND', 'code':422}	
		elif user:
			app.logger.debug("User is not authorized to access ApiCreateUser service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.info("End of ApiCreateUser - " + str(clientip))
			return {'status': False,  'message': 'COMMON_UNAUTHORIZED', 'code':403}
		else:
			app.logger.info("Invalid token or Session expired - " + str(clientip))
			app.logger.info("End of ApiCreateUser - " + str(clientip))
			return {'status': False, 'message': 'COMMON_INVALID_TOKEN', 'code':401}


def validatingInterviewerMobile(value,user,clientip):
	app.logger.info("validating Intereviewer Info")
	interviewer_mobile = value
	app.logger.debug("Interviewer mobile value exists "+str(interviewer_mobile)+" - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
	if interviewer_mobile:
		isMobile = validateString(value)
		if isMobile:
			app.logger.debug("Mobile not empty "+str(interviewer_mobile)+" - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
			if interviewer_mobile.isdigit():
				interviewer_mobile_number = long(interviewer_mobile)
				if interviewer_mobile_number > 0:
					app.logger.debug("Candidate mobile is exists - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
					return value,True
				else:
					app.logger.debug("Mobile number is invalid - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
					app.logger.info("End of validatingInterviewerMobile - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
					return {'status': False, 'message': 'COMMON_MOBILE_NUMBER_INVALID', 'code':422},False	
			else:
				app.logger.debug("Mobile number is not digit - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
				app.logger.info("End of validatingInterviewerMobile - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
				return {'status': False, 'message': 'COMMON_MOBILE_NUMBER_NOT_DIGIT', 'code':422},False	
		else:
			app.logger.debug("Mobile not empty "+str(interviewer_mobile)+" - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
			raise emptyStringError()
 

def validatingInterviewerEmail(value,user,clientip):
	interviewer_email = value
	isInterviewerEmailValid = validateEmail(interviewer_email)
	if isInterviewerEmailValid:
		app.logger.debug("Email is valid"+str(interviewer_email)+" - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
		return interviewer_email,True
	else:
		app.logger.debug("Email is not valid"+str(interviewer_email)+" - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
		raise invalidEmailError()


def validatingInterviewerFName(value,user,clientip):
	interviewer_fname = value
	app.logger.info("validatingInterviewerName")
	isInterviewerFirstName = validateString(interviewer_fname)
	if isInterviewerFirstName:
		app.logger.debug("Interview_fname not empty "+str(interviewer_fname)+" - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
		return interviewer_fname,True
	else:
		app.logger.debug("Interviewer_fname empty "+str(interviewer_fname)+" - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
		raise emptyStringError()

def validatingInterviewerLName(value,user,clientip):
	interviewer_lname = value
	app.logger.info("validatingInterviewerName")
	isInterviewerFirstName = validateString(interviewer_lname)
	if isInterviewerFirstName:
		app.logger.debug("Interview_fname not empty "+str(interviewer_lname)+" - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
		return interviewer_lname,True
	else:
		app.logger.debug("Interviewer_fname empty "+str(interviewer_lname)+" - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
		raise emptyStringError()

def creatingInterviewers(interviewers_list,user,clientip):
	app.logger.info("creatingInterviewers()")
	app.logger.info(interviewers_list)
	app.logger.info(user)
	list_of_interviewers=[]
	
	primary_interviewer_details = None
	primary_interviewer = interviewers_list['primary_interviewer']
	interviewer, message , status = createInterviewer(primary_interviewer, user, clientip)
	if status:
		primary_interviewer_details = interviewer
	else:
		return None, [], message, False

	#now check other interviewers exist or not
	other_interviewers = interviewers_list['interviewer_list']
	app.logger.info("other_interviewers")
	app.logger.info(other_interviewers)
	if len(other_interviewers) > 0:
		app.logger.info("other_interviewers exists")
		for other_interviewer in other_interviewers:
			other_interviewer_data, message , status = createInterviewer(other_interviewer, user, clientip)
			if status:
				list_of_interviewers.append(other_interviewer_data)
			else:
				return None, [], message, False
	
	app.logger.info("End of creatingInterviewers")
	app.logger.info(primary_interviewer_details)
	app.logger.info(list_of_interviewers)
	return primary_interviewer_details, list_of_interviewers, 'Success', True

def createInterviewer(regular_interviewer, user, clientip):
	# validate user details
	app.logger.info("Accessing createInterviewer")

	if regular_interviewer['firstname']:
		rfirstname,rFirstNameStatus = validatingInterviewerFName(regular_interviewer['firstname'],user,clientip)
		if rFirstNameStatus:
			app.logger.info("Interviewer Firstname Exists - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
		else:
			app.logger.info("Interviewer Firstname not Exists - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
			return None, rfirstname, False
	else:
		app.logger.info("Interviewer Firstname not Exists - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
		return None, {'status':False,'message':'Interviewer firstname not entered','code':422},False

	if regular_interviewer['lastname']:
		rlastname,rLastNameStatus = validatingInterviewerLName(regular_interviewer['lastname'],user,clientip)
		if rLastNameStatus:
			app.logger.info("Interviewer Lastname  Exists - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
		else:
			app.logger.info("Interviewer Lastname not Exists - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
			return None, rlastname,False
	else:
		app.logger.info("Interviewer Lastname not Exists - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
		return None, {'status':False,'message':'Interviewer lastname not entered','code':422},False

	if regular_interviewer['email']:
		remail,rEmailStatus = validatingInterviewerEmail(regular_interviewer['email'],user,clientip)
		if rEmailStatus:
			app.logger.info("Interviewer Email Exists - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
		else:
			app.logger.info("Interviewer Email not Exists - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
			return None, remail,False
	else:
		app.logger.info("Interviewer Email Exists - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
		return None, {'status':False,'message':'Interviewer email not entered','code':422},False

	if regular_interviewer['mobile']:
		rmobile,rMobileStatus = validatingInterviewerMobile(regular_interviewer['mobile'],user,clientip)
		if rmobile:
			app.logger.info("Interviewer Mobile Exists - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
		else:
			app.logger.info("Interviewer Mobile not Exists - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
			return None, rmobile,False
	else:
		app.logger.info("Interviewer Mobile not Exists - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
		return None, {'status':False,'message':'Interviewer mobile not entered','code':422},False

	# get email id
	regular_interviewer_email = regular_interviewer['email']
	
	# check email already registered?
	interviewer = User().query.filter(and_(func.lower(User.email) == func.lower(regular_interviewer_email),User.orgid == user['orgid'])).first()
	app.logger.info("interviewer exists or not - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
	app.logger.info(interviewer)

	if interviewer:
		if interviewer.role == C.INTERVIEWER:
			interviewer.firstname = regular_interviewer['firstname']
			interviewer.lastname = regular_interviewer['lastname']
			interviewer.email = regular_interviewer['email']
			interviewer.mobile = regular_interviewer['mobile']	
			interviewer.updatedby = user['userid']
			interviewer.updatedtime = datetime.utcnow()
		else:
			app.logger.debug("Interviewer email already exists as an other Role - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
			app.logger.info("End of creatingInterviewers - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
			return None, {'status': False, 'message': 'APISCHEDULEINTERVIEW_INTERVIEWER_EMAIL_IN_OTHER_ROLE', 'code':422},False
	else:
		interviewer = User()
		interviewer.firstname = regular_interviewer['firstname']
		interviewer.lastname = regular_interviewer['lastname']
		interviewer.email = regular_interviewer['email']
		interviewer.mobile = regular_interviewer['mobile']
		interviewer.role = C.INTERVIEWER
		interviewer.status = True
		interviewer.orgid = user['orgid']
		interviewer.createdby = user['userid']
		interviewer.createdtime = datetime.utcnow()
		db.session.add(interviewer)
	try:
		app.logger.debug("Adding interviewer details - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
		db.session.commit()
		return {'userid':interviewer.userid,'email':interviewer.email, 'mobile': interviewer.mobile, 'firstname': interviewer.firstname, 'lastname': interviewer.lastname}, 'Success', True		
	except exc.SQLAlchemyError:
		app.logger.debug("Error while adding interviewer row in database - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
		app.logger.info("End of creatingInterviewers - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
		db.session.rollback()
		return None, {'status': False, 'message': 'COMMON_ERROR_IN_ROW_UPDATE', 'code':422},False


def creatingCandidate(candidate_details,user,clientip):
	app.logger.info("creatingCandidate()")
	app.logger.debug(user)
	app.logger.debug("candidate_details")
	app.logger.debug(candidate_details)
	app.logger.info(candidate_details['candidate_fname'])
	app.logger.info(candidate_details['candidate_lname'])
	app.logger.info(candidate_details['candidate_email'])
	app.logger.info(candidate_details['candidate_mobile'])
	candidate = User().query.filter(and_(func.lower(User.email) == func.lower(candidate_details['candidate_email']),User.orgid == user['orgid'])).first()
	if candidate:
		app.logger.debug("User exists with given candidate_email - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
		if candidate.role == C.INTERVIEWEE:
			app.logger.debug("Candidate exists with given email as a Candidate - " + str(clientip) + "- [ " + str(user['orgid'])+" : "+str(user['orgid'])+" ]")
			candidate.firstname = candidate_details['candidate_fname']
			candidate.lastname = candidate_details['candidate_lname']
			candidate.email = candidate_details['candidate_email']
			candidate.mobile = candidate_details['candidate_mobile']
			candidate.updatedby = user['userid']
			candidate.updatedtime = datetime.utcnow()
		else:
			app.logger.debug("Candidate email already exists as an other Role - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
			app.logger.info("End of creatingCandidate - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
			return None,{'status': False, 'message': 'APISCHEDULEINTERVIEW_CANDIDATE_EMAIL_IN_OTHER_ROLE', 'code':422},False
	else:
		app.logger.debug("Candidate not exists with given userid. so, new row create in users table - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
		candidate = User()
		candidate.firstname = candidate_details['candidate_fname']
		candidate.lastname = candidate_details['candidate_lname']
		candidate.email = candidate_details['candidate_email']
		candidate.mobile = candidate_details['candidate_mobile']
		candidate.role = C.INTERVIEWEE
		candidate.status = True
		candidate.orgid = user['orgid']
		candidate.createdby = user['userid']
		candidate.createdtime = datetime.utcnow()
		db.session.add(candidate)
	try:
		app.logger.debug("Adding user details - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
		db.session.commit()
		# if candidate:
		app.logger.debug("Candidate row is updated/created in users table "+str(candidate.userid)+" - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
		return {'userid':candidate.userid,'email':candidate.email, 'mobile': candidate.mobile, 'firstname': candidate.firstname, 'lastname': candidate.lastname},None,True
	# else:
		# 	app.logger.debug("Candidate row is not updated/created in users table "+str(candidate.userid)+" - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
		# 	app.logger.info("End of creatingCandidate - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
		# 	return None,{'status': False, 'message': 'APISCHEDULEINTERVIEW_CANDIDATE_NOT_EXIST', 'code':403},False
	except exc.SQLAlchemyError:
		app.logger.debug("Error while adding candidate row in database - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
		app.logger.info("End of creatingCandidate - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
		db.session.rollback()
		return None,{'status': False, 'message': 'COMMON_ERROR_IN_ROW_UPDATE', 'code':422},False

def checkInterviewConflictForInterviewer(interviewers,date,slot_id,duration,user,clientip):
	#validate interview conflicts for interviewer
	app.logger.info("Acessing checkInterviewConflictForInterviewer - [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
	app.logger.info(interviewers)
	for interviewer in interviewers:
		app.logger.debug("interviewer details- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
		app.logger.debug(interviewer)
		interview_available = db.session.query(Interviews).join(Interview_Interviewer).filter(and_(Interview_Interviewer.interviewerid == interviewer['userid'],Interviews.date == date,Interviews.slot_id == slot_id,Interviews.interviewstatus == C.INTERVIEW_BOOKED)).first()
		if interview_available:
			app.logger.debug("Interviewer has already interview exists on given duration - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
			app.logger.info("End of checkInterviewConflictForInterviewer - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
			return {'status': False, 'message': 'APISCHEDULEINTERVIEW_INTERVIEW_EXIST', 'code':422},False
		else:
			before_interview_available = db.session.query(Interviews).join(Interview_Interviewer).filter(and_(Interview_Interviewer.interviewerid == interviewer['userid'],Interviews.date == date,Interviews.slot_id == slot_id-1,Interviews.interviewstatus == C.INTERVIEW_BOOKED)).first()
			if before_interview_available:
				app.logger.debug("Interiewer has already interview on before slot time - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
				if before_interview_available:
					if before_interview_available.duration == C.HALFANHOUR:
						app.logger.debug("Valid: Slot duration is HALF AN HOUR.. So proceed- " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
					else:
						app.logger.debug("Interviewer has already interview exists on given duration - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
						app.logger.info("End of checkInterviewConflictForInterviewer - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
						return {'status': False, 'message': 'APISCHEDULEINTERVIEW_INTERVIEW_EXIST', 'code':422}	,False	
			

		if duration == C.ONEHOUR:
			app.logger.debug("Duration is "+str(C.ONEHOUR)+" - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
			after_interview_available = db.session.query(Interviews).join(Interview_Interviewer).filter(and_(Interview_Interviewer.interviewerid == interviewer['userid'],Interviews.date == date,Interviews.slot_id == slot_id+1,Interviews.interviewstatus == C.INTERVIEW_BOOKED)).first()
			if after_interview_available:
				app.logger.debug("Interviewer has already interview exists on given duration - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
				app.logger.info("End of checkInterviewConflictForInterviewer - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
				return {'status': False, 'message': 'APISCHEDULEINTERVIEW_INTERVIEW_EXIST', 'code':422},False
				
		
		# Checking		
		if interview_available:
			app.logger.debug("Interviewer have already slot exists with on duration - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
			app.logger.info("End of checkInterviewConflictForInterviewer - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
			db.session.rollback()
			return {'status': False, 'message': 'APISCHEDULEINTERVIEW_INTERVIEW_EXIST_INTERVIEWER', 'code':422},False		
		else:
			return {'status':'success'},True
		

def checkInterviewConflictForInterviewerUpdate(interviewid,interviewers,date,slot_id,duration,user,clientip):
	#validate interview conflicts for interviewer
	app.logger.info("Acessing checkInterviewConflictForInterviewerUpdate - [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
	app.logger.info(interviewers)
	for interviewer in interviewers:
		app.logger.debug("interviewer details- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
		app.logger.debug(interviewer)
		interview_available = db.session.query(Interviews).join(Interview_Interviewer).filter(and_(Interview_Interviewer.interviewerid == interviewer['userid'],Interviews.date == date,Interviews.slot_id == slot_id,Interviews.interviewstatus == C.INTERVIEW_BOOKED,Interviews.interviewid !=interviewid, Interview_Interviewer.active_status==True)).first()
		app.logger.info("interview_available")
		app.logger.info(interview_available)
		if interview_available:
			app.logger.debug("Interviewer has already interview exists on given duration - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
			app.logger.info("End of checkInterviewConflictForInterviewerUpdate - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
			return {'status': False, 'message': 'APISCHEDULEINTERVIEW_INTERVIEW_EXIST', 'code':422},False
		else:
			before_interview_available = db.session.query(Interviews).join(Interview_Interviewer).filter(and_(Interview_Interviewer.interviewerid == interviewer['userid'],Interviews.date == date,Interviews.slot_id == slot_id-1,Interviews.interviewstatus == C.INTERVIEW_BOOKED,Interviews.interviewid !=Interview_Interviewer.interviewid)).first()
			if before_interview_available:
				app.logger.debug("Interiewer has already interview on before slot time - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
				if before_interview_available:
					if before_interview_available.duration == C.HALFANHOUR:
						app.logger.debug("Valid: Slot duration is HALF AN HOUR.. So proceed- " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
					else:
						app.logger.debug("Interviewer has already interview exists on given duration - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
						app.logger.info("End of checkInterviewConflictForInterviewerUpdate - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
						return {'status': False, 'message': 'APISCHEDULEINTERVIEW_INTERVIEW_EXIST', 'code':422}	,False	
			

		if duration == C.ONEHOUR:
			app.logger.debug("Duration is "+str(C.ONEHOUR)+" - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
			after_interview_available = db.session.query(Interviews).join(Interview_Interviewer).filter(and_(Interview_Interviewer.interviewerid == interviewer['userid'],Interviews.date == date,Interviews.slot_id == slot_id+1,Interviews.interviewstatus == C.INTERVIEW_BOOKED,Interviews.interviewid !=Interview_Interviewer.interviewid)).first()
			if after_interview_available:
				app.logger.debug("Interviewer has already interview exists on given duration - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
				app.logger.info("End of checkInterviewConflictForInterviewerUpdate - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
				return {'status': False, 'message': 'APISCHEDULEINTERVIEW_INTERVIEW_EXIST', 'code':422},False
				
		
		# Checking		
		if interview_available:
			app.logger.debug("Interviewer have already slot exists with on duration - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
			app.logger.info("End of checkInterviewConflictForInterviewerUpdate - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
			db.session.rollback()
			return {'status': False, 'message': 'APISCHEDULEINTERVIEW_INTERVIEW_EXIST_INTERVIEWER', 'code':422},False		
		else:
			return {'status':'success'},True


def checkInterviewConflictForCandidate(candidate,date,slot_id,duration,user,clientip):
	#validate interview conflicts for candidate
	app.logger.info("Accessing checkInterviewConflictForCandidate - [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
	app.logger.info("candidate details- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
	app.logger.info(candidate)
	interview_available = db.session.query(Interviews).join(Interview_Candidate).filter(and_(Interview_Candidate.candidateid == candidate['userid'],Interviews.date == date,Interviews.slot_id == slot_id,Interviews.interviewstatus == C.INTERVIEW_BOOKED)).first()
	if interview_available:
		app.logger.debug("Candidate has already interview exists on given duration - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
		app.logger.info("End of checkInterviewConflictForCandidate - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
		return {'status': False, 'message': 'APISCHEDULEINTERVIEW_INTERVIEW_EXIST', 'code':422},False
	else:
		before_interview_available = db.session.query(Interviews).join(Interview_Candidate).filter(and_(Interview_Candidate.candidateid == candidate['userid'],Interviews.date == date,Interviews.slot_id == slot_id-1,Interviews.interviewstatus == C.INTERVIEW_BOOKED)).first()
	
		if before_interview_available:
			app.logger.debug("Candidate has already interview on before slot time - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
			if before_interview_available:
				if before_interview_available.duration == C.HALFANHOUR:
					app.logger.debug("Valid: Slot duration is HALF AN HOUR.. So proceed- " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
				else:
					app.logger.debug("Candidate has already interview exists on given duration - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
					app.logger.info("End of checkInterviewConflictForCandidate - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
					return {'status': False, 'message': 'APISCHEDULEINTERVIEW_INTERVIEW_EXIST', 'code':422},False	
		

	if duration == C.ONEHOUR:
		app.logger.debug("Duration is "+str(C.ONEHOUR)+" - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
		after_interview_available = db.session.query(Interviews).join(Interview_Candidate).filter(and_(Interview_Candidate.candidateid == candidate['userid'],Interviews.date == date,Interviews.slot_id == slot_id+1,Interviews.interviewstatus == C.INTERVIEW_BOOKED)).first()
		if after_interview_available:
			app.logger.debug("Candidate has already interview exists on given duration - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
			app.logger.info("End of checkInterviewConflictForCandidate - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
			return {'status': False, 'message': 'APISCHEDULEINTERVIEW_INTERVIEW_EXIST', 'code':422},False
			
	
	# Checking		
	if interview_available:
		app.logger.debug("Interviewer have already slot exists with on duration - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
		app.logger.info("End of checkInterviewConflictForCandidate - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
		db.session.rollback()
		return {'status': False, 'message': 'APISCHEDULEINTERVIEW_INTERVIEW_EXIST_INTERVIEWER', 'code':422},False		

	app.logger.info("End of checkInterviewConflictForCandidate")
	return {'status':'success'},True



def checkInterviewConflictForCandidateUpdate(interviewid,candidate,date,slot_id,duration,user,clientip):
	#validate interview conflicts for candidate
	app.logger.info("Accessing checkInterviewConflictForCandidateUpdate - [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
	app.logger.info("candidate details- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
	app.logger.info(candidate)
	interview_available = db.session.query(Interviews).join(Interview_Candidate).filter(and_(Interview_Candidate.candidateid == candidate['userid'],Interviews.date == date,Interviews.slot_id == slot_id,Interviews.interviewstatus == C.INTERVIEW_BOOKED,Interviews.interviewid != interviewid, Interview_Candidate.active_status==True)).first()
	app.logger.info("interview_available")
	app.logger.info(interview_available)
	if interview_available:
		app.logger.debug("Candidate has already interview exists on given duration - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
		app.logger.info("End of checkInterviewConflictForCandidateUpdate - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
		return {'status': False, 'message': 'APISCHEDULEINTERVIEW_INTERVIEW_EXIST', 'code':422},False
	else:
		before_interview_available = db.session.query(Interviews).join(Interview_Candidate).filter(and_(Interview_Candidate.candidateid == candidate['userid'],Interviews.date == date,Interviews.slot_id == slot_id-1,Interviews.interviewstatus == C.INTERVIEW_BOOKED,Interviews.interviewid != Interview_Candidate.interviewid)).first()
	
		if before_interview_available:
			app.logger.debug("Candidate has already interview on before slot time - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
			if before_interview_available:
				if before_interview_available.duration == C.HALFANHOUR:
					app.logger.debug("Valid: Slot duration is HALF AN HOUR.. So proceed- " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
				else:
					app.logger.debug("Candidate has already interview exists on given duration - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
					app.logger.info("End of checkInterviewConflictForCandidateUpdate - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
					return {'status': False, 'message': 'APISCHEDULEINTERVIEW_INTERVIEW_EXIST', 'code':422},False	
		

	if duration == C.ONEHOUR:
		app.logger.debug("Duration is "+str(C.ONEHOUR)+" - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
		after_interview_available = db.session.query(Interviews).join(Interview_Candidate).filter(and_(Interview_Candidate.candidateid == candidate['userid'],Interviews.date == date,Interviews.slot_id == slot_id+1,Interviews.interviewstatus == C.INTERVIEW_BOOKED,Interviews.interviewid != Interview_Candidate.interviewid)).first()
		if after_interview_available:
			app.logger.debug("Candidate has already interview exists on given duration - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
			app.logger.info("End of checkInterviewConflictForCandidateUpdate - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
			return {'status': False, 'message': 'APISCHEDULEINTERVIEW_INTERVIEW_EXIST', 'code':422},False
			
	
	# Checking		
	if interview_available:
		app.logger.debug("Interviewer have already slot exists with on duration - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
		app.logger.info("End of checkInterviewConflictForCandidateUpdate - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
		db.session.rollback()
		return {'status': False, 'message': 'APISCHEDULEINTERVIEW_INTERVIEW_EXIST_INTERVIEWER', 'code':422},False		

	app.logger.info("End of checkInterviewConflictForCandidateUpdate")
	return {'status':'success'},True

def updateInterviewDetails(primary_interviewer, interviewers_list,existing_interviewer_ids,existing_candidate_ids,candidate,session,user,clientip):
	app.logger.info("Accessing updateInterviewDetails - [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
	app.logger.debug(session)
	app.logger.debug(primary_interviewer)
	app.logger.debug(interviewers_list)
	app.logger.debug(candidate)
	app.logger.debug("Interviewer don't have slot on given duration.So, schedule interview - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
	interview = Interviews().query.get(session['interviewid'])
	if interview is not None:
		interview.orgid = user['orgid']
		interview.interviewtitle= session['interviewtitle']
		interview.interviewdescription= session['interviewdescription']
		interview.date= session['date']
		interview.slot_id= session['slot_id']
		interview.duration= session['duration']
		interview.prerecording_enabled= session['prerecording_enabled']
		interview.interviewstatus =C.INTERVIEW_BOOKED
		interview.ftid = session['feedback_template_id']
		interview.timezone = session['admin_timezone']
		interview.scheduleadmin = user['userid']	
		interview.updatedby = user['userid']
		interview.updatedtime = datetime.now()
		interview.custom_categories_data = session['custom_categories_data']
		
		try:
			app.logger.debug("Committing Interview data to create an interview slot - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
			#db.session.commit()
			db.session.flush()
		except exc.SQLAlchemyError:
			app.logger.debug("Error while committing interview row in database - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
			app.logger.info("End of updateInterviewDetails - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
			db.session.rollback()
			return {'status': False, 'message': 'COMMON_ERROR_IN_ROW_UPDATE', 'code':422}, False


		try:
			organization_data = Organization().query.get(interview.orgid)
			app.logger.debug(organization_data)
			interview_date = session['date']
			slot_details = slot.sd[interview.slot_id]
			interview_date = (datetime.strptime(interview_date, '%Y-%m-%d') + timedelta(hours=slot_details[0],minutes=slot_details[1]))
			start_time = interview_date.strftime('%Y-%m-%dT%H:%M:%S.%f')
			end_time = (interview_date + timedelta(minutes = interview.duration)).strftime('%Y-%m-%dT%H:%M:%S.%f')

			# Calling IB2 Backend for Join URL's
			url = "http://localhost:5003/meet/update"
			payload = {
				"meeting_id": interview.archiveid,
	    		"start_time": start_time,
	    		"end_time": end_time,
	    		"duration": session['duration'],
	    		"org_name": str(organization_data.name),
	    		"org_logo": str(organization_data.logo),
	    		"instructions": {
	    			"special": [],
	    			"interview": []
	    		},
	    		"meet_options": {
	    			"code_editor": True,
	    			"whiteboard": True,
	    			"recording": True
	    		},
	    	}

			app.logger.info(payload)
			response_decoded_json = requests.post(url, json=payload)
			response_json = response_decoded_json.json()
			app.logger.debug(response_json)
			if response_json['success'] == False:
				app.logger.info("error while sending data to IB2 Backend")
				db.session.rollback()
				return {'status': False, 'message': 'ERROR_WHILE_SENDING_DATA_TO_IB2_BACKEND', 'code':422}, False
		except Exception as e:
			app.logger.info(e)
			app.logger.info("error while sending data to IB2 Backend")
			db.session.rollback()
			return {'status': False, 'message': 'ERROR_WHILE_SENDING_DATA_TO_IB2_BACKEND', 'code':422}, False

		new_list_of_interviewers = []
		
		# update primary interviewer
		primary_interviewer_interview = Interview_Interviewer().query.filter(Interview_Interviewer.interviewid == interview.interviewid,Interview_Interviewer.interviewerid == primary_interviewer['userid']).first()
		if primary_interviewer_interview:
			primary_interviewer_interview.isprimary=True
			primary_interviewer_interview.active_status=True
		else:	
			primary_interviewer_interview = Interview_Interviewer()
			primary_interviewer_interview.interviewerid = primary_interviewer['userid']
			primary_interviewer_interview.interviewid = interview.interviewid
			primary_interviewer_interview.isprimary = True
			primary_interviewer_interview.active_status=True
			primary_interviewer_interview.orgid = user['orgid']
			primary_interviewer_interview.joinurl = generateJoinInterviewURL(primary_interviewer['userid'])
			db.session.add(primary_interviewer_interview)

		new_list_of_interviewers.append(primary_interviewer['userid'])
		try:
			app.logger.info("Committing Interview_Interviewer Details - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
			db.session.commit()
			# interviewerJoiningNotificationToCelery(primary_interviewer,primary_interviewer_interview,user,clientip)
			db.session.expunge(primary_interviewer_interview)
		except Exception as e:
			db.session.rollback()
			app.logger.info("Exception while Saving Details - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
			app.logger.exception(e.message)
			app.logger.info("End of updateInterviewDetails - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")

		# Update Other Interviewers
		for other_interviewer in interviewers_list:
			app.logger.debug("Other Interviewer")
			app.logger.debug(other_interviewer)
			regular_interviewer_interview = Interview_Interviewer().query.filter(Interview_Interviewer.interviewid== interview.interviewid,Interview_Interviewer.interviewerid== other_interviewer['userid']).first()
			app.logger.debug("In regular_interviewer_interview value")

			app.logger.debug(regular_interviewer_interview)
			if regular_interviewer_interview:
				app.logger.info("regular_interviewer_interview.interviewerid")
				regular_interviewer_interview.active_status=True
				regular_interviewer_interview.isprimary = False
			else:
				regular_interviewer_interview = Interview_Interviewer()
				regular_interviewer_interview.interviewerid = other_interviewer['userid']
				regular_interviewer_interview.interviewid = interview.interviewid
				regular_interviewer_interview.orgid = user['orgid']
				regular_interviewer_interview.isprimary = False
				regular_interviewer_interview.active_status=True
				regular_interviewer_interview.joinurl = generateJoinInterviewURL(other_interviewer['userid'])
				db.session.add(regular_interviewer_interview)
			new_list_of_interviewers.append(other_interviewer['userid'])
			try:
				app.logger.debug("Committing Interview_Interviewer Details - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
				db.session.commit()
				# interviewerJoiningNotificationToCelery(other_interviewer,regular_interviewer_interview,user,clientip)
				db.session.expunge(regular_interviewer_interview)
			except Exception as e:
				db.session.rollback()
				app.logger.debug("Exception while Saving Details - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
				app.logger.exception(e.message)
				app.logger.debug("End of updateInterviewDetails - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
		app.logger.debug("new_list_of_interviewers")
		app.logger.debug(new_list_of_interviewers)

		app.logger.info("Saving Candidate Details to Interview_Candidate Table")
		interview_candidate = Interview_Candidate().query.filter(Interview_Candidate.interviewid== interview.interviewid,Interview_Candidate.candidateid== candidate['userid']).first()
		candidate_old = []
		candidate_new_list = []
		if interview_candidate:
			app.logger.info("Interview_Candidate exists")
			interview_candidate.active_status = True
		else:
			interview_candidate = Interview_Candidate()
			interview_candidate.candidateid = candidate['userid']
			interview_candidate.interviewid = interview.interviewid
			interview_candidate.active_status = True
			interview_candidate.joinurl = generateJoinInterviewURL(candidate['userid'])
			interview_candidate.orgid = user['orgid']
		candidate_new_list.append(candidate['userid'])
		try:
			app.logger.debug("Committing Candidate data to create an interview slot - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
			db.session.add(interview_candidate)
			db.session.commit()
			# candidateJoiningNotificationToCelery(candidate,interview_candidate,user,clientip)
			db.session.expunge(interview_candidate)
		except Exception as er:
			app.logger.info("Exception while sending Email/SMS notifications - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
			app.logger.exception(er.message)
			app.logger.info("End of updateInterviewDetails - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
			return {'status': False, 'message': 'COMMON_ERROR_SENDING_NOTIFICATIONS', 'code':422},False
		
		update_interviewers_ids= Interview_Interviewer().query.filter(Interview_Interviewer.interviewid == interview.interviewid).all()
		old_interviewers=[]
		app.logger.debug("Existing Interviewers")
		app.logger.debug(existing_interviewer_ids)

		app.logger.debug("update_interviewers_ids")
		app.logger.debug(update_interviewers_ids)
		
		app.logger.debug("** update interviewers joining status *****")
		new_interviewer_ids=new_list_of_interviewers
		
		app.logger.debug("existing_interviewer_ids")
		app.logger.debug(existing_interviewer_ids)
		if existing_interviewer_ids is not None:
			app.logger.debug("existing_interviewer_ids is not none")
			for eid in existing_interviewer_ids:
				app.logger.debug("eid")
				app.logger.debug(eid)
				app.logger.debug("new_interviewer_ids")
				app.logger.debug(new_interviewer_ids)
				interviewer = User().query.get(eid)
				interview_interviewer_details = Interview_Interviewer().query.filter(Interview_Interviewer.interviewid == interview.interviewid,Interview_Interviewer.interviewerid == eid).first()
				interviewer_details={'firstname':interviewer.firstname,'lastname':interviewer.lastname,'userid':interviewer.userid,'mobile':interviewer.mobile,'email':interviewer.email}					
				if eid in new_interviewer_ids:					
					app.logger.info("id exists. no need to update")
					app.logger.debug(eid)
					
					try:
						interviewerJoiningNotificationToCelery(interviewer_details,interview_interviewer_details,user,clientip)
					except Exception as e:
						app.logger.info(e)
						app.logger.info("Error occured while sending Interviewer notification")
					#db.session.expunge(interview_interviewer_details)
					# db.session.expunge(interview_interviewer_details)				
				else:
					app.logger.info("id not exist. Update status to false")
					app.logger.debug("eid is")
					app.logger.debug(eid)
					interviewers_updated_interviewer = Interview_Interviewer().query.filter(Interview_Interviewer.interviewerid == eid,Interview_Interviewer.interviewid== interview.interviewid).first()
					app.logger.debug("interviewers_updated_interviewer")
					app.logger.debug(interviewers_updated_interviewer)
					if interviewers_updated_interviewer:
						app.logger.info("update interviewers_updated_interviewer active status false")
						app.logger.debug(interviewers_updated_interviewer)
						interviewers_updated_interviewer.active_status = False
						db.session.commit()
						try:
							interviewerCancelledNotificationToCelery(interviewer_details,interview_interviewer_details,user,clientip)	
						except Exception as e:
							app.logger.info(e)
							app.logger.info("error while sending notification")
						#db.session.expunge(interview_interviewer_details)
						# db.session.expunge(interviewers_updated_interviewer)
		
			for nid in new_interviewer_ids:
				if nid in existing_interviewer_ids:
					app.logger.info("No need to send email because already send on above loop for interviewer")
				else:
					new_interviewer = User().query.get(nid)
					new_interview_interviewer_details = Interview_Interviewer().query.filter(Interview_Interviewer.interviewid == interview.interviewid,Interview_Interviewer.interviewerid == nid).first()
					new_interviewer_details={'firstname':new_interviewer.firstname,'lastname':new_interviewer.lastname,'userid':new_interviewer.userid,'mobile':new_interviewer.mobile,'email':new_interviewer.email}
					app.logger.info("Send notification for newly registered interviewer ")
					try:
						interviewerJoiningNotificationToCelery(new_interviewer_details,new_interview_interviewer_details,user,clientip)
					except Exception as e:
						app.logger.info(e)
						app.logger.info("error while sending notification")

		new_candidate_ids = candidate_new_list
		app.logger.debug("Candidate New ids")
		app.logger.debug(new_candidate_ids)
		
		app.logger.debug("existing_candidate_ids")
		app.logger.debug(existing_candidate_ids)
					

		if existing_candidate_ids is not None:
			app.logger.debug("existing_candidate_ids")
			app.logger.debug(existing_candidate_ids)
			for ecid in existing_candidate_ids:
				app.logger.debug("ecid")
				app.logger.debug(ecid)
				candidate = User().query.get(ecid)
				candidate_details={'firstname':candidate.firstname,'lastname':candidate.lastname,'userid':candidate.userid,'mobile':candidate.mobile,'email':candidate.email}
				interview_candidate_details = Interview_Candidate().query.filter(Interview_Candidate.interviewid== interview.interviewid,Interview_Candidate.candidateid== ecid).first()
				if ecid in new_candidate_ids:

					app.logger.info("id exists. no need to update")
					try:
						candidateJoiningNotificationToCelery(candidate_details,interview_candidate_details,user,clientip)
					except Exception as e:
						app.logger.info(e)
						app.logger.info("Error occured while sending candidate notification")
					# db.session.expunge(interview_candidate_details)
				else:
					app.logger.info("Candidate old and New ids")
					app.logger.debug(new_candidate_ids)
					app.logger.debug(existing_candidate_ids)
					interview_updated_candidate = Interview_Candidate().query.filter(Interview_Candidate.candidateid == ecid,Interview_Candidate.interviewid== interview.interviewid).first()
					app.logger.debug("interview_updated_candidate")
					app.logger.debug(interview_updated_candidate)
					if interview_updated_candidate:
						app.logger.debug("interview_updated_candidate")
						app.logger.debug(interview_updated_candidate)
						interview_updated_candidate.active_status = False
						db.session.commit()
						try:
							candidateCancelledNotificationToCelery(candidate_details,interview_candidate_details,user,clientip)
						except Exception as e:
							app.logger.info(e)
							app.logger.info("error while sending notification")
						# db.session.expunge(interview_updated_candidate)
						# db.session.expunge(interview_candidate_details)
			for nid in new_candidate_ids:
				if nid in existing_candidate_ids:
					app.logger.info("No need to send email because already send on above loop for candidate")
				else:
					app.logger.info("Send notification for newly registered candidate ")
					new_candidate = User().query.get(nid)
					new_candidate_details={'firstname':new_candidate.firstname,'lastname':new_candidate.lastname,'userid':new_candidate.userid,'mobile':new_candidate.mobile,'email':new_candidate.email}
					new_interview_candidate_details = Interview_Candidate().query.filter(Interview_Candidate.interviewid== interview.interviewid,Interview_Candidate.candidateid== ecid).first()
					try:
						candidateJoiningNotificationToCelery(new_candidate_details,new_interview_candidate_details,user,clientip)
					except Exception as e:
						app.logger.info(e)
						app.logger.info("error while sending notification")

			return {'status':'success'},True

	else:
		app.logger.debug("Invalid interviewid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
		app.logger.exception(e.message)
		app.logger.info("End of updateInterviewDetails - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
		return {'status': False, 'message': 'APIUPDATEINTERVIEW_INTERVIEW_NOT_EXIST', 'code':404}


def saveInterviewDetails(primary_interviewer, interviewers_list,candidate,session,user,clientip):
	app.logger.info("Accessing saveInterviewDetails - [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
	app.logger.info(session)
	app.logger.info(primary_interviewer)
	app.logger.info(interviewers_list)
	app.logger.info(candidate)
	app.logger.info(user)
	app.logger.debug("Interviewer don't have slot on given duration.So, schedule interview - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
	#create interview
	interview = Interviews()
	interview.orgid = user['orgid']
	interview.interviewtitle= session['interviewtitle']
	interview.interviewdescription= session['interviewdescription']
	interview.date= session['date']
	interview.slot_id= session['slot_id']
	interview.duration= session['duration']
	interview.interviewstatus =C.INTERVIEW_BOOKED
	interview.ftid = session['feedback_template_id']
	interview.timezone = session['admin_timezone']
	interview.scheduleadmin = user['userid']
	interview.createdtime = datetime.utcnow()
	interview.createdby = user['userid']
	interview.prerecording_enabled = session['prerecording_enabled']
	org_custom_data = OrgCustomCategories().query.filter(OrgCustomCategories.orgid==interview.orgid).first()
	app.logger.info(org_custom_data)

	try:
		OrganizationData = Organization().query.get(interview.orgid)
		app.logger.debug(OrganizationData)
		interview_date = session['date']
		slot_details = slot.sd[interview.slot_id]
		interview_date = (datetime.strptime(interview_date, '%Y-%m-%d') + timedelta(hours=slot_details[0],minutes=slot_details[1]))
		start_time = interview_date.strftime('%Y-%m-%dT%H:%M:%S.%f')
		end_time = (interview_date + timedelta(minutes = interview.duration)).strftime('%Y-%m-%dT%H:%M:%S.%f')

		# Calling IB2 Backend for Join URL's
		url = "http://localhost:5003/meet/create"
		payload = {
	    	"start_time": start_time,
	    	"end_time": end_time,
	    	"duration": session['duration'],
	    	"org_name": str(OrganizationData.name),
	    	"org_logo": str(OrganizationData.logo),
	    	"instructions": {
	    		"special": [],
	    		"interview": []
	    	},
	    	"meet_options": {
	    		"code_editor": True,
	    		"whiteboard": True,
	    		"recording": True
	    	},
	    	"users": [
	    		{
	    			"user_role": 'interviewer',
	    			"user_email": primary_interviewer['email'],
	    			"user_name": primary_interviewer['firstname'] + ' ' + primary_interviewer['lastname'],
	    			"user_image": '',
	    			"agora_uid": 1
	    		},
	    		{
	    			"user_role": 'candidate',
	    			"user_email": candidate['email'],
	    			"user_name": candidate['firstname'] + ' ' + candidate['lastname'],
	    			"user_image": '',
	    			"agora_uid": 2,
	    		},
	    	]
	    }

		app.logger.info(payload)
		response_decoded_json = requests.post(url, json=payload)
		response_json = response_decoded_json.json()
		app.logger.debug(response_json)
		# interview_candidate.joinurl = response_json['data']['data']['insert_meeting_users']['returning'][1]['meeting_user_id']
		# interview_candidate.orgid = user['orgid']
		interview.archiveid = response_json['data']['data']['insert_meeting_users']['meeting_id']
		
	except Exception as e:
		app.logger.info(e)
		app.logger.info("error while sending data to IB2 Backend")
		db.session.rollback()
		return {'status': False, 'message': 'ERROR_WHILE_SENDING_DATA_TO_IB2_BACKEND', 'code':422}, False


	if org_custom_data is not None:
		if org_custom_data.status == C.CC_ACTIVE:
			interview.custom_categories_data = session['custom_categories_data']
		else:
			interview.custom_categories_data = {}
	else:
		pass
	try:
		app.logger.debug("Committing Interview data to create an interview slot - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
		db.session.add(interview)
		# db.session.commit()
		db.session.flush()
	except exc.SQLAlchemyError:
		app.logger.debug("Error while committing interview row in database - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
		app.logger.info("End of saveInterviewDetails - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
		db.session.rollback()
		return {'status': False, 'message': 'COMMON_ERROR_IN_ROW_UPDATE', 'code':422}, False

	app.logger.info("Saving Candidate Details to Interview_Candidate Table")
	interview_candidate = Interview_Candidate()
	interview_candidate.candidateid = candidate['userid']
	interview_candidate.interviewid = interview.interviewid
	interview_candidate.joinurl = response_json['data']['data']['insert_meeting_users']['returning'][1]['meeting_user_id']
	interview_candidate.orgid = user['orgid']

	try:
		app.logger.debug("Committing Candidate data to create an interview slot - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
		db.session.add(interview_candidate)
		db.session.commit()
		interview_candidate_data = Interview_Candidate().query.filter(Interview_Candidate.interviewid == interview_candidate.interviewid).first() 
		try:
			candidateJoiningNotificationToCelery(candidate,interview_candidate_data,user,clientip)
		except Exception as e:
			app.logger.info(e)
			app.logger.info("error while sending candidate notification")
	#	db.session.expunge(interview_candidate)
	except Exception as er:
		app.logger.info("Exception while sending Email/SMS notifications - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
		app.logger.exception(er.message)
		app.logger.info("End of saveInterviewDetails - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
		# return {'status': False, 'message': 'COMMON_ERROR_SENDING_NOTIFICATIONS', 'code':422},False

		
	# update interviewers in Interview_Interviewer
	interview_data = Interviews().query.get(interview.interviewid)
	# update primary interviewer
	primary_interviewer_interview = Interview_Interviewer()
	primary_interviewer_interview.interviewerid = primary_interviewer['userid']
	primary_interviewer_interview.interviewid = interview.interviewid
	primary_interviewer_interview.isprimary = True
	primary_interviewer_interview.orgid = user['orgid']
	#primary_interviewer_interview.joinurl = generateJoinInterviewURL(primary_interviewer['userid'])
	primary_interviewer_interview.joinurl = response_json['data']['data']['insert_meeting_users']['returning'][0]['meeting_user_id']
	db.session.add(primary_interviewer_interview)
	try:
		app.logger.info("Committing Interview_Interviewer Details - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
		db.session.commit()
		primary_interview_interviewer_data = Interview_Interviewer().query.filter(Interview_Interviewer.interviewid == primary_interviewer_interview.interviewid,Interview_Interviewer.interviewerid  == primary_interviewer['userid']).first()
		try:
			interviewerJoiningNotificationToCelery(primary_interviewer,primary_interview_interviewer_data,user,clientip)
		except Exception as e:
			app.logger.info(e)
			app.logger.info("error while sending interviewer notification")
		app.logger.info("primary_interview_interviewer_data")
		app.logger.info(primary_interview_interviewer_data)
		app.logger.info(primary_interview_interviewer_data)
		app.logger.info(primary_interview_interviewer_data.interviewerid)
		# db.session.flush(primary_interviewer_interview)
		# db.session.expunge(primary_interviewer_interview)
		# db.session.expunge(primary_interview_interviewer_data)
	except Exception as e:
		db.session.rollback()
		app.logger.info("Exception while Saving Details - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
		app.logger.exception(e.message)
		app.logger.info("End of saveInterviewDetails - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")

	# Update Other Interviewers
	for other_interviewer in interviewers_list:
		regular_interviewer_interview = Interview_Interviewer()
		regular_interviewer_interview.interviewerid = other_interviewer['userid']
		regular_interviewer_interview.interviewid = interview.interviewid
		regular_interviewer_interview.isprimary = False
		regular_interviewer_interview.orgid = user['orgid']
		regular_interviewer_interview.joinurl = generateJoinInterviewURL(other_interviewer['userid'])
		db.session.add(regular_interviewer_interview)
		try:
			app.logger.info("Committing Interview_Interviewer  Details for Regular interviewer - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
			db.session.commit()

			regular_interview_interviewer_data = Interview_Interviewer().query.filter(Interview_Interviewer.interviewid == regular_interviewer_interview.interviewid,Interview_Interviewer.interviewerid  == other_interviewer['userid']).first()
			app.logger.info("regular_interview_interviewer_data")
			app.logger.info(regular_interview_interviewer_data)
			app.logger.info(regular_interview_interviewer_data.interviewerid)
			app.logger.info("other_interviewer in regular_interviewer")
			app.logger.info(other_interviewer)
			interviewerJoiningNotificationToCelery(other_interviewer,regular_interview_interviewer_data,user,clientip)
			# db.session.expunge(other_interviewer)
			db.session.expunge(regular_interviewer_interview)
		except Exception as e:
			db.session.rollback()
			app.logger.info("Exception while Saving Details - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
			app.logger.exception(e.message)
			app.logger.info("End of saveInterviewDetails - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
	app.logger.info("end of saveInterviewDetails")
	return {'status':'success'},True

def interviewerJoiningNotificationToCelery(interviewer_data,interview_interviewers,user,clientip):
	try:
		app.logger.info("interviewerJoiningNotificationToCelery")
		app.logger.info(interviewer_data)
		app.logger.info(interview_interviewers)
		user_details = User().query.get(interviewer_data['userid'])
		notification_type = C.NOTIFICATION_JOININTERVIEWURL
		url = app.config['APP_URL']+'/join/'+interview_interviewers.joinurl
		app.logger.debug("Sending Emails/SMS to Interviewer "+str(interviewer_data['email'])+" - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
		
		# notify_schedule_task.apply_async(args=[[user_details],interview_interviewers,notification_type], kwargs={'grp': C.INTERVIEWER,'url':url, 'bnd': False, 'eml': True, 'mbl': True})
		notify_schedule([user_details], interview_interviewers, notification_type, C.INTERVIEWER, url, False, True, True)
		
		# notify_schedule(usrs,interview_data,n_type,grp=C.INTERVIEWER, url=None, bnd=False, eml=False, mbl=False)

		notif_type = C.NOTIFICATION_INTERVIEWFEEDBACKURL
		url_feedback = app.config['APP_URL']+'/feedback/'+interview_interviewers.joinurl 
		# notify_schedule_task.apply_async(args=[[user_details],interview_interviewers,notif_type], kwargs={'grp': C.INTERVIEWER,'url':url_feedback,'bnd': False, 'eml': True, 'mbl': True})
		notify_schedule([user_details], interview_interviewers, notif_type, C.INTERVIEWER, url_feedback, False, True, True)
		
		app.logger.info("End of interviewerJoiningNotificationToCelery")
	except Exception as e:
		app.logger.info("--------------error while sending notification--------------")
		app.logger.info(e)
		app.logger.info("End of interviewerJoiningNotificationToCelery")

def interviewerCancelledNotificationToCelery(interviewer_data,interview_interviewers,user,clientip):
	try:
		app.logger.info("interviewerCancelledNotificationToCelery")
		app.logger.info(interviewer_data)
		app.logger.info(interview_interviewers)
		user_details = User().query.get(interviewer_data['userid'])
		
		interview_interviewers = Interview_Interviewer().query.filter(Interview_Interviewer.interviewid == interview_interviewers.interviewid,Interview_Interviewer.interviewerid == interviewer_data['userid']).first()
		notification_type = C.NOTIFICATION_INTERVIEW_CANCELLATION
		app.logger.debug("Sending Emails/SMS to Interviewer "+str(interviewer_data['email'])+" - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
		# notify_schedule_task.apply_async(args=[[user_details],interview_interviewers,notification_type], kwargs={'grp': C.INTERVIEWER,'url':None, 'bnd': False, 'eml': True, 'mbl': True})
		notify_schedule([user_details],interview_interviewers,notification_type, C.INTERVIEWER, None, False, True, True)


		app.logger.info("End of interviewerCancelledNotificationToCelery")
		
	except Exception as e:
		app.logger.info("--------------error while sending notification--------------")
		app.logger.info(e)
		app.logger.info("End of interviewerCancelledNotificationToCelery")


def asyncinterviewScheduledNotificationToCelery(candidate_data,async_interview_id,user,clientip):
	try:
		app.logger.info("asyncinterviewScheduledNotificationToCelery")
		app.logger.info(candidate_data)
		app.logger.info(async_interview_id)
		user_details = User().query.get(candidate_data['userid'])
		asyncInterview = AsyncInterviews().query.filter(AsyncInterviews.asyncid == async_interview_id,AsyncInterviews.userid == candidate_data['userid']).first()
		notification_type = C.NOTIFICATION_ASYNC_INTERVIEW_SCHEDULE
		app.logger.debug("Sending Emails/SMS to Candidte  "+str(candidate_data['email'])+" - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
			

		# notify_schedule_task.apply_async(args=[[user_details],asyncInterview,notification_type], kwargs={'grp': C.INTERVIEWEE,'url':None, 'bnd': False, 'eml': True, 'mbl': True})
		notify_schedule([user_details], asyncInterview, notification_type, C.INTERVIEWEE, None, False, True, True)
		app.logger.info("End of asyncinterviewScheduledNotificationToCelery")
	except Exception as e:
		app.logger.info("--------------error while sending notification--------------")
		app.logger.info(e)
		app.logger.info("End of asyncinterviewScheduledNotificationToCelery")


def candidateCancelledNotificationToCelery(candidate_data,interview_candidate,user,clientip):
	
	try:
		app.logger.info("Accessing candidateCancelledNotificationToCelery")
		app.logger.info(candidate_data)
		app.logger.info(interview_candidate)
		user_details = User().query.get(candidate_data['userid'])
		notification_type = C.NOTIFICATION_INTERVIEW_CANCELLATION
		interview_candidate = Interview_Candidate().query.filter(Interview_Candidate.interviewid == interview_candidate.interviewid,Interview_Candidate.candidateid == candidate_data['userid']).first()
		app.logger.debug("Sending Emails/SMS to Candidate for cancellation "+str(candidate_data['email'])+" - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")

		# notify_schedule_task.apply_async(args=[[user_details],interview_candidate,notification_type], kwargs={'grp': C.INTERVIEWEE,'url':None, 'bnd': False, 'eml': True, 'mbl': True})

		notify_schedule([user_details], interview_candidate, notification_type, C.INTERVIEWEE, None, False, True, True)
		app.logger.info("End of candidateCancelledNotificationToCelery")
	except Exception as e:
		app.logger.info("--------------error while sending notification--------------")
		app.logger.info(e)
		app.logger.info("End of candidateCancelledNotificationToCelery")


def asyncCandidateCancelledNotificationToCelery(candidate_data,interview_candidate,user,clientip):
	try:
		app.logger.info("Accessing asyncCandidateCancelledNotificationToCelery")
		app.logger.info(candidate_data)
		app.logger.info(interview_candidate)
		user_details = User().query.get(candidate_data['userid'])
		notification_type = C.NOTIFICATION_ASYNC_INTERVIEW_CANCELLATION
		asyncinterview = AsyncInterviews().query.filter(AsyncInterviews.asyncid == interview_candidate.asyncid,AsyncInterviews.userid == candidate_data['userid']).first()
		app.logger.debug("Sending Emails/SMS to Candidate for cancellation "+str(candidate_data['email'])+" - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
		
		
		# notify_schedule_task.apply_async(args=[[user_details],asyncinterview,notification_type], kwargs={'grp': C.INTERVIEWEE,'url':None, 'bnd': False, 'eml': True, 'mbl': True})

		notify_schedule([user_details], asyncinterview, notification_type, C.INTERVIEWEE, None, False, True, True)
		app.logger.info("End of asyncCandidateCancelledNotificationToCelery")
	except Exception as e:
		app.logger.info("--------------error while sending notification--------------")
		app.logger.info(e)
		app.logger.info("End of asyncCandidateCancelledNotificationToCelery")


def candidateJoiningNotificationToCelery(candidate_data,interview_candidate,user,clientip):
	try:
		app.logger.info("candidateJoiningNotificationToCelery")
		app.logger.info(candidate_data)
		app.logger.info(interview_candidate)
		user_details = User().query.get(candidate_data['userid'])
		notification_type = C.NOTIFICATION_JOININTERVIEWURL
		url = app.config['APP_URL']+'/join/'+interview_candidate.joinurl
		app.logger.debug("Sending Emails/SMS to Candidate for joining "+str(candidate_data['email'])+" - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
		
		# notify_schedule_task.apply_async(args=[[user_details],interview_candidate,notification_type], kwargs={'grp': C.INTERVIEWEE,'url':url, 'bnd': False, 'eml': True, 'mbl': True})

		notify_schedule([user_details], interview_candidate, notification_type,C.INTERVIEWEE,  url, False, True, True)
		app.logger.info("End of candidateJoiningNotificationToCelery")

	except Exception as e:
		app.logger.info("--------------error while sending notification--------------")
		app.logger.info(e)
		app.logger.info("End of candidateJoiningNotificationToCelery")

def isPrimaryInterviewer(interviewer_list):
	app.logger.info("isPrimaryInterviewer")
	for interviewer in interviewer_list:
		app.logger.info(interviewer)
		if interviewer == 'primary_interviewer':
			interviewer = interviewer_list['primary_interviewer']
			primary_interviewer_email = interviewer['email']
			user_details = User.query.filter(User.email == primary_interviewer_email).first()
			if user_details:
				primary_interviewer = {'userid':user_details.userid,'email':user_details.email}
			else:
				return {'status':False,'message':'ISPRIMARYINTERVIEWER_NOT_EXISTS','code':422},False

	return primary_interviewer,True

class ApiScheduleInterview(RestResource):
	def post(self):
		app.logger.info("Accessing ApiScheduleInterview")
		clientip = request.remote_addr
		app.logger.debug("Check user token - " + str(clientip))
		status, user = user_auth(admin_access=C.ADMIN)
		app.logger.debug("request.form")
		app.logger.debug(request.form)
		if status and user:
			app.logger.debug("User is authorized to access ApiScheduleInterview service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			# read parameters
			try:
				app.logger.debug(request.data)
				app.logger.debug(request.json)
				app.logger.debug(request.args)
				app.logger.debug("Parsing Schedule Interview details - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				args = parser.parse(ScheduleInterviewSchema(), request, locations=request_locations)
			except Exception as e:
				app.logger.debug("Error while parsing user details - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.exception(e.message)
				app.logger.info("End of ApiScheduleInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': False, 'message': 'COMMON_ERROR_REQ_MORE_FIELD', 'code':422}
			
			app.logger.debug(args)
			try:
				interviewtitle = args['interviewtitle']
				app.logger.debug("Interview title value exists "+str(interviewtitle)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				isInterviewTitle = validateString(args['interviewtitle'])
				if isInterviewTitle:
					app.logger.debug("Interview title not empty"+str(interviewtitle)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				else:
					app.logger.debug("Interview title empty "+str(interviewtitle)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					raise emptyStringError()


				interviewdescription = args['interviewdescription']
				app.logger.debug("Interview Description value exists "+str(interviewdescription)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				isInterviewDescription = validateString(args['interviewdescription'])
				if isInterviewDescription:
					app.logger.debug("Interview description not empty "+str(interviewdescription)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				else:
					app.logger.debug("Interview description empty "+str(interviewdescription)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					raise emptyStringError()


				date = args['date']
				app.logger.debug("Interview date value exists "+str(date)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				dateValidation = datetime.strptime(date, "%Y-%m-%d")
				current_date = datetime.utcnow()
				current_date_string = datetime.strftime(current_date, "%Y-%m-%d")
				current_datetime = datetime.strptime(current_date_string, "%Y-%m-%d")
				if dateValidation:
					app.logger.debug("Date format is valid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					if dateValidation < current_datetime :
						app.logger.debug("interview date is less than current date"+str(date)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						app.logger.info("End of ApiScheduleInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						return {'status': False, 'message': 'APISCHEDULEINTERVIEW_DATE_LESS_THAN_CURRENT_DATE', 'code':422}	
					else:
						app.logger.info("Date is greater than current date")

				else:
					app.logger.debug("Date format is not valid "+str(date)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.info("End of ApiScheduleInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {'status': False, 'message': 'APISCHEDULEINTERVIEW_DATE_INVALID', 'code':422}	

				slot_id = args['slot_id']
				app.logger.debug("Interview slot_id value "+str(slot_id)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				if slot_id > 0 and slot_id <= C.MAXDAYSLOTS:
					app.logger.info("Interview is digit - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					
				else:
					app.logger.debug("Slot id is not valid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.info("End of ApiScheduleInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {'status': False, 'message': 'APISCHEDULEINTERVIEW_SLOT_INVALID', 'code':422}	

				duration = args['duration']
				app.logger.debug("Interview duration value exists "+str(duration)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				if duration == C.HALFANHOUR or duration == C.ONEHOUR :
					app.logger.debug("Interview duration value is valid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					pass
				else:
					app.logger.debug("Invalid interview duration - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.info("End of ApiScheduleInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {'status': False, 'message': 'APISCHEDULEINTERVIEW_DURATION_INVALID', 'code':422}

				prerecording_enabled = args['prerecording_enabled']
				app.logger.debug("prerecording_enabled value exists "+str(prerecording_enabled)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				if prerecording_enabled:
					app.logger.debug("Pre recording is enabled - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				else:
					app.logger.debug("Pre recording is disabled - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")

				try:
					custom_categories_data = json.loads(args['custom_categories_data'])
					check_json = json.loads(args['custom_categories_data'])
				except Exception as e:
					app.logger.info("error while receiving custom_categories_data")
					app.logger.info(e)
					return {'code':422,'status':False,'message':'ERROR_RECEIVING_CC_DATA'},422
				
				app.logger.info("custom_categories_data value exists")
				app.logger.info(custom_categories_data)

				candidate_fname = args['candidate_fname']
				app.logger.debug("Candidate fname exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				isCandidateFirstName = validateString(args['candidate_fname'])
				if isCandidateFirstName:
					app.logger.debug("Candidate_fname not empty "+str(candidate_fname)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				else:
					app.logger.debug("Candidate_fname empty "+str(candidate_fname)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					raise emptyStringError()
				
				candidate_lname = args['candidate_lname']
				app.logger.debug("Candidate lname exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				isCandidateLastName = validateString(args['candidate_lname'])
				if isCandidateLastName:
					app.logger.debug("Candidate_fname not empty "+str(candidate_lname)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				else:
					app.logger.debug("Candidate_fname empty "+str(candidate_lname)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					raise emptyStringError()
				
				admin_timezone = args['localtimezone']
				app.logger.debug("Time Zone exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				islocalTimeZone = validateString(args['localtimezone'])
				if islocalTimeZone:
					app.logger.debug("Time Zone not empty "+admin_timezone+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				else:
					app.logger.debug("Time Zone empty "+admin_timezone+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					raise emptyStringError()

				app.logger.debug("Candidate mobile value exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				candidate_mobile = args['candidate_mobile']
				if candidate_mobile:
					isMobile = validateString(candidate_mobile)
					if isMobile:
						if candidate_mobile.isdigit():
							app.logger.debug("Mobile not empty "+str(candidate_mobile)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							candidate_mobile_number = long(candidate_mobile)
							if candidate_mobile_number > 0:
								app.logger.debug("Candidate mobile is exists "+str(candidate_mobile)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							else:
								app.logger.debug("Mobile number is invalid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
								app.logger.info("End of ApiScheduleInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
								return {'status': False, 'message': 'COMMON_MOBILE_NUMBER_INVALID', 'code':422}	
						else:
							app.logger.debug("Mobile number is not digit - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							app.logger.info("End of ApiScheduleInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							return {'status': False, 'message': 'COMMON_MOBILE_NUMBER_NOT_DIGIT', 'code':422}	
					else:
						app.logger.debug("Mobile empty "+str(candidate_mobile)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						raise emptyStringError()
				
				candidate_email= args['candidate_email']
				app.logger.debug("Candidate email value exists  "+str(candidate_email)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")	
				isCandidateEmailValid = validateEmail(candidate_email)
				if isCandidateEmailValid:
					app.logger.debug("Email is valid"+str(candidate_email)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")			
				else:
					app.logger.debug("Email is not valid"+str(candidate_email)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					raise invalidEmailError()
				
				
				orgid = args['orgid']
				app.logger.debug("Organization id value exists "+str(orgid)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				if orgid > 0 :
					app.logger.debug("Organization id is digit - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					
				else:
					app.logger.debug("Organization id is not a digit - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.info("End of ApiScheduleInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {'status': False, 'message': 'APISCHEDULEINTERVIEW_ORGID_NOT_DIGIT', 'code':422}	

				feedback_template_id = args['feedback_template_id']
				app.logger.debug("feedback_template_id  value exists "+str(orgid)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				if feedback_template_id > 0 :
					app.logger.debug("feedback_template_id is digit - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					
				else:
					app.logger.debug("feedback_template_id is not a digit - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.info("End of ApiScheduleInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {'status': False, 'message': 'APISCHEDULEINTERVIEW_FEEDBACK_TEMPLATE_ID_NOT_DIGIT', 'code':422}	


				interviewer_list = json.loads(args['interviewers_list'])
				app.logger.info("interviewer_list")
				app.logger.info(interviewer_list)
				if len(interviewer_list) >= 1:
					app.logger.info("Interviewes length greater than 1")
					app.logger.info(interviewer_list)
				else:
					return {'status': False, 'message': 'APISCHEDULEINTERVIEW_NO_INTERVIEWER_EXIST', 'code':422}	
				
				
				common_details = {'email':user.email,'orgid':user.orgid,'userid':user.userid}
				candidate_details ={'candidate_email':args['candidate_email'],'candidate_mobile':args['candidate_mobile'],'candidate_fname':args['candidate_fname'],'candidate_lname':args['candidate_lname']}
				app.logger.info("candidate_details")
				app.logger.info(candidate_details)
				#Candidate Creation
				candidate,message,isCreateCandidateSuccess = creatingCandidate(candidate_details,common_details,clientip)

				if isCreateCandidateSuccess:
					if candidate:
						#Interviewer Creation
						primary_interviewer, other_interviewers, message, isCreateInterviewerSuccess = creatingInterviewers(interviewer_list,common_details,clientip)
						app.logger.info("other_interviewers in after creatingInterviewers")
						app.logger.info(other_interviewers)
						if isCreateInterviewerSuccess:
							all_interviewers = list(other_interviewers)
							all_interviewers.append(primary_interviewer)
							# Interviewer Slot Availability
							isSlotAvailableForInterviewer,isinterviewerSlot = checkInterviewConflictForInterviewer(all_interviewers,date,slot_id,duration,common_details,clientip)
							
							if isinterviewerSlot:
								# Candidate Slot Availability
								isSlotAvailableForCandidate,isCandidateSlot = checkInterviewConflictForCandidate(candidate,date,slot_id,duration,common_details,clientip)
								if isCandidateSlot:
									app.logger.info("Candidate have availability")
								else:
									app.logger.info("Candidate  may not have availability")
									return isSlotAvailableForCandidate
							else:
								app.logger.info("One of the interviewer may not have availability")
								return isSlotAvailableForInterviewer
						else:
							app.logger.info("Sorry no candidate created")
							return message

					else:
						app.logger.info("Sorry no candidate created.So, Unable to schedule interview")
						return candidate
				else:
					return message

				app.logger.info("other_interviewers")
				app.logger.info(other_interviewers)
				# Getting user id after add the new user
				session_details={'interviewtitle':interviewtitle,'interviewdescription':interviewdescription,'date':date,'slot_id':slot_id,'duration':duration,'admin_timezone':admin_timezone,'orgid':orgid,'feedback_template_id':feedback_template_id,'prerecording_enabled':prerecording_enabled,'custom_categories_data':custom_categories_data}
				interview_details,isDataSaved = saveInterviewDetails(primary_interviewer, other_interviewers,candidate,session_details,common_details,clientip)
				if isDataSaved:
					app.logger.debug("Successfully booked Interview")
					return {'status': True, 'message': 'APISCHEDULEINTERVIEW_BOOKED', 'code':201}
				else:
					return interview_details
				

			except RequiredFieldsMissing as re:
				app.logger.info("One or more required fileds missing - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.exception(re)
				app.logger.info("End of ApiScheduleInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_REQ_MORE_FIELD', 'code':422}
			
			except IntegrityError as ie:
				app.logger.info("Error: DB Exceeding limit Error - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.error(ie.message)
				app.logger.info("End of ApiScheduleInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_CONTACT_SUPPORT', 'code':409}
			except DataError as de:
				app.logger.info("DataBase field value out of range - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.error(de.message)
				app.logger.info("End of ApiScheduleInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_INVALID_DATA', 'code':422}
			except InvalidRequestError as ir:
				app.logger.info("received invalid request - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.exception(ir)
				app.logger.error(ir.message)
				app.logger.info("End of ApiScheduleInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_INVALID_REQUSET', 'code':422}
			except ValueError as ve:
				app.logger.info("Date Format is invalid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.error(ve.message)
				app.logger.info("End of ApiScheduleInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': str(ve.message), 'code':422}
			except ValueMissing as vm:
				app.logger.info("Received invalid input - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.error(vm.message)
				app.logger.info("End of ApiScheduleInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_INVALID_INPUT', 'code':422}
			except invalidEmailError as iee:
				app.logger.info("Email ID is not valid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.exception(iee)
				app.logger.error(iee.message)
				app.logger.info("End of ApiScheduleInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_EMAIL_NOT_VALID', 'code':422}
			except emptyStringError as ese:
				app.logger.info("One of the field is Empty - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.exception(ese)
				app.logger.error(ese.message)
				app.logger.info("End of ApiScheduleInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_ONE_OF_EMPTY_FIELD', 'code':422}
			except Exception as e:
				app.logger.info("Error while Schedule Interview- ")
				app.logger.exception(e.message)
				app.logger.info("End of ApiScheduleInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': False, 'message': 'APISCHEDULEINTERVIEW_ERROR_IN_SCHEDULING', 'code':422}

		elif user:
			app.logger.debug("User is not authorized to access ApiScheduleInterview service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.info("End of ApiScheduleInterview - " + str(clientip))
			return {'status': False,  'message': 'COMMON_UNAUTHORIZED', 'code':403}
		else:
			app.logger.info("Invalid token or Session expired - " + str(clientip))
			app.logger.info("End of ApiScheduleInterview - " + str(clientip))
			return {'status': False, 'message': 'COMMON_INVALID_TOKEN', 'code':401}


def generateJoinInterviewURL(userid):
	app.logger.info("Accessing generateJoinInterviewURL")
	epoch = datetime(1970,1,1)
	current_time = datetime.utcnow()
	delta_time = (current_time - epoch).total_seconds()
	final_value = long(delta_time)
	url = str(userid)+""+str(final_value)
	app.logger.info("End of generateJoinInterviewURL")
	return url

class ApiUpdateUpcomingInterview(RestResource):
	def post(self,interviewid):
		app.logger.info("Accessing ApiUpdateUpcomingInterview")
		clientip = request.remote_addr

		app.logger.debug("Check user token - " + str(clientip))

		try:
			status,user = user_auth(admin_access=C.ADMIN)
		except Exception as e:
			app.logger.info("error while checking authentication - " + str(clientip))
			app.logger.exception(e)
			app.logger.info("end of ApiUpdateUpcomingInterview - " + str(clientip))
			return {'status': False, 'message': 'COMMON_ERROR_IN_AUTHENTICATION', 'code':422}
		if status and user:
			app.logger.debug("User is authorized to access ApiUpdateUpcomingInterview service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.debug("Parsing Interview Update Details - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			try:
				args = parser.parse(InterviewUpdateSchema(), request, locations=request_locations)
			except Exception as e:
				app.logger.debug("Error while parsing interview details - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.exception(e.message)
				app.logger.info("End of ApiUpdateUpcomingInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': False, 'message': 'COMMON_ERROR_REQ_MORE_FIELD', 'code':422}

			#validation for parsed values
			try:
				app.logger.debug("request")
				app.logger.debug(request)
				app.logger.debug("request.args")
				app.logger.debug(request.args)
				app.logger.debug("request.data")
				app.logger.debug(request.data)
				app.logger.debug('args')
				app.logger.debug(args)
				app.logger.debug("request.form")
				app.logger.debug(request.form)


				interviewtitle = args['interviewtitle']
				app.logger.debug("Interview title value exists "+str(interviewtitle)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				isInterviewTitle = validateString(args['interviewtitle'])
				if isInterviewTitle:
					app.logger.debug("Interview title not empty"+str(interviewtitle)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				else:
					app.logger.debug("Interview title empty "+str(interviewtitle)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					raise emptyStringError()


				interviewdescription = args['interviewdescription']
				app.logger.debug("Interview Description value exists "+str(interviewdescription)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				isInterviewDescription = validateString(args['interviewdescription'])
				if isInterviewDescription:
					app.logger.debug("Interview description not empty "+str(interviewdescription)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				else:
					app.logger.debug("Interview description empty "+str(interviewdescription)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					raise emptyStringError()

				date = args['date']
				app.logger.debug("Interview date value exists "+str(date)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				dateValidation = datetime.strptime(date, "%Y-%m-%d")
				if dateValidation:
					app.logger.debug("Date format is valid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				else:
					app.logger.debug("Date format is not valid "+str(date)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.info("End of ApiUpdateUpcomingInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {'status': False, 'message': 'APISCHEDULEINTERVIEW_DATE_INVALID', 'code':422}	

				slot_id = args['slot_id']
				app.logger.debug("Interview slot_id value "+str(slot_id)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				if slot_id > 0 and slot_id <= C.MAXDAYSLOTS:
					app.logger.info("Interview is digit - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					
				else:
					app.logger.debug("Slot id is not valid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.info("End of ApiUpdateUpcomingInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {'status': False, 'message': 'APISCHEDULEINTERVIEW_SLOT_INVALID', 'code':422}	

				duration = args['duration']
				app.logger.debug("Interview duration value exists "+str(duration)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				if duration == C.HALFANHOUR or duration == C.ONEHOUR :
					app.logger.debug("Interview duration value is valid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					pass
				else:
					app.logger.debug("Invalid interview duration - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.info("End of ApiUpdateUpcomingInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {'status': False, 'message': 'APISCHEDULEINTERVIEW_DURATION_INVALID', 'code':422}

				prerecording_enabled = args['prerecording_enabled']
				app.logger.debug("prerecording_enabled value exists "+str(prerecording_enabled)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				if prerecording_enabled:
					app.logger.debug("Pre recording is enabled - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				else:
					app.logger.debug("Pre recording is disabled - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")

				candidate_fname = args['candidate_fname']
				app.logger.debug("Candidate fname exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				isCandidateFirstName = validateString(args['candidate_fname'])
				if isCandidateFirstName:
					app.logger.debug("Candidate_fname not empty "+str(candidate_fname)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				else:
					app.logger.debug("Candidate_fname empty "+str(candidate_fname)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					raise emptyStringError()
				
				candidate_lname = args['candidate_lname']
				app.logger.debug("Candidate lname exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				isCandidateLastName = validateString(args['candidate_lname'])
				if isCandidateLastName:
					app.logger.debug("Candidate_fname not empty "+str(candidate_lname)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				else:
					app.logger.debug("Candidate_fname empty "+str(candidate_lname)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					raise emptyStringError()

				
				app.logger.debug("Candidate mobile value exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				candidate_mobile = args['candidate_mobile']
				if candidate_mobile:
					isMobile = validateString(candidate_mobile)
					if isMobile:
						if candidate_mobile.isdigit():
							app.logger.debug("Mobile not empty "+str(candidate_mobile)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							candidate_mobile_number = long(candidate_mobile)
							if candidate_mobile_number > 0:
								app.logger.debug("Candidate mobile is exists "+str(candidate_mobile)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							else:
								app.logger.debug("Mobile number is invalid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
								app.logger.info("End of ApiUpdateUpcomingInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
								return {'status': False, 'message': 'COMMON_MOBILE_NUMBER_INVALID', 'code':422}	
						else:
							app.logger.debug("Mobile number is not digit - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							app.logger.info("End of ApiUpdateUpcomingInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							return {'status': False, 'message': 'COMMON_MOBILE_NUMBER_NOT_DIGIT', 'code':422}	
					else:
						app.logger.debug("Mobile empty "+str(candidate_mobile)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						raise emptyStringError()
				
				candidate_email= args['candidate_email']
				app.logger.debug("Candidate email value exists  "+str(candidate_email)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")	
				isCandidateEmailValid = validateEmail(candidate_email)
				if isCandidateEmailValid:
					app.logger.debug("Email is valid"+str(candidate_email)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")			
				else:
					app.logger.debug("Email is not valid"+str(candidate_email)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					raise invalidEmailError()
				
				
				admin_timezone = args['localTimeZone']
				app.logger.debug("Time Zone exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				islocalTimeZone = validateString(args['localTimeZone'])
				if islocalTimeZone:
					app.logger.debug("Time Zone not empty "+admin_timezone+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				else:
					app.logger.debug("Time Zone empty "+admin_timezone+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					raise emptyStringError()

				try:
					custom_categories_data = json.loads(args['custom_categories_data'])
					check_json = json.loads(args['custom_categories_data'])
				except Exception as e:
					app.logger.info("error while receiving custom_categories_data")
					app.logger.info(e)
					return {'code':422,'status':False,'message':'ERROR_RECEIVING_CC_DATA'},422
				
				feedback_template_id = args['feedback_template_id']
				app.logger.debug("feedback_template_id  value exists "+str(user.orgid)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				if feedback_template_id > 0 :
					app.logger.debug("feedback_template_id is digit - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					
				else:
					app.logger.debug("feedback_template_id is not a digit - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.info("End of ApiScheduleInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {'status': False, 'message': 'APISCHEDULEINTERVIEW_FEEDBACK_TEMPLATE_ID_NOT_DIGIT', 'code':422}	


				interviewer_list = json.loads(args['interviewers_list'])
				app.logger.info("interviewer_list")
				app.logger.info(interviewer_list)
				if len(interviewer_list) >= 1:
					app.logger.info("Interviewes length greater than 1")
					app.logger.info(interviewer_list)
				else:
					return {'status': False, 'message': 'APISCHEDULEINTERVIEW_NO_INTERVIEWER_EXIST', 'code':422}	
				#check interview exist or not
				interview = Interviews().query.get(interviewid)
				if interview:
					existing_interviewer_ids = []
					existing_interviewers = Interview_Interviewer().query.filter(Interview_Interviewer.interviewid == interviewid).all()
					# existing_interviewers_result = ExistingInterviewersInInterviewSchema(existing_interviewers,many=True).data
					if existing_interviewers is not None:
						for intid in existing_interviewers:
							existing_interviewer_ids.append(intid.interviewerid)

					existing_candidate_ids = []
					existing_candidate = Interview_Candidate().query.filter(Interview_Candidate.interviewid == interviewid).all()
					# existing_candidate_result = ExistingCandidateInInterviewSchema(existing_candidate,many=True).data
					if existing_candidate is not None:
						for cand in existing_candidate:
							existing_candidate_ids.append(cand.candidateid)

					common_details = {'email':user.email,'orgid':user.orgid,'userid':user.userid}
					# candidate_details ={'candidate_email':candidate_email,'candidate_mobile':candidate_mobile,'candidate_fname':'candidate_fname','candidate_lname':'candidate_lname'}
					candidate_details ={'candidate_email':args['candidate_email'],'candidate_mobile':args['candidate_mobile'],'candidate_fname':args['candidate_fname'],'candidate_lname':args['candidate_lname']}
					#Candidate Creation
					candidate,message,isCreateCandidateSuccess = creatingCandidate(candidate_details,common_details,clientip)

					if isCreateCandidateSuccess:
						if candidate:
							#Interviewer Creation
							primary_interviewer, other_interviewers, message, isCreateInterviewerSuccess = creatingInterviewers(interviewer_list,common_details,clientip)
							app.logger.info("other_interviewers in after creatingInterviewers")
							app.logger.info(other_interviewers)
							if isCreateInterviewerSuccess:
								all_interviewers = list(other_interviewers)
								all_interviewers.append(primary_interviewer)
								# Interviewer Slot Availability
								isSlotAvailableForInterviewer,isinterviewerSlot = checkInterviewConflictForInterviewerUpdate(interviewid,all_interviewers,date,slot_id,duration,common_details,clientip)
								
								if isinterviewerSlot:
									# Candidate Slot Availability
									isSlotAvailableForCandidate,isCandidateSlot = checkInterviewConflictForCandidateUpdate(interviewid,candidate,date,slot_id,duration,common_details,clientip)
									if isCandidateSlot:
										app.logger.info("Candidate have availability")
									else:
										app.logger.info("Candidate  may not have availability")
										return isSlotAvailableForCandidate
								else:
									app.logger.info("One of the interviewer may not have availability")
									return isSlotAvailableForInterviewer
							else:
								app.logger.info("Sorry no candidate created")
								return message

						else:
							app.logger.info("Sorry no candidate created.So, Unable to schedule interview")
							return candidate
					else:
						return message

					app.logger.info("other_interviewers")
					app.logger.info(other_interviewers)
					# Getting user id after add the new user
					session_details= {'interviewtitle':interviewtitle,'interviewid':interviewid,'interviewdescription':interviewdescription,'date':date,'slot_id':slot_id,'duration':duration,'admin_timezone':admin_timezone,'orgid':user.orgid,'feedback_template_id':feedback_template_id,'prerecording_enabled':prerecording_enabled,'custom_categories_data':custom_categories_data}
					interview_details,isDataSaved = updateInterviewDetails(primary_interviewer, other_interviewers,existing_interviewer_ids,existing_candidate_ids,candidate,session_details,common_details,clientip)
					if isDataSaved:
						app.logger.debug("Successfully booked Interview")
						app.logger.info("end of ApiUpdateUpcomingInterview")
						return {'status': True, 'message': 'APIUPCOMINGINTERVIEW_UPDATED', 'code':201}

					else:
						app.logger.debug("Failed to booked interview")
						app.logger.info("end of ApiUpdateUpcomingInterview")
						return interview_details
						
				else:
					app.logger.debug("Invalid interviewid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.exception(e.message)
					app.logger.info("End of ApiUpdateUpcomingInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {'status': False, 'message': 'APIUPDATEINTERVIEW_INTERVIEW_NOT_EXIST', 'code':404}	
			
			except RequiredFieldsMissing as re:
				app.logger.info("One or more required fileds missing - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.exception(re)
				app.logger.info("End of ApiUpdateUpcomingInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_REQ_MORE_FIELD', 'code':422}
		
			except IntegrityError as ie:
				app.logger.info("Error: DB Exceeding limit Error - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.error(ie.message)
				app.logger.info("End of ApiUpdateUpcomingInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_CONTACT_SUPPORT', 'code':409}

			except invalidEmailError as iee:
				app.logger.info("Email ID is not valid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.exception(iee)
				app.logger.error(iee.message)
				app.logger.info("End of ApiUpdateUpcomingInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_EMAIL_NOT_VALID', 'code':422}

			except emptyStringError as ese:
				app.logger.info("One of the field is Empty - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.exception(ese)
				app.logger.error(ese.message)
				app.logger.info("End of ApiUpdateUpcomingInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_ONE_OF_EMPTY_FIELD', 'code':422}

			except ValueMissing as vm:
				app.logger.info("Received invalid input - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.error(vm.message)
				app.logger.info("End of ApiUpdateUpcomingInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_INVALID_INPUT', 'code':422}					
		
			except Exception as e:
				app.logger.debug("Error while parsing Interview Details")
				app.logger.exception(e.message)
				app.logger.info("End of ApiUpdateUpcomingInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': False, 'message': 'COMMON_ERROR_REQ_MORE_FIELD', 'code':422}
		elif user:
			app.logger.debug("User is not authorized to access ApiUpdateUpcomingInterview service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.info("End of ApiUpdateUpcomingInterview - " + str(clientip))
			return {'status': False,  'message': 'COMMON_UNAUTHORIZED', 'code':403}
		else:
			app.logger.info("Invalid token or Session expired - " + str(clientip))
			app.logger.info("End of ApiUpdateUpcomingInterview - " + str(clientip))
			return {'status': False, 'message': 'COMMON_INVALID_TOKEN', 'code':401}


#list of admins
class ApiUserList(RestResource):
	def get(self):
		app.logger.info("Accessing ApiUserList")
		clientip = request.remote_addr

		app.logger.debug("Check user token - " + str(clientip))

		try:
			status, user = user_auth(admin_access=C.SUPERADMIN)
		except Exception as e:
			app.logger.info("error while checking authentication - " + str(clientip))
			app.logger.exception(e)
			app.logger.info("end of ApiUserList - " + str(clientip))
			return {'status': False, 'message': 'COMMON_ERROR_IN_AUTHENTICATION', 'code':422}
		if status and user:
			app.logger.debug("User is authorized to access ApiUserList service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			try:
				if 'orgid' not in request.args:
					app.logger.debug("orgid not found - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					raise RequiredFieldsMissing()
				else:
					orgid = request.args['orgid']
					if orgid.isdigit():
						pass
						organization_details = Organization().query.get(orgid)
						if organization_details:
							app.logger.info("organization exist - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						else:
							app.logger.debug("orgid not in our records")
							app.logger.info("end of ApiUserList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							return {'status': False, 'message': 'APIUSERLIST_ORGID_NOT_EXIST', 'code':422}
					else:
						app.logger.debug("Invalid orgid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						return {'status': False, 'message': 'APICREATEUSER_ORGID_INVALID', 'code':406}
			except RequiredFieldsMissing as re:
				app.logger.info("Orgid missing - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.exception(re)
				app.logger.info("End of ApiUserList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': False, 'message': 'APIUSERLIST_ORGID_MISSING', 'code':422}

			query = "select * from users where role ="+str(C.ADMIN)+" and orgid = "+str(orgid) +";"
			app.logger.debug("final query")
			app.logger.debug(query)
			result = db.engine.execute(query)
			listOfUsers = UserBaseSchema(partial=('userid','orgid','role','email','firstname','lastname','mobile','status')).dump(result, many=True).data
			if len(listOfUsers) == 0 :
				app.logger.debug("no admins created for this "+organization_details.name+" organization - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.info("end of ApiUserList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': True, 'message': 'APIUSER_USER_NOT_EXIST', 'user_list':[], 'code':200, 'organization_name': organization_details.name}
			app.logger.info("end of ApiUserList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			return {'status': True, 'message': 'APIUSERLIST_FETCH_SUCCESS', 'user_list':listOfUsers, 'code':200,'organization_name': organization_details.name}

		elif user:
			app.logger.debug("User is not authorized to access ApiUserList service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.info("end of ApiUserList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			return {'status': False,  'message': 'COMMON_UNAUTHORIZED', 'code':403}
		else:
			app.logger.info("Invalid token or Session expired - " + str(clientip))
			app.logger.info("end of ApiUserList - " + str(clientip))
			return {'status': False, 'message': 'COMMON_INVALID_TOKEN', 'code':401}


class ApiValidateSuccessInterview(RestResource):
	def get(self,joining_link):
		app.logger.info("Accessing ApiValidateSuccessInterview")
		clientip = request.remote_addr
		app.logger.debug("Check user joining link - " + str(joining_link))
		interview_interviewer = Interview_Interviewer().query.filter(Interview_Interviewer.joinurl==joining_link).first()
		if interview_interviewer:
			interview = Interviews().query.filter(Interviews.interviewid==interview_interviewer.interviewid).first()
			app.logger.debug("Interviewid:" + str(interview_interviewer.interviewid) + " Interviewerid:" + str(interview_interviewer.interviewerid))
		else:
			app.logger.info("Invalid interviewid - " + str(clientip))
			app.logger.info("End of ApiValidateSuccessInterview - " + str(clientip))
			return {'status': False, 'message': 'APIVALIDATESUCCESSINTERVIEW_INVALID_FEEDBACK_URL', 'code':404}
		if interview is not None:
			interviewstatus = interview.interviewstatus
			feedback = InterviewFeedback().query.filter(and_(InterviewFeedback.interviewid==interview_interviewer.interviewid,InterviewFeedback.interviewer_id==interview_interviewer.interviewerid)).first()
			# feedback = interview.feedback
			logo = getOrgLogo(interview.orgid)
			if interviewstatus == C.INTERVIEW_BOOKED:
				app.logger.info("Interview didn't start - " + str(clientip))
				app.logger.info("End of ApiValidateSuccessInterview - " + str(clientip))
				return {'status': False, 'message': 'APIVALIDATESUCCESSINTERVIEW_INT_NOT_START','logo':logo, 'code':406}
			elif interviewstatus == C.INTERVIEW_OT_INTERVIEWER_JOINED or interviewstatus == C.INTERVIEW_OT_CANDIDATE_JOINED  or interviewstatus == C.INTERVIEW_INTERVIEWER_JOINED_AND_QUIT or  interviewstatus == C.INTERVIEW_CANDIDATE_JOINED_AND_QUIT:
				app.logger.info("Interview Inprogress - " + str(clientip))
				app.logger.info("End of ApiValidateSuccessInterview - " + str(clientip))
				return {'status': False, 'message': 'APIVALIDATESUCCESSINTERVIEW_INT_PROGRESS','logo':logo, 'code':406}
			elif interviewstatus == C.INTERVIEW_INTERVIEWER_ONLY_JOINED or interviewstatus == C.INTERVIEW_INTERVIEWER_TERMINATE:
				app.logger.info("Interviewer only joined - " + str(clientip))
				app.logger.info("End of ApiValidateSuccessInterview - " + str(clientip))
				return {'status': False, 'message': 'APIVALIDATESUCCESSINTERVIEW_INTERVIEWER_ONLY_JOINED','logo':logo, 'code':401}
			elif interviewstatus == C.INTERVIEW_CANDIDATE_ONLY_JOINED or interviewstatus == C.INTERVIEW_CANDIDATE_TERMINATE:
				app.logger.info("Candidate only joined - " + str(clientip))
				app.logger.info("End of ApiValidateSuccessInterview - " + str(clientip))
				return {'status': False, 'message': 'APIVALIDATESUCCESSINTERVIEW_CANDIDATE_ONLY_JOINED','logo':logo, 'code':401}
			elif interviewstatus == C.INTERVIEW_SUCCESSFUL or interviewstatus == C.INTERVIEW_IN_PROGRESS:
				tracking_data  = InterviewTracking().query.filter(and_(InterviewTracking.interviewid==interview_interviewer.interviewid,InterviewTracking.interviewerid==interview_interviewer.interviewerid)).first()
				if tracking_data is not None:
					if feedback is None:
						ftid = interview.ftid
						# app.logger.debug("ftid")
						# app.logger.debug(ftid)
						feedbackTemplate = FeedbackTemplate().query.filter(FeedbackTemplate.ftid==ftid).first()
						feedback_rating = feedbackTemplate.max_rating
						feedback_title = feedbackTemplate.title
						feedback_categories = feedbackTemplate.categories
						slot_id = interview.slot_id
						slottime =  slot.sd[slot_id]
						if slottime[1] == 0:
							min = '00'
						else:
							min = '30'
						slotTime = str(interview.date) + " " + str(slottime[0]) + ":" + min + " UTC"
						interviewerid = interview_interviewer.interviewerid
						interviewer = User().query.filter(User.userid==interviewerid).first()
						login_token = interviewer.generate_t()
						interview_candidate = Interview_Candidate().query.filter(Interview_Candidate.interviewid==interview.interviewid).first()
						candidateid = interview_candidate.candidateid
						candidate = User().query.filter(User.userid==candidateid).first()
						app.logger.info("Interview Successful - " + str(clientip))
						app.logger.info("End of ApiValidateSuccessInterview - " + str(clientip))
						return {'status': True, 'message': 'APIVALIDATESUCCESSINTERVIEW_INT_SUCCESS','slotTime':slotTime, 'candidateName': candidate.firstname,'interviewerName': interviewer.firstname,'login_token':login_token,'logo':logo,'feedback_rating':feedback_rating,'feedback_title':feedback_title,'feedback_categories':feedback_categories, 'code':200}
					else:
						app.logger.info("Feedback for this interview already stored - " + str(clientip))
						app.logger.info("End of ApiValidateSuccessInterview - " + str(clientip))
						return {'status': False, 'message': 'APIVALIDATESUCCESSINTERVIEW_INT_FEEDBACK_EXIST','logo':logo, 'code':401}
				else:
					app.logger.info("Interviewer didn't join Interview - " + str(clientip))
					app.logger.info("End of ApiValidateSuccessInterview - " + str(clientip))
					return {'status': False, 'message': 'APIVALIDATESUCCESSINTERVIEW_INT_DIDNT_JOIN','logo':logo, 'code':401}
			elif interviewstatus == C.INTERVIEW_FAILED:
				app.logger.info("Interview Failed - " + str(clientip))
				app.logger.info("End of ApiValidateSuccessInterview - " + str(clientip))
				return {'status': False, 'message': 'APIVALIDATESUCCESSINTERVIEW_INT_FAILED','logo':logo, 'code':401}
			elif interviewstatus == C.INTERVIEW_CANCELLED:
				app.logger.info("Interview Cancelled - " + str(clientip))
				app.logger.info("End of ApiValidateSuccessInterview - " + str(clientip))
				return {'status': False, 'message': 'APIVALIDATESUCCESSINTERVIEW_INT_CANCELLED','logo':logo, 'code':401}
		else:
			app.logger.info("Invalid interviewid - " + str(clientip))
			app.logger.info("End of ApiValidateSuccessInterview - " + str(clientip))
			return {'status': False, 'message': 'APIVALIDATESUCCESSINTERVIEW_INVALID_FEEDBACK_URL', 'code':404}

#Store Sucessful interview feedback
class ApiStoreInterviewFeedback(RestResource):
	def post(self,joining_link):
		app.logger.info("Accessing ApiStoreInterviewFeedback")
		clientip = request.remote_addr

		app.logger.info("Check joining link - " + str(joining_link))

		try:
			status, user = user_auth(admin_access=C.SUPERADMIN)
		except Exception as e:
			app.logger.info("error while checking authentication - " + str(clientip))
			app.logger.exception(e)
			app.logger.info("end of ApiUserList - " + str(clientip))
			return {'status': False, 'message': 'COMMON_ERROR_IN_AUTHENTICATION', 'code':422}

		interview_interviewer = Interview_Interviewer().query.filter(Interview_Interviewer.joinurl==joining_link).first()
		if interview_interviewer is not None:
			app.logger.info("Valid joining link - " + str(clientip) + "- [ " + str(interview_interviewer.interviewerid)+" : "+str(interview_interviewer.orgid)+" ]")
			result = Interviews().query.filter(Interviews.interviewid==interview_interviewer.interviewid).first()
			interviewstatus = result.interviewstatus;
			logo = getOrgLogo(result.orgid)
			app.logger.info("Interview status :" + str(interviewstatus))
			#check if interviewer joined or not
			if interviewstatus == C.INTERVIEW_IN_PROGRESS or interviewstatus == C.INTERVIEW_SUCCESSFUL:
				isJoined = InterviewTracking().query.filter(and_(InterviewTracking.interviewid == interview_interviewer.interviewid,InterviewTracking.interviewerid == interview_interviewer.interviewerid)).all()
				if isJoined is None:
					app.logger.info("Interviewer did not joined Interview - " + str(clientip) + "- [ " + str(interview_interviewer.interviewerid)+" : "+str(result.orgid)+" ]")
					app.logger.info("end of ApiStoreInterviewFeedback - " + str(clientip) + "- [ " + str(interview_interviewer.interviewerid)+" : "+str(result.orgid)+" ]")
					return {'status': False, 'message': 'APISTOREINTERVIEWFEEDBACK_FEEDBACK_DISABLED_NOT_JOIN', 'code':403}
				else:
					pass
				app.logger.info("Interview is in Inprogress or success state, So Feedback can be stored - " + str(clientip) + "- [ " + str(interview_interviewer.interviewerid)+" : "+str(result.orgid)+" ]")
				try:
					inputData = parser.parse(StoreInterviewFeedbackSchema(), request, locations=request_locations)
					feedback = json.loads(inputData['feedback'])
				except Exception as e:
					app.logger.info("Feedback is not a valid json - " + str(clientip) + "- [ " + str(interview_interviewer.interviewerid)+" : "+str(result.orgid)+" ]")
					app.logger.exception(e)
					app.logger.info("end of ApiStoreInterviewFeedback - " + str(clientip) + "- [ " + str(interview_interviewer.interviewerid)+" : "+str(result.orgid)+" ]")
					return {'status': False, 'message':'APISTOREINTERVIEWFEEDBACK_INVALID_JSON','logo':logo, 'code':422}

				insertFeedback = InterviewFeedback().query.filter(and_(InterviewFeedback.interviewid==interview_interviewer.interviewid,InterviewFeedback.interviewer_id==interview_interviewer.interviewerid)).first()
				if insertFeedback:
					return {'status': False, 'message': 'APIVALIDATESUCCESSINTERVIEW_INT_FEEDBACK_EXIST', 'code':422}
				else:
					insertFeedback = InterviewFeedback()
					insertFeedback.created_time = datetime.utcnow()
				insertFeedback.interviewid = interview_interviewer.interviewid
				insertFeedback.interviewer_id = interview_interviewer.interviewerid
				insertFeedback.rating = inputData['rating']
				insertFeedback.isprimary = interview_interviewer.isprimary
				insertFeedback.feedback = json.dumps(feedback)
				insertFeedback.orgid = interview_interviewer.orgid
				insertFeedback.updated_time = datetime.utcnow()
				interview_interviewer.feedback_available = True
				db.session.add(insertFeedback)
				db.session.commit()
				app.logger.info("Feedback update success - " + str(clientip) + "- [ " + str(interview_interviewer.interviewerid)+" : "+str(result.orgid)+" ]")
				app.logger.info("end of ApiStoreInterviewFeedback - " + str(clientip) + "- [ " + str(interview_interviewer.interviewerid)+" : "+str(result.orgid)+" ]")
				return {'status': True, 'message': 'APIVALIDATESUCCESSINTERVIEW_FEEDBACK_UPDATED','logo':logo, 'code':201}
			else:
				app.logger.info("Interview is not in Inprogress or success state, so Feedback can not be stored - " + str(clientip) + "- [ " + str(interview_interviewer.interviewerid)+" : "+str(result.orgid)+" ]")
				app.logger.info("end of ApiStoreInterviewFeedback - " + str(clientip) + "- [ " + str(interview_interviewer.interviewerid)+" : "+str(result.orgid)+" ]")
				return{'status': False, 'message': 'APISTOREINTERVIEWFEEDBACK_FEEDBACK_DISABLED', 'code':403}
		else:
			app.logger.info("Invalid joining link - " + str(joining_link))
			app.logger.info("end of ApiStoreInterviewFeedback")
			return {'status': False, 'message': 'APIVALIDATESUCCESSINTERVIEW_INVALID_JOINING_URL', 'code':403}
	def put(self,joining_link):
		app.logger.info("Accessing ApiStoreInterviewFeedback - Update")
		clientip = request.remote_addr

		app.logger.info("Check joining link - " + str(joining_link))

		try:
			status, user = user_auth(admin_access=C.SUPERADMIN)
		except Exception as e:
			app.logger.info("error while checking authentication - " + str(clientip))
			app.logger.exception(e)
			app.logger.info("end of ApiUserList - " + str(clientip))
			return {'status': False, 'message': 'COMMON_ERROR_IN_AUTHENTICATION', 'code':422}

		interview_interviewer = Interview_Interviewer().query.filter(Interview_Interviewer.joinurl==joining_link).first()
		if interview_interviewer is not None:
			app.logger.info("Valid joining link - " + str(clientip) + "- [ " + str(interview_interviewer.interviewerid)+" : "+str(interview_interviewer.orgid)+" ]")
			result = Interviews().query.filter(Interviews.interviewid==interview_interviewer.interviewid).first()
			interviewstatus = result.interviewstatus;
			logo = getOrgLogo(result.orgid)
			app.logger.info("Interview status :" + str(interviewstatus))
			#check if interviewer joined or not
			if interviewstatus == C.INTERVIEW_IN_PROGRESS or interviewstatus == C.INTERVIEW_SUCCESSFUL:
				isJoined = InterviewTracking().query.filter(and_(InterviewTracking.interviewid == interview_interviewer.interviewid,InterviewTracking.interviewerid == interview_interviewer.interviewerid)).all()
				if isJoined is None:
					app.logger.info("Interviewer did not joined Interview - " + str(clientip) + "- [ " + str(interview_interviewer.interviewerid)+" : "+str(result.orgid)+" ]")
					app.logger.info("end of ApiStoreInterviewFeedback - " + str(clientip) + "- [ " + str(interview_interviewer.interviewerid)+" : "+str(result.orgid)+" ]")
					return {'status': False, 'message': 'APISTOREINTERVIEWFEEDBACK_FEEDBACK_DISABLED_NOT_JOIN', 'code':403}
				else:
					pass
				app.logger.info("Interview is in Inprogress or success state, So Feedback can be stored - " + str(clientip) + "- [ " + str(interview_interviewer.interviewerid)+" : "+str(result.orgid)+" ]")
				try:
					inputData = parser.parse(StoreInterviewFeedbackSchema(), request, locations=request_locations)
					feedback = json.loads(inputData['feedback'])
				except Exception as e:
					app.logger.info("Feedback is not a valid json - " + str(clientip) + "- [ " + str(interview_interviewer.interviewerid)+" : "+str(result.orgid)+" ]")
					app.logger.exception(e)
					app.logger.info("end of ApiStoreInterviewFeedback - " + str(clientip) + "- [ " + str(interview_interviewer.interviewerid)+" : "+str(result.orgid)+" ]")
					return {'status': False, 'message':'APISTOREINTERVIEWFEEDBACK_INVALID_JSON','logo':logo, 'code':422}
				try:
					interviewFeedback = InterviewFeedback().query.filter(and_(InterviewFeedback.interviewid==interview_interviewer.interviewid,InterviewFeedback.interviewer_id==interview_interviewer.interviewerid)).first()
					if interviewFeedback:
						app.logger.info("Interviewe Feedback already exist - " + str(clientip))
						app.logger.info("old feedback")
						old_feedback = interviewFeedback.feedback
						app.logger.info("new feedback update to existing one")
						interviewFeedback.feedback = json.dumps(feedback)
						app.logger.info("update database")
						db.session.commit()
						# app.logger.info("end of ApiStoreInterviewFeedback - " + str(clientip) + "- [ " + str(interview_interviewer.interviewerid)+" : "+str(result.orgid)+" ]")
						# return {'status': False, 'message': 'APIVALIDATESUCCESSINTERVIEW_INT_FEEDBACK_EXIST', 'code':401}
					else:
						app.logger.info("feedback not existed")
				except Exception as e:
					app.logger.info("error while processing feedback")
					app.logger.info(e)
				app.logger.info("Feedback update success - " + str(clientip) + "- [ " + str(interview_interviewer.interviewerid)+" : "+str(result.orgid)+" ]")
				app.logger.info("end of ApiStoreInterviewFeedback - " + str(clientip) + "- [ " + str(interview_interviewer.interviewerid)+" : "+str(result.orgid)+" ]")
				return {'status': True, 'message': 'APIVALIDATESUCCESSINTERVIEW_FEEDBACK_UPDATED','logo':logo, 'code':201}
			else:
				app.logger.info("Interview is not in Inprogress or success state, so Feedback can not be stored - " + str(clientip) + "- [ " + str(interview_interviewer.interviewerid)+" : "+str(result.orgid)+" ]")
				app.logger.info("end of ApiStoreInterviewFeedback - " + str(clientip) + "- [ " + str(interview_interviewer.interviewerid)+" : "+str(result.orgid)+" ]")
				return{'status': False, 'message': 'APISTOREINTERVIEWFEEDBACK_FEEDBACK_DISABLED', 'code':403}
		else:
			app.logger.info("Invalid joining link - " + str(joining_link))
			app.logger.info("end of ApiStoreInterviewFeedback - Update")
			return {'status': False, 'message': 'APIVALIDATESUCCESSINTERVIEW_INVALID_JOINING_URL', 'code':403}


#Interview Join using joining link
class ApiValidateJoiningLink(RestResource):
	def get(self,joining_link):
		app.logger.info("Accessing ApiValidateJoiningLink")
		clientip = request.remote_addr
		app.logger.info("Check joining link - " + str(joining_link))	

		result_interviewer = Interview_Interviewer().query.filter(Interview_Interviewer.joinurl == joining_link).first()
		result_candidate = Interview_Candidate().query.filter(Interview_Candidate.joinurl == joining_link).first()
		# result = db.session.query(Interview_Interviewer).join(Interview_Candidate).filter(or_(Interview_Interviewer.joinurl == joining_link,Interview_Candidate.joinurl == joining_link)).first()
		app.logger.info(result_interviewer)
		app.logger.info(result_candidate)
		if result_interviewer is not None or result_candidate is not None:
			
			if result_interviewer is not None:
				if result_interviewer.joinurl == joining_link:
					result = result_interviewer
					role = 	C.INTERVIEWER
					userid = result_interviewer.interviewerid
			else:
				if result_candidate.joinurl == joining_link:
					result = result_candidate
					role = C.INTERVIEWEE
					userid = result_candidate.candidateid
			logo = getOrgLogo(result.orgid)

			app.logger.info("Valid joining link - " + str(clientip) + "- [ " + str(userid)+" : "+str(result.orgid)+" ]")
			interview = Interviews().query.get(result.interviewid) 
			if interview.interviewstatus==C.INTERVIEW_SUCCESSFUL :
				app.logger.info("Interview Live: Interview: "+str(result.interviewid)+" Interview completed - " + str(clientip) + "- [ " + str(userid)+" : "+str(result.orgid)+" ]")
				app.logger.info("End of ApiValidateJoiningLink - " + str(clientip) + "- [ " + str(userid)+" : "+str(result.orgid)+" ]")
				return {'status': False, 'message': 'APIINTERVIEWAUTH_INT_COMPLETED','logo':logo, 'code':410}
			elif interview.interviewstatus == C.INTERVIEW_FAILED or interview.interviewstatus == C.INTERVIEW_CANCELLED:
				app.logger.info("Interview Live: Interview: "+str(result.interviewid)+" Interview Cancelled - " + str(clientip) + "- [ " + str(userid)+" : "+str(result.orgid)+" ]")
				app.logger.info("End of ApiValidateJoiningLink - " + str(clientip) + "- [ " + str(userid)+" : "+str(result.orgid)+" ]")
				return {'status': False, 'message': 'APIINTERVIEWAUTH_INT_CANCELLED','logo':logo, 'code':410}
			app.logger.info("Check date and time - " + str(clientip) + "- [ " + str(userid)+" : "+str(result.orgid)+" ]")
			# interview = Interviews().query.get(result.interviewid)
			slot_id = interview.slot_id
			diffTime = slot.get_now_delta_seconds(interview.date,slot_id)
			duration = interview.duration * 60
			if diffTime < 10:
				#success casse
				if 10 >= diffTime >= (duration * -1):
					userIs = user_details = User().query.get(userid)
					app.logger.info("generat token  - " + str(clientip))
					login_token = userIs.generate_t()
					remaining_seconds = duration - -1*diffTime
					app.logger.info("end of ApiValidateJoiningLink - " + str(clientip) + "- [ " + str(userid)+" : "+str(result.orgid)+" ]")
					return  {'status': True, 'name':userIs.firstname, 'role': role,'email':userIs.email,'duration':interview.duration, 'remaining_seconds':remaining_seconds, 'login_token':login_token, 'interviewid': interview.interviewid,'logo':logo,'code':200}
				#expiry case
				else:
					app.logger.info("Interview expired - " + str(clientip) + "- [ " + str(userid)+" : "+str(result.orgid)+" ]")
					app.logger.info("end of ApiValidateJoiningLink - " + str(clientip) + "- [ " + str(userid)+" : "+str(result.orgid)+" ]")
					return {'status': False, 'message': 'APIVALIDATEJOININGLINK_INT_EXPIRED','logo':logo, 'code':410}
			#start later case
			else:
				slottime =  slot.sd[slot_id]
				if slottime[1] == 0:
					min = '00'
				else:
					min = '30'
				slotTime = str(interview.date) + " " + str(slottime[0]) + ":" + min
				# message = "APIVALIDATEJOININGLINK_ERROR_PART1" + slotTime +"APIVALIDATEJOININGLINK_ERROR_PART2"
				message = "Interview will start at " + slotTime +" UTC"
				app.logger.info(message + " - " + str(clientip) + "- [ " + str(userid)+" : "+str(result.orgid)+" ]")
				app.logger.info("end of ApiValidateJoiningLink - " + str(clientip) + "- [ " + str(userid)+" : "+str(result.orgid)+" ]")
				return  {'status': False, 'message': message,'logo':logo, 'code':406,'messageFlag':True}
			
		else:
			app.logger.info("Invalid joining link - " + str(joining_link))
			app.logger.info("end of ApiValidateJoiningLink - " + str(clientip))
			return {'status': False, 'message': 'APIVALIDATEJOININGLINK_INT_EXPIRED', 'code':404}
			
# class ApiValidateJoiningLink(RestResource):
# 	def get(self,joining_link):
# 		app.logger.info("Accessing ApiValidateJoiningLink")
# 		clientip = request.remote_addr
# 		app.logger.info("Check joining link - " + str(clientip))	

# 		result = Interviews().query.filter(or_(Interviews.interviewer_joinurl==joining_link, Interviews.candidate_joinurl==joining_link)).first()
# 		if result is not None:
# 			logo = getOrgLogo(result.orgid)
# 			if result.interviewer_joinurl == joining_link:
# 				role = 	C.INTERVIEWER
# 				userid = result.interviewerid
# 			else:
# 				role = C.INTERVIEWEE
# 				userid = result.candidateid
# 			app.logger.info("Valid joining link - " + str(clientip) + "- [ " + str(userid)+" : "+str(result.orgid)+" ]")
# 			app.logger.info("Check date and time - " + str(clientip) + "- [ " + str(userid)+" : "+str(result.orgid)+" ]")
# 			slot_id = result.slot_id
# 			diffTime = slot.get_now_delta_seconds(result.date,slot_id)
# 			duration = result.duration * 60
# 			if diffTime < 10:
# 				#success casse
# 				if 10 >= diffTime >= (duration * -1):
# 					userIs = user_details = User().query.get(userid)
# 					app.logger.info("generat token  - " + str(clientip))
# 					login_token = userIs.generate_t()
# 					remaining_seconds = duration - -1*diffTime
# 					app.logger.info("end of ApiValidateJoiningLink - " + str(clientip) + "- [ " + str(userid)+" : "+str(result.orgid)+" ]")
# 					return  {'status': True, 'name':userIs.firstname, 'role': role, 'duration':result.duration, 'remaining_seconds':remaining_seconds, 'login_token':login_token, 'interviewid': result.interviewid,'logo':logo,  'code':200}
# 				#expiry case
# 				else:
# 					app.logger.info("Interview expired - " + str(clientip) + "- [ " + str(userid)+" : "+str(result.orgid)+" ]")
# 					app.logger.info("end of ApiValidateJoiningLink - " + str(clientip) + "- [ " + str(userid)+" : "+str(result.orgid)+" ]")
# 					return {'status': False, 'message': 'APIVALIDATEJOININGLINK_INT_EXPIRED','logo':logo, 'code':410}
# 			#start later case
# 			else:
# 				slottime =  slot.sd[slot_id]
# 				if slottime[1] == 0:
# 					min = '00'
# 				else:
# 					min = '30'
# 				slotTime = str(result.date) + " " + str(slottime[0]) + ":" + min
# 				# message = "APIVALIDATEJOININGLINK_ERROR_PART1" + slotTime +"APIVALIDATEJOININGLINK_ERROR_PART2"
# 				message = "Interview will start at " + slotTime +" UTC"
# 				app.logger.info(message + " - " + str(clientip) + "- [ " + str(userid)+" : "+str(result.orgid)+" ]")
# 				app.logger.info("end of ApiValidateJoiningLink - " + str(clientip) + "- [ " + str(userid)+" : "+str(result.orgid)+" ]")
# 				return  {'status': False, 'message': message,'logo':logo, 'code':406,'messageFlag':True}
			
# 		else:
# 			app.logger.info("Invalid joining link - " + str(clientip))
# 			app.logger.info("end of ApiValidateJoiningLink - " + str(clientip))
# 			return {'status': False, 'message': 'APIVALIDATEJOININGLINK_INT_EXPIRED', 'code':404}
			
			

# authorize user for interview
class ApiInterviewAuth(RestResource):
	def post(self, interviewid):
		clientip = request.remote_addr
		app.logger.info("Accessing ApiInterviewAuth - "+str(clientip))
		status,auth_dict = user_auth(admin_access=C.NORMAL, return_requestor=True)
		app.logger.info("status")
		app.logger.info(status)
		app.logger.info("auth_dict")
		app.logger.info(auth_dict)
		
		if status:
			app.logger.info("user exist")
		else:
			app.logger.info("user not exist")
			return {'status': False, 'message': 'unauthorized', 'code':403}
		requestor_class = auth_dict
		user = requestor_class

		interview = Interviews.query.get(interviewid)
		
		app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" wants to start Interview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
		if not interview:
			app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Interview not found - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.info("End of ApiInterviewAuth - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			return {'status': False, 'message': 'COMMON_INTERVIEW_NOT_FOUND', 'code':404}

		logo = getOrgLogo(interview.orgid)

		elapsed_seconds = -slot.get_now_delta_seconds(interview.date, interview.slot_id)
		if elapsed_seconds < 0 :
			app.logger.info("Elapsed seconds < 0 "+str(elapsed_seconds)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Interview time not yet started - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.info("End of ApiInterviewAuth - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			return {'status': False, 'message': 'APIINTERVIEWAUTH_INT_NOT_YET_START','logo':logo, 'code':406}
			
		duration = interview.duration * 60

		if elapsed_seconds > duration:
			app.logger.debug("Elapsed seconds > duration "+str(elapsed_seconds)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Interview time over. So can't start the Interview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.info("End of ApiInterviewAuth - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			return {'status': False, 'message': 'APIINTERVIEWAUTH_INT_EXPIRED','logo':logo, 'code':410}
		
		app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Current Interview Status: "+str(interview.interviewstatus)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
		
		if interview.interviewstatus==C.INTERVIEW_CANDIDATE_TERMINATE :
			app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Candidate joined and exit from interview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.info("End of ApiInterviewAuth - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			return {'status': False, 'message': 'APIINTERVIEWAUTH_CANDIDATE_INT_CLOSED','logo':logo, 'code':410}
		elif interview.interviewstatus == C.INTERVIEW_INTERVIEWER_TERMINATE:
			app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Interviewer joined and exit from interview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.info("End of ApiInterviewAuth - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			return {'status': False, 'message': 'APIINTERVIEWAUTH_INTERVIEWER_INT_CLOSED','logo':logo, 'code':410}
		elif interview.interviewstatus==C.INTERVIEW_SUCCESSFUL :
			app.logger.info("Interview Live: Interview: "+str(interviewid)+" Interview completed - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.info("End of ApiInterviewAuth - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			return {'status': False, 'message': 'APIINTERVIEWAUTH_INT_COMPLETED','logo':logo, 'code':410}
		elif interview.interviewstatus == C.INTERVIEW_FAILED or interview.interviewstatus == C.INTERVIEW_CANCELLED:
			app.logger.info("Interview Live: Interview: "+str(interviewid)+" Interview Cancelled - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.info("End of ApiInterviewAuth - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			return {'status': False, 'message': 'APIINTERVIEWAUTH_INT_CANCELLED','logo':logo, 'code':410}
		
		session_is_booked = False
		generate_ot = False
		both_joined = False
		
		# Creating interview tracking object for join
		interview_tracking = InterviewTracking()
		interview_tracking.interviewid = interviewid
		interview_tracking.orgid =  interview.orgid
		interview_tracking.activity = C.INTERVIEW_TRACKING_JOIN

		if interview.interviewstatus == C.INTERVIEW_BOOKED:
			app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Interview Status is Booked. So Create new OpenTok Session ID - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			session_is_booked = True
			generate_ot = True
		if interview.sessionid is None:
			app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Interview Status is Booked. So Create new OpenTok Session ID - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			session_is_booked = True
			generate_ot = True
		interview_interviewer = None
		if user_is_interviewer(requestor_class):
			app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" User is Interviewer - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			interview_interviewer = Interview_Interviewer().query.filter(and_(Interview_Interviewer.interviewid == interview.interviewid,Interview_Interviewer.interviewerid == user.userid, Interview_Interviewer.active_status== True )).first()
			
			if interview_interviewer:
				if interview_interviewer.interviewerid != user.userid:
					app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Oh..He/She not correct Interviewer to join Interview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.info("End of ApiInterviewAuth - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {'status': False, 'message': 'APIINTERVIEWAUTH_UNAUTHORIZED', 'code':403}
			else:
				app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Oh..He/She not correct Interviewer to join Interview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.info("End of ApiInterviewAuth - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': False, 'message': 'APIINTERVIEWAUTH_UNAUTHORIZED', 'code':403}

			interview_tracking.activitytime = datetime.utcnow()
			interview_tracking.interviewerid = interview_interviewer.interviewerid
			
			if session_is_booked:
				app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Interviewer Joining first - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Change current Interview status to Interviewer Joined - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				interview.interviewstatus = C.INTERVIEW_OT_INTERVIEWER_JOINED
				# interview.interviewer_join_time = datetime.utcnow()
				
				app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Interviewer Joining Time is "+str(interview.interviewer_join_time)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")				
			elif interview.interviewstatus == C.INTERVIEW_OT_CANDIDATE_JOINED:
				app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Candidate already Joined. Now Interviewer Jointing to Interview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Change current Interview status to Interviewer Joined - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				# interview.interviewer_join_time = datetime.utcnow()
				app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Interviewer Joining Time "+str(interview.interviewer_join_time)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Update Both Joined True in Interviewer joing block - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				both_joined = True
			elif interview.interviewstatus == C.INTERVIEW_IN_PROGRESS:
				app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Both are joined and Interviewer again join to Interview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				# interview_tracking.activitytime = datetime.utcnow()
				# interview_tracking.interviewerid = interview_interviewer.interviewerid
			# elif interview.interviewstatus == C.INTERVIEW_OT_INTERVIEWER_JOINED:
			# 	app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" One of the Interviewer already joined . Now,Other interview joined to Interview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				# interview_tracking.activitytime = datetime.utcnow()
				# interview_tracking.interviewerid = interview_interviewer.interviewerid
		
		elif user_is_candidate(requestor_class):
			app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" User is Candidate - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			interview_candidate = Interview_Candidate().query.filter(and_(Interview_Candidate.interviewid == interview.interviewid,Interview_Candidate.active_status== True )).first()
			if interview_candidate:
				if interview_candidate.candidateid != user.userid:
					app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Oh..He/She not correct Candidate to join Interview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.info("End of ApiInterviewAuth - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {'status': False, 'message': 'APIINTERVIEWAUTH_UNAUTHORIZED', 'code':403}
			else:
				app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Oh..He/She not correct Candidate to join Interview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.info("End of ApiInterviewAuth - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': False, 'message': 'APIINTERVIEWAUTH_UNAUTHORIZED', 'code':403}
			interview_tracking.activitytime = datetime.utcnow()
			interview_tracking.candidateid = interview_candidate.candidateid
			if session_is_booked:
				app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Candidate Joining first - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Change current Interview status to Candidate Joined - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				interview.interviewstatus = C.INTERVIEW_OT_CANDIDATE_JOINED
				# interview.candidate_join_time = datetime.utcnow()
				
				
				app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Candidate Joining Time "+str(interview.candidate_join_time)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				
			elif interview.interviewstatus == C.INTERVIEW_OT_INTERVIEWER_JOINED:
				app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Interviewer already Joined. Now Candidate Jointing to Interview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Change current Interview status to Candidate Joined - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				
				# interview.candidate_join_time = datetime.utcnow()
				
				app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Candidate Joining Time "+str(interview.candidate_join_time)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				
				app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Update Both Joined True in Candidate joing block - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				both_joined = True
			elif interview.interviewstatus == C.INTERVIEW_IN_PROGRESS:
				app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Both are joined and candidate again join to Interview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				
		else:
			app.logger.info("Unauthorized to access - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.info("End of ApiInterviewAuth - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			return {'status': False, 'message': 'APIINTERVIEWAUTH_UNAUTHORIZED', 'code':403}


		if both_joined:
			app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Change status to Interview in Progress - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")			
			interview.interviewstatus = C.INTERVIEW_IN_PROGRESS
		if generate_ot:
			app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Create new OpenTok room for Interview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.info("create interview room with recording "+str(interview.prerecording_enabled))
			
			try:
				app.logger.info(interview.prerecording_enabled)
				ot_id_new = ot.create_session(interview.prerecording_enabled)
			except Exception as e:
				app.logger.info("Interview Live: Interview: "+str(interviewid)+" Error while creating Opentok room - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.info(e)
				app.logger.info("End of ApiInterviewAuth - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': False, 'message': 'APIINTERVIEWAUTH_UNAUTHORIZED_ERROR', 'code':422}
			
			
			if not ot_id_new:
				app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Oops.. OpenTok send Error. Cannot Create Room for Interview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.info("End of ApiInterviewAuth - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': False, 'message': 'APIINTERVIEWAUTH_UNAUTHORIZED_ERROR', 'code':422}
			
			app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Update Opentok Session ID to interview table - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			interview.sessionid = ot_id_new
			app.logger.info("Interview sessionid")
			app.logger.info(ot_id_new)
		
		app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Commit all changes to DB - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
		app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Now Generate token to join Interview room - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
		
		oei = duration - elapsed_seconds
		ot_id = interview.sessionid
		ot_key = app.config['OT_API_KEY']
		app.logger.info("ot_key")
		app.logger.info(ot_key)
		app.logger.info("ot_id")
		app.logger.info(ot_id)

		try:
			ot_token = ot.create_token(ot_id, role='p', ot_expires_in=oei)
			app.logger.info("ot_token")
			app.logger.info(ot_token)

		except Exception as e:
			app.logger.info("Interview Live: Interview: "+str(interviewid)+" Error while creating token for Opentok session - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.info(e)
			app.logger.info("End of ApiInterviewAuth - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			return {'status': False, 'message': 'APIINTERVIEWAUTH_UNAUTHORIZED_ERROR', 'code':422}
		
		try:
			app.logger.info("save all details to DB - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			db.session.add(interview_tracking)

			if interview_interviewer:
				app.logger.info("interview_interviewer exist. Set is_joined as True")
				interview_interviewer.is_joined = True
			db.session.commit()
		except Exception as te:
			app.logger.info("Error while saving details to DB - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.info(te)
			app.logger.info("End of ApiInterviewAuth - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			db.session.rollback()
			return {'status': False, 'message': te.message , 'code':422}

		app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Send OpenTok values to Client(Browser) - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
		

		app.logger.info("Share interview information to user")

		# interview_data = interview_serialize()
		archiveid = None
		if interview.archiveid is not None:
			archiveid = interview.archiveid

		interviewtracking_started = InterviewTracking().query.filter(and_(InterviewTracking.interviewid == interviewid,InterviewTracking.activity == C.INTERVIEW_TRACKING_RECORDING_STARTED)).first()
		interviewtracking_end = InterviewTracking().query.filter(and_(InterviewTracking.interviewid == interviewid,InterviewTracking.activity == C.INTERVIEW_TRACKING_RECORDING_STOPPED)).first()
		
		
		if interviewtracking_started:
			isStartRecording = True
		else:
			isStartRecording = False
		
		if interviewtracking_end:
			isStopRecording = True
		else:
			isStopRecording = False


		app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" End of ApiInterviewAuth - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
		return {'status': True, 'message': 'APIINTERVIEWAUTH_INT_SUCCESS', 'code':200, 'ot_key': ot_key, 'ot_id': ot_id, 'ot_token': ot_token, 'interviewid': interviewid, 'remaining_time':oei,'archiveid':archiveid,'isStartRecording':isStartRecording,'isStopRecording':isStopRecording,'interviewDescription':interview.interviewdescription,'prerecording_enabled':interview.prerecording_enabled}




def interview_serialize(o,rc=C.INTERVIEWEE, lst=False):
	app.logger.info("Accessing session_serialize()")
	if user_is_super_admin(rc) or user_is_super_admin(rc):
		app.logger.info("End of session_serialize() for admin")
		return InterviewAdminSchema().dump(o, many=lst).data
	elif user_is_interviewer(rc):
		app.logger.info("End of session_serialize() for interviewer")
		return InterviewInterviewerSchema().dump(o, many=lst).data
	elif user_is_candidate(rc):
		app.logger.info("End of session_serialize() for other roles")
		return InterviewCandidateSchema().dump(o, many=lst).data
	
	
# To change the status when Interview is running as Interview in Progress. And also change when users(candidate or interviewer) quit from the interview	
#when both are quit then it become successful
class ApiInterviewStatusUpdate(RestResource):
	def get(self,input):
		clientip = request.remote_addr
		app.logger.info("Accessing ApiInterviewStatusUpdate - "+str(clientip))
		app.logger.info("This function invokes when Candidate/Interviewer end the interview (Forcfully or Time out)")
		status,auth_dict = user_auth(admin_access=C.ALLOW, return_requestor=True)
		requestor_class = auth_dict
		user = requestor_class
		values=input.split(',')
		interviewid=values[0]
		incommingStatus=int(values[1])
		
		if not interviewid:
			app.logger.info("Interview Live: Interview: "+str(interviewid)+" interviewid not found - "+str(clientip)+" - [ "+str(user.userid)+" ] ")
			return {'status': False, 'message': 'APIINTERVIEWSTATUSUPDATE_INTID_MISSING' , 'code':422}
		if not incommingStatus:
			app.logger.info("Interview Live: Interview: "+str(interviewid)+" Client Status not found - "+str(clientip)+" - [ "+str(user.userid)+" ] ")
			return {'status': False, 'message': 'APIINTERVIEWSTATUSUPDATE_STATUS_MISSING' , 'code':422}

		interview = Interviews.query.get(interviewid)
		status=int(interview.interviewstatus)
		
		# Creating interview tracking object
		interview_tracking = InterviewTracking()
		interview_tracking.interviewid = interviewid
		interview_tracking.orgid =  interview.orgid
		interview_tracking.activity = C.INTERVIEW_TRACKING_END
		interview_tracking.activitytime =  datetime.utcnow()

		if user.role == C.INTERVIEWEE:
			interview_tracking.candidateid = user.userid
		elif user.role == C.INTERVIEWER:
			interview_tracking.interviewerid =  user.userid
		
		db.session.add(interview_tracking)
		db.session.commit()
				
		if (incommingStatus == C.INTERVIEW_INTERVIEWER_TERMINATE):
			user = User.query.get(user.userid)
			app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Interviewer wants to Quit Interview - "+str(clientip)+" - [ "+str(user.userid)+" ] ")
		if (incommingStatus == C.INTERVIEW_CANDIDATE_TERMINATE):
			user = User.query.get(user.userid)
			app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Candidate wants to Quit Interview - "+str(clientip)+" - [ "+str(user.userid)+" ] ")
		
		app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" input Status is "+str(incommingStatus)+" - "+str(clientip)+" - [ "+str(user.userid)+" ] ")
		
		app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" find Interview from DB - "+str(clientip)+" - [ "+str(user.userid)+" ] ")
		
		
		app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Current Status of Interview "+str(status)+" - "+str(clientip)+" - [ "+str(user.userid)+" ] ")
		
		if(status >= C.INTERVIEW_IN_PROGRESS):
			# if(status > C.INTERVIEW_IN_PROGRESS):
			# 	app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Already one of user quit and now second user wants to Quit - "+str(clientip)+" - [ "+str(user.userid)+" ] ")
				
			# 	if status == C.INTERVIEW_INTERVIEWER_JOINED_AND_QUIT and interview.interviewstatus == C.INTERVIEW_INTERVIEWER_JOINED_AND_QUIT:
			# 		app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Interviewer already Quit : Other interviewer wants to Quit - "+str(clientip)+" - [ "+str(user.userid)+" ] ")
					
			# 		app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Update Candidate close time - "+str(clientip)+" - [ "+str(user.userid)+" ] ")
					
			# 	elif status == C.INTERVIEW_CANDIDATE_JOINED_AND_QUIT and interview.interviewstatus == C.INTERVIEW_CANDIDATE_JOINED_AND_QUIT:
			# 		app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Candidate already Quit : Other Candidate wants to Quit - "+str(clientip)+" - [ "+str(user.userid)+" ] ")
					
			# 		app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Update Interviewer close time - "+str(clientip)+" - [ "+str(user.userid)+" ] ")
				
			# 	elif status == C.INTERVIEW_CANDIDATE_JOINED_AND_QUIT and interview.interviewstatus != C.INTERVIEW_INTERVIEWER_JOINED_AND_QUIT:
			# 		app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Candidate already Quit : Interviewer wants to Quit - "+str(clientip)+" - [ "+str(user.userid)+" ] ")
					
			# 		app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Update Interviewer close time - "+str(clientip)+" - [ "+str(user.userid)+" ] ")
			# 		interview.interviewstatus = status
			# 		db.session.commit()

			# 	elif status == C.INTERVIEW_CANDIDATE_JOINED_AND_QUIT and interview.interviewstatus != C.INTERVIEW_INTERVIEWER_JOINED_AND_QUIT:
			# 		app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Candidate already Quit : Interviewer wants to Quit - "+str(clientip)+" - [ "+str(user.userid)+" ] ")
					
			# 		app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Update Interviewer close time - "+str(clientip)+" - [ "+str(user.userid)+" ] ")
			# 		interview.interviewstatus = status
			# 		db.session.commit()
				
			# 	app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Update Interview status to Success and commit into DB")
				
				
			# 	app.logger.info("End of ApiInterviewStatusUpdate - "+str(clientip)+" - [ "+str(user.userid)+" ] ")
			# 	return {'status': True, 'message': 'APIINTERVIEWSTATUSUPDATE_SUCCESS' , 'code':200}
				
			# else:
			app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" One of User wants to exit - "+str(clientip)+" - [ "+str(user.userid)+" ] ")
			status = incommingStatus
			if status == C.INTERVIEW_INTERVIEWER_TERMINATE:
				app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" change Status to Interviewer Joined and Quit - "+str(clientip)+" - [ "+str(user.userid)+" ] ")
				status = C.INTERVIEW_INTERVIEWER_JOINED_AND_QUIT
				app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Update Interviewer Quit time - "+str(clientip)+" - [ "+str(user.userid)+" ] ")
				app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Time : "+str(interview.interviewer_end_time)+" - "+str(clientip)+" - [ "+str(user.userid)+" ] ")
				
			elif status == C.INTERVIEW_CANDIDATE_TERMINATE:
				app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" change Status to Candidate Joined and Quit - "+str(clientip)+" - [ "+str(user.userid)+" ] ")
				status = C.INTERVIEW_CANDIDATE_JOINED_AND_QUIT
				
				app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Update Candidate Quit time - "+str(clientip)+" - [ "+str(user.userid)+" ] ")
				app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Time : "+str(interview.candidate_end_time)+" - "+str(clientip)+" - [ "+str(user.userid)+" ] ")
				
			# interview.interviewstatus = status
			app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Update Status into DB - "+str(clientip)+" - [ "+str(user.userid)+" ] ")
			db.session.commit()
			app.logger.info("End of ApiInterviewStatusUpdate - "+str(clientip)+" - [ "+str(user.userid)+" ] ")
			return {'status': True, 'message': 'APIINTERVIEWSTATUSUPDATE_SUCCESS' , 'code':200}
		elif any([status == C.INTERVIEW_OT_INTERVIEWER_JOINED , status == C.INTERVIEW_OT_CANDIDATE_JOINED]):
			app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Only one User Join the Interview and wants to Quit - "+str(clientip)+" - [ "+str(user.userid)+" ] ")
			
			if incommingStatus == C.INTERVIEW_INTERVIEWER_TERMINATE:
				app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Only Interviewer Joined and wants to Quit Interview - "+str(clientip)+" - [ "+str(user.userid)+" ] ")
			elif incommingStatus == C.INTERVIEW_CANDIDATE_TERMINATE:
				app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Only Candidate Joined and wants to Quit Interview - "+str(clientip)+" - [ "+str(user.userid)+" ] ")
			app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Update Interview status to "+str(incommingStatus)+" - "+str(clientip)+" - [ "+str(user.userid)+" ] ")
			# interview.interviewstatus = incommingStatus
			
			app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Update DB - "+str(clientip)+" - [ "+str(user.userid)+" ] ")
			db.session.commit()
			app.logger.info("End of ApiInterviewStatusUpdate - "+str(clientip)+" - [ "+str(user.userid)+" ] ")
			return {'status': True, 'message': 'APIINTERVIEWSTATUSUPDATE_SUCCESS' , 'code':200}
				
		else:
			app.logger.info("Interview Live: Interview: "+str(interviewid)+" "+user.email +" Unknown Status "+str(incommingStatus)+" received.   - "+str(clientip)+" - [ "+str(user.userid)+" ] ")

			app.logger.info("End of ApiInterviewStatusUpdate - "+str(clientip)+" - [ "+str(user.userid)+" ] ")
			return {'status': False, 'message': 'APIINTERVIEWSTATUSUPDATE_ERROR' , 'code':422}


# Joining to Interview 
class ApiJoinLiveInterview(RestResource):
	def get(self, interviewid):
		clientip = request.remote_addr
		app.logger.info("Accessing ApiJoinLiveInterview - "+str(clientip))
		status,auth_dict = user_auth(admin_access=C.ALLOW, return_requestor=True)
		app.logger.info(auth_dict)
		requestor_class = auth_dict
		app.logger.info(requestor_class)
		user = requestor_class
		app.logger.info(user)
		interview = Interviews.query.get(interviewid)
		
		#Add rdt to calculate remaining time of a interview .. Used to end the interview based on Server Time exactly
		if not interview:
			app.logger.info("Interview not found - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.info("End of ApiJoinLiveInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			return {'status': False, 'message': 'COMMON_INTERVIEW_NOT_FOUND', 'code':404}

		logo = getOrgLogo(interview.orgid)

		if interview.interviewstatus==C.INTERVIEW_SUCCESSFUL :
			app.logger.info("Interview Live: Interview: "+str(interviewid)+" Interview completed - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.info("End of ApiJoinLiveInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			return {'status': False, 'message': 'APIINTERVIEWAUTH_INT_COMPLETED','logo':logo, 'code':410}
		elif interview.interviewstatus == C.INTERVIEW_FAILED or interview.interviewstatus == C.INTERVIEW_CANCELLED:
			app.logger.info("Interview Live: Interview: "+str(interviewid)+" Interview Cancelled - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.info("End of ApiJoinLiveInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			return {'status': False, 'message': 'APIINTERVIEWAUTH_INT_CANCELLED','logo':logo, 'code':410}
		duration = interview.duration * 60
		diffTime = slot.get_now_delta_seconds(interview.date,interview.slot_id)
		remaining_seconds = duration - (-1*diffTime)
		rdt = slot.get_remaining_time(interview.slot_id)				
		
		currentDate = datetime.now()
		server_time = datetime.strftime(currentDate, "%Y-%m-%d %H:%M")

		regular_interviewer=[]
		primary_interviewer =[]
		interviewers_list=[]
		interviewer_details =[]
		candidate_details =[]
		user_details=[]
		interview_interviewer = Interview_Interviewer().query.filter(and_(Interview_Interviewer.interviewid == interview.interviewid,Interview_Interviewer.active_status == True)).all()
		if interview_interviewer:
			for i in interview_interviewer:
				app.logger.info(i)
				interviewers_list = User().query.get(i.interviewerid)
				
				if interviewers_list:
					if i.isprimary == True:
						user_details.append({'primary_interviewerid':i.interviewerid})
						primary_interviewer.append({'firstname':interviewers_list.firstname,'lastname':interviewers_list.lastname,'mobile':interviewers_list.mobile,'email':interviewers_list.email})
					else:
						user_details.append({'interviewerid':i.interviewerid})
						regular_interviewer.append({'firstname':interviewers_list.firstname,'lastname':interviewers_list.lastname,'mobile':interviewers_list.mobile,'email':interviewers_list.email})
		else:
			app.logger.info("Interviewer not found - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.info("End of ApiJoinLiveInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			return {'status': False, 'message': 'COMMON_INTERVIEWER_NOT_FOUND', 'code':404}

		interview_candidate = Interview_Candidate().query.filter(and_(Interview_Candidate.interviewid == interview.interviewid,Interview_Candidate.active_status == True)).all()
		candidate_list =[]
		app.logger.info("interview_candidate")
		app.logger.info(interview_candidate)
		if interview_candidate:
			candidate_details = User().query.get(interview_candidate[0].candidateid)
			if candidate_details:
				user_details.append({'candidateid':candidate_details.userid})
				candidate_list.append({'firstname':interviewers_list.firstname,'lastname':interviewers_list.lastname,'mobile':interviewers_list.mobile,'email':interviewers_list.email})
			else: 
				app.logger.info("Interviewer not found - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.info("End of ApiJoinLiveInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': False, 'message': 'COMMON_CANDIDATE_NOT_FOUND', 'code':404}				
		
		interviewer_list ={'primaryinterviewer':primary_interviewer,'interviewer_list':regular_interviewer,'candidate_list':candidate_list,'user_details':user_details, 'interviewid': interview.interviewid, 'id': interview.interviewid, 'interviewtitle': interview.interviewtitle}
		# interviewers_list.append(interviewer_list)

		# ret = interview_serialize(interview, rc=requestor_class, lst=False)
		# ret1 = {'rtime': rdt , 'server_time':server_time,'interview':interviewers_list, 'remaining_seconds':remaining_seconds,'logo':logo}
		interviewtracking_started = InterviewTracking().query.filter(and_(InterviewTracking.interviewid == interviewid,InterviewTracking.activity == C.INTERVIEW_TRACKING_RECORDING_STARTED)).first()
		interviewtracking_end = InterviewTracking().query.filter(and_(InterviewTracking.interviewid == interviewid,InterviewTracking.activity == C.INTERVIEW_TRACKING_RECORDING_STOPPED)).first()
		app.logger.info("interviewtracking_started and stopped")
		app.logger.info(interviewtracking_started)
		app.logger.info(interviewtracking_end)
		if interviewtracking_started:
			isStartRecording = True
		else:
			isStartRecording = False
		if interviewtracking_end:
			isStopRecording = True
		else:
			isStopRecording = False


		ret1 = {'rdt': rdt , 'server_time':server_time, 'remaining_seconds':remaining_seconds,'logo':logo, 'candidate_details':candidate_list, 'interviewers_list':  interviewer_list, 'interview': interviewer_list,'isStartRecording':isStartRecording,'isStopRecording':isStopRecording,'prerecording_enabled':interview.prerecording_enabled}
		
			
		app.logger.info("End of ApiJoinLiveInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
		return ret1 , 200


class ApiUpcomingInterviewsList(RestResource):
	def get(self): 
		app.logger.info("Accessing ApiUpcomingInterviewsList")
		clientip = request.remote_addr
		status, user = user_auth(admin_access=C.ADMIN)
		if status and user:
			app.logger.debug("User is authorized to access ApiUpcomingInterviewsList service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			# read parameters
			app.logger.info("Request args")
			app.logger.info(request.args)
			if 'status' in request.args:
				app.logger.info("Status exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			else:
				app.logger.info("Status not exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				raise RequiredFieldsMissing()
			if 'orgid' in request.args:
				app.logger.info("Orgid exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			else:
				app.logger.info("Orgid not exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				raise RequiredFieldsMissing()
			if 'start_date' in request.args:
				app.logger.info("Start date exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			else:
				app.logger.info("Start Date not exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				raise RequiredFieldsMissing()
			if 'end_date' in request.args:
				app.logger.info("End Date exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			else:
				app.logger.info("End Date not exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				raise RequiredFieldsMissing()
			try:
				app.logger.info("Request args")
				app.logger.info(request.args)
				interviewstatus = request.args['status']
				orgid = request.args['orgid']
				start_date = request.args['start_date']
				end_date = request.args['end_date']


				if int(interviewstatus) >=0 :
					app.logger.info("Status is valid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					interviewstatus = int(interviewstatus)
				else:
					app.logger.info("Status is not valid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.info("End of ApiUpcomingInterviewsList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {'status': False, 'message': 'APIUPDATEUSER_STATUS_INVALID', 'code':422}

				if int(orgid) > 0:
					app.logger.info("Orgid is valid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				else:
					app.logger.info("Orgid is not valid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.info("End of ApiUpcomingInterviewsList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {'status': False, 'message': 'APICREATEUSER_ORGID_INVALID', 'code':422}

				isStartDateString = validateString(start_date)
				if isStartDateString:
					app.logger.info("Start_date is not empty - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					dateValidation = datetime.strptime(start_date, "%Y-%m-%d")
					s_date = date(*map(int, start_date.split('-')))
					if dateValidation:
						app.logger.debug("Date format is valid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					else:
						app.logger.debug("Date format is not valid "+str(start_date)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						app.logger.info("End of ApiUpcomingInterviewsList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						return {'status': False, 'message': 'APISCHEDULEINTERVIEW_DATE_INVALID', 'code':422}
				else:
					app.logger.debug("Start_date is empty"+str(start_date)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					raise emptyStringError()

				isEndDateString = validateString(end_date)
				if isEndDateString:
					app.logger.info("Start_date is not empty - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					dateValidation = datetime.strptime(end_date, "%Y-%m-%d")
					e_date = date(*map(int, end_date.split('-')))
					if dateValidation:
						app.logger.debug("Date format is valid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					else:
						app.logger.debug("Date format is not valid "+str(end_date)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						app.logger.info("End of ApiUpcomingInterviewsList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						return {'status': False, 'message': 'APISCHEDULEINTERVIEW_DATE_INVALID', 'code':422}
				else:
					app.logger.debug("Start_date is empty"+str(end_date)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					raise emptyStringError()
				
				list_of_interviews=[]
				regular_interviewer_data=[]
				primary_interviewer =[]
				result_data=[]
				candidate_data =[]
				list_of_interviewers=[]
				final_data=[]
				if s_date <= e_date:
					slot_id,cur_date = calculateCurrentSlot()
#					upcomingInterviews = "select i.interviewid, i.date, i.slot_id, i.duration,i.interviewstatus, (select u.email from users as u where u.userid=(select c.interviewerid from interview_interviewer as c where c.interviewid=i.interviewid and c.isprimary=true and active_status = true)) as interviewer_email, (select u.email from users as u where u.userid=(select c.candidateid from interview_candidate as c where c.interviewid=i.interviewid and c.active_status = true)) as candidate_email from interviews as i where (i.date >= '"+str(start_date)+"' and  i.date <= '"+str(end_date)+"') "
					upcomingInterviews = "select i.interviewid, i.date, i.slot_id, i.duration,i.interviewstatus, (select u.email from users as u where u.userid=(select c.interviewerid from interview_interviewer as c where c.interviewid=i.interviewid and c.isprimary=true and active_status = true)) as interviewer_email, (select u.email from users as u where u.userid=(select c.candidateid from interview_candidate as c where c.interviewid=i.interviewid and c.active_status = true)) as candidate_email,(select c.joinurl from interview_interviewer as c where c.interviewid=i.interviewid and c.isprimary=true and active_status = true) as interviewer_joinurl, (select c.joinurl from interview_candidate as c where c.interviewid=i.interviewid and c.active_status = true) as candidate_joinurl from interviews as i where (i.date >= '"+str(start_date)+"' and  i.date <= '"+str(end_date)+"') "
					# if start_date == cur_date and end_date == cur_date:
					# 	upcomingInterviews+= " (i.date = '"+str(start_date)+"' and slot_id >= "+str(slot_id)+") "

					# if start_date == cur_date and end_date != cur_date:
					# 	upcomingInterviews+= "((i.date ='"+str(start_date)+"' and i.slot_id >= "+str(slot_id)+") or (i.date >'"+str(start_date)+"' and i.date <='"+str(end_date)+"')) "

					# if start_date != cur_date and end_date != cur_date:
					# 	upcomingInterviews+= " (i.date >= '"+str(start_date)+"' and  i.date <= '"+str(end_date)+"') "

					if interviewstatus > 0:
						upcomingInterviews +=" and i.interviewstatus="+str(interviewstatus)+" "
					upcomingInterviews += " and i.orgid="+str(orgid)+";"
					app.logger.info(upcomingInterviews)
					upcoming_interviews = db.engine.execute(upcomingInterviews)
					result = UpcomingInterviewsListSchema().dump(upcoming_interviews,many = True).data
					
					app.logger.info("End of ApiUpcomingInterviewsList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {'status':True,'code':200,'message':'APIUPCOMINGINTERVIEWS_SUCCESS','listOfInterviews':result}
				else:
					app.logger.debug("Start Date must be less than End Date - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.info("End of ApiUpcomingInterviewsList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {'status':False,'code':405,'message':'APIUPCOMINGINTERVIEWS_DATE_COMPARISION'}
			
			except RequiredFieldsMissing as re:
				app.logger.info("One or more required fileds missing - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.exception(re)
				app.logger.info("End of ApiUpcomingInterviewsList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_REQ_MORE_FIELD', 'code':422}
			
			except IntegrityError as ie:
				app.logger.info("Error: DB Exceeding limit Error - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.error(ie.message)
				app.logger.info("End of ApiUpcomingInterviewsList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_CONTACT_SUPPORT', 'code':409}
			except DataError as de:
				app.logger.info("DataBase field value out of range - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.error(de.message)
				app.logger.info("End of ApiUpcomingInterviewsList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_INVALID_DATA', 'code':422}
			except InvalidRequestError as ir:
				app.logger.info("received invalid request - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.exception(ir)
				app.logger.error(ir.message)
				app.logger.info("End of ApiUpcomingInterviewsList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_INVALID_REQUSET', 'code':422}
			except ValueError as ve:
				app.logger.info("Date Format is invalid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.error(ve.message)
				app.logger.info("End of ApiUpcomingInterviewsList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': str(ve.message), 'code':422}
			except ValueMissing as vm:
				app.logger.info("Received invalid input - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.error(vm.message)
				app.logger.info("End of ApiUpcomingInterviewsList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_INVALID_INPUT', 'code':422}
			except invalidEmailError as iee:
				app.logger.info("Email ID is not valid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.exception(iee)
				app.logger.error(iee.message)
				app.logger.info("End of ApiUpcomingInterviewsList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_EMAIL_NOT_VALID', 'code':422}
			except emptyStringError as ese:
				app.logger.info("One of the field is Empty - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.exception(ese)
				app.logger.error(ese.message)
				app.logger.info("End of ApiUpcomingInterviewsList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_ONE_OF_EMPTY_FIELD', 'code':422}
			except Exception as e:
				app.logger.info("Error while ApiUpcomingInterviewsList- " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.exception(e.message)
				app.logger.info("End of ApiUpcomingInterviewsList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': False, 'message': 'APIUPCOMINGINTERVIEWS_FAIL_INTERVIEWS', 'code':422}

		elif user:
			app.logger.debug("User is not authorized to access ApiUpcomingInterviewsList service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.info("End of ApiUpcomingInterviewsList  - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			return {'status': False,  'message': 'COMMON_UNAUTHORIZED', 'code':403}
		else:
			app.logger.info("Invalid token or Session expired - " + str(clientip))
			app.logger.info("End of ApiUpcomingInterviewsList - " + str(clientip))
			return {'status': False, 'message': 'COMMON_INVALID_TOKEN', 'code':401}


def calculateCurrentSlot():
	app.logger.info("Accessing calculateCurrentSlot")
	currentDate = datetime.now()
	cur_time = datetime.strftime(currentDate, "%H:%M")
	cur_date = datetime.strftime(currentDate,"%Y-%m-%d")
	cur_time_split = cur_time.split(':')
	time_hours = int(cur_time_split[0])
	time_minutes = int(cur_time_split[1])
	if time_minutes >=0 and  time_minutes <=30 :
		app.logger.debug(time_minutes)
		slot_id = time_hours *2+1
	else:
		app.logger.debug(time_minutes)
		slot_id = time_hours *2 + 2	
	app.logger.info("End of calculateCurrentSlot")
	return slot_id,cur_date


class ApiPreviousInterviewsList(RestResource):
	def get(self):
		clientip = request.remote_addr
		app.logger.info("Accessing ApiPreviousInterviewsList - "+str(clientip))
		status, user = user_auth(admin_access=C.ADMIN)
		if status and user:

			app.logger.debug("User is authorized to access ApiPreviousInterviewsList service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			if 'status' in request.args:
				app.logger.info("Status exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			else:
				app.logger.info("Status not exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.info("End of ApiPreviousInterviewsList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': False, 'message': 'APIUPDATEUSER_STATUS_INVALID', 'code':422}
			if 'orgid' in request.args:
				app.logger.info("Orgid exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			else:
				app.logger.info("Orgid not exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.info("End of ApiPreviousInterviewsList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': False, 'message': 'APIORGUPDATE_ORG_NOT_FOUND', 'code':422}
			if 'start_date' in request.args:
				app.logger.info("Start date exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			else:
				app.logger.info("Start Date not exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.info("End of ApiPreviousInterviewsList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': False, 'message': 'APIPREVIOUSINTERVIEWS_STDATE_ERROR', 'code':422}
			if 'end_date' in request.args:
				app.logger.info("End Date exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			else:
				app.logger.info("End Date not exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.info("End of ApiPreviousInterviewsList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': False, 'message': 'APIPREVIOUSINTERVIEWS_EDATE_ERROR', 'code':422}
			
			try:
				app.logger.info("Request args")
				app.logger.info(request.args)
				interviewstatus = request.args['status']
				orgid = request.args['orgid']
				start_date = request.args['start_date']
				end_date = request.args['end_date']
			
				if int(interviewstatus) >= 0:
					interviewstatus = int(interviewstatus)
					app.logger.info("Status is valid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				else:
					app.logger.info("Status is not valid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.info("End of ApiPreviousInterviewsList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {'status': False, 'message': 'APIUPDATEUSER_STATUS_INVALID', 'code':422}

				if int(orgid) > 0:
					app.logger.info("Orgid is valid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				else:
					app.logger.info("Orgid is not valid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.info("End of ApiPreviousInterviewsList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {'status': False, 'message': 'APICREATEUSER_ORGID_INVALID', 'code':422}

				isStartDateString = validateString(start_date)
				if isStartDateString:
					app.logger.info("Start_date is not empty - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					dateValidation = datetime.strptime(start_date, "%Y-%m-%d")
					s_date = date(*map(int, start_date.split('-')))
					if dateValidation:
						app.logger.debug("Date format is valid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					else:
						app.logger.debug("Date format is not valid "+str(start_date)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						app.logger.info("End of ApiPreviousInterviewsList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						return {'status': False, 'message': 'APISCHEDULEINTERVIEW_DATE_INVALID', 'code':422}
				else:
					app.logger.debug("Start_date is empty"+str(start_date)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					raise emptyStringError()

				isEndDateString = validateString(end_date)
				if isEndDateString:
					app.logger.info("Start_date is not empty - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					dateValidation = datetime.strptime(end_date, "%Y-%m-%d")
					e_date = date(*map(int, end_date.split('-')))
					if dateValidation:
						app.logger.debug("Date format is valid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					else:
						app.logger.debug("Date format is not valid "+str(end_date)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						app.logger.info("End of ApiPreviousInterviewsList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						return {'status': False, 'message': 'APISCHEDULEINTERVIEW_DATE_INVALID', 'code':422}
				else:
					app.logger.debug("Start_date is empty"+str(end_date)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					raise emptyStringError()
			
				slot_id,cur_date = calculateCurrentSlot()

				# Fetching all rows based on Data
				if s_date <= e_date:
					# previousInterviews = "select i.interviewid,i.interviewstatus,i.orgid,i.interviewerid,i.candidateid,i.date,i.slot_id,i.createdtime,(select u.email as candidate_email from users as u where u.userid = i.candidateid),(select u.mobile as candidate_mobile from users as u where u.userid = i.candidateid),(select u.mobile as interviewer_mobile from users as u where u.userid  = i.interviewerid),(select u.email as interviewer_email from users as u where u.userid = i.interviewerid)  from interviews as i where "
					previousInterviews = "select i.interviewid, i.date, i.slot_id, i.duration,i.interviewstatus, (select u.email from users as u where u.userid=(select c.interviewerid from interview_interviewer as c where c.interviewid=i.interviewid and c.isprimary=true and c.active_status = true)) as interviewer_email, (select u.email from users as u where u.userid=(select c.candidateid from interview_candidate as c where c.interviewid=i.interviewid and c.active_status = true)) as candidate_email from interviews as i where i.date >='"+str(start_date)+"' and i.date <= '"+str(end_date)+"'"
					# if end_date == cur_date and start_date == cur_date:
					# 	previousInterviews +="i.date = '"+str(start_date)+"' and i.slot_id <="+str(slot_id)+" "
					# if  start_date != cur_date  and end_date == cur_date :
					# 	previousInterviews +="((i.date >= '"+str(start_date)+"' and  i.date < '"+str(end_date)+"') or (i.date ='"+str(end_date)+"' and slot_id <= "+str(slot_id)+" ))"
					# if start_date != cur_date and end_date != cur_date:
					# 	previousInterviews += "i.date >='"+str(start_date)+"' and i.date <= '"+str(end_date)+"' "
					if interviewstatus > 0:
						previousInterviews +="and i.interviewstatus="+str(interviewstatus)+" "
					previousInterviews += "and i.orgid="+str(orgid)+";"
					app.logger.info(previousInterviews)
					previous_interviews = db.engine.execute(previousInterviews)
					result = PreviousInterviewsListSchema().dump(previous_interviews,many = True).data
					app.logger.info("End of ApiPreviousInterviewsList - " + str(clientip))
					return {'status':True,'code':200,'message':'APIUPCOMINGINTERVIEWS_SUCCESS','listOfInterviews':result}
				else:
					app.logger.debug("Start Date must be less than End Date - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.info("End of ApiPreviousInterviewsList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {'status': False, 'message': 'APIUPCOMINGINTERVIEWS_DATE_COMPARISION', 'code':422}
		

			except RequiredFieldsMissing as re:
				app.logger.info("One or more required fileds missing - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.exception(re)
				app.logger.info("End of ApiPreviousInterviewsList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_REQ_MORE_FIELD', 'code':422}
			
			except IntegrityError as ie:
				app.logger.info("Error: DB Exceeding limit Error - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.error(ie.message)
				app.logger.info("End of ApiPreviousInterviewsList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_CONTACT_SUPPORT', 'code':409}
			except DataError as de:
				app.logger.info("DataBase field value out of range - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.error(de.message)
				app.logger.info("End of ApiPreviousInterviewsList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_INVALID_DATA', 'code':422}
			except InvalidRequestError as ir:
				app.logger.info("received invalid request - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.exception(ir)
				app.logger.error(ir.message)
				app.logger.info("End of ApiPreviousInterviewsList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_INVALID_REQUSET', 'code':422}
			except ValueError as ve:
				app.logger.info("Date Format is invalid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.error(ve.message)
				app.logger.info("End of ApiPreviousInterviewsList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': str(ve.message), 'code':422}
			except ValueMissing as vm:
				app.logger.info("Received invalid input - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.error(vm.message)
				app.logger.info("End of ApiPreviousInterviewsList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_INVALID_INPUT', 'code':422}
			except Exception as e:
				app.logger.info("Error while ApiPreviousInterviewsList- " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.exception(e.message)
				app.logger.info("End of ApiPreviousInterviewsList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': False, 'message': 'APIPREVIOUSINTERVIEWS_FAIL_INTERVIEWS', 'code':422}

		elif user:
			app.logger.debug("User is not authorized to access ApiPreviousInterviewsList service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.info("End of ApiPreviousInterviewsList - " + str(clientip))
			return {'status': False,  'message': 'COMMON_UNAUTHORIZED', 'code':403}
		else:
			app.logger.info("Invalid token or Session expired - " + str(clientip))
			app.logger.info("End of ApiPreviousInterviewsList - " + str(clientip))
			return {'status': False, 'message': 'COMMON_INVALID_TOKEN', 'code':401}
		
		


class ApiTestSystem(RestResource):
	def get(self):
		app.logger.info("Accessing ApiTestSystem")
		clientip = request.remote_addr
		try:
			app.logger.info("Generating a session for test system - " + str(clientip))
			ot_id = ot.create_session(record=False)
			ot_token = ot.create_token(ot_id, role='p', ot_expires_in=app.config['TEST_SYSTEM_OPENTOK_EXPIRES_IN'])
			ot_key = app.config['OT_API_KEY']
			ret = {'ot_key': ot_key, 'ot_id': ot_id, 'ot_token': ot_token}
			return {'status':True,'result':ret,'message':'APITESTSYSTEM_OT_CREATION','code':200}
		except Exception as e:
			app.logger.info("Exception while generating a session for test system - " + str(clientip))
			return {'status':False,'message':'APITESTSYSTEM_OT_CREATION_ERROR','code':405}



class ApiOrganizationInterviewers(RestResource):
	def get(self,orgid):
		app.logger.info("Accessing ApiOrganizationInterviewers")
		clientip = request.remote_addr
		status, user = user_auth(admin_access=C.ADMIN)
		if status and user:

			app.logger.debug("User is authorized to access ApiOrganizationInterviewers service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			interviewers = User().query.filter(and_(User.orgid == orgid,User.role == C.INTERVIEWER)).all()
			if interviewers:
				app.logger.debug("Interviewers Exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				interviewer_list = UserBaseSchema(partial=('userid','orgid','role','email','firstname','lastname')).dump(interviewers, many=True).data
				return {'status': True, 'message': 'APIORGANIZATIONINTERVIEWERS_SUCCESS','interviewers':interviewer_list, 'code':200}	
			else:
				app.logger.debug("Interviewers not Exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				interviewer_list = []
				return {'status': True, 'message': 'APIORGANIZATIONINTERVIEWERS_FAIL','interviewers':interviewer_list, 'code':200}	
		elif user:
			app.logger.debug("User is not authorized to access ApiOrganizationInterviewers service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.info("End of ApiOrganizationInterviewers  - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			return {'status': False,  'message': 'COMMON_UNAUTHORIZED', 'code':403}
		else:
			app.logger.info("Invalid token or Session expired - " + str(clientip))
			app.logger.info("End of ApiOrganizationInterviewers - " + str(clientip))
			return {'status': False, 'message': 'COMMON_INVALID_TOKEN', 'code':401}



class ApiOrganizationCandidates(RestResource):
	def get(self,orgid):
		app.logger.info("Accessing ApiOrganizationCandidates")
		clientip = request.remote_addr
		status, user = user_auth(admin_access=C.ADMIN)
		if status and user:
			app.logger.debug("User is authorized to access ApiOrganizationCandidates service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			candidates = User().query.filter(and_(User.orgid == orgid,User.role == C.INTERVIEWEE)).all()
			if candidates:
				app.logger.debug("Candidates Exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				candidate_list = UserBaseSchema(partial=('userid','orgid','role','email','firstname','lastname')).dump(candidates, many=True).data
				return {'status': True, 'message': 'APIORGANIZATIONCANDIDATES_SUCCESS','candidates':candidate_list, 'code':200}	
			else:
				app.logger.debug("Candidates not Exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				candidate_list = []
				return {'status': True, 'message': 'APIORGANIZATIONCANDIDATES_FAIL','candidates':candidate_list, 'code':200}	
		elif user:
			app.logger.debug("User is not authorized to access ApiOrganizationCandidates service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.info("End of ApiOrganizationCandidates - " + str(clientip))
			return {'status': False,  'message': 'COMMON_UNAUTHORIZED', 'code':403}
		else:
			app.logger.info("Invalid token or Session expired - " + str(clientip))
			app.logger.info("End of ApiOrganizationCandidates - " + str(clientip))
			return {'status': False, 'message': 'COMMON_INVALID_TOKEN', 'code':401}


class ApiInterviewDetails(RestResource):
	def get(self,interviewid):
		app.logger.info("Accessing ApiInterviewDetails")
		clientip = request.remote_addr
		status, user = user_auth(admin_access=C.ADMIN)
		if status and user:

			app.logger.debug("User is authorized to access ApiInterviewDetails service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			try:
				interview = Interviews().query.get(interviewid)
				if interview:
					app.logger.debug("Interviews Exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					details,error,status = allinterviewDetails(interviewid,clientip,user)
					app.logger.debug("details")
					app.logger.debug(details)
					interview_details = []
					# interview_details.append(details)
					app.logger.debug("interview_details")
					app.logger.debug(interview_details)


					query = "select i.interviewid,i.interviewer_id,i.feedback,(select email from users as u where u.userid=i.interviewer_id) as email,(select firstname from users as u where u.userid=i.interviewer_id) as firstname,(select lastname from users as u where u.userid=i.interviewer_id) as lastname from feedback as i where i.interviewid=" + str(interviewid) +";"

					app.logger.info("query")
					app.logger.info(query)
					try:
						result = db.engine.execute(query) 
						interviewerDetailsWithFeedbackList = InterviewerDetailsWithFeedbackSchema().dump(result, many=True).data
						app.logger.info(interviewerDetailsWithFeedbackList)
						# app.logger.info(interviews_list)
						app.logger.debug("interview.archiveid")
						app.logger.debug(interview.archiveid)
				
						if interview.archiveid:
							app.logger.info("get archive link - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							videoLink = archive.getArchiveURL(interview, user)
							app.logger.info('videoLink')
							app.logger.info(videoLink)
							return {'status': True, 'message': 'APIINTERVIEWDETAILS_SUCCESS','interview_details':details,'feedback_details':interviewerDetailsWithFeedbackList,'videoLink':videoLink, 'code':200}	
					except Exception as e:
						app.logger.info(e)
					
					return {'status': True, 'message': 'APIINTERVIEWDETAILS_SUCCESS','interview_details':details,'feedback_details':interviewerDetailsWithFeedbackList, 'code':200}	
					
				else:
					app.logger.debug("Interviews not Exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					pass
					
				return {'status': True, 'message': 'APIINTERVIEWDETAILS_NOT_EXIST', 'code':200}	
			except Exception  as e:
				app.logger.debug("Error in ApiInterviewDetails - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.debug(str(e.message))
				app.logger.info("End of ApiInterviewDetails - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': False, 'message': 'APIINTERVIEWDETAILS_FAIL', 'code':405}	
		elif user:
			app.logger.debug("User is not authorized to access ApiInterviewDetails service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.info("End of ApiInterviewDetails  - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			return {'status': False,  'message': 'COMMON_UNAUTHORIZED', 'code':403}
		else:
			app.logger.info("Invalid token or Session expired - " + str(clientip))
			app.logger.info("End of ApiInterviewDetails - " + str(clientip))
			return {'status': False, 'message': 'COMMON_INVALID_TOKEN', 'code':401}

class ApiSearchInterviewers(RestResource):
	def get(self,orgid):
		app.logger.info("Accessing ApiSearchInterviewers")
		clientip = request.remote_addr
		status, user = user_auth(admin_access=C.ADMIN)
		if status and user:
			app.logger.debug("User is authorized to access ApiSearchInterviewers service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			search_word = request.args.get('interviewer_name')
			if search_word:
				interviewer_list=[]
				query = "select userid ,firstname,lastname, email,role,orgid,mobile from users where role="+str(C.INTERVIEWER)+"  and (COALESCE(lower(firstname),'') like lower('%%"+str(search_word)+"%%') or COALESCE(lower(lastname),'') like lower('%%"+str(search_word)+"%%') or COALESCE(lower(email),'') like lower('%%"+str(search_word)+"%%'))  and orgid ="+str(orgid)+";"
				interviewers = db.engine.execute(query)
				interviewer_list = UserBaseSchema(partial=('userid','orgid','role','email','firstname','lastname','mobile')).dump(interviewers, many=True).data
				return {'status': True, 'message': 'APIORGANIZATIONINTERVIEWERS_SUCCESS','interviewers':interviewer_list, 'code':200}	
			else:
				app.logger.info("search key word not found - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.info("End of ApiSearchInterviewers - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': False, 'message': 'APIORGANIZATIONINTERVIEWERS_FAIL', 'code':403}
		elif user:
			app.logger.debug("User is not authorized to access ApiSearchInterviewers service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.info("End of ApiSearchInterviewers - " + str(clientip))
			return {'status': False,  'message': 'COMMON_UNAUTHORIZED', 'code':403}
		else:
			app.logger.info("Invalid token or Session expired - " + str(clientip))
			app.logger.info("End of ApiSearchInterviewers - " + str(clientip))
			return {'status': False, 'message': 'COMMON_INVALID_TOKEN', 'code':401}

class ApiSearchCandidates(RestResource):
	def get(self,orgid):
		app.logger.info("Accessing ApiSearchCandidates")
		clientip = request.remote_addr
		status, user = user_auth(admin_access=C.ADMIN)
		if status and user:
			app.logger.debug("User is authorized to access ApiSearchCandidates service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			search_word = request.args.get('candidate_name')
			if search_word:
				candidate_list=[]
				query = "select userid ,firstname,lastname, email,role,orgid,mobile from users where role="+str(C.INTERVIEWEE)+"  and (COALESCE(lower(firstname),'') like lower('%%"+str(search_word)+"%%') or COALESCE(lower(lastname),'') like lower('%%"+str(search_word)+"%%') or COALESCE(lower(email),'') like lower('%%"+str(search_word)+"%%')) and orgid ="+str(orgid)+" ;"
				candidates = db.engine.execute(query)
				candidate_list = UserBaseSchema(partial=('userid','orgid','role','email','firstname','lastname','mobile')).dump(candidates, many=True).data
				return {'status': True, 'message': 'APIORGANIZATIONCANDIDATES_SUCCESS','candidates':candidate_list, 'code':200}	
			else:
				app.logger.info("search key word not found - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.info("End of ApiSearchCandidates - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': False, 'message': 'APIORGANIZATIONCANDIDATES_FAIL', 'code':403}
		elif user:
			app.logger.debug("User is not authorized to access ApiSearchCandidates service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.info("End of ApiSearchCandidates - " + str(clientip))
			return {'status': False,  'message': 'COMMON_UNAUTHORIZED', 'code':403}
		else:
			app.logger.info("Invalid token or Session expired - " + str(clientip))
			app.logger.info("End of ApiSearchCandidates - " + str(clientip))
			return {'status': False, 'message': 'COMMON_INVALID_TOKEN', 'code':401}

def getInterviewStatusCountBetweenDates(fromDate, toDate,orgid,role):
	app.logger.info("from getInterviewStatusCountBetweenDates")
	if role == 1:
		query = 'select interviewstatus, count(*) from interviews where date >= \''+fromDate+'\' and date <= \''+toDate+'\' group by interviewstatus order by interviewstatus;'
	else:
		query = 'select interviewstatus, count(*) from interviews where date >= \''+fromDate+'\' and date <= \''+toDate+'\' and orgid ='+ str(orgid) +' group by interviewstatus order by interviewstatus;'
	results = db.engine.execute(query)
	listed = 0
	booked = 0
	successful = 0
	failed = 0
	onlyJoined = 0
	
	for result in results:
		listed += result.count
		if(result.interviewstatus in [1,2,3,4,5,6]):
			booked += result.count
		if(result.interviewstatus in [11]):
			successful += result.count
		if(result.interviewstatus in [12]):
			app.logger.debug("failed found")
			failed += result.count
		if(result.interviewstatus in [7,8,9,10]):
			app.logger.debug("only joined found")
			onlyJoined += result.count
	return listed,booked, successful, failed, onlyJoined


def getMinutesDetails(fromDate,toDate,orgid,role):
	if role == 1:
		app.logger.debug("get all organization details for Super Admin")
		query = 'select count(*) as totalinterviews, sum(subscribeminutes) as subscribeminutes, sum(recordingtime) as recordingtime, sum(recordingsize) as recordingsize from interviews where interviewstatus not in ('+str(C.INTERVIEW_BOOKED) +','+str(C.INTERVIEW_FAILED)+') and date >= \''+fromDate+'\' and date <= \''+toDate+'\';'
		
		async_recording_time_query = "select at.recorded_length from asynctest as at where at.asyncid in (select t.interviewid from async_interviewtracking as t where date(activitytime) >= '"+fromDate+"' and date(activitytime) <= '"+toDate+"');"
		async_interviews_count_query = "select distinct interviewid from async_interviewtracking where date(activitytime) >= '"+fromDate+"' and date(activitytime) <= '"+toDate+"';"
	else:
		app.logger.debug("get details for the organization "+str(orgid))
		query = 'select count(*) as totalinterviews, sum(subscribeminutes) as subscribeminutes, sum(recordingtime) as recordingtime, sum(recordingsize) as recordingsize from interviews where interviewstatus not in ('+str(C.INTERVIEW_BOOKED) +','+str(C.INTERVIEW_FAILED)+') and orgid = ' + str(orgid) + ' and date >= \''+fromDate+'\' and date <= \''+toDate+'\';'

		async_recording_time_query = "select at.recorded_length from asynctest as at where at.asyncid in (select t.interviewid from async_interviewtracking as t where orgid = " + str(orgid) + " and date(activitytime) >= '"+fromDate+"' and date(activitytime) <= '"+toDate+"');"
		async_interviews_count_query = "select distinct interviewid from async_interviewtracking where orgid = " + str(orgid) + " and date(activitytime) >= '"+fromDate+"' and date(activitytime) <= '"+toDate+"';"
	app.logger.debug("query")
	app.logger.debug(query)
	results = db.engine.execute(query)

	app.logger.debug("results")
	app.logger.debug(results)
	app.logger.debug(results.rowcount)
	totalInterviews = totalSubscribeMinutes = totalRecordingTime = totalRecordingSize = totalAsyncInterviews = totalAsyncRecordedTime = 0
	for row in results:
		app.logger.info("row")
		app.logger.info(row)
		# app.logger.info(row.totalInterviews)
		jsonResults = OrganizationUsageSchema().dump(row, many=False).data
		app.logger.info("jsonResults")
		app.logger.info(jsonResults)
		totalInterviews = jsonResults['totalinterviews']
		totalSubscribeMinutes = jsonResults['subscribeminutes']
		totalRecordingTime = jsonResults['recordingtime']
		totalRecordingSize = jsonResults['recordingsize']
		app.logger.debug("totalSubscribeMinutes")
		app.logger.debug(totalSubscribeMinutes)
		app.logger.debug("totalRecordingTime")
		app.logger.debug(totalRecordingTime)
		app.logger.debug("totalRecordingSize")
		app.logger.debug(totalRecordingSize)


	app.logger.info("async_interviews_count_query")
	app.logger.info(async_interviews_count_query)
	app.logger.info("async_recording_time_query")
	app.logger.info(async_recording_time_query)


	async_interviews_results = db.engine.execute(async_interviews_count_query)

	asyncInterviewsJsonResults = OrganizationAsyncUsageSchema().dump(async_interviews_results, many=True).data

	async_recording_time_results = db.engine.execute(async_recording_time_query)
	asyncRecordingTimeJsonResults = RecordingTimeOnlySchema().dump(async_recording_time_results, many=True).data

	return totalInterviews, totalSubscribeMinutes, totalRecordingTime, totalRecordingSize, len(asyncInterviewsJsonResults), asyncRecordingTimeJsonResults



def getCurrentWeekDates():
		app.logger.info("in getNext7Dates()")
		listOfDates = []
		today = datetime.utcnow().date().strftime('%d-%b-%Y')
		dt = datetime.strptime(str(today), '%d-%b-%Y')
		start = dt - timedelta(days=dt.weekday())
		end = start + timedelta(days=6)

		for date in daterange( start, end ):
			converted_date = date.strftime('%d-%b-%Y')
			listOfDates.append(converted_date)
		
		return listOfDates

def daterange( start_date, end_date ):
	app.logger.info("in daterange")
	for n in range( ( end_date - start_date ).days + 1 ):
		yield start_date + timedelta( n )

def past_n_weeks(yr, wk, n):
	app.logger.info("yr , wk, n")
	app.logger.info(str(yr) + " "+str(wk)+" "+str(n))
	ret = [(yr, wk)]
	for i in xrange(n - 1):
		if wk > 0:
			wk -= 1
			ret.append((yr, wk))
		else:
			yr -= 1
			wk = 53
			while week_range(yr, wk)[0] >= week_range(yr + 1, 0)[0]:
				wk -= 1
			ret.append((yr, wk))
	app.logger.info("ret")
	app.logger.info(ret)
	return ret

def week_range(yr, wk):
	s = ' '
	d = str(yr) + s + str(wk) + s
	f = '%Y' + s + '%U' + s + '%w'
	strt = datetime.strptime(d + '0', f).date()
	end = datetime.strptime(d + '6', f).date()
	nxt = end + timedelta(days=1)
	return strt, end, nxt



def appendListedCountWithLabels(labels,listed,loadBy):
	app.logger.debug("in appendListedCountWithLabels")
	# app.logger.debug("labels")
	# app.logger.debug(labels)
	# app.logger.debug("listed")
	# app.logger.debug(listed)

	i = 0
	for l in labels:
		if loadBy != 'Month':
			labels[i] = l + " (" + str(listed[i]) + ")"
		else:
			labels[i][1] = labels[i][1] + " (" + str(listed[i]) + ")"
		i=i+1
	return labels


	
class ApiDashboard(RestResource):
	def get(self):
		app.logger.info("in ApiDashboard")
		clientip = request.remote_addr
		status,user = user_auth()
		if status and user:
			if user_is_admin(user):
				app.logger.info("User is Admin - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				role=2
			elif user_is_super_admin(user):
				app.logger.info("User is Super Admin - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				role=1
			else:
				app.logger.debug("User is not authorized to access ApiDashboard service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.info("End of ApiDashboard - " + str(clientip))
				return {'status': False,  'message': 'COMMON_UNAUTHORIZED', 'code':403}
		else:
			app.logger.info("Invalid token or Session expired - " + str(clientip))
			app.logger.info("End of ApiDashboard - " + str(clientip))
			return {'status': False, 'message': 'COMMON_INVALID_TOKEN', 'code':401}

		
		
		#if admin or superAdmin Proceed here
		loadBy = request.args["loadBy"]
		app.logger.info("loadBy")
		app.logger.info(loadBy)

		interviews_dates_info = []
		interviews_listed = []
		interviews_booked = []
		interviews_successful = []
		interviews_failed = []
		interviews_only_joined = []
		interviews_info = []
		
		totalInterviews = 0
		totalSubscribeMinutes = 0
		totalRecordingTime = 0
		totalRecordingSize = 0

		##code for testing
		# interview_data = Interviews().query.get(175)
		# slot_date_time = get_date_and_time_format_email_s(interview_data.date,interview_data.slot_id,interview_data.timezone)
		# app.logger.info("slot_date_time")
		# app.logger.info(slot_date_time)

		if loadBy=="Month":
			app.logger.info("get Month interviews information - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			now = datetime.now()
			yr = int(now.strftime('%Y'))
			wk = int(now.strftime('%U'))
			mn = int(now.strftime('%m'))
			last_date = calendar.monthrange(yr, mn)[1]
			# past 6 weeks info
			for wki in reversed(past_n_weeks(yr, wk, app.config['DASHBOARD_LIMITS']['graph'])):
				app.logger.info("reverse : "+str(wki))
			
				yr = wki[0]
				wk = wki[1]
				
				s = ' '
				d = str(yr) + s + str(wk) + s
				f = '%Y' + s + '%U' + s + '%w'
				start = datetime.strptime(d + '0', f).date()
				end = datetime.strptime(d + '6', f).date()
				
				interviews_dates_info.append([start.strftime('%b %d'),end.strftime('%b %d')])
				result = getInterviewStatusCountBetweenDates(start.strftime('%d-%b-%Y'),end.strftime('%d-%b-%Y'),user.orgid,role)
				interviews_listed.append(int(result[0]))
				interviews_booked.append(int(result[1]))
				interviews_successful.append(int(result[2]))
				interviews_failed.append(int(result[3]))
				interviews_only_joined.append(int(result[4]))

			interviews_dates_info = appendListedCountWithLabels(interviews_dates_info,interviews_listed,loadBy)

			interviews_info = {'interviews_dates_info':interviews_dates_info, 'interviews_listed':interviews_listed, 'interviews_booked':interviews_booked, 'interviews_successful':interviews_successful, 'interviews_failed':interviews_failed, 'interviews_only_joined':interviews_only_joined}

			firstDate, lastDate = get_month_day_range(datetime.now())
			
			app.logger.debug("firstDate")
			app.logger.debug(datetime.strftime(firstDate, "%Y-%m-%d"))
			app.logger.debug("lastDate")
			app.logger.debug(datetime.strftime(lastDate, "%Y-%m-%d"))
			# get subscribed min, recording min and recording size
			totalInterviews, totalSubscribeMinutes, totalRecordingTime, totalRecordingSize, totalAsyncInterviews, totalAsyncRecordedTime = getMinutesDetails(datetime.strftime(firstDate, "%Y-%m-%d"),datetime.strftime(lastDate, "%Y-%m-%d"),user.orgid,user.role)

			ret = {'status':True,'interviews_info':interviews_info, 'totalSubscribeMinutes':totalSubscribeMinutes, 'totalRecordingTime':totalRecordingTime, 'totalRecordingSize':totalRecordingSize,'totalInterviews':totalInterviews, 'totalAsyncInterviews':totalAsyncInterviews, 'totalAsyncRecordedTime':totalAsyncRecordedTime, 'code':200}
			return ret	
		elif loadBy=="Year":
			app.logger.info("get Year interviews information - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			now = datetime.now()
			yr = int(now.strftime('%Y'))
			list_of_months = [1,2,3,4,5,6,7,8,9,10,11,12]

			for month in list_of_months:
				_, num_days = calendar.monthrange(yr, month)

				start = datetime(yr, month, 1)
				end = datetime(yr, month, num_days)

				interviews_dates_info.append(start.strftime('%b'))
				result = getInterviewStatusCountBetweenDates(start.strftime('%d-%b-%Y'),end.strftime('%d-%b-%Y'),user.orgid,role)
				interviews_listed.append(int(result[0]))
				interviews_booked.append(int(result[1]))
				interviews_successful.append(int(result[2]))
				interviews_failed.append(int(result[3]))
				interviews_only_joined.append(int(result[4]))

			interviews_dates_info = appendListedCountWithLabels(interviews_dates_info,interviews_listed,loadBy)

			interviews_info = {'interviews_dates_info':interviews_dates_info, 'interviews_only_joined':interviews_only_joined, 'interviews_booked':interviews_booked, 'interviews_successful':interviews_successful, 'interviews_failed':interviews_failed}
			
			firstDate, lastDate = get_year_day_range(datetime.now())
			
			app.logger.debug("firstDate")
			app.logger.debug(datetime.strftime(firstDate, "%Y-%m-%d"))
			app.logger.debug("lastDate")
			app.logger.debug(datetime.strftime(lastDate, "%Y-%m-%d"))
			# get subscribed min, recording min and recording size
			totalInterviews, totalSubscribeMinutes, totalRecordingTime, totalRecordingSize, totalAsyncInterviews, totalAsyncRecordedTime = getMinutesDetails(datetime.strftime(firstDate, "%Y-%m-%d"),datetime.strftime(lastDate, "%Y-%m-%d"),user.orgid,user.role)

			ret = {'status':True,'interviews_info':interviews_info, 'totalSubscribeMinutes':totalSubscribeMinutes, 'totalRecordingTime':totalRecordingTime, 'totalRecordingSize':totalRecordingSize,'totalInterviews':totalInterviews,'totalAsyncInterviews':totalAsyncInterviews, 'totalAsyncRecordedTime':totalAsyncRecordedTime, 'code':200}
			return ret
			
		else:
			app.logger.info("get Week interviews information - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			
			# find start and end date of current week
			now = datetime.now()
			yr = int(now.strftime('%Y'))
			wk = int(now.strftime('%U'))
			s = ' '
			d = str(yr) + s + str(wk) + s
			f = '%Y' + s + '%U' + s + '%w'
			start = datetime.strptime(d + '0', f).date()
			end = datetime.strptime(d + '6', f).date()
			
			# This is for SESSIONS
			for date in getCurrentWeekDates():
				# date_strf = datetime.strftime(date, "%d-%b-%Y")
				split_date_time =date.split('-')
				format_date = slot.ordinal(split_date_time[0])+" "+split_date_time[1]+" , "+split_date_time[2]
				interviews_dates_info.append(format_date)
				result = getInterviewStatusCountBetweenDates(date,date,user.orgid,role)
				interviews_listed.append(int(result[0]))
				interviews_booked.append(int(result[1]))
				interviews_successful.append(int(result[2]))
				interviews_failed.append(int(result[3]))
				interviews_only_joined.append(int(result[4]))

			interviews_dates_info = appendListedCountWithLabels(interviews_dates_info,interviews_listed,loadBy)

			interviews_info = {'interviews_dates_info':interviews_dates_info, 'interviews_only_joined':interviews_only_joined, 'interviews_booked':interviews_booked, 'interviews_successful':interviews_successful, 'interviews_failed':interviews_failed}

			app.logger.debug("start")
			app.logger.debug(datetime.strftime(start, "%Y-%m-%d"))
			app.logger.debug("end")
			app.logger.debug(datetime.strftime(start, "%Y-%m-%d"))
			
			# get subscribed min, recording min and recording size
			totalInterviews, totalSubscribeMinutes, totalRecordingTime, totalRecordingSize, totalAsyncInterviews, totalAsyncRecordedTime = getMinutesDetails(datetime.strftime(start, "%Y-%m-%d"),datetime.strftime(end, "%Y-%m-%d"),user.orgid,user.role)

			ret = {'status':True,'interviews_info':interviews_info, 'totalSubscribeMinutes':totalSubscribeMinutes, 'totalRecordingTime':totalRecordingTime, 'totalRecordingSize':totalRecordingSize,'totalInterviews':totalInterviews,'totalAsyncInterviews':totalAsyncInterviews, 'totalAsyncRecordedTime':totalAsyncRecordedTime, 'code':200}
			return ret
		

class ApiUpdateArchiveId(RestResource):
	def post(self,interviewid):
		app.logger.info("Accessing ApiUpdateArchiveId")
		clientip = request.remote_addr
		status, user = user_auth()
		if status and user:
			app.logger.debug("User is authorized to access ApiUpdateArchiveId service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")

			app.logger.info("interviewid")
			app.logger.info(interviewid)
			
			inputData = json.loads(request.data)
			app.logger.debug("inputData")
			app.logger.debug(inputData)
			archiveid = inputData['archiveid']
			sessionid = inputData['sessionid']
			app.logger.debug("archiveid")
			app.logger.debug(archiveid)
			
			app.logger.debug("sessionid")
			app.logger.debug(sessionid)
			
			

			interview = Interviews.query.get(interviewid)
			if interview:
				# check session id is same or not
				if interview.sessionid == sessionid:
					if interview.archiveid:
						app.logger.info("archiveid already update - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						app.logger.info("End of ApiUpdateArchiveId - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						return {'status': False, 'message': 'APIUPDATEARCHIVE_ALREADY_UPDATED', 'code':416}
					else:
						interview.archiveid = archiveid
						db.session.commit()
						app.logger.info("archiveid Successfully updated - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						app.logger.info("End of ApiUpdateArchiveId - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						return {'status': True, 'message': 'APIUPDATEARCHIVE_UPDATED_SUCCESS', 'code':200}
				else:
					app.logger.info("both sessionid's are not same - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.info("End of ApiUpdateArchiveId - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {'status': False, 'message': 'APIUPDATEARCHIVE_SESSIONIDS_DIFF', 'code':416}
			else:
				app.logger.info("interview not found - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.info("End of ApiUpdateArchiveId - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': False, 'message': 'COMMON_INTERVIEW_NOT_FOUND', 'code':403}	
		elif user:
			app.logger.debug("User is not authorized to access ApiUpdateArchiveId service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.info("End of ApiUpdateArchiveId - " + str(clientip))
			return {'status': False,  'message': 'COMMON_UNAUTHORIZED', 'code':403}
		else:
			app.logger.info("Invalid token or Session expired - " + str(clientip))
			app.logger.info("End of ApiUpdateArchiveId - " + str(clientip))
			return {'status': False, 'message': 'COMMON_INVALID_TOKEN', 'code':401}


class ApiOrganizationsNames(RestResource):
	def get(self):
		app.logger.info("Accessing ApiOrganizationsNames")
		clientip = request.remote_addr
		status, user = user_auth()
		if status and user:
			app.logger.debug("User is authorized to access ApiOrganizationsNames service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			organizations = "select orgid,name,location from organization order by orgid;"
			organzation_names = db.engine.execute(organizations)
			if organzation_names:
				org_names = OrganizationSchema(partial=('orgid','name','location')).dump(organzation_names,many=True).data
				app.logger.info("Organizations Exist - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': True, 'message': 'APIORGANIZATIONNAMES_SUCCESS', 'code':200,'organization_names':org_names}
			else:
				app.logger.info("No Organization Exist - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': True, 'message': 'COMMON_ORG_NOT_EXIST', 'code':200 }
		elif user:
			app.logger.debug("User is not authorized to access ApiOrganizationsNames service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.info("End of ApiOrganizationsNames - " + str(clientip))
			return {'status': False, 'message': 'COMMON_UNAUTHORIZED', 'code':403}
		else:
			app.logger.info("Invalid token or Session expired - " + str(clientip))
			app.logger.info("End of ApiOrganizationsNames - " + str(clientip))
			return {'status': False, 'message': 'COMMON_INVALID_TOKEN', 'code':401}	


class ApiUser(RestResource):
	def get(self,userid):
		app.logger.info("Accessing ApiUser")
		clientip = request.remote_addr
		status, user = user_auth()
		if status and user:
			app.logger.debug("User is authorized to access ApiUser service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			user_details = User().query.get(userid)
			if user_details:
				user_data = UserBaseSchema().dump(user_details).data
				app.logger.info("User Exist - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': True, 'message': 'APIUSER_SUCCESS', 'code':200,'user_details':user_data}
			else:
				app.logger.info("No User Exist - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': True, 'message': 'APIUSER_USER_NOT_EXIST', 'code':200 }
		elif user:
			app.logger.debug("User is not authorized to access ApiUser service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.info("End of ApiUser - " + str(clientip))
			return {'status': False, 'message': 'COMMON_UNAUTHORIZED', 'code':403}
		else:
			app.logger.info("Invalid token or Session expired - " + str(clientip))
			app.logger.info("End of ApiUser - " + str(clientip))
			return {'status': False, 'message': 'COMMON_INVALID_TOKEN', 'code':401}	


class ApiUploadOrgLogo(RestResource):
	def post(self,orgid):
		app.logger.info("in ApiUploadOrgLogo")
		clientip = request.remote_addr
		status, user = user_auth(admin_access=C.SUPERADMIN)
		if status and user:
			app.logger.debug("request.files")
			app.logger.debug(request.files)
			pass
		elif user:
			app.logger.debug("User is not authorized to access ApiUploadOrgLogo service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.info("end of ApiUploadOrgLogo - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			return {'status': False,  'message': 'COMMON_UNAUTHORIZED', 'code':403}
		else:
			app.logger.info("Invalid token or Session expired - " + str(clientip))
			app.logger.info("end of ApiUploadOrgLogo - " + str(clientip))
			return {'status': False, 'message': 'COMMON_INVALID_TOKEN', 'code':401}
		try:
			organization   = Organization().query.get(orgid)
			if organization:
				pass
			else:
				app.logger.info("Organization does not exist - " + str(clientip))
				app.logger.info("end of ApiUploadOrgLogo - " + str(clientip))
				return {'status': False, 'message': 'COMMON_ORG_NOT_EXIST', 'code':404}
			if request.files:
				app.logger.info("in files  - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				image_file = request.files['organization_logo']
				path = app.config['LOGOS_DIRECTORY']
				app.logger.debug(path)
				try:
					lst = os.listdir(path)
					app.logger.info("List of images  - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.info(lst)
				except OSError:
					app.logger.info("No Image Exist  - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					pass #ignore errors
				else:
					app.logger.info("logo name")
					app.logger.info(str(orgid)+"_"+image_file.filename)
					# update logo name
					image_file.filename = str(orgid)+"_"+image_file.filename.replace(" ", "")
					app.logger.debug("updated file name")
					app.logger.debug(image_file.filename)
					
				if not allowed_image(image_file.filename):
					app.logger.info("end of ApiUploadOrgLogo  - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {'status': False, 'message': 'APIUPLOADORGLOGO_IMG_INVALID_EXTENSION', 'code':400}
				else:
					filename = upload_image(image_file)
				organization.logo = filename
				db.session.commit()
				app.logger.info(filename)
				image_url = os.path.join(app.config['APP_URL']+app.config['LOGOS_DIRECTORY'], filename)
				app.logger.info(image_url)
				app.logger.info("end of ApiUploadOrgLogo  - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': True, 'image_url': image_url, 'code':200}
			else:
				app.logger.info("Error: No form data exits  - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.info("end of ApiUploadOrgLogo  - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': False, 'message': 'APIUPLOADORGLOGO_NO_FORM_DATA', 'code':406}
		except Exception as ee:
			app.logger.info("error while uploading image  - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.info(ee)
			app.logger.info("end of ApiUploadOrgLogo  - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")	
			return {'status': False, 'message': 'APIUPLOADORGLOGO_ERROR', 'code':408}

def upload_image(fl):
	app.logger.info("Accessing upload_image()")
	app.logger.info(fl)
	fl.save(os.path.join(app.config['LOGOS_DIRECTORY'], fl.filename))
	return fl.filename

def allowed_image(filename):
	app.logger.info("Accessing allowed_image()")
	return '.' in filename and filename.rsplit('.', 1)[1] in app.config['ALLOWED_IMAGE_EXTENSIONS']

class ApiGetOrganizationUsageDetails(RestResource):
	def get(self,orgid):
		app.logger.info("in ApiGetOrganizationUsageDetails")
		clientip = request.remote_addr
		status, user = user_auth(admin_access=C.SUPERADMIN)
		if status and user:
			data = request.args
			fromDate = data['fromDate']
			toDate = data['toDate']
			app.logger.info("User is authorized to access ApiGetOrganizationUsageDetails service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			query = 'select count(*) as totalinterviews,sum(subscribeminutes) as subscribeminutes, sum(recordingtime) as recordingtime, sum(recordingsize) as recordingsize from interviews where interviewstatus not in ('+str(C.INTERVIEW_BOOKED) +','+str(C.INTERVIEW_FAILED)+') and orgid = ' + str(orgid) + ' and date >= \''+fromDate+'\' and date <= \''+toDate+'\';'
			app.logger.debug("query")
			app.logger.debug(query)

			results = db.engine.execute(query)

			app.logger.debug("results")
			app.logger.debug(results)
			app.logger.debug(results.rowcount)
			for row in results:
				jsonResults = OrganizationUsageSchema().dump(row, many=False).data
				totalinterviews = jsonResults['totalinterviews']
				subscribeminutes = jsonResults['subscribeminutes']
				recordingtime = jsonResults['recordingtime']
				recordingsize = jsonResults['recordingsize']
				app.logger.debug("subscribeminutes")
				app.logger.debug(subscribeminutes)
				app.logger.debug("recordingtime")
				app.logger.debug(recordingtime)
				app.logger.debug("recordingsize")
				app.logger.debug(recordingsize)

			totalAsyncInterviews = totalAsyncRecordedTime = 0
			
			async_recording_time_query = "select at.recorded_length from asynctest as at where at.asyncid in (select t.interviewid from async_interviewtracking as t where orgid = " + str(orgid) + " and date(activitytime) >= '"+fromDate+"' and date(activitytime) <= '"+toDate+"');"
			async_interviews_count_query = "select distinct interviewid from async_interviewtracking where orgid = " + str(orgid) + " and date(activitytime) >= '"+fromDate+"' and date(activitytime) <= '"+toDate+"';"

			app.logger.info("async_interviews_count_query")
			app.logger.info(async_interviews_count_query)
			app.logger.info("async_recording_time_query")
			app.logger.info(async_recording_time_query)
			
			async_interviews_results = db.engine.execute(async_interviews_count_query)

			asyncInterviewsJsonResults = OrganizationAsyncUsageSchema().dump(async_interviews_results, many=True).data

			async_recording_time_results = db.engine.execute(async_recording_time_query)
			asyncRecordingTimeJsonResults = RecordingTimeOnlySchema().dump(async_recording_time_results, many=True).data
			
			return {'status': True, 'subscribeminutes': subscribeminutes,'recordingtime':recordingtime,'recordingsize':recordingsize,'totalinterviews':totalinterviews,'totalAsyncInterviews':len(asyncInterviewsJsonResults),'totalAsyncRecordedTime':asyncRecordingTimeJsonResults,'code':200}
		elif user:
			app.logger.debug("User is not authorized to access ApiGetOrganizationUsageDetails service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.info("end of ApiGetOrganizationUsageDetails - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			return {'status': False,  'message': 'COMMON_UNAUTHORIZED', 'code':403}
		else:
			app.logger.info("Invalid token or Session expired - " + str(clientip))
			app.logger.info("end of ApiGetOrganizationUsageDetails - " + str(clientip))
			return {'status': False, 'message': 'COMMON_INVALID_TOKEN', 'code':401}


# def getOrganizationContactEmail(user.orgid):

	
def generate_send_reset_password(user):
	app.logger.info("Accessing generate_send_reset_password()")
	rt = user.generate_reset_token()
	user.reset_token = rt
	db.session.commit()
	url = app.config['APP_URL'] + L.reset_password.format(rt=rt)
	meta = {'app_name': app.config['APP_NAME']}

	org_signature,org_logo,contactemail,org_name = organizationSignatureAndLogo(user.orgid)
	content = {'url': url,'signature':org_signature,'logo':org_logo,'contactemail':contactemail}
	uid = user.userid
	if uid:
		user_details = User.query.get(uid)
		if user_details:
			if user_details.password:
				app.logger.info("In reset password")
				em = Email().em_reset_password(meta, user, content)
			else:	
				app.logger.info("In create password")
				app.logger.info(user_details.password)		
				em = Email().em_create_password(meta, user, content)
				app.logger.info(type(em))
				
		else:
			app.logger.info("Unauthorized user")
			app.logger.info("End of generate_send_reset_password()")
			return "Unauthorized user"
	else:
		app.logger.info("Invalid User")
		app.logger.info("End of generate_send_reset_password()")
		return "Invalid User"

	app.logger.info("End of  generate_send_reset_password()")



class ApiSendResetPasswordLink(RestResource):
	def post(self):
		clientip = request.remote_addr
		app.logger.info("Accessing ApiSendResetPasswordLink - "+str(clientip))
		args = parser.parse(ResetPasswordEmail(), request, locations=request_locations)
		user = User.query.filter(and_(func.lower(User.email) == func.lower(args['email'])),or_(User.role==C.ADMIN,User.role==C.SUPERADMIN)).first()
		if not user:
			app.logger.info("Invalid email "+str(args['email'])+" - "+str(clientip))
			app.logger.info("End of ApiSendResetPasswordLink - "+str(clientip))
			return {'status': False, 'message': 'APISENDRESETPASSWORD_EMAIL_NOT_FOUND', 'code':403}
		elif user.role in [C.INTERVIEWER,C.INTERVIEWEE]:
			app.logger.info("Reset password service is not available for user.role:" +str(user.role)+" and email: "+str(args['email'])+" - "+str(clientip))
			return {'status': False, 'message': 'APISENDRESETPASSWORD_SERVICE_NOT_AVAILABLE', 'code':401}

		# try:
		app.logger.info("Sending Email for Reset Password to "+str(user.email)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
		generate_send_reset_password(user)
		app.logger.info("End of ApiSendResetPasswordLink - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
		return {'status': True, 'message': 'APISENDRESETPASSWORD_EMAIL_RESET_SENT', 'code':200}
		# except Exception as emailVerError:
			# app.logger.info("Exception while sending Email for Reset Password to"+str(user.email)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			# app.logger.info(emailVerError)
			# app.logger.info("End of ApiSendResetPasswordLink - "+str(clientip)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			# return {'status': False, 'message': 'APISENDRESETPASSWORD_EMAIL_FAIL', 'code':500}
		
		


class ApiUserResetPassword(RestResource):
	def post(self):
		clientip = request.remote_addr
		app.logger.info("Accessing ApiUserResetPassword - "+str(clientip))
		try:
			args = parser.parse(ResetPassword(), request, locations=request_locations)
			reset_user = User.reset_password(args['reset_token'], args['password'])
		except Exception as e:
			app.logger.info("Exception while reset password - "+str(clientip))
			app.logger.info(e)
			app.logger.info("End of ApiUserResetPassword - "+str(clientip))
			return {'status': False, 'message': 'APISENDRESETPASSWORD_EMAIL_FAIL', 'code':500}
		if not reset_user:
			app.logger.info("Invalid or expired reset token - "+str(clientip))
			app.logger.info("End of ApiUserResetPassword - "+str(clientip))
			return {'status': False, 'message': 'APIUSERRESETPASSWORD_INVALID_RESET_PWDTOKEN', 'code':401}
		try:
			app.logger.info("Committing reset password to database - "+str(clientip))
			db.session.commit()
			app.logger.info("End of ApiUserResetPassword - "+str(clientip))
			return {'status': True, 'message': 'APIUSERRESETPASSWORD_SUCCESS', 'code':200}
		except Exception as sqlEx:
			app.logger.info("Exception while reset password to database - "+str(clientip))
			app.logger.info(sqlEx)
			app.logger.info("End of ApiUserResetPassword - "+str(clientip))
			return {'status': False, 'message': 'APISENDRESETPASSWORD_EMAIL_FAIL', 'code':500}
		


def get_month_day_range(date):
	first_day = date.replace(day = 1)
	last_day = date.replace(day = calendar.monthrange(date.year, date.month)[1])
	return first_day, last_day		

def get_year_day_range(date):
	first_day = date.replace(month = 1 , day = 1)
	last_day = date.replace(month = 12, day = 31)
	return first_day, last_day		


class ApiCreateFeedbackTemplate(RestResource):
	def post(self):
		app.logger.info("in ApiCreateFeedbackTemplate")
		clientip = request.remote_addr
		status,user = user_auth()
		if status and user:
			if user_is_admin(user) or user_is_super_admin(user):
				app.logger.info("User is authorized to access ApiCreateFeedbackTemplate service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				# app.logger.debug("Userid is : " + str(user.userid))
				userid = user.userid		
			else:
				app.logger.debug("User is not authorized to access ApiCreateFeedbackTemplate service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.info("End of ApiCreateFeedbackTemplate - " + str(clientip))
				return {'status': False,  'message': 'COMMON_UNAUTHORIZED', 'code':403}
		else:
			app.logger.info("Invalid token or Session expired - " + str(clientip))
			app.logger.info("End of ApiCreateFeedbackTemplate - " + str(clientip))
			return {'status': False, 'message': 'COMMON_INVALID_TOKEN', 'code':401}
		#bussiness logic
		# app.logger.debug("request.args")
		# app.logger.debug(request.args)
		# app.logger.debug("request.data")
		# app.logger.debug(request.data)
		try:
			args = parser.parse(FeedbackTemplateCreateSchema(), request, locations=request_locations)
		except Exception as e:
			app.logger.info("error while parsing Feedback template details - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.exception(e)
			app.logger.info("End of ApiCreateFeedbackTemplate - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			return {'status': False, 'message': 'APICREATEFEEDBACKTEMPLATE_FIELDS_MISSING', 'code':422}
		app.logger.debug("args")
		app.logger.debug(args)

		feedbackTemplate = FeedbackTemplate()
		title = args['title']
		isTitleValid = validateString(title)
		if isTitleValid:
			#app.logger.debug("Title")
			#app.logger.debug(title)
			app.logger.debug("Title is not Empty - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			feedbackTemplate.title = title
		else:
			app.logger.debug("Title empty "+str(title)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			return {'status': False, 'message': 'Title should not be empty.', 'code':422}
		max_rating = args['max_rating']
		if max_rating > 0:
			feedbackTemplate.max_rating = max_rating
		else:
			app.logger.debug("Max Rating is not Valid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			return {'status': False, 'message': 'Max rating is invalid', 'code':422}
		try:
			categories = json.loads(args['categories'])
			#app.logger.debug("categories")
			#app.logger.debug(categories)
		except Exception as e:
			app.logger.info("categories is not a valid json - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.exception(e)
			app.logger.info("end of ApiCreateFeedbackTemplate - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			return {'status': False, 'message':'APICREATEFEEDBACKTEMPLATE_INVALID_JSON','logo':logo, 'code':422}

		feedbackTemplate.categories = args['categories']
		feedbackTemplate.created_by = userid
		feedbackTemplate.orgid = user.orgid
		feedbackTemplate.created_time = datetime.utcnow()
		feedbackTemplate.status = args['status']
		db.session.add(feedbackTemplate)
		app.logger.debug("Update database - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
		db.session.commit()
		app.logger.info("end of ApiCreateFeedbackTemplate - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
		return {'status': True, 'message': 'APICREATEFEEDBACKTEMPLATE_CREATE_SUCCESSFUL', 'code':201}


class ApiUpdateFeedbackTemplate(RestResource):
	def post(self,ftid):
		app.logger.info("in ApiUpdateFeedbackTemplate")
		clientip = request.remote_addr
		status,user = user_auth()
		if status and user:
			if user_is_admin(user) or user_is_super_admin(user):
				app.logger.info("User is authorized to access ApiUpdateFeedbackTemplate service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				# app.logger.debug("Userid is : " + str(user.userid))
				userid = user.userid		
			else:
				app.logger.debug("User is not authorized to access ApiUpdateFeedbackTemplate service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.info("End of ApiUpdateFeedbackTemplate - " + str(clientip))
				return {'status': False,  'message': 'COMMON_UNAUTHORIZED', 'code':403}
		else:
			app.logger.info("Invalid token or Session expired - " + str(clientip))
			app.logger.info("End of ApiUpdateFeedbackTemplate - " + str(clientip))
			return {'status': False, 'message': 'COMMON_INVALID_TOKEN', 'code':401}

		feedbackTemplate = FeedbackTemplate().query.get(ftid)
		if (user.orgid != feedbackTemplate.orgid):
			app.logger.debug("User is not authorized to update this template - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.info("End of ApiUpdateFeedbackTemplate - " + str(clientip))
			return {'status': False,  'message': 'UNAUTHORIZE_TO_UPDATE_THISFEEDBACK', 'code':403}
			
		try:
			args = parser.parse(FeedbackTemplateCreateSchema(), request, locations=request_locations)
		except Exception as e:
			app.logger.info("error while parsing Feedback template details - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.exception(e)
			app.logger.info("End of ApiUpdateFeedbackTemplate - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			return {'status': False, 'message': 'APIUPDATEFEEDBACKTEMPLATE_FIELDS_MISSING', 'code':422}
		app.logger.debug("args")
		app.logger.debug(args)

		title = args['title']
		isTitleValid = validateString(title)
		if isTitleValid:
			#app.logger.debug("Title")
			#app.logger.debug(title)
			app.logger.debug("Title is not Empty - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			feedbackTemplate.title = title
		else:
			app.logger.debug("Title empty "+str(title)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			return {'status': False, 'message': 'Title should not be empty.', 'code':422}
		max_rating = args['max_rating']
		if max_rating > 0:
			feedbackTemplate.max_rating = max_rating
		else:
			app.logger.debug("Max Rating is not Valid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			return {'status': False, 'message': 'Max rating is invalid', 'code':422}
		try:
			categories = json.loads(args['categories'])
			#app.logger.debug("categories")
			#app.logger.debug(categories)
		except Exception as e:
			app.logger.info("categories is not a valid json - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.exception(e)
			app.logger.info("end of ApiUpdateFeedbackTemplate - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			return {'status': False, 'message':'ApiUpdateFeedbackTemplate_INVALID_JSON','logo':logo, 'code':422}

		feedbackTemplate.categories = args['categories']
		feedbackTemplate.updated_by = userid
		feedbackTemplate.orgid = user.orgid
		feedbackTemplate.updated_time = datetime.utcnow()
		feedbackTemplate.status = args['status']
		app.logger.debug("feedbackTemplate.status")
		app.logger.debug(feedbackTemplate.status)
		db.session.add(feedbackTemplate)
		app.logger.debug("Update database - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
		db.session.commit()
		app.logger.info("end of ApiUpdateFeedbackTemplate - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
		return {'status': True, 'message': 'APIUPDATEFEEDBACKTEMPLATE_UPDATE_SUCCESSFUL', 'code':201}


class ApiFeedbackTemplateList(RestResource):
	def get(self):
		app.logger.info("in ApiFeedbackTemplateList")
		clientip = request.remote_addr
		status,user = user_auth()
		if status and user:
			if user_is_admin(user):
				app.logger.info("User is Admin & authorized to access ApiFeedbackTemplateList service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				role=2
			elif user_is_super_admin(user):
				app.logger.info("User is Super Admin & authorized to access ApiFeedbackTemplateList service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				role=1
			else:
				app.logger.debug("User is not authorized to access ApiFeedbackTemplateList service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.info("End of ApiFeedbackTemplateList - " + str(clientip))
				return {'status': False,  'message': 'COMMON_UNAUTHORIZED', 'code':403}
		else:
			app.logger.info("Invalid token or Session expired - " + str(clientip))
			app.logger.info("End of ApiFeedbackTemplateList - " + str(clientip))
			return {'status': False, 'message': 'COMMON_INVALID_TOKEN', 'code':401}
		#bussiness logic
		orgid = user.orgid
		if (role==1):
			query="select * from feedback_template where orgid = " + str(orgid) +";"
		else:
			superadmin = User.query.filter(User.role == C.SUPERADMIN).first()
			query = "select * from feedback_template where orgid = "+str(orgid) +" or (orgid = " + str(superadmin.orgid) + "and status = true);"

		app.logger.debug("final query")
		app.logger.debug(query)
		result = db.engine.execute(query)
		listOfFeedbackTemplate = FeedbackTemplateUpdateSchema(partial=('ftid','title','categories','max_rating','status')).dump(result, many=True).data
		app.logger.debug("listOfFeedbackTemplate")
		app.logger.debug(listOfFeedbackTemplate)
		if len(listOfFeedbackTemplate) == 0 :
			app.logger.debug("no templates created for this organization - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.info("end of ApiFeedbackTemplateList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			return {'status': True, 'message': 'FEEDBACK_TEMPLATES_NOT_EXIST', 'feedback_template_list':[], 'code':200}
		app.logger.info("end of ApiFeedbackTemplateList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
		return {'status': True, 'message': 'FEEDBACK_TEMPLATES_FETCH_SUCCESS', 'feedback_template_list':listOfFeedbackTemplate, 'code':200}

			
def allinterviewDetails(interviewid,clientip,user):
	app.logger.info("Accessing allinterviewDetails")
	interview = Interviews.query.get(interviewid)
	
	#Add rdt to calculate remaining time of a interview .. Used to end the interview based on Server Time exactly
	if not interview:
		app.logger.info("Interview not found - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
		app.logger.info("End of allinterviewDetails - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
		return None,{'status': False, 'message': 'COMMON_INTERVIEW_NOT_FOUND', 'code':404},False
	
	logo = getOrgLogo(interview.orgid)
	duration = interview.duration * 60

	
	regular_interviewer=[]
	primary_interviewer =[]
	interviewers_list=[]
	interviewer_details =[]
	candidate_details =[]
	user_details=[]
	interview_interviewer = Interview_Interviewer().query.filter(and_(Interview_Interviewer.interviewid == interview.interviewid,Interview_Interviewer.active_status == True)).all()
	if interview_interviewer:
		for i in interview_interviewer:
			app.logger.info(i)
			interviewers_list = User().query.get(i.interviewerid)
			
			if interviewers_list:
				if i.isprimary == True:
					user_details.append({'primary_interviewerid':i.interviewerid})
					primary_interviewer.append({'firstname':interviewers_list.firstname,'lastname':interviewers_list.lastname,'mobile':interviewers_list.mobile,'email':interviewers_list.email})
				else:
					user_details.append({'interviewerid':i.interviewerid})
					regular_interviewer.append({'firstname':interviewers_list.firstname,'lastname':interviewers_list.lastname,'mobile':interviewers_list.mobile,'email':interviewers_list.email})
	else:
		app.logger.info("Interviewer not found - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
		app.logger.info("End of allinterviewDetails - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
		return None,{'status': False, 'message': 'COMMON_INTERVIEWER_NOT_FOUND', 'code':404},False

	interview_candidate = Interview_Candidate().query.filter(and_(Interview_Candidate.interviewid == interview.interviewid,Interview_Candidate.active_status == True)).first()
	candidate_data =[]
	app.logger.info("interview_candidate")
	app.logger.info(interview_candidate)
	if interview_candidate:
		candidate_details = User().query.get(interview_candidate.candidateid)
		app.logger.info("candidate_details in interview_candidate")
		app.logger.info(candidate_details)
		if candidate_details:
			user_details.append({'candidateid':candidate_details.userid})
			candidate_data.append({'firstname':candidate_details.firstname,'lastname':candidate_details.lastname,'mobile':candidate_details.mobile,'email':candidate_details.email})
		else: 
			app.logger.info("Interviewer not found - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.info("End of allinterviewDetails - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			return None,{'status': False, 'message': 'COMMON_CANDIDATE_NOT_FOUND', 'code':404},False				

	cc_result,cc_status,status = getCcDataByInterviewidByOrgidByInterviewtype(intid=interview.interviewid,orgid=interview.orgid,interview_type=C.INTERVIEW_TYPE_FACETOFACE)
	
	interview_date ={'date':interview.date}
	date_result = InterviewDetailsSchema().dump(interview_date).data
	interviewDateTime = getInterviewTimeByDateSlotid(interview.date,interview.slot_id)
	interview_list ={'primaryinterviewer':primary_interviewer,'interviewer_list':regular_interviewer,'candidate_data':candidate_data,'user_details':user_details, 'interviewid': interview.interviewid,'interviewtitle': interview.interviewtitle,'ftid':interview.ftid,'interiewstatus':interview.interviewstatus,'date':date_result,'slot_id':interview.slot_id,'duration':interview.duration,'interviewdescription':interview.interviewdescription,'recording':interview.prerecording_enabled,'interviewtime':interviewDateTime,'custom_categories_data':cc_result,'cc_status':cc_status}
	
	app.logger.info("End of allinterviewDetails - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
	return interview_list ,None,True


def calculateRating(feedback_obj_dumps):
	final_namearray = []
	final_ratingarray = []
	temp_list = []
	masked_arrays = []
	final_array=[]
	avg_array = []
	lenlist = []
	for i in feedback_obj_dumps:
			feedback_dumps = json.dumps(i)
			feedback_json = json.loads(feedback_dumps,object_pairs_hook=OrderedDict)
			for k,v in feedback_json.items():
				json_obj = json.loads(v)
				ratingarray=[]
				namearray=[]
				for obj in json_obj:
					for i,k in obj.items():
						if i == 'name':
							namearray.append(k)
						if i == 'rating':
							ratingarray.append(k)

				final_ratingarray.append(ratingarray)
				final_namearray.append(namearray)

	for i in final_ratingarray:
		lenlist.append(len(i))
	max = np.amax(lenlist)

	for i in final_ratingarray:
		if len(i) <= max:
			for j in range(max - len(i)):
				i.append(None)

	# and add it to temp_array 
	for j in range(max):
		for i in range(len(final_ratingarray)):
			temp_list.append(final_ratingarray[i][j])
		masked_arrays.append(ma.masked_values(temp_list, None))
		del temp_list[:]

	# Avg of each array 
	for i in masked_arrays:
		avg_array.append(np.ma.average(i))

	names = final_namearray[0]

	for i in range(0,len(names)):
		final_array.append({"name" : names[i], "rating" : math.floor(avg_array[i])})

	return final_array

class ApiInterviewFeedbackDownload(RestResource):
	def get(self, interviewid, tempTimeStampVar):
		# tempTimeStampVar - This variable is used to differentiate the requests in Browser level, if you make contionus calls then Browser cant identify without a unique variable in URL
		clientip = request.remote_addr
		app.logger.info("Accessing ApiInterviewFeedbackDownload - "+str(clientip))
		status, user = user_auth(admin_access=C.ADMIN)
		if status and user:
			app.logger.debug("User is authorized to access ApiInterviewFeedbackDownload service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			interview = Interviews().query.get(interviewid)
			if interview:
				app.logger.debug("Interviews Exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				feedbackdetails_query = "select i.interviewid,i.interviewer_id,(select email from users as u where u.userid=i.interviewer_id) as email,(select firstname from users as u where u.userid=i.interviewer_id) as firstname,(select lastname from users as u where u.userid=i.interviewer_id) as lastname,(select ii.isprimary from interview_interviewer as ii where ii.interviewid="+str(interviewid)+" and ii.interviewerid=i.interviewer_id and ii.active_status=true) from feedback as i where i.interviewid=" + str(interviewid) +";"
				app.logger.info("feedbackdetails_query - "+str(clientip)+" - [ "+str(user.userid)+" ] ")
				app.logger.info(feedbackdetails_query)
				try:
					result = db.engine.execute(feedbackdetails_query) 
					interviewerDetailsWithFeedbackList = InterviewerDetailsWithFeedbackSchema(partial=('email','firstname','lastname','interviewer_id','interviewid','isprimary')).dump(result, many=True).data
					app.logger.debug(interviewerDetailsWithFeedbackList)
					meta = {'app_name': app.config['APP_NAME']}
		
					time_slot=['00:00am','00:30am','01:00am','01:30am','02:00am','02:30am','03:00am','03:30am','04:00am','04:30am','05:00am','05:30am','06:00am','06:30am','07:00am','07:30am','08:00am','08:30am','09:00am','09:30am','10:00am','10:30am','11:00am','11:30am','12:00pm','12:30pm','01:00pm','01:30pm','02:00pm','02:30pm','03:00pm','03:30pm','04:00pm','04:30pm','05:00pm','05:30pm','06:00pm','06:30pm','07:00pm','07:30pm','08:00pm','08:30pm','09:00pm','09:30pm','10:00pm','10:30pm','11:00pm','11:30pm']
					time_slots= time_slot[interview.slot_id-1]
					format_date_only = slot.get_date_and_time_format_email(interview.date,interview.slot_id,interview.timezone)
					app.logger.debug("interviewerDetailsWithFeedbackList - "+str(clientip)+" - [ "+str(user.userid)+" ] ")
					app.logger.debug(interviewerDetailsWithFeedbackList)					
					
					if interviewerDetailsWithFeedbackList:
						app.logger.info("After Feedback Dictionary Conversion - "+str(clientip)+" - [ "+str(user.userid)+" ] ")
						feedback_query = "select feedback from feedback where interviewid =" +str(interviewid)
						app.logger.debug("feedback_query- "+str(clientip)+" - [ "+str(user.userid)+" ] ")
						app.logger.debug(feedback_query)
						feedbacks_result = db.engine.execute(feedback_query)
						feedbacks = FeedbacksSchema().dump(feedbacks_result, many=True).data
						app.logger.debug("feedbacks- "+str(clientip)+" - [ "+str(user.userid)+" ] ")
						feedback_obj_dumps = json.dumps(feedbacks)
						app.logger.debug("feedback_obj_dumps- "+str(clientip)+" - [ "+str(user.userid)+" ] ")
						app.logger.debug(feedback_obj_dumps)
						feedback_data = calculateRating(feedbacks)
						app.logger.debug("feedback_data- "+str(clientip)+" - [ "+str(user.userid)+" ] ")
						app.logger.debug(feedback_data)
						feedback_obj = json.loads(json.dumps(feedback_data), object_pairs_hook=OrderedDict)
						org_details = Organization().query.get(user.orgid)
						org_data= {'name':org_details.name,'address':org_details.address,'location':org_details.location,'country':org_details.country}
						org_logo_url = app.config['LOGOS_DIRECTORY'] +str(org_details.logo)
						candidate_query = "select i.interviewid,i.candidateid,(select email from users as u where u.userid=i.candidateid) as email,(select firstname from users as u where u.userid=i.candidateid) as firstname,(select lastname from users as u where u.userid=i.candidateid) as lastname from interview_candidate as i where i.interviewid=" + str(interviewid) +" and i.active_status = true;"
						candidate_result = db.engine.execute(candidate_query)
						candidate_details = InterviewCandidateDetailsSchema(partial=('lastname','email','interviewid','candidateid','firstname')).dump(candidate_result, many=True).data
						
						app.logger.debug("feedback_obj - "+str(clientip)+" - [ "+str(user.userid)+" ] ")
						app.logger.debug(feedback_obj)

						app.logger.debug("candidate_details - "+str(clientip)+" - [ "+str(user.userid)+" ] ")
						app.logger.debug(candidate_details)
						
						try:
							html = render_template('feedback_candidate.html', meta=meta, org_logo_url= org_logo_url,org_data = org_data, session=interview,feedback=feedback_obj,details=interviewerDetailsWithFeedbackList, user=user, timeDate=time_slots ,format_date_only = format_date_only,candidate_details=candidate_details)
							app.logger.debug("feedback_candidate")
							app.logger.debug(html)

							pdf_feedback_candidate = pdf.htmltopdf(html)
							app.logger.debug("pdfsio - "+str(clientip)+" - [ "+str(user.userid)+" ] ")
							app.logger.debug(pdf_feedback_candidate)
							pdf_feedback_candidate.seek(0)
							fn = 'interview_feedback{}.pdf'.format(interview.interviewid)
							return send_file(pdf_feedback_candidate, mimetype='application/pdf', as_attachment=True, attachment_filename=fn)
						except Exception as et:
							app.logger.info("Error while generating pdf - "+str(clientip)+" - [ "+str(user.userid)+" ] ")
							app.logger.info(et)
							e2 = sys.exc_info()[0]
							app.logger.info(e2)
							app.logger.info("End of ApiInterviewFeedbackDownload - "+str(clientip)+" - [ "+str(user.userid)+" ] ")
							return {'status': False, 'message': 'APIINTERVIEWFEEDBACKDOWNLOAD_PDF_NOT_GENERATE','code':422}
					else:
						app.logger.info("Provider notes not available - "+str(clientip)+" - [ "+str(user.userid)+" ] ")
						app.logger.info("End of ApiInterviewFeedbackDownload - "+str(clientip)+" - [ "+str(user.userid)+" ] ")
						return {'status': False, 'message': 'APIINTERVIEWFEEDBACKDOWNLOAD_PDF_NOT_PROVIDED','code':422}
			
				except Exception as e:
					app.logger.info(e)
					return {'status': False, 'message': 'APIINTERVIEWFEEDBACKDOWNLOAD_PDF_NOT_GENERATE','code':422}
			else:
				app.logger.info("Interview not found - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.info("End of ApiInterviewFeedbackDownload - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': False, 'message': 'COMMON_INTERVIEW_NOT_FOUND', 'code':404}
				
			return {'status': True, 'message': 'APIINTERVIEWDETAILS_NOT_EXIST', 'code':200}	
		elif user:
			app.logger.debug("User is not authorized to access ApiInterviewFeedbackDownload service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.info("End of ApiInterviewFeedbackDownload - " + str(clientip))
			return {'status': False,  'message': 'COMMON_UNAUTHORIZED', 'code':403}
		else:
			app.logger.info("Invalid token or Session expired - " + str(clientip))
			app.logger.info("End of ApiInterviewFeedbackDownload - " + str(clientip))
			return {'status': False, 'message': 'COMMON_INVALID_TOKEN', 'code':401}

class ApiGetInterviewDetails(RestResource):
	def get(self, interviewid):
		app.logger.info("Accessing ApiGetInterviewDetails")
		clientip = request.remote_addr
		status, user = user_auth(admin_access=C.ADMIN)
		if status and user:
			app.logger.debug("User is authorized to access ApiGetInterviewDetails service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			details,error,status = allinterviewDetails(interviewid,clientip,user)
			if status:
				return details,200
			else:
				return error
		elif user:
			app.logger.debug("User is not authorized to access ApiGetInterviewDetails service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.info("End of ApiGetInterviewDetails - " + str(clientip))
			return {'status': False,  'message': 'COMMON_UNAUTHORIZED', 'code':403}
		else:
			app.logger.info("Invalid token or Session expired - " + str(clientip))
			app.logger.info("End of ApiGetInterviewDetails - " + str(clientip))
			return {'status': False, 'message': 'COMMON_INVALID_TOKEN', 'code':401}


def getInterviewTimeByDateSlotid(interviewDate,slotid):
	slottime =  slot.sd[slotid]
	if slottime[1] == 0:
		min = '00'
	else:
		min = '30'
	slotTime = str(interviewDate) + " " + str(slottime[0]) + ":" + min + " UTC"
	return slotTime


class ApiInterviewStartRecording(RestResource):
	def post(self, sessionid):
		app.logger.info("Accessing ApiInterviewStartRecording ")
		clientip = request.remote_addr
		status,user = user_auth(admin_access=C.INTERVIEWER)
		app.logger.info("status and user")
		app.logger.info(status)
		app.logger.info(user)
		permissionToStartRecording = True
		permissionToStopRecording = True
		if user and status and sessionid:
			app.logger.debug("User is authorized to access ApiInterviewStartRecording service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			try:
				app.logger.info("check interview by interviewid")
				interview_id = request.args.get('interviewid')
			except Exception as exp:
				app.logger.info("interviewid not found in request - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.info("End of ApiInterviewStartRecording - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {"status":False, "message":"APIINTERVIEWSTARTRECORDING_INTERVIEWID_NOT_FOUND",'code':422,'isStartRecording':False}

			try:
				interview = Interviews().query.get(interview_id)
				# interview = Interviews().query.filter(Interviews.sessionid == sessionid).first()
				if interview:
					app.logger.info("interview "+str(interview.interviewid)+" found")
					app.logger.info("check archiveid exist or not")
					if interview.archiveid:
						app.logger.info("archive id found "+str(interview.archiveid))
						archiveDetails = ot.get_archive(interview.archiveid)
						archiveDetailsInJson = json.loads(archiveDetails.json())
						app.logger.info("archive started/paused")
						app.logger.info(archiveDetailsInJson)
						app.logger.info(type(archiveDetailsInJson))
						app.logger.info(archiveDetailsInJson["status"])

						if archiveDetailsInJson["status"] == 'started' or archiveDetailsInJson["status"] == 'paused':
							app.logger.info("we can give permission to stop archive")
							permissionToStartRecording = False
							permissionToStopRecording = True
							app.logger.info("no permission to start interview")
							app.logger.info("this interview already STARTED RECORDING")
							app.logger.info("End of ApiInterviewStartRecording - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							return {"status":False, "message":"APIINTERVIEWSTARTRECORDING_ALREADY_STARTED_RECORDING",'code':489,'isStartRecording':permissionToStartRecording, 'permissionToStopRecording': permissionToStopRecording}
						else:
							app.logger.info("we cann't give permission to start archive")
							permissionToStartRecording = False
							permissionToStopRecording = False
							app.logger.info("no permission to start interview")
							app.logger.info("this interview already STOPPED RECORDING")
							app.logger.info("End of ApiInterviewStartRecording - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							return {"status":False, "message":"APIINTERVIEWSTARTRECORDING_ALREADY_RECORDED",'code':489,'isStartRecording':permissionToStartRecording, 'permissionToStopRecording': permissionToStopRecording}						
					else:
						app.logger.info("archiveid not found")
						permissionToStartRecording = True
						permissionToStopRecording = False
				else:
					app.logger.info("end of ApiInterviewStartRecording - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.info("Interview not exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {'status':False,'message':'APIINTERVIEWSTARTRECORDING_INTERVIEW_NOT_EXISTS','code':422,'isStartRecording':False}			
				
			except Exception as ote:
				app.logger.info("Issue with Opentok server connection - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.info("End of ApiInterviewStartRecording - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {"status":False, "message":"APIINTERVIEWSTARTRECORDING_INTERVIEW_NOT_EXISTS",'code':422,'isStartRecording':False}
			


			if permissionToStartRecording:
				app.logger.info("Proceed to start recording")
			else:
				app.logger.info("no permission to start recording")
				app.logger.info("this interview already recorded")
				app.logger.info("End of ApiInterviewStartRecording - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {"status":False, "message":"APIINTERVIEWSTARTRECORDING_ALREADY_STARTED_RECORDING",'code':422,'isStartRecording':False}

			try:
				archiveId=ot.startRecording(sessionid)
				app.logger.info("archiveId")
				app.logger.info(archiveId)
				if archiveId:
					pass
				else:
					app.logger.info("Issue with Opentok server connection - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.info("End of ApiInterviewStartRecording - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {"status":False, "message":"APIINTERVIEWSTARTRECORDING_CONNECTION_ISSUE_WITH_OPENTOK",'code':422,'isStartRecording':False}
			except Exception as e:
				app.logger.info("error while start the recording using opentok")
				app.logger.info(e)
				app.logger.info("End of ApiInterviewStartRecording - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': False, 'message': 'APIINTERVIEWSTARTRECORDING_CONNECTION_ISSUE_WITH_OPENTOK','code':422,'isStartRecording':False}

			try:
				app.logger.info("now update to interview table with archive id")
				interview.prerecording_enabled = True
				if interview.archiveid is None:
					interview.archiveid=archiveId
					db.session.commit()
					app.logger.info("Save changes to interview table - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")				
				else:
					app.logger.info("End of ApiInterviewStartRecording - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {'status':True,'code':202,'message':'APIINTERVIEWSTARTRECORDING_SUCCESS','archiveid': archiveId,'isStartRecording':True}, 202
				
				app.logger.info("now update to interview tracking table with event start interview")
				interview_tracking = InterviewTracking()
				interview_tracking.interviewid = interview_id
				interview_tracking.orgid =  interview.orgid
				interview_tracking.activity = C.INTERVIEW_TRACKING_RECORDING_STARTED
				interview_tracking.activitytime = datetime.utcnow()
				db.session.add(interview_tracking)
				
				db.session.commit()
				app.logger.info("interview_tracking")
				app.logger.info(interview_tracking)
					
				app.logger.info("End of ApiInterviewStartRecording - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status':True,'code':202,'message':'APIINTERVIEWSTARTRECORDING_SUCCESS','archiveid': archiveId,'isStartRecording':True}, 202
				
			except IntegrityError as ie:
				app.logger.info("Error: Database related - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.error(ie)
				app.logger.error(ie.message)
				app.logger.info("End of ApiInterviewStartRecording - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': False, 'message': 'COMMON_ERROR_CONTACT_SUPPORT', 'code':409,'isStartRecording':False}
			except InvalidRequestError as ir:
				
				app.logger.info("received invalid request - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.exception(ir)
				app.logger.error(ir.message)
				app.logger.info("End of ApiInterviewStartRecording - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': False, 'message': 'COMMON_ERROR_INVALID_REQUSET', 'code':422,'isStartRecording':False}
			except Exception as e:
				app.logger.info("error while updating details")
				app.logger.info(e)
				app.logger.info("End of ApiInterviewStartRecording - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': False, 'message': 'APIINTERVIEWSTARTRECORDING_STARTED','code':422,'isStartRecording':False}
		elif user:
			app.logger.debug("User is not authorized to access ApiGetInterviewDetails service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.info("End of ApiInterviewStartRecording - " + str(clientip))
			return {'status': False,  'message': 'COMMON_UNAUTHORIZED', 'code':403}
		else:
			app.logger.info("Invalid token or Session expired - " + str(clientip))
			app.logger.info("End of ApiInterviewStartRecording - " + str(clientip))
			return {'status': False, 'message': 'COMMON_INVALID_TOKEN', 'code':401,'isStartRecording':False}



class ApiInterviewStopRecording(RestResource):
	def post(self, archiveid):
		app.logger.info("Accessing ApiInterviewStopRecording ")
		clientip = request.remote_addr
		status, user = user_auth(admin_access=C.INTERVIEWER)
		if user and status:
			app.logger.debug("User is authorized to access ApiInterviewStopRecording service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			
			try:
				app.logger.info("check interview by interviewid")
				interview_id = request.args.get('interviewid')
			except Exception as exp:
				app.logger.info("interviewid not found in request - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.error(exp)
				app.logger.info("End of ApiInterviewStopRecording - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {"status":False, "message":"APIINTERVIEWSTOPRECORDING_INTERVIEWID_NOT_FOUND",'code':422,'isStopRecording':False}

			try:
				app.logger.info("find interview by interview_id")
				interview = Interviews().query.get(interview_id)
				# interview = Interviews().query.filter(Interviews.sessionid == sessionid).first()
				if interview:
					app.logger.info("interview "+str(interview.interviewid)+" found")
					app.logger.info("check archiveid exist or not")
					if interview.archiveid:
						app.logger.info("archive id found "+str(interview.archiveid))
						archiveDetails = ot.get_archive(interview.archiveid)
						archiveDetailsInJson = json.loads(archiveDetails.json())
						app.logger.info(archiveDetailsInJson)
						app.logger.info("check archive status")
						app.logger.info(archiveDetailsInJson["status"])

						if archiveDetailsInJson["status"] == 'started' or archiveDetailsInJson["status"] == 'paused':
							app.logger.info("we can give permission to stop archive")
							permissionToStartRecording = False
							permissionToStopRecording = True
						else:
							app.logger.info("we cann't give permission to start archive")
							permissionToStartRecording = False
							permissionToStopRecording = False
							app.logger.info("no permission to stop interview")
							app.logger.info("this recording already stopped")
							app.logger.info("End of ApiInterviewStopRecording - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							return {"status":False, "message":"APIINTERVIEWSTOPRECORDING_ALREADY_STOPPED",'code':422,'isStopRecording':False}
					else:
						app.logger.info("archiveid not found")
						permissionToStartRecording = True
						permissionToStopRecording = False
						app.logger.info("no permission to stop interview")
						app.logger.info("existing recording not found")
						app.logger.info("End of ApiInterviewStopRecording - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						return {"status":False, "message":"APIINTERVIEWSTOPRECORDING_RECORDING_NOT_FOUND",'code':422,'isStopRecording':False}
				else:
					app.logger.info("end of ApiInterviewStopRecording - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.info("Interview not exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {'status':False,'message':'APIINTERVIEWSTOPRECORDING_INTERVIEW_NOT_EXISTS','code':422,'isStopRecording':False}			
				
			except Exception as ote:
				app.logger.info("error while getting interview details")
				app.logger.error(ote)
				app.logger.info("End of ApiInterviewStopRecording - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {"status":False, "message":"APIINTERVIEWSTOPRECORDING_DB_ERROR",'code':422,'isStopRecording':False}
			


			if permissionToStopRecording: 
				app.logger.info("Proceed to stop recording")
			else:
				app.logger.info("no permission to stop recording")
				app.logger.info("this interview already recorded")
				app.logger.info("End of ApiInterviewStopRecording - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {"status":False, "message":"APIINTERVIEWSTOPRECORDING_NO_PERMISSION_TO_STOP",'code':422,'isStopRecording':False}
			
			app.logger.info("now stop the recording")
			
			try:
				archive_id=ot.stopRecording(archiveid)
			except Exception as ote:
				app.logger.info("Issue with Opentok server connection - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.error(ote)
				app.logger.info("end of ApiInterviewStopRecording - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {"status":False, "message":"APIINTERVIEWSTOPRECORDING_CONNECTION_ISSUE_WITH_OPENTOK",'code':422,'isStopRecording':False}
			app.logger.info("update stop recording status in db")
			try:
				if archive_id:
					interview_tracking = InterviewTracking()
					interview_tracking.interviewid = interview_id
					interview_tracking.orgid =  interview.orgid
					interview_tracking.activity = C.INTERVIEW_TRACKING_RECORDING_STOPPED
					interview_tracking.activitytime = datetime.utcnow()
					db.session.add(interview_tracking)
					db.session.commit()
					
					app.logger.info("Recording stopped successfully - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.info("End of ApiInterviewStopRecording - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {"status":True, "message":"APIINTERVIEWSTOPRECORDING_SUCCESS",'code':202,'archiveid':archive_id,'isStopRecording':True}		
				else:
					app.logger.info("Issue with Opentok server connection - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.info("end of ApiInterviewStopRecording - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {"status":False, "message":"APIINTERVIEWSTOPRECORDING_NO_RECORDING_FOUND",'code':422,'isStopRecording':False}
			except Exception as e:
				app.logger.info("error while updating tracking details")
				app.logger.info(e)
				app.logger.info("End of ApiInterviewStopRecording - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': False, 'message': 'APIINTERVIEWSTOPRECORDING_STOPPED','code':422,'isStopRecording':False}
		elif user:
			app.logger.debug("User is not authorized to access ApiGetInterviewDetails service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.info("End of ApiInterviewStopRecording - " + str(clientip))
			return {'status': False,  'message': 'COMMON_UNAUTHORIZED', 'code':403}
		else:
			app.logger.info("Invalid token or Session expired - " + str(clientip))
			app.logger.info("End of ApiInterviewStopRecording - " + str(clientip))
			return {'status': False, 'message': 'COMMON_INVALID_TOKEN', 'code':401,'isStopRecording':False}

def validateQuestions(questions_list,user,clientip):
	app.logger.info("Accessing validateQuestions")
	list_of_questions_with_duration = []
	if len(questions_list) > 0:
		for question_data in questions_list:
			app.logger.info("question_data")
			app.logger.info(question_data)
			question = question_data['question']
			duration = question_data['duration']
			# Question validation
			isQuestionExists = validateString(question)
			if isQuestionExists:
				app.logger.debug("question not empty "+str(question)+" - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
			else:
				app.logger.debug("question empty "+str(question)+" - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
				raise emptyStringError()

			# Duration validation
			if duration:
				try:
					app.logger.debug("duration is digit "+str(duration)+" - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
					duration_conversion = long(duration)
					if duration_conversion > 0:
						app.logger.debug("duration is valid digit "+str(duration)+" - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
					else:
						app.logger.debug("duration is not a valid digit "+str(duration)+" - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
						return None,{'code':422,'status':'failed','message':'APIASYNCINTERVIEWESCHEDULE_QUESTIONS_DURATION_NOT_VALID_DIGIT'},False
				except Exception as de:
					app.logger.debug("duration is not a valid digit "+str(duration)+" - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
					return None,{'code':422,'status':'failed','message':'APIASYNCINTERVIEWESCHEDULE_QUESTIONS_DURATION_NOT_VALID_DIGIT'},False
			else:
				app.logger.debug("duration not digit "+str(duration)+" - " + str(clientip) + "- [ " + str(user['userid'])+" : "+str(user['orgid'])+" ]")
				return None,{'code':422,'status':'failed','message':'APIASYNCINTERVIEWESCHEDULE_QUESTIONS_DURATION_NOT_DIGIT'},False

			# Creating question to Question table if qustions not exists		
			question_result,question_error,question_creation_status = createQuestion(question,user)
			if question_creation_status:
				app.logger.info("question_result")
				app.logger.info(question_result)
				list_of_questions_with_duration.append({'question_id':question_result.asyncqid,'question_duration':duration})
			else:
				app.logger.info("question_error")
				return None,question_error,False
		
		app.logger.debug("End of validateQuestions")
		app.logger.debug("list_of_questions_with_duration")
		app.logger.debug(list_of_questions_with_duration)
		return  list_of_questions_with_duration,None,True
	else:
		app.logger.info("Questions list not exists")
		return None,{'code':422,'status':'failed','message':'APIASYNCINTERVIEWESCHEDULE_QUESTIONS_LIST_DATA_MISSING'},False

def createQuestion(question,user):
	app.logger.info("Accessing createQuestion")
	question_availability = AsyncQuestions().query.filter(AsyncQuestions.question == question,AsyncQuestions.orgid == user['orgid']).first()
	if question_availability:
		app.logger.info("Question already exits")
	else:
		app.logger.info("Adding new question to DB")
		question_availability = AsyncQuestions()
		question_availability.question = question
		question_availability.orgid = user['orgid']
		question_availability.createdby = user['userid']
		question_availability.createdtime = datetime.utcnow()

		# Adding row to session obj
		db.session.add(question_availability)
		try:
			db.session.commit()
		except Exception as e:
			app.logger.info(" Exception while adding Questions")
			return None,{'status':'success','code':422,'message':'APIASYNCINTERVIEWESCHEDULE_NEW_QUESTION_ADDING'},False

	app.logger.info("End of createQuestion")
	return question_availability,None,True


class ApiAsyncInterviewSchedule(RestResource):
	def post(self):
		app.logger.info("Accessing ApiAsyncInterviewSchedule")
		clientip = request.remote_addr
		status, user = user_auth(admin_access=C.ADMIN)
		if status and user:
			app.logger.debug("User is authorized to access ApiAsyncInterviewSchedule service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			try:
				args = parser.parse(AsyncInterviewScheduleSchema(), request, locations=request_locations)
				app.logger.debug("args")
				app.logger.debug(args)
				candidate_fname = args['candidate_firstname']
				app.logger.debug("Candidate fname exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				isCandidateFirstName = validateString(args['candidate_firstname'])
				if isCandidateFirstName:
					app.logger.debug("Candidate_fname not empty "+str(candidate_fname)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				else:
					app.logger.debug("Candidate_fname empty "+str(candidate_fname)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					raise emptyStringError()
				
				candidate_lname = args['candidate_lastname']
				app.logger.debug("Candidate lname exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				isCandidateLastName = validateString(args['candidate_lastname'])
				if isCandidateLastName:
					app.logger.debug("Candidate_fname not empty "+str(candidate_lname)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				else:
					app.logger.debug("Candidate_fname empty "+str(candidate_lname)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					raise emptyStringError()

				
				app.logger.debug("Candidate mobile value exists or not - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				candidate_mobile = args['candidate_mobile']
				if candidate_mobile:
					isMobile = validateString(candidate_mobile)
					if isMobile:
						if candidate_mobile.isdigit():
							app.logger.debug("Mobile not empty "+str(candidate_mobile)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							candidate_mobile_number = long(candidate_mobile)
							if candidate_mobile_number > 0:
								app.logger.debug("Candidate mobile is exists "+str(candidate_mobile)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							else:
								app.logger.debug("Mobile number is invalid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
								app.logger.info("End of ApiUpdateUpcomingInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
								return {'status': False, 'message': 'COMMON_MOBILE_NUMBER_INVALID', 'code':422}	
						else:
							app.logger.debug("Mobile number is not digit - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							app.logger.info("End of ApiUpdateUpcomingInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							return {'status': False, 'message': 'COMMON_MOBILE_NUMBER_NOT_DIGIT', 'code':422}	
					else:
						app.logger.debug("Mobile empty "+str(candidate_mobile)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						raise emptyStringError()
				
				candidate_email= args['candidate_email']
				app.logger.debug("Candidate email value exists  "+str(candidate_email)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")	
				isCandidateEmailValid = validateEmail(candidate_email)
				
				if isCandidateEmailValid:
					app.logger.debug("Email is valid"+str(candidate_email)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")			
				else:
					app.logger.debug("Email is not valid"+str(candidate_email)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					raise invalidEmailError()

				try:
					custom_categories_data = json.loads(args['custom_categories_data'])
					check_json = json.loads(args['custom_categories_data'])
				except Exception as e:
					app.logger.info("error while receiving custom_categories_data")
					app.logger.info(e)
					return {'code':422,'status':False,'message':'ERROR_RECEIVING_CC_DATA'},422

				# validate timezone 
				local_timezone = args['timezone']
				app.logger.debug("Loacl timezone exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				islocalTimeZone = validateString(local_timezone)
				if islocalTimeZone:
					app.logger.debug("local_timezone not empty "+str(local_timezone)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				else:
					app.logger.debug("local_timezone empty "+str(local_timezone)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					raise emptyStringError()
				
				# Validate valid_till
				validtilldate = args['validtilldate']
				isScheduleDateString = validateString(validtilldate)
				if isScheduleDateString:
					app.logger.info("validtilltime is not empty - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					try:
						validTillDate = datetime.strptime(validtilldate,"%Y-%m-%d")
						validTillTime = validTillDate+timedelta(hours=23,minutes=59)
						present_date_time  = datetime.utcnow()
						present_date = datetime.strftime(present_date_time,"%Y-%m-%d")
						cur_date = datetime.strptime(present_date,"%Y-%m-%d")
						app.logger.debug("cur_date")
						app.logger.debug(cur_date)
						app.logger.debug("validTillTime")
						app.logger.debug(validTillTime)
						if validTillDate < cur_date:
							app.logger.info("End of ApiAsyncInterviewSchedule - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							return  {'status': False, 'message': 'APISCHEDULEINTERVIEW_SCHEDULETIME_LESS_THAN_CURRENT_DATE','code':422}
						
						app.logger.debug("Date format is valid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					
					except Exception as InvalidDateFormat:
						app.logger.info(InvalidDateFormat)
						app.logger.debug("Date format is not valid "+str(validtilldate)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						app.logger.info("End of ApiAsyncInterviewSchedule - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						return {'status': False, 'message': 'APISCHEDULEINTERVIEW_DATE_INVALID', 'code':422}
				else:
					app.logger.debug("validtilldate is empty"+str(validtilldate)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					raise emptyStringError()

				common_details = {'email':user.email,'orgid':user.orgid,'userid':user.userid}
				#Candidate Creation
				candidate_details ={'candidate_email':args['candidate_email'],'candidate_mobile':args['candidate_mobile'],'candidate_fname':args['candidate_firstname'],'candidate_lname':args['candidate_lastname']}
				app.logger.info("candidate_details")
				app.logger.info(candidate_details)
				candidate_result,candidate_error,candidate_creation_status = creatingCandidate(candidate_details,common_details,clientip)
				if candidate_creation_status:
					app.logger.info("Candidate Created")
				else:
					return candidate_error
				questions_list = json.loads(args['questions_list'])
				if questions_list:
					app.logger.info("questions_list exists")
					
				else:
					app.logger.info("Questions list not exists")
					app.logger.info("End of ApiAsyncInterviewSchedule - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {'status': False, 'message': 'APISCHEDULEINTERVIEW_QUESTIONS_LIST_MISSING', 'code':422}	
				
				
				asyncinterview = AsyncInterviews()
				asyncinterview.userid = candidate_result['userid']
				asyncinterview.orgid = common_details['orgid']
				asyncinterview.scheduledtime = datetime.utcnow()
				asyncinterview.interviewstatus = C.ASYNC_INTERVIEW_SCHEDULED
				asyncinterview.joiningurl = generateJoinInterviewURL(candidate_result['userid'])
				asyncinterview.createdby = common_details['userid']
				asyncinterview.createdtime = datetime.utcnow()
				asyncinterview.valid_till = validTillTime
				asyncinterview.timezone = local_timezone
				org_custom_data = OrgCustomCategories().query.filter(OrgCustomCategories.orgid==asyncinterview.orgid).first()
				if org_custom_data is not None:
					if org_custom_data.status == C.CC_ACTIVE:
						asyncinterview.custom_categories_data = custom_categories_data
					else:
						asyncinterview.custom_categories_data = {}
				else:
					pass
				db.session.add(asyncinterview)
				try:
					db.session.flush()
				except Exception as dbError:
					db.session.rollback()
					app.logger.info(dbError.message)
					app.logger.info(" End of ApiAsyncInterviewSchedule - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {'status': False, 'message': 'APIASYNCINTERVIEWESCHEDULE_ASYNCINTERVIEW_NOT_CREATED', 'code':422}

				async_questions_result,async_questions_error,async_questions_status=validateQuestions(questions_list,common_details,clientip) 
				if async_questions_status:
					app.logger.info("async_questions_result")
					app.logger.info(async_questions_result)
					for question_data in async_questions_result:
						app.logger.info("question_data")
						app.logger.info(question_data)
						asynctest = AsyncTest()
						asynctest.asyncid = asyncinterview.asyncid
						asynctest.asyncqid = question_data['question_id']
						asynctest.duration = question_data['question_duration']
						asynctest.createdby = common_details['userid']
						asynctest.createdtime = datetime.utcnow()
						asynctest.isactive = True
						db.session.add(asynctest)
						try:
							db.session.commit()
							db.session.flush(asynctest)
						except Exception as dbError:
							db.session.rollback()
							app.logger.info(dbError.message)
							app.logger.info(" End of ApiAsyncInterviewSchedule - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							return {'status': False, 'message': 'APIASYNCINTERVIEWESCHEDULE_ASYNCTEST_NOT_CREATED', 'code':422}

					try:
						asyncinterviewScheduledNotificationToCelery(candidate_result,asyncinterview.asyncid,common_details,clientip)
					except Exception as e:
						app.logger.info(e)
						app.logger.info("error while sending notification")
					return {'status':True,'message':'APIASYNCINTERVIEWESCHEDULE_SUCCESS','code':200}

				else:
					app.logger.info(" End of ApiAsyncInterviewSchedule - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return async_questions_error,async_questions_error['code'] 

			except JSONDecodeError as re:
				app.logger.info("Json decode error - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.exception(re)
				app.logger.info("End of ApiAsyncInterviewSchedule - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_JSON_CONVERSION', 'code':422}

			except RequiredFieldsMissing as re:
				app.logger.info("One or more required fileds missing - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.exception(re)
				app.logger.info("End of ApiAsyncInterviewSchedule - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_REQ_MORE_FIELD', 'code':422}
		
			except IntegrityError as ie:
				app.logger.info("Error: DB Exceeding limit Error - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.error(ie.message)
				app.logger.info("End of ApiAsyncInterviewSchedule - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_CONTACT_SUPPORT', 'code':409}

			except invalidEmailError as iee:
				app.logger.info("Email ID is not valid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.exception(iee)
				app.logger.error(iee.message)
				app.logger.info("End of ApiAsyncInterviewSchedule - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_EMAIL_NOT_VALID', 'code':422}

			except emptyStringError as ese:
				app.logger.info("One of the field is Empty - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.exception(ese)
				app.logger.error(ese.message)
				app.logger.info("End of ApiAsyncInterviewSchedule - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_ONE_OF_EMPTY_FIELD', 'code':422}

			except ValueMissing as vm:
				app.logger.info("Received invalid input - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.error(vm.message)
				app.logger.info("End of ApiAsyncInterviewSchedule - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_INVALID_INPUT', 'code':422}					
		
			except Exception as e:
				app.logger.debug("Error while parsing Async Interview Details - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.exception(e.message)
				app.logger.info("End of ApiAsyncInterviewSchedule - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': False, 'message': 'APIASYNCINTERVIEWESCHEDULE_FIELDS_MISSING', 'code':422}

		elif user:
			app.logger.debug("User is not authorized to access ApiAsyncInterviewSchedule service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.info("End of ApiAsyncInterviewSchedule - " + str(clientip))
			return {'status': False,  'message': 'COMMON_UNAUTHORIZED', 'code':403}
		else:
			app.logger.info("Invalid token or Session expired - " + str(clientip))
			app.logger.info("End of ApiAsyncInterviewSchedule - " + str(clientip))
			return {'status': False, 'message': 'COMMON_INVALID_TOKEN', 'code':401}

class ApiValidateAsyncJoiningLink(RestResource):
	def get(self,joining_link):
		app.logger.info("Accessing ApiValidateAsyncJoiningLink")
		clientip = request.remote_addr
		asyncInterviewData = AsyncInterviews().query.filter(AsyncInterviews.joiningurl == joining_link).first()
		if asyncInterviewData is not None:
			userid = asyncInterviewData.userid
			orgid = asyncInterviewData.orgid
			logo = getOrgLogo(orgid)
			interviewstatus = asyncInterviewData.interviewstatus
			app.logger.info("Valid joining link - " + str(clientip) + "- [ " + str(userid)+" : "+str(orgid)+" ]")
			if interviewstatus == C.ASYNC_INTERVIEW_SCHEDULED:
				scheduledtime = asyncInterviewData.scheduledtime
				valid_till = asyncInterviewData.valid_till
				present_time  = datetime.utcnow()
				if (present_time < scheduledtime):
					#will start at scheduledtime
					starttime = datetime.strftime(scheduledtime,"%Y-%m-%d %H:%M")
					validtill = datetime.strftime(valid_till,"%Y-%m-%d %H:%M")
					message = "Interview will start at " + str(scheduledtime) + ", ends at " + str(valid_till)
					app.logger.info(message + " - " + str(clientip) + "- [ " + str(userid)+" : "+str(orgid)+" ]")
					app.logger.info("end of ApiValidateAsyncJoiningLink - " + str(clientip) + "- [ " + str(userid)+" : "+str(orgid)+" ]")
					return  {'status': False,'logo':logo,'start_time':starttime,'end_time':validtill, 'code':406}
				elif (scheduledtime < present_time) and (present_time < valid_till):
					#in progress
					app.logger.info("Async Interview can start - " + str(clientip) + "- [ " + str(userid)+" : "+str(orgid)+" ]")
					app.logger.info("Generating login token - " + str(clientip) + "- [ " + str(userid)+" : "+str(orgid)+" ]")
					user = User().query.filter(User.userid == userid).first()
					login_token = user.generate_t()
					user_firstname = user.firstname
					user_lastname = user.lastname
					email = user.email
					mobile = user.mobile
					interviewid = asyncInterviewData.asyncid
					validtill = datetime.strftime(valid_till,"%Y-%m-%d %H:%M")
					app.logger.info("End of ApiValidateAsyncJoiningLink - " + str(clientip) + "- [ " + str(userid)+" : "+str(orgid)+" ]")
					return {'status': True, 'login_token':login_token,'user_firstname':user_firstname,'user_lastname':user_lastname,'email':email,'mobile':mobile,'logo':logo,'interviewid':interviewid,'valid_till':validtill,'code':200}
				elif (valid_till < present_time):
					app.logger.info("Async Interview Expired - " + str(clientip) + "- [ " + str(userid)+" : "+str(orgid)+" ]")
					app.logger.info("End of ApiValidateAsyncJoiningLink - " + str(clientip) + "- [ " + str(userid)+" : "+str(orgid)+" ]")
					return {'status': False, 'message': 'APIVALIDATEASYNCJOININGLINK_INT_EXPIRED', 'code':410,'logo':logo}
			elif interviewstatus == C.ASYNC_INTERVIEW_IN_PROGRESS:
				#inprogress case
				app.logger.info("Async Interview Failed - " + str(clientip) + "- [ " + str(userid)+" : "+str(orgid)+" ]")
				app.logger.info("End of ApiValidateAsyncJoiningLink - " + str(clientip) + "- [ " + str(userid)+" : "+str(orgid)+" ]")
				return {'status': False, 'message': 'APIVALIDATEASYNCJOININGLINK_INT_FAILED', 'code':410,'logo':logo}
			elif interviewstatus == C.ASYNC_INTERVIEW_CANCELLED:
				app.logger.info("Async Interview Cancelled - " + str(clientip) + "- [ " + str(userid)+" : "+str(orgid)+" ]")
				app.logger.info("End of ApiValidateAsyncJoiningLink - " + str(clientip) + "- [ " + str(userid)+" : "+str(orgid)+" ]")
				return {'status': False, 'message': 'APIVALIDATEASYNCJOININGLINK_INT_CANCELLED', 'code':404,'logo':logo}
			elif interviewstatus == C.ASYNC_INTERVIEW_SUCCESS:
				app.logger.info("Async Interview already completed - " + str(clientip) + "- [ " + str(userid)+" : "+str(orgid)+" ]")
				app.logger.info("End of ApiValidateAsyncJoiningLink - " + str(clientip) + "- [ " + str(userid)+" : "+str(orgid)+" ]")
				return {'status': False, 'message': 'APIVALIDATEASYNCJOININGLINK_INT_COMPLETED', 'code':410,'logo':logo}
			else:
				app.logger.info("Async Interview Expired - " + str(clientip) + "- [ " + str(userid)+" : "+str(orgid)+" ]")
				app.logger.info("End of ApiValidateAsyncJoiningLink - " + str(clientip) + "- [ " + str(userid)+" : "+str(orgid)+" ]")
				return {'status': False, 'message': 'APIVALIDATEASYNCJOININGLINK_INT_EXPIRED', 'code':410,'logo':logo}
		else:
			app.logger.info("Invalid joining link - " + str(clientip))
			app.logger.info("End of ApiValidateAsyncJoiningLink - " + str(clientip))
			return {'status': False, 'message': 'APIVALIDATESUCCESSINTERVIEW_INVALID_JOINING_URL', 'code':404}


class ApiStartAsyncInterview(RestResource):
	def post(self, interviewid):
		clientip = request.remote_addr
		app.logger.info("Accessing ApiStartAsyncInterview - "+str(clientip))
		status,auth_dict = user_auth(admin_access=C.INTERVIEWEE, return_requestor=True)
		app.logger.debug("status")
		app.logger.debug(status)
		app.logger.debug("auth_dict")
		app.logger.debug(auth_dict)
		requestor_class = auth_dict
		user = requestor_class
		if status and auth_dict:
			try:
				args = parser.parse(JoiningUrlSchema(), request, locations=request_locations)
				app.logger.debug("args")
				app.logger.debug(args)
				joiningurl = args['joiningurl']
				app.logger.debug("joiningurl")
				app.logger.debug(joiningurl)
			except Exception as e:
				app.logger.debug("Error while parsing joiningurl - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.exception(e.message)
				app.logger.info("End of ApiStartAsyncInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': False, 'message': 'APISTARTASYNCINTERVIEW_REQ_FIELD_MISSING', 'code':422}
			app.logger.info("Async Interview Live: Interview: "+str(interviewid)+" "+user.email +" wants to start Async Interview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			asyncInterview = AsyncInterviews().query.filter(AsyncInterviews.asyncid == interviewid).first()
			if user.orgid != asyncInterview.orgid or joiningurl != asyncInterview.joiningurl:
				app.logger.info("User is not authorized to access this interview")
				app.logger.info("End of ApiStartAsyncInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': False, 'message': 'APIVALIDATEASYNCJOININGLINK_INT_NOT_AUTHORIZED', 'code':403}
			if asyncInterview is not None:
				app.logger.info("User is authorized to access ApiStartAsyncInterview")
				logo = getOrgLogo(user.orgid)
				if asyncInterview.interviewstatus == C.ASYNC_INTERVIEW_SCHEDULED:
					query = "select a.asyncid,a.asynctestid,a.asyncqid,a.duration from asynctest as a where a.asyncid =" + str(interviewid) + " and a.isactive = true;"
					app.logger.debug("final query")
					app.logger.debug(query)
					results = db.engine.execute(query)
					# results = AsyncTest().query.filter(AsyncTest.asyncid == interviewid).all()
					listOfQues = AsyncTestQuestionsSchema(partial=('asynctestid','asyncid','asyncqid','duration')).dump(results, many=True).data

					if listOfQues is not None:
						asyncInterview.interviewstatus = C.ASYNC_INTERVIEW_IN_PROGRESS
						asyncInterviewTracking = AsyncInterviewTracking()
						asyncInterviewTracking.interviewid = interviewid
						asyncInterviewTracking.candidateid = user.userid
						asyncInterviewTracking.orgid = user.orgid
						asyncInterviewTracking.activity = C.ASYNC_INTERVIEW_TRACKING_JOIN
						asyncInterviewTracking.activitytime = datetime.utcnow()
						db.session.add(asyncInterviewTracking)
						db.session.commit()
						app.logger.info("Async Interview status changed to :" + str(C.ASYNC_INTERVIEW_IN_PROGRESS) + " - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						app.logger.info("Async Interview Tracking status updated to :" + str(C.ASYNC_INTERVIEW_TRACKING_JOIN) + " - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						app.logger.info("End of ApiValidateAsyncJoiningLink - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						return {'status': True, 'ques_data':listOfQues,'code':200,'logo':logo}
					else:
						listOfQues = []
						app.logger.info("End of ApiValidateAsyncJoiningLink - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						return {'status': True, 'ques_data':listOfQues,'code':200,'logo':logo}
				elif asyncInterview.interviewstatus == C.ASYNC_INTERVIEW_IN_PROGRESS:
					app.logger.info("Async Interview Failed - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.info("End of ApiValidateAsyncJoiningLink - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {'status': False, 'message': 'APIVALIDATEASYNCJOININGLINK_INT_FAILED', 'code':410,'logo':logo}
				elif asyncInterview.interviewstatus == C.ASYNC_INTERVIEW_SUCCESS:
					app.logger.info("Async Interview completed")
					app.logger.info("End of ApiStartAsyncInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {'status': False, 'message': 'APIVALIDATEASYNCJOININGLINK_INT_COMPLETED', 'code':410}
				elif asyncInterview.interviewstatus == C.ASYNC_INTERVIEW_CANCELLED:
					app.logger.info("Async Interview Cancelled")
					app.logger.info("End of ApiStartAsyncInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {'status': False, 'message': 'APIVALIDATEASYNCJOININGLINK_INT_CANCELLED', 'code':404}
				else:
					app.logger.info("Async Interview Expired")
					app.logger.info("End of ApiStartAsyncInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {'status': False, 'message': 'APIVALIDATEASYNCJOININGLINK_INT_EXPIRED', 'code':410}
			else:
				app.logger.info("Async Interview Live: Async Interview: "+str(interviewid)+" "+user.email +" Interview not found - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.info("End of ApiStartAsyncInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': False, 'message': 'COMMON_INTERVIEW_NOT_FOUND', 'code':404}


class ApiAsyncPreviousInterviewsList(RestResource):
	def get(self):
		app.logger.info("Accessing ApiAsyncPreviousInterviewsList")
		clientip = request.remote_addr
		status, user = user_auth(admin_access=C.ADMIN)
		if status and user:
			app.logger.debug("User is authorized to access ApiAsyncPreviousInterviewsList service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			if 'status' in request.args:
				app.logger.info("Status exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			else:
				app.logger.info("Status not exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.info("End of ApiAsyncPreviousInterviewsList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': False, 'message': 'APIUPDATEUSER_STATUS_INVALID', 'code':422}
			if 'orgid' in request.args:
				app.logger.info("Orgid exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			else:
				app.logger.info("Orgid not exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.info("End of ApiAsyncPreviousInterviewsList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': False, 'message': 'APIORGUPDATE_ORG_NOT_FOUND', 'code':422}
			if 'start_date' in request.args:
				app.logger.info("Start date exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			else:
				app.logger.info("Start Date not exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.info("End of ApiAsyncPreviousInterviewsList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': False, 'message': 'APIPREVIOUSINTERVIEWS_STDATE_ERROR', 'code':422}
			if 'end_date' in request.args:
				app.logger.info("End Date exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			else:
				app.logger.info("End Date not exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.info("End of ApiAsyncPreviousInterviewsList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': False, 'message': 'APIPREVIOUSINTERVIEWS_EDATE_ERROR', 'code':422}
			
			try:
				app.logger.info("Request args")
				app.logger.info(request.args)
				interviewstatus = request.args['status']
				orgid = request.args['orgid']
				start_date = request.args['start_date']
				end_date = request.args['end_date']
			
				current_time = datetime.utcnow()
				if int(interviewstatus) >= 0:
					interviewstatus = int(interviewstatus)
					app.logger.info("Status is valid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				else:
					app.logger.info("Status is not valid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.info("End of ApiAsyncPreviousInterviewsList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {'status': False, 'message': 'APIUPDATEUSER_STATUS_INVALID', 'code':422}

				if int(orgid) > 0:
					app.logger.info("Orgid is valid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				else:
					app.logger.info("Orgid is not valid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.info("End of ApiAsyncPreviousInterviewsList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {'status': False, 'message': 'APICREATEUSER_ORGID_INVALID', 'code':422}

				isStartDateString = validateString(start_date)
				if isStartDateString:
					app.logger.info("Start_date is not empty - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					try:
						startdateValidation = datetime.strptime(start_date, "%Y-%m-%d")
						app.logger.info(startdateValidation)
						start_d = startdateValidation + timedelta(hours=0,minutes=0)  
						app.logger.debug("Start Date format is valid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						# if startdateValidation > current_time:
						# 	app.logger.info("End of ApiAsyncPreviousInterviewsList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						# 	return  {'status': False, 'message': 'APIASYNCPREVIOUSINTERVIEWS_STARTDATE_GREATER_THAN_CURRENT_DATE','code':422}

					except Exception as InvalidDateFormat:
						app.logger.info(InvalidDateFormat)
						app.logger.debug("Start Date format is not valid "+str(start_date)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						app.logger.info("End of ApiAsyncPreviousInterviewsList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						return {'status': False, 'message': 'APISCHEDULEINTERVIEW_DATE_INVALID', 'code':422}
				else:
					app.logger.debug("Start_date is empty"+str(start_date)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					raise emptyStringError()

				isEndDateString = validateString(end_date)
				if isEndDateString:
					app.logger.info("End Date is not empty - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					try:
						enddateValidation = datetime.strptime(end_date, "%Y-%m-%d")
						end_d = enddateValidation + timedelta(hours=23,minutes=59)  
						app.logger.info(enddateValidation)
						app.logger.debug("End Date format is valid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						# if enddateValidation > current_time:
						# 	app.logger.info("End of ApiAsyncPreviousInterviewsList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						# 	return  {'status': False, 'message': 'APIASYNCPREVIOUSINTERVIEWS_ENDDATE_GREATER_THAN_CURRENT_DATE','code':422}
					
					except Exception as InvalidDateFormat:
						app.logger.info(InvalidDateFormat)
						app.logger.debug("End Date format is not valid "+str(end_date)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						app.logger.info("End of ApiAsyncPreviousInterviewsList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						return {'status': False, 'message': 'APISCHEDULEINTERVIEW_DATE_INVALID', 'code':422}
				else:
					app.logger.debug("Start_date is empty"+str(end_date)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					raise emptyStringError()
			
				slot_id,cur_date = calculateCurrentSlot()

				app.logger.debug("startdateValidation and enddateValidation")
				app.logger.debug(startdateValidation)
				app.logger.debug(enddateValidation)
				# Fetching all rows based on Database
				if startdateValidation <= enddateValidation:
					# previousInterviews = "select i.interviewid,i.interviewstatus,i.orgid,i.interviewerid,i.candidateid,i.date,i.slot_id,i.createdtime,(select u.email as candidate_email from users as u where u.userid = i.candidateid),(select u.mobile as candidate_mobile from users as u where u.userid = i.candidateid),(select u.mobile as interviewer_mobile from users as u where u.userid  = i.interviewerid),(select u.email as interviewer_email from users as u where u.userid = i.interviewerid)  from interviews as i where "
					previousInterviews = "select i.asyncid, i.scheduledtime, i.orgid, i.interviewstatus,i.valid_till,i.userid, (select u.email from users as u where u.userid= i.userid) as email from asyncinterviews as i where i.scheduledtime >= '"+str(start_d)+"' and i.scheduledtime <= '"+str(end_d)+"'"
					if interviewstatus > 0:
						previousInterviews +=" and i.interviewstatus="+str(interviewstatus)+" "
					previousInterviews += " and i.orgid="+str(orgid)+";"
					app.logger.info(previousInterviews)
					previous_interviews = db.engine.execute(previousInterviews)
					app.logger.info(previous_interviews)
					result = AsyncPreviousInterviewsListSchema().dump(previous_interviews,many = True).data
					app.logger.debug(result)
					app.logger.info("End of ApiAsyncPreviousInterviewsList - " + str(clientip))
					return {'status':True,'code':200,'message':'APIASYNCPREVIOUSINTERVIEWS_SUCCESS','listOfInterviews':result}
				else:
					app.logger.debug("Start Date must be less than End Date - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.info("End of ApiAsyncPreviousInterviewsList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {'status': False, 'message': 'APIUPCOMINGINTERVIEWS_DATE_COMPARISION', 'code':422}
		

			except RequiredFieldsMissing as re:
				app.logger.info("One or more required fileds missing - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.exception(re)
				app.logger.info("End of ApiPreviousInterviewsList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_REQ_MORE_FIELD', 'code':422}
			
			except IntegrityError as ie:
				app.logger.info("Error: DB Exceeding limit Error - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.error(ie.message)
				app.logger.info("End of ApiPreviousInterviewsList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_CONTACT_SUPPORT', 'code':409}
			except DataError as de:
				app.logger.info("DataBase field value out of range - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.error(de.message)
				app.logger.info("End of ApiPreviousInterviewsList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_INVALID_DATA', 'code':422}
			except InvalidRequestError as ir:
				app.logger.info("received invalid request - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.exception(ir)
				app.logger.error(ir.message)
				app.logger.info("End of ApiPreviousInterviewsList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_INVALID_REQUSET', 'code':422}
			except ValueError as ve:
				app.logger.info("Date Format is invalid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.error(ve.message)
				app.logger.info("End of ApiPreviousInterviewsList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': str(ve.message), 'code':422}
			except ValueMissing as vm:
				app.logger.info("Received invalid input - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.error(vm.message)
				app.logger.info("End of ApiPreviousInterviewsList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_INVALID_INPUT', 'code':422}
			except Exception as e:
				app.logger.info("Error while ApiPreviousInterviewsList- " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.exception(e.message)
				app.logger.info("End of ApiPreviousInterviewsList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': False, 'message': 'APIASYNCPREVIOUSINTERVIEWS_LIST_FAILS', 'code':422}
		elif user:
			app.logger.debug("User is not authorized to access ApiAsyncPreviousInterviewsList service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.info("End of ApiAsyncPreviousInterviewsList - " + str(clientip))
			return {'status': False,  'message': 'COMMON_UNAUTHORIZED', 'code':403}
		else:
			app.logger.info("Invalid token or Session expired - " + str(clientip))
			app.logger.info("End of ApiAsyncPreviousInterviewsList - " + str(clientip))
			return {'status': False, 'message': 'COMMON_INVALID_TOKEN', 'code':401}



class ApiAsyncQuestionsList(RestResource):
	def get(self):
		app.logger.info("Accessing ApiAsyncQuestionsList")
		clientip = request.remote_addr
		status, user = user_auth(admin_access=C.ADMIN)
		if status and user:
			app.logger.debug("User is authorized to access ApiAsyncQuestionsList service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			try:
				super_admin_details = User().query.filter(User.role == C.SUPERADMIN).first()
				# list_of_questions = AsyncQuestions().query.filter(or_(AsyncQuestions.orgid == user.orgid,AsyncQuestions.orgid == super_admin_details.orgid)).all()
				question = request.args.get('question')
				app.logger.info("question")
				app.logger.info(question)
				isQuestionvalid = validateString(question)
				if isQuestionvalid:
					app.logger.debug("Question is not Empty - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				else:
					app.logger.debug("Question empty  - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					raise emptyStringError()
				# list_of_questions = AsyncQuestions().query.filter(and_(or_(AsyncQuestions.orgid == user.orgid,AsyncQuestions.orgid == super_admin_details.orgid),)).all()
				questions_query = "select asyncqid,question from asyncquestions where ( orgid = "+str(user.orgid)+" or orgid ="+str(super_admin_details.orgid)+" ) and COALESCE(lower(question),'') like lower('%%"+str(question)+"%%');"
				app.logger.info("questions_query")
				app.logger.info(questions_query)
				list_of_questions = db.engine.execute(questions_query)
				questions_list = AsyncQuestionsListSchema().dump(list_of_questions,many=True).data
				
				return {'status':'success','message':'APIASYNCQUESTIONSLIST','code':200,'questions_list':questions_list}
			except emptyStringError as ese:
				app.logger.info("One of the field is Empty - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.exception(ese)
				app.logger.error(ese.message)
				app.logger.info("End of ApiAsyncQuestionsList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': False, 'message': 'COMMON_ERROR_ONE_OF_EMPTY_FIELD', 'code':422}
			except Exception as e:
				app.logger.info("Error while ApiAsyncQuestionsList- " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.exception(e.message)
				app.logger.info("End of ApiAsyncQuestionsList - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': False, 'message': 'APIASYNCPREVIOUSINTERVIEWS_FAILS', 'code':422}
		elif user:
			app.logger.debug("User is not authorized to access ApiAsyncQuestionsList service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.info("End of ApiAsyncQuestionsList - " + str(clientip))
			return {'status': False,  'message': 'COMMON_UNAUTHORIZED', 'code':403}
		else:
			app.logger.info("Invalid token or Session expired - " + str(clientip))
			app.logger.info("End of ApiAsyncQuestionsList - " + str(clientip))
			return {'status': False, 'message': 'COMMON_INVALID_TOKEN', 'code':401}

# class ApiGetAsyncQuestion(RestResource):
# 	def get(self,asyncqid):
# 		app.logger.info("Accessing ApiGetAsyncQuestion")
# 		clientip = request.remote_addr
# 		status, user = user_auth(admin_access=C.INTERVIEWEE)
# 		if status and user:
# 			app.logger.debug("User is authorized to access ApiGetAsyncQuestion service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
# 			try:
# 				data = AsyncQuestions().query.filter(AsyncQuestions.asyncqid == asyncqid).first()
# 				if data.orgid == user.orgid:
# 					app.logger.info("End of ApiGetAsyncQuestion - " + str(clientip))
# 					return {'status':'success','code':200,'question':data.question}
# 				else:
# 					app.logger.debug("User is not authorized to access ApiGetAsyncQuestion service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
# 					app.logger.info("End of ApiGetAsyncQuestion - " + str(clientip))
# 					return {'status': False,  'message': 'COMMON_UNAUTHORIZED', 'code':403}
# 			except Exception as e:
# 				app.logger.info("Error while ApiGetAsyncQuestion- " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
# 				app.logger.exception(e.message)
# 				app.logger.info("End of ApiGetAsyncQuestion - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
# 				return {'status': False, 'message': 'APIGETASYNCQUESTION_FAIL', 'code':422}
# 		elif user:
# 			app.logger.debug("User is not authorized to access ApiGetAsyncQuestion service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
# 			app.logger.info("End of ApiGetAsyncQuestion - " + str(clientip))
# 			return {'status': False,  'message': 'COMMON_UNAUTHORIZED', 'code':403}
# 		else:
# 			app.logger.info("Invalid token or Session expired - " + str(clientip))
# 			app.logger.info("End of ApiGetAsyncQuestion - " + str(clientip))
# 			return {'status': False, 'message': 'COMMON_INVALID_TOKEN', 'code':401}


class ApiGetAsyncQuestion(RestResource):
	def get(self,asynctestid,asyncqid):
		app.logger.info("Accessing ApiGetAsyncQuestion")
		clientip = request.remote_addr
		status, user = user_auth(admin_access=C.INTERVIEWEE)
		if status and user:
			app.logger.debug("User is authorized to access ApiGetAsyncQuestion service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.debug("asynctestid:"+str(asynctestid)+" asyncqid:"+str(asyncqid))
			#check if question is assigned to that testid
			asyncTest = AsyncTest().query.filter(and_(AsyncTest.asynctestid == asynctestid,AsyncTest.asyncqid == asyncqid)).first()
			if asyncTest is not None:
				if (asyncTest.isanswered == False):
					asyncInterview = AsyncInterviews().query.filter(AsyncInterviews.asyncid==asyncTest.asyncid).first()
					if asyncInterview.interviewstatus==C.ASYNC_INTERVIEW_IN_PROGRESS:
						scheduledtime = asyncInterview.scheduledtime
						validtill = asyncInterview.valid_till
						presenttime = datetime.utcnow()
						if ((scheduledtime < presenttime) and (presenttime < validtill)):
							app.logger.debug("User is authorized to get question at this time " + str(presenttime))
							asyncquestion = AsyncQuestions().query.filter(and_(AsyncQuestions.asyncqid == asyncqid,AsyncQuestions.orgid == user.orgid)).first()
							app.logger.debug("asyncquestion")
							app.logger.debug(asyncquestion)
							return {'status':'success','code':200,'question':asyncquestion.question}
						else:
							app.logger.debug("User is not authorized to get question at this time " + str(presenttime))
							return {'status': False,'message':'APIGETASYNCQUESTION_NOT_AUTH_INTIME','code':403}
					elif asyncInterview.interviewstatus==C.ASYNC_INTERVIEW_SCHEDULED:
						app.logger.info("Async Interview " + str(asyncTest.asyncid) +" didn't start")
						return {'status': False,'message':'APIGETASYNCQUESTION_INT_DIDNT_START','code':406}
					elif asyncInterview.interviewstatus==C.ASYNC_INTERVIEW_CANCELLED:
						app.logger.info("Async Interview " + str(asyncTest.asyncid) +" cancelled")
						return {'status': False,'message':'APIGETASYNCQUESTION_INT_CANCELLED','code':410}
					elif asyncInterview.interviewstatus==C.ASYNC_INTERVIEW_SUCCESS:
						app.logger.info("Async Interview " + str(asyncTest.asyncid) +" completed")
						return {'status': False,'message':'APIGETASYNCQUESTION_INT_COMPLETED','code':410}
					elif asyncInterview.interviewstatus==C.ASYNC_INTERVIEW_EXPIRED:
						app.logger.info("Async Interview " + str(asyncTest.asyncid) +" expired")
						return {'status': False,'message':'APIGETASYNCQUESTION_INT_EXPIRED','code':410}
					else:
						app.logger.info("Async Interview " + str(asyncTest.asyncid) +" failed")
						return {'status': False,'message':'APIGETASYNCQUESTION_INT_FAILED','code':410}
				else:
					app.logger.info("Async Interview: " + str(asyncTest.asyncid) + ", Question: " + str(asyncqid) + " is already Answered")
					return {'status': False,'message':'APIGETASYNCQUESTION_INT_QUES_ANSWERED','code':410}
		elif user:
			app.logger.debug("User is not authorized to access ApiGetAsyncQuestion service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.info("End of ApiGetAsyncQuestion - " + str(clientip))
			return {'status': False,  'message': 'COMMON_UNAUTHORIZED', 'code':403}
		else:
			app.logger.info("Invalid token or Session expired - " + str(clientip))
			app.logger.info("End of ApiGetAsyncQuestion - " + str(clientip))
			return {'status': False, 'message': 'COMMON_INVALID_TOKEN', 'code':401}




class ApiCameraTagWebhookhandling(RestResource):
	def post(self):
		app.logger.info("Accessing ApiCameraTagWebhookhandling")
		null = ''
		clientip = request.remote_addr
		try:
			app.logger.debug("request.data")
			app.logger.debug(request.data)
		except Exception as e:
			app.logger.info("there is not request data in cameratag webhook")
			app.logger.debug("End of ApiCameraTagWebhookhandling")
			return 422
		try:
			if request.data is not None:
				data = request.data
				evalArray = eval(data)
				#app.logger.debug("printing required values")
				#app.logger.debug("videolink:" + str(evalArray['uuid']))
				#app.logger.debug("description:" + str(evalArray['description']))
				#app.logger.debug("metadata:")
				#app.logger.debug(evalArray['metadata'])

				videolink = evalArray['uuid']
				# asynctestid = int(evalArray['description'])
				metadata = evalArray['metadata']

				app.logger.debug(metadata)
				app.logger.debug(type(metadata))
				metadata = json.dumps(metadata)
				metaEval = eval(metadata)
				asynctestid = metaEval['id']
				recorded_length = 0
				medias = evalArray['medias']
				for k,v in medias.iteritems():
					app.logger.info("k "+ str(k))
					app.logger.info("v "+ str(v))
					if k=='mp4' or k=='mp3':						
						mp4 = v
						for k1,v1 in mp4.iteritems():
							if k1=='length':
								app.logger.info("recorded length")
								recorded_length = v1


				asynctest = AsyncTest().query.filter(AsyncTest.asynctestid == asynctestid).first()
				if asynctest is not None:
					# if asynctest.videolink is None:
					# 	asynctest.videolink = videolink
					# 	asynctest.recorded_length = recorded_length
					# 	db.session.commit()
					# 	app.logger.info("Async Interview :" + str(asynctest.asyncid) + "Test :" + str(asynctestid) + "Video link updated")
					# 	return
					# else:
					# 	app.logger.info("Async Interview :" + str(asynctest.asyncid) + "Test :" + str(asynctestid) + "Video link already updated")
					# 	return
					# this code for update video links even if exist
					app.logger.info("recorded_length")
					app.logger.info(recorded_length)
					asynctest.videolink = videolink
					asynctest.recorded_length = recorded_length
					db.session.commit()
					app.logger.info("Async Interview :" + str(asynctest.asyncid) + "Test : " + str(asynctestid) + " Video link updated")
					return
				app.logger.info("Async Interview Test : " + str(asynctestid) + " not found in database")
				app.logger.debug("End of ApiCameraTagWebhookhandling")
				return 200
			else :
				app.logger.info("there is not request data in cameratag webhook")
				app.logger.debug("End of ApiCameraTagWebhookhandling")
				return 422
		except Exception as exp:
			app.logger.info("error while parsing metadata of cameratag")
			app.logger.info("End of ApiCameraTagWebhookhandling")
			return 422
			
			
	

class ApiEndAsyncInterview(RestResource):
	def post(self,interviewid):
		clientip = request.remote_addr
		app.logger.info("Accessing ApiEndAsyncInterview - "+str(clientip))
		status,auth_dict = user_auth(admin_access=C.INTERVIEWEE)
		app.logger.debug("status")
		app.logger.debug(status)
		app.logger.debug("auth_dict")
		app.logger.debug(auth_dict)
		if status and auth_dict:
			app.logger.debug("User is authorized to access ApiEndAsyncInterview service - " + str(clientip) + "- [ " + str(auth_dict.userid)+" : "+str(auth_dict.orgid)+" ]")
			asyncinterview = AsyncInterviews().query.filter(AsyncInterviews.asyncid == interviewid).first()
			if asyncinterview is not None:
				if asyncinterview.interviewstatus == C.ASYNC_INTERVIEW_IN_PROGRESS:
					asyncinterview.interviewstatus = C.ASYNC_INTERVIEW_SUCCESS
					asyncInterviewTracking = AsyncInterviewTracking()
					asyncInterviewTracking.interviewid = interviewid
					asyncInterviewTracking.candidateid = auth_dict.userid
					asyncInterviewTracking.orgid = auth_dict.orgid
					asyncInterviewTracking.activity = C.ASYNC_INTERVIEW_TRACKING_END
					asyncInterviewTracking.activitytime = datetime.utcnow()
					db.session.add(asyncInterviewTracking)
					db.session.commit()
					app.logger.info("Async Interview status changed to :" + str(C.ASYNC_INTERVIEW_SUCCESS) + " - " + str(clientip) + "- [ " + str(auth_dict.userid)+" : "+str(auth_dict.orgid)+" ]")
					app.logger.info("Async Interview Tracking status updated to :" + str(C.ASYNC_INTERVIEW_TRACKING_END) + " - " + str(clientip) + "- [ " + str(auth_dict.userid)+" : "+str(auth_dict.orgid)+" ]")
					app.logger.info("End of ApiEndAsyncInterview - "+str(clientip) + " - " + str(clientip) + "- [ " + str(auth_dict.userid)+" : "+str(auth_dict.orgid)+" ]")
					return {'status': True, 'message': 'APIENDSYNCINTERVIEW_SUCCES', 'code':201}
				elif asyncinterview.interviewstatus == C.ASYNC_INTERVIEW_SCHEDULED:
					app.logger.info("Interview " + str(interviewid) + " didn't started")
					return {'status': False, 'message': 'APIENDSYNCINTERVIEW_NOT_STARTED', 'code':401}
				elif asyncinterview.interviewstatus == C.ASYNC_INTERVIEW_CANCELLED:
					app.logger.info("Interview " + str(interviewid) + " already cancelled")
					return {'status': False, 'message': 'APIENDSYNCINTERVIEW_CANCELLED', 'code':401}
				elif asyncinterview.interviewstatus == C.ASYNC_INTERVIEW_EXPIRED:
					app.logger.info("Interview " + str(interviewid) + " already expired")
					return {'status': False, 'message': 'APIENDSYNCINTERVIEW_EXPIRED', 'code':401}
				elif asyncinterview.interviewstatus == C.ASYNC_INTERVIEW_SUCCESS:
					app.logger.info("Interview " + str(interviewid) + " already completed")
					return {'status': False, 'message': 'APIENDSYNCINTERVIEW_COMPLETED', 'code':401}
				else:
					app.logger.info("Interview " + str(interviewid) + " already failed")
					return {'status': False, 'message': 'APIENDSYNCINTERVIEW_FAILED', 'code':401}
			else:
				app.logger.info("Async Interview Live: Async Interview: "+str(interviewid)+" "+user.email +" Interview not found - " + str(clientip) + "- [ " + str(auth_dict.userid)+" : "+str(auth_dict.orgid)+" ]")
				app.logger.info("End of ApiEndAsyncInterview - " + str(clientip) + "- [ " + str(auth_dict.userid)+" : "+str(auth_dict.orgid)+" ]")
				return {'status': False, 'message': 'COMMON_INTERVIEW_NOT_FOUND', 'code':404}
		else:
			app.logger.debug("User is not authorized to access ApiEndAsyncInterview service - " + str(clientip) )
			app.logger.info("End of ApiEndAsyncInterview - " + str(clientip))
			return {'status': False,  'message': 'COMMON_UNAUTHORIZED', 'code':403}


class ApiViewAsyncInterviewDetails(RestResource):
	def get(self,asyncid):
		app.logger.info("Acessing ApiViewAsyncInterviewDetails")
		clientip = request.remote_addr
		status, user = user_auth(admin_access=C.ADMIN)
		if status and user:
			app.logger.debug("User is authorized to access ApiViewAsyncInterviewDetails service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			try:
				data = AsyncInterviews().query.filter(AsyncInterviews.asyncid == asyncid,AsyncInterviews.orgid == user.orgid).first()
				if data:
					query_asyc_interview = "select ai.asyncid,ai.scheduledtime,ai.valid_till,ai.interviewstatus,ai.createdtime,ai.custom_categories_data,(select u.email from users as u where u.userid = ai.userid),(select u.mobile from users as u where u.userid = ai.userid),(select u.firstname from users as u where u.userid = ai.userid),(select u.lastname from users as u where u.userid = ai.userid)  from asyncinterviews as ai where ai.asyncid = "+str(asyncid)+";"

					query_async_video = "select at.asyncid,at.asyncqid,at.duration,at.attempted_time,at.recorded_length,at.isanswered,(select aq.question from asyncquestions as aq where aq.asyncqid = at.asyncqid) as question,at.duration,at.videolink,at.recorded_length from asynctest as at where at.asyncid = "+str(asyncid)+" and at.isactive = true;"
					
					async_interview = db.engine.execute(query_asyc_interview)
					async_videos = db.engine.execute(query_async_video)
					interview_details = AsyncInterviewDetailsSchema().dump(async_interview,many=True).data
					question_videos = AsyncInterviewQuestionVideosSchema().dump(async_videos,many=True).data

					cc_result,cc_status,status = getCcDataByInterviewidByOrgidByInterviewtype(intid=asyncid,orgid=user.orgid,interview_type=C.INTERVIEW_TYPE_ASYNC)

					return {'status':True,'message':'APIVIEWASYNCINTERVIEWDETAILS_PASS','code':200,'interview_details':interview_details,'question_videos':question_videos,'custom_categories_data':cc_result,'cc_status':cc_status},200
				else:
					app.logger.info("End of ApiViewAsyncInterviewDetails - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {'status': False, 'message': 'APIVIEWASYNCINTERVIEWDETAILS_INTERVIEW_NOT_EXISTS', 'code':422}	

			except Exception as e:
				app.logger.info("Error while ApiViewAsyncInterviewDetails- " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.exception(e.message)
				app.logger.info("End of ApiViewAsyncInterviewDetails - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': False, 'message': 'APIVIEWASYNCINTERVIEWDETAILS_FAIL', 'code':422}
		elif user:
			app.logger.debug("User is not authorized to access ApiViewAsyncInterviewDetails service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.info("End of ApiViewAsyncInterviewDetails - " + str(clientip))
			return {'status': False,  'message': 'COMMON_UNAUTHORIZED', 'code':403}
		else:
			app.logger.info("Invalid token or Session expired - " + str(clientip))
			app.logger.info("End of ApiViewAsyncInterviewDetails - " + str(clientip))
			return {'status': False, 'message': 'COMMON_INVALID_TOKEN', 'code':401}


class ApiUpdateIsAnsweredFlag(RestResource):
	def get(self,asynctestid,asyncqid):
		app.logger.info("Accessing ApiGetAsyncQuestion")
		clientip = request.remote_addr
		status, user = user_auth(admin_access=C.INTERVIEWEE)
		if status and user:
			app.logger.debug("User is authorized to access ApiUpdateIsAnsweredFlag service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.debug("asynctestid:"+str(asynctestid)+" asyncqid:"+str(asyncqid))
			asyncTest = AsyncTest().query.filter(and_(AsyncTest.asynctestid == asynctestid,AsyncTest.asyncqid == asyncqid)).first()
			if asyncTest is not None:
				asyncInterview = AsyncInterviews().query.filter(AsyncInterviews.asyncid==asyncTest.asyncid).first()
				if asyncInterview.interviewstatus == C.ASYNC_INTERVIEW_IN_PROGRESS:
					asyncTest.isanswered = True
					asyncTest.attempted_time = datetime.utcnow()
					asyncTest.updatedby = user.userid
					asyncTest.updatedtime = datetime.utcnow()
					db.session.commit()
					app.logger.debug("Async Interview test isAnswered flag updated to : True - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.info("End of ApiUpdateIsAnsweredFlag - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {'status': True,'message':'APIUPDATEISANSWEREDFLAG_SUCCESS', 'code':201}
				else:
					app.logger.debug("Async Interview not in progress - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.info("End of ApiUpdateIsAnsweredFlag - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {'status': False,'message':'APIUPDATEISANSWEREDFLAG_INT_NOT_INPROGRESS' , 'code':410}
			else:
				app.logger.debug("Async Interview Live: Async Interview: "+str(asyncTest.asyncid)+" not found - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.info("End of ApiUpdateIsAnsweredFlag - " + str(clientip) + "- [ " + str(auth_dict.userid)+" : "+str(auth_dict.orgid)+" ]")
				return {'status': False, 'message': 'COMMON_INTERVIEW_NOT_FOUND', 'code':404}
		else:
			app.logger.debug("User is not authorized to access ApiUpdateIsAnsweredFlag service - " + str(clientip) )
			app.logger.info("End of ApiUpdateIsAnsweredFlag - " + str(clientip))
			return {'status': False,  'message': 'COMMON_UNAUTHORIZED', 'code':403}


class ApiUpdateAsyncInterview(RestResource):
	def post(self,asyncid):
		app.logger.info("Accessing ApiUpdateAsyncInterview")
		clientip = request.remote_addr
		app.logger.debug("Check user token - " + str(clientip))
		try:
			status, user = user_auth(admin_access=C.ADMIN)
		except Exception as e:
			app.logger.info("Error while checking authentication - " + str(clientip))
			app.logger.exception(e)
			app.logger.info("End of ApiUpdateAsyncInterview - " + str(clientip))
			return {'status': False, 'message': 'COMMON_ERROR_IN_AUTHENTICATION', 'code':422}
		if status and user:
			app.logger.debug("User is authorized to access ApiUpdateAsyncInterview service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			try:
				app.logger.debug("request")
				app.logger.debug(request)
				app.logger.debug("request.form")
				app.logger.debug(request.form)
				app.logger.debug("request.data")
				app.logger.debug(request.data)
				args = parser.parse(AsyncInterviewUpdateSchema(), request, locations=request_locations)
				app.logger.debug("args")
				app.logger.debug(args)
				#check weather interview exist or not
				asyncinterview = AsyncInterviews().query.filter(AsyncInterviews.asyncid == asyncid).first()
				if asyncinterview is not None:
					#valid
					oldCandidateId = asyncinterview.userid
					candidate_fname = args['candidate_firstname']
					app.logger.debug("Candidate fname exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					isCandidateFirstName = validateString(args['candidate_firstname'])
					if isCandidateFirstName:
						app.logger.debug("Candidate_fname not empty "+str(candidate_fname)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					else:
						app.logger.debug("Candidate_fname empty "+str(candidate_fname)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						raise emptyStringError()
					
					candidate_lname = args['candidate_lastname']
					app.logger.debug("Candidate lname exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					isCandidateLastName = validateString(args['candidate_lastname'])
					if isCandidateLastName:
						app.logger.debug("Candidate_fname not empty "+str(candidate_lname)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					else:
						app.logger.debug("Candidate_fname empty "+str(candidate_lname)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						raise emptyStringError()

					
					app.logger.debug("Candidate mobile value exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					candidate_mobile = args['candidate_mobile']
					if candidate_mobile:
						isMobile = validateString(candidate_mobile)
						if isMobile:
							if candidate_mobile.isdigit():
								app.logger.debug("Mobile not empty "+str(candidate_mobile)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
								candidate_mobile_number = long(candidate_mobile)
								if candidate_mobile_number > 0:
									app.logger.debug("Candidate mobile is exists "+str(candidate_mobile)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
								else:
									app.logger.debug("Mobile number is invalid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
									app.logger.info("End of ApiUpdateUpcomingInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
									return {'status': False, 'message': 'COMMON_MOBILE_NUMBER_INVALID', 'code':422}	
							else:
								app.logger.debug("Mobile number is not digit - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
								app.logger.info("End of ApiUpdateUpcomingInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
								return {'status': False, 'message': 'COMMON_MOBILE_NUMBER_NOT_DIGIT', 'code':422}	
						else:
							app.logger.debug("Mobile empty "+str(candidate_mobile)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							raise emptyStringError()
					
					candidate_email= args['candidate_email']
					app.logger.debug("Candidate email value exists  "+str(candidate_email)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")	
					isCandidateEmailValid = validateEmail(candidate_email)
					
					if isCandidateEmailValid:
						app.logger.debug("Email is valid"+str(candidate_email)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")			
					else:
						app.logger.debug("Email is not valid"+str(candidate_email)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						raise invalidEmailError()
					# validate timezone 
					local_timezone = args['timezone']
					isScheduleDateString = validateString(local_timezone)
					if isScheduleDateString:
						app.logger.info("local_timezone is not empty - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					else:
						app.logger.debug("scheduledtime is empty"+str(start_date)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						raise emptyStringError()
					
					try:
						custom_categories_data = json.loads(args['custom_categories_data'])
						check_json = json.loads(args['custom_categories_data'])
					except Exception as e:
						app.logger.info("error while receiving custom_categories_data")
						app.logger.info(e)
						return {'code':422,'status':False,'message':'ERROR_RECEIVING_CC_DATA'},422

					# Validate valid_till
					validtilldate = args['validtilldate']
					isScheduleDateString = validateString(validtilldate)
					if isScheduleDateString:
						app.logger.info("validtilltime is not empty - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						try:
							validTillDate = datetime.strptime(validtilldate,"%Y-%m-%d")
							app.logger.debug(validTillDate)
							validTillTime = validTillDate+timedelta(hours=23,minutes=59)
							app.logger.debug(validTillTime)
							present_date_time  = datetime.utcnow()
							app.logger.debug(present_date_time)
							present_date = datetime.strftime(present_date_time,"%Y-%m-%d")
							cur_date = datetime.strptime(present_date,"%Y-%m-%d")
							app.logger.debug("cur_date")
							app.logger.debug(cur_date)
							app.logger.debug("validTillTime")
							app.logger.debug(validTillTime)
							if validTillDate < cur_date:
								app.logger.info("End of ApiUpdateAsyncInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
								return  {'status': False, 'message': 'APISCHEDULEINTERVIEW_SCHEDULETIME_LESS_THAN_CURRENT_DATE','code':422}
							
							app.logger.debug("Date format is valid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						
						except Exception as InvalidDateFormat:
							app.logger.info(InvalidDateFormat)
							app.logger.debug("Date format is not valid "+str(validtilldate)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							app.logger.info("End of ApiUpdateAsyncInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
							return {'status': False, 'message': 'APISCHEDULEINTERVIEW_DATE_INVALID', 'code':422}
					else:
						app.logger.debug("validtilldate is empty"+str(validtilldate)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						raise emptyStringError()

					common_details = {'email':user.email,'orgid':user.orgid,'userid':user.userid}
					#Candidate Creation
					candidate_details ={'candidate_email':args['candidate_email'],'candidate_mobile':args['candidate_mobile'],'candidate_fname':args['candidate_firstname'],'candidate_lname':args['candidate_lastname']}
					app.logger.info("candidate_details")
					app.logger.info(candidate_details)
					candidate_result,candidate_error,candidate_creation_status = creatingCandidate(candidate_details,common_details,clientip)
					if candidate_creation_status:
						app.logger.info("Candidate Created")
					else:
						return candidate_error
					if candidate_result['userid'] != asyncinterview.userid:
						asyncinterview = AsyncInterviews()
						asyncinterview.joiningurl = generateJoinInterviewURL(candidate_result['userid'])
						app.logger.info("New Joining url is generated due to change of user - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					else:
						pass
					app.logger.debug('questions_list')
					app.logger.debug(args['questions_list'])
					questions_list = json.loads(args['questions_list'])
					if questions_list:
						app.logger.info("questions_list exists")
						
					else:
						app.logger.info("Questions list not exists")
						app.logger.info("End of ApiUpdateAsyncInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						return {'status': False, 'message': 'APISCHEDULEINTERVIEW_QUESTIONS_LIST_MISSING', 'code':422}	
		
					asyncinterview.userid = candidate_result['userid']
					asyncinterview.orgid = common_details['orgid']
					asyncinterview.scheduledtime = datetime.utcnow()
					asyncinterview.interviewstatus = C.ASYNC_INTERVIEW_SCHEDULED
					asyncinterview.createdby = common_details['userid']
					asyncinterview.createdtime = datetime.utcnow()
					asyncinterview.valid_till = validTillTime
					asyncinterview.timezone = local_timezone
					asyncinterview.updatedby = user.userid
					asyncinterview.updatedtime = datetime.utcnow()
					asyncinterview.custom_categories_data = custom_categories_data

					if oldCandidateId != asyncinterview.userid:
						db.session.add(asyncinterview)
					db.session.commit()
					try:
						db.session.flush()
					except Exception as dbError:
						db.session.rollback()
						app.logger.info(dbError.message)
						app.logger.info(" End of ApiUpdateAsyncInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						return {'status': False, 'message': 'APIUPDATEASYNCINTERVIEW_ASYNCTEST_NOT_UPDATED', 'code':422}

					async_questions_result,async_questions_error,async_questions_status=validateQuestions(questions_list,common_details,clientip)
					if async_questions_status:
						app.logger.info("async_questions_result")
						app.logger.info(async_questions_result)
						asynctestdata = AsyncTest().query.filter(AsyncTest.asyncid == asyncid).all()
						for async_test in asynctestdata:
							async_test.isactive = False
							async_test.updatedby = user.userid
							async_test.updatedtime = datetime.utcnow()
							try:
								db.session.commit()
								db.session.flush(async_test)
							except Exception as dbError:
								db.session.rollback()
								app.logger.info(dbError.message)
								app.logger.info(" End of ApiUpdateAsyncInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
								return {'status': False, 'message': 'APIUPDATEASYNCINTERVIEW_ASYNCTEST_NOT_UPDATED', 'code':422}
						for question_data in async_questions_result:
							app.logger.info("question_data")
							app.logger.info(question_data)
							asynctests = AsyncTest()
							asynctests.asyncid = asyncinterview.asyncid
							asynctests.asyncqid = question_data['question_id']
							asynctests.duration = question_data['question_duration']
							asynctests.createdby = common_details['userid']
							asynctests.createdtime = datetime.utcnow()
							asynctests.isactive = True
							try:
								db.session.add(asynctests)
								db.session.commit()
								db.session.flush(asynctests)
								app.logger.debug("asynctest commit & flush success")
							except Exception as dbError:
								db.session.rollback()
								app.logger.info(dbError.message)
								app.logger.info(" End of ApiUpdateAsyncInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
								return {'status': False, 'message': 'APIUPDATEASYNCINTERVIEW_ASYNCTEST_NOT_UPDATED', 'code':422}
						if oldCandidateId != candidate_result['userid']:
							app.logger.debug("Cancel Async Interview " + str(asyncid))
							oldAsync = AsyncInterviews().query.filter(AsyncInterviews.asyncid == asyncid).first()
							oldAsync.interviewstatus = C.ASYNC_INTERVIEW_CANCELLED
							db.session.commit()
							app.logger.debug("Sending cancel email to old user")
							candidateData = User().query.filter(User.userid == oldCandidateId).first()
							candidate_data={'firstname':candidateData.firstname,'lastname':candidateData.lastname,'userid':candidateData.userid,'mobile':candidateData.mobile,'email':candidateData.email}
							common_details = {'email':user.email,'orgid':user.orgid,'userid':user.userid}
							try:
								asyncCandidateCancelledNotificationToCelery(candidate_data,oldAsync,common_details,clientip)
							except Exception as e:
								app.logger.info(e)
								app.logger.info("error while sending notification")
							app.logger.debug("cancel email sent")
						try:
							asyncinterviewScheduledNotificationToCelery(candidate_result,asyncinterview.asyncid,common_details,clientip)
						except Exception as e:
							app.logger.info(e)
							app.logger.info("error while sending notification")
						app.logger.info("End of ApiUpdateAsyncInterview - " + str(clientip))
						return {'status':True,'message':'APIUPDATEASYNCINTERVIEW_SUCCESS','code':200}
					else:
						app.logger.info(" End of ApiUpdateAsyncInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						return async_questions_error

				else:
					app.logger.debug("Async Interview "+ str(asyncqid) +"does not exist - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.info("End of ApiUpdateAsyncInterview - " + str(clientip))
					return {'status': False,  'message': 'APIUPDATEASYNCINTERVIEW_INT_NOT_EXIST', 'code':404}
			except JSONDecodeError as re:
				app.logger.info("Json decode error - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.exception(re)
				app.logger.info("End of ApiUpdateAsyncInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_JSON_CONVERSION', 'code':422}

			except RequiredFieldsMissing as re:
				app.logger.info("One or more required fileds missing - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.exception(re)
				app.logger.info("End of ApiUpdateAsyncInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_REQ_MORE_FIELD', 'code':422}
		
			except IntegrityError as ie:
				app.logger.info("Error: DB Exceeding limit Error - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.error(ie.message)
				app.logger.info("End of ApiUpdateAsyncInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_CONTACT_SUPPORT', 'code':409}

			except invalidEmailError as iee:
				app.logger.info("Email ID is not valid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.exception(iee)
				app.logger.error(iee.message)
				app.logger.info("End of ApiUpdateAsyncInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_EMAIL_NOT_VALID', 'code':422}

			except emptyStringError as ese:
				app.logger.info("One of the field is Empty - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.exception(ese)
				app.logger.error(ese.message)
				app.logger.info("End of ApiUpdateAsyncInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_ONE_OF_EMPTY_FIELD', 'code':422}

			except ValueMissing as vm:
				app.logger.info("Received invalid input - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.error(vm.message)
				app.logger.info("End of ApiUpdateAsyncInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_INVALID_INPUT', 'code':422}
			except Exception as e:
				app.logger.info("error while parsing AsyncInterview details - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.exception(e)
				app.logger.info("End of ApiUpdateAsyncInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': False, 'message': 'APIUPDATEASYNCINTERVIEW_FIELDS_MISSING', 'code':422}
		elif user:
			app.logger.debug("User is not authorized to access ApiUpdateAsyncInterview service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.info("End of ApiUpdateAsyncInterview - " + str(clientip))
			return {'status': False,  'message': 'COMMON_UNAUTHORIZED', 'code':403}
		else:
			app.logger.info("Invalid token or Session expired - " + str(clientip))
			app.logger.info("End of ApiUpdateAsyncInterview - " + str(clientip))
			return {'status': False, 'message': 'COMMON_INVALID_TOKEN', 'code':401}


class ApiCancelLiveInterview(RestResource):
	def post(self, interviewid):
		app.logger.info("Acessing ApiCancelLiveInterview")
		clientip = request.remote_addr
		status, user = user_auth(admin_access=C.ADMIN)

		if status and user:
			common_details = {'email':user.email,'orgid':user.orgid,'userid':user.userid}
			app.logger.debug("User is authorized to access ApiCancelLiveInterview service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			try:
				app.logger.info("check interview "+str(interviewid)+" exist or not ")
				interview = Interviews().query.get(interviewid)
				app.logger.info(interview.candidate_joinurl)
				if interview and interview.orgid == user.orgid:
					app.logger.info("interview exist. Cancel now")
					if interview.interviewstatus == C.INTERVIEW_BOOKED:
						interviewers = Interview_Interviewer().query.filter(and_(Interview_Interviewer.interviewid == interviewid,Interview_Interviewer.active_status == True)).all()
						candidateData = Interview_Candidate().query.filter(and_(Interview_Candidate.interviewid == interviewid,Interview_Candidate.active_status == True)).first()
						interview.interviewstatus = C.INTERVIEW_CANCELLED
						interview.updatedtime = datetime.utcnow()
						interview.updatedby = user.userid
						db.session.commit()
						app.logger.info("interview canceld successfully")
						#send emails
						for i in interviewers:
							interviewer = User().query.filter(User.userid ==i.interviewerid).first()
							interview_interviewer_details = Interview_Interviewer().query.filter(Interview_Interviewer.interviewid == i.interviewid,Interview_Interviewer.interviewerid == i.interviewerid).first()
							interviewer_details={'firstname':interviewer.firstname,'lastname':interviewer.lastname,'userid':interviewer.userid,'mobile':interviewer.mobile,'email':interviewer.email}
							interviewerCancelledNotificationToCelery(interviewer_details,interview_interviewer_details,common_details,clientip)
						app.logger.debug("Started Candidate Email notifications process")
						candidate = User().query.filter(User.userid ==candidateData.candidateid).first()
						interview_candidate = Interview_Candidate().query.filter(Interview_Candidate.candidateid == candidate.userid,Interview_Candidate.interviewid== interview.interviewid).first()
						candidate_data={'firstname':candidate.firstname,'lastname':candidate.lastname,'userid':candidate.userid,'mobile':candidate.mobile,'email':candidate.email}
						
						candidateCancelledNotificationToCelery(candidate_data,interview_candidate,common_details,clientip)
						app.logger.info("Sent Email notifications")
						app.logger.info("end of ApiCancelLiveInterview")

						#Removing record in Hasura DB
						url = "http://localhost:5003/meet/cancel"

						payload = {
							"meeting_user_id": candidateData.joinurl
						}

						app.logger.info(payload)

						response_decoded_json = requests.post(url, json=payload)
						response_json = response_decoded_json.json()
						app.logger.debug(response_json)

						if response_json['success']:
							return {'status': True, 'code':200, 'message': 'INTERVIEW_CANCELLED_SUCCESSFULLY'}, 200
						else:
							app.logger.info("Error from IB2 Backend")
							app.logger.info("end of ApiCancelLiveInterview")
							return {'status': False, 'code':403, 'message': 'IB2_BACKEND_ISSUE'}, 200		
					else:
						app.logger.info("interview status is other than booked. So cann't cancel")
						app.logger.info("end of ApiCancelLiveInterview")
						return {'status': False, 'code':403, 'message': 'INTERVIEW_OTHER_THAN_BOOKED'}, 200
				else:
					app.logger.info("Interview not found")
					app.logger.info("end of ApiCancelLiveInterview")
					return {'status': False, 'code':403, 'message': 'COMMON_INTERVIEW_NOT_FOUND'}
			except Exception as e:
				app.logger.info("error while checking interview status")
				app.logger.info(e)
				app.logger.info("end of ApiCancelLiveInterview")
				return {'status': False, 'code':403, 'message': 'COMMON_ERROR_CANCEL_INTERVIEW'}
		else:
			app.logger.info("Invalid token or Session expired - " + str(clientip))
			app.logger.info("End of ApiCancelLiveInterview - " + str(clientip))
			return {'status': False, 'message': 'COMMON_INVALID_TOKEN', 'code':401}




class ApiCancelAsyncInterview(RestResource):
	def post(self, asyncid):
		app.logger.info("Acessing ApiCancelAsyncInterview")
		clientip = request.remote_addr
		status, user = user_auth(admin_access=C.ADMIN)
		if status and user:
			app.logger.debug("User is authorized to access ApiCancelAsyncInterview service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			try:
				app.logger.info("check interview "+str(asyncid)+" exist or not ")
				
				interview = AsyncInterviews().query.get(asyncid)
				if interview and interview.orgid == user.orgid:
					app.logger.info("interview exist. Cancel now")
					if interview.interviewstatus == C.ASYNC_INTERVIEW_SCHEDULED:
						interview.interviewstatus = C.ASYNC_INTERVIEW_CANCELLED
						interview.updatedtime = datetime.utcnow()
						interview.updatedby = user.userid
						db.session.commit()
						candidate = User().query.filter(User.userid ==interview.userid).first()
						candidate_data={'firstname':candidate.firstname,'lastname':candidate.lastname,'userid':candidate.userid,'mobile':candidate.mobile,'email':candidate.email}
						common_details = {'email':user.email,'orgid':user.orgid,'userid':user.userid}
						asyncCandidateCancelledNotificationToCelery(candidate_data,interview,common_details,clientip)
						app.logger.info("interview canceld successfully")
						app.logger.info("end of ApiCancelAsyncInterview")
						return {'status': True, 'code':200, 'message': 'INTERVIEW_CANCELLED_SUCCESSFULLY'}, 200
					else:
						app.logger.info("interview status is other than booked. So cann't cancel")
						app.logger.info("end of ApiCancelAsyncInterview")
						return {'status': False, 'code':403, 'message': 'INTERVIEW_OTHER_THAN_BOOKED'}, 200
				else:
					app.logger.info("Interview not found")
					app.logger.info("end of ApiCancelAsyncInterview")
					return {'status': False, 'code':403, 'message': 'COMMON_INTERVIEW_NOT_FOUND'}
			except Exception as e:
				app.logger.info("error while checking interview status")
				app.logger.info(e)
				app.logger.info("end of ApiCancelAsyncInterview")
				return {'status': False, 'code':403, 'message': 'COMMON_ERROR_CANCEL_INTERVIEW'}
		else:
			app.logger.info("Invalid token or Session expired - " + str(clientip))
			app.logger.info("End of ApiCancelAsyncInterview - " + str(clientip))
			return {'status': False, 'message': 'COMMON_INVALID_TOKEN', 'code':401}


class ApiShareInterview(RestResource):
	def post(self,interview_id):
		app.logger.info("Accessing ApiShareInterview")
		clientip = request.remote_addr
		status, user = user_auth(admin_access=C.ADMIN)
		if status and user:
			app.logger.debug("User is authorized to access ApiShareInterview service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			jsonData = request.get_json()
			app.logger.info(jsonData)
			if 'emails' not in jsonData or 'interview_type' not in jsonData or 'valid_till' not in jsonData or 'share_video' not in jsonData or 'share_feedback' not in jsonData:
				app.logger.info("Error while parsing user details - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.info("End of ApiShareInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': False, 'message': 'APISHAREINTERVIEW_FIELDS_MISSING', 'code':422},422
			else:
				pass

			emails =jsonData['emails']
			interview_type = jsonData['interview_type']
			valid_till = jsonData['valid_till'] + ' 23:59:59'
			share_video = jsonData['share_video']
			share_feedback = jsonData['share_feedback']
			
			app.logger.debug('validate interview_type')
			if interview_type == C.INTERVIEW_TYPE_FACETOFACE or interview_type == C.INTERVIEW_TYPE_ASYNC:
				pass
			else:
				message = 'ISSUE_WITH_INTERVIEW_TYPE'
				app.logger.info("Issue with Interview type")
				app.logger.info("End of ApiShareInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': False, 'message': message, 'code':422},422

			app.logger.info('valid_till')
			app.logger.info(valid_till)
			current_time = str(datetime.utcnow())
			if (current_time < valid_till):
				pass
			else:
				message = 'ISSUE_WITH_VALID_TILL'
				app.logger.info("Issue with Valid till")
				app.logger.info("End of ApiShareInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': False, 'message': message, 'code':422},422

			app.logger.debug('validate share_video')
			if share_video:
				pass
			else:
				share_video = False

			app.logger.debug('validate share_feedback')
			if share_feedback:
				pass
			else:
				share_feedback = False

			app.logger.debug("required fields are valid")

			if interview_type == C.INTERVIEW_TYPE_FACETOFACE:
				app.logger.info("user requested to share a Face to Face interview")
				app.logger.info("validate interview")
				interview = Interviews().query.filter(Interviews.interviewid==interview_id).first()
				if interview is not None:
					app.logger.info("Face to face Interview exist")
					app.logger.info("interview.createdby")
					app.logger.info(interview.createdby)
					app.logger.info("user.userid")
					app.logger.info(user.userid)
					if interview.orgid == user.orgid:
						pass
					else:
						message = 'UNAUTHORIZE_TO_SHARE_INTERVIEW'
						app.logger.info("Unauthorized to share interview")
						app.logger.info("End of ApiShareInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						return {'message':message,'code':422,'status':False},422
				else:
					message = 'INTERVIEW_NOT_FOUND'
					app.logger.info("interview with interview_id : " +str(interview_id) + " not found")
					app.logger.info("End of ApiShareInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {'message':message,'code':404,'status':False},404
			elif interview_type == C.INTERVIEW_TYPE_ASYNC:
				app.logger.info("user requested to share a Asyc interview")
				app.logger.info("validate interview")
				interview = AsyncInterviews().query.filter(AsyncInterviews.asyncid == interview_id).first()
				if interview:
					app.logger.info("Async Interview exist")
					if interview.orgid == user.orgid:
						pass
					else:
						message = 'COMMON_UNAUTHORIZED'
						app.logger.info("Unauthorized to share interview")
						app.logger.info("End of ApiShareInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						return {'message':message,'code':422,'status':False},422
				else:
					message = 'INTERVIEW_NOT_FOUND'
					app.logger.info("Async interview with asyncid : " +str(interview_id) + " not found")
					app.logger.info("End of ApiShareInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {'message':message,'code':404,'status':False},404
			else:
				message = 'ISSUE_WITH_INTERVIEW_TYPE'
				app.logger.info("Invalid interview type")
				app.logger.info("End of ApiShareInterview - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'message':message,'code':422,'status':False},422

			if interview_type == C.INTERVIEW_TYPE_FACETOFACE:
				interview_id = interview.interviewid
				interviewType = 'face to face'
			else:
				interview_id = interview.asyncid
				interviewType = 'async'

			fault_emails = []
			for e in emails:
				app.logger.debug("sharing interview with")
				app.logger.debug(e)
				isEmailValid = validateEmail(e)
				if isEmailValid:
					message,status = shareInterview(interview_type=interview_type,interview_id=interview_id,email_id=e,shared_by=interview.createdby,valid_till=valid_till,share_video=share_video,share_feedback=share_feedback)
					if status:
						url = app.config['APP_URL'] + L.view_interview.format(access_link=str(message.access_link))
						org_signature,org_logo,contactemail,org_name = organizationSignatureAndLogo(interview.orgid)
						content = {'interview_type':interviewType,'admin_firstname':str(user.firstname),'email_id':str(message.email_id),'passcode':str(message.passcode),'url':url,'signature':org_signature,'logo':org_logo,'contactemail':contactemail,'org_name':org_name}
						meta = {'app_name': app.config['APP_NAME']}
						app.logger.debug('content')
						app.logger.debug(content)
						app.logger.debug('meta')
						app.logger.debug(meta)
						# send email notification
						if user.orgid == 13:
							app.logger.info("send custom share email to user")
							em = Email().em_share_interview_custom(meta=meta,content=content)
						else:	
							app.logger.info("send general share email to user")
							em = Email().em_share_interview(meta=meta,content=content)
					else:
						app.logger.info(message)
						app.logger.info("End of ApiShareInterview - " + str(clientip))
						return {'status':False,'code':422,'message':message},422
				else:
					fault_emails.append(e)
			app.logger.info("End of ApiShareInterview - " + str(clientip))
			return {'status':True,'code':201,'message':'INTERVIEWS_SHARED_SUCCESS','fault_emails':fault_emails},201

		else:
			app.logger.info("Invalid token or Session expired - " + str(clientip))
			app.logger.info("End of ApiShareInterview - " + str(clientip))
			return {'status': False, 'message': 'COMMON_INVALID_TOKEN', 'code':401},401


def sharing_link_generator(size=11, chars=string.ascii_uppercase + string.digits):
	app.logger.info("in sharing_link_generator")
	code = ''.join(random.choice(chars) for _ in range(size))
	isShareLinkExist = ShareInterviews.query.filter_by(access_link = code).first()
	if isShareLinkExist:
		app.logger.info("Sharing access_link exist with same code. regenerate it")
		share_link_generator()
	else:
		app.logger.info("successfully generated sharing link")
		return code

def passcode_link_generator(size=6, chars=string.ascii_uppercase + string.digits):
	app.logger.info("in passcode_link_generator")
	code = ''.join(random.choice(chars) for _ in range(size))
	isShareLinkExist = ShareInterviews.query.filter_by(passcode = code).first()
	if isShareLinkExist:
		app.logger.info("passcode exist with same code. regenerate it")
		passcode_link_generator()
	else:
		app.logger.info("successfully generated passcode")
		return code

def shareInterview(interview_type,interview_id,email_id,shared_by,valid_till,share_video,share_feedback):
	shareInterview = ShareInterviews().query.filter(and_(ShareInterviews.email_id==email_id,ShareInterviews.interview_type==interview_type,ShareInterviews.interview_id==interview_id)).first()
	app.logger.info("In shareInterview")
	if shareInterview is None:
		shareInterview = ShareInterviews()
		shareInterview.passcode = passcode_link_generator()
		shareInterview.access_link = sharing_link_generator()
		shareInterview.created_time = datetime.utcnow()
		shareInterview.interview_type = interview_type
		shareInterview.interview_id = interview_id
		shareInterview.email_id = email_id
		shareInterview.shared_by = shared_by
	else:
		app.logger.info("already exist with same details, enabling it")
	shareInterview.valid_till = valid_till
	shareInterview.is_enabled = True
	shareInterview.share_video = share_video
	shareInterview.share_feedback = share_feedback
	db.session.add(shareInterview)
	try:
		db.session.commit()
	except exc.SQLAlchemyError:
		app.logger.debug("Error while committing interview row in database")
		app.logger.info("End of ShareInterview")
		db.session.rollback()
		return 'COMMON_ERROR_IN_ROW_UPDATE',False
	app.logger.info("end of shareInterview")
	return shareInterview,True

class AuthShareInterviewByPasscode(RestResource):
	def get(self, access_link):
		app.logger.info("Accessing AuthShareInterviewByPasscode")
		clientip = request.remote_addr
		app.logger.debug("Check user token - " + str(clientip))
		share_interview_obj = ShareInterviews().query.filter(and_(ShareInterviews.access_link==access_link)).first()
		if share_interview_obj:
			current_time = str(datetime.utcnow())
			if share_interview_obj.is_enabled == False or (str(share_interview_obj.valid_till) < current_time):
				message = 'ACCESS_LINK_EXPIRED'
				app.logger.info("access_link expired or disabled -" +str(clientip))
				app.logger.info("End of AuthShareInterviewByPasscode - " +str(clientip))
				return {'status':False,'code':403,'message':message},403
			else:
				pass
			app.logger.info("Valid access link -" + str(clientip))
			passcode = request.args['passcode']
			if share_interview_obj.passcode == passcode:
				app.logger.info("Passcode is valid")
			else:
				message = 'APIAUTHSHAREINTERVIEWBYPASSCODE_INVALID_PASSCODE'
				app.logger.info("Invalid passcode -" +str(clientip))
				app.logger.info("End of AuthShareInterviewByPasscode - " +str(clientip))
				return {'status':False,'code':403,'message':message},403
		else:
			message = 'APIAUTHSHAREINTERVIEWBYPASSCODE_INVALID_ACCESS_LINK'
			app.logger.info("Invalid Access link -" +str(clientip))
			app.logger.info("End of AuthShareInterviewByPasscode - " +str(clientip))
			return {'status':False,'code':404,'message':'APIAUTHSHAREINTERVIEWBYPASSCODE_INVALID_ACCESS_LINK'},404

		details,status = getShareInterviewDetails(share_interview_obj)
		if status:
			app.logger.info("returning interview details")
			app.logger.info("end of AuthShareInterviewByPasscode")
			if share_interview_obj.interview_type == C.INTERVIEW_TYPE_FACETOFACE:
				return {'code':200,'status':True,'interview_type':share_interview_obj.interview_type,'interview_details':details['interview_details'],'feedback_details':details['feedback_details'],'videoLink':details['videoLink'], 'orgid': details['orgid']},200
			else:
				return {'code':200,'status':True,'interview_type':share_interview_obj.interview_type,'interview_details':details['interview_details'],'question_videos':details['question_videos'], 'orgid': details['orgid']},200
		else:
			app.logger.info(details)
			app.logger.info("end of AuthShareInterviewByPasscode")
			return {'code':422,'status':False,'message':details},422


def getShareInterviewDetails(share_interview_obj):
	app.logger.info("in getShareInterviewDetails")
	app.logger.info("check if link is active or not")
	current_time = str(datetime.utcnow())
	if share_interview_obj.is_enabled == False or (str(share_interview_obj.valid_till) < current_time):
		message = 'ACCESS_LINK_EXPIRED'
		app.logger.info("Interview share link is expired or disabled")
		return message,False
	else:
		pass
	app.logger.info("check interview_id is valid or not & get interview details")
	if share_interview_obj.interview_type == C.INTERVIEW_TYPE_FACETOFACE:
		interview = Interviews().query.filter(Interviews.interviewid == share_interview_obj.interview_id).first()
	else:
		interview = AsyncInterviews().query.filter(AsyncInterviews.asyncid==share_interview_obj.interview_id).first()

	if interview is not None:
		pass
	else:
		message = 'INTERVIEW_NOT_FOUND'
		app.logger.info("Interview not found")
		return message,False

	if share_interview_obj.interview_type == C.INTERVIEW_TYPE_FACETOFACE:
		maskedInterviewDetails,error,status = getMaskedInterviewDetails(share_interview_obj.interview_id)
		query = "select i.interviewid,i.interviewer_id,i.feedback,(select email from users as u where u.userid=i.interviewer_id) as email,(select firstname from users as u where u.userid=i.interviewer_id) as firstname,(select lastname from users as u where u.userid=i.interviewer_id) as lastname from feedback as i where i.interviewid=" + str(interview.interviewid) +";"
		app.logger.info("query")
		app.logger.info(query)
		try:
			result = db.engine.execute(query) 
			interviewerDetailsWithFeedbackList = InterviewerDetailsWithFeedbackSchema().dump(result, many=True).data
			app.logger.info(interviewerDetailsWithFeedbackList)
			app.logger.debug("check if video share is there or not")
			if share_interview_obj.share_video:
				try:
					if interview.archiveid:
						app.logger.info("get archive link")
						videoLink = archive.getArchiveURL(session = interview, user = False)
						app.logger.info('videoLink')
						app.logger.info(videoLink)
					else:
						videoLink = False
				except Exception as e:
					message = 'ISSUE_WHILE_GETTING_VIDEO'
					app.logger.info(e)
					return message,False
			else:
				videoLink = False
			if share_interview_obj.share_feedback:
				pass
			else:
				interviewerDetailsWithFeedbackList = False
			details = {'interview_details':maskedInterviewDetails,'feedback_details':interviewerDetailsWithFeedbackList,'videoLink':videoLink, 'orgid': interview.orgid}
			app.logger.info('returning face to face interview details')
			return details,True
		except Exception as e:
			app.logger.info(e)
	else:
		query_asyc_interview = "select ai.asyncid,ai.scheduledtime,ai.valid_till,ai.interviewstatus,ai.createdtime,(select u.email from users as u where u.userid = ai.userid),(select u.mobile from users as u where u.userid = ai.userid),(select u.firstname from users as u where u.userid = ai.userid),(select u.lastname from users as u where u.userid = ai.userid)  from asyncinterviews as ai where ai.asyncid = "+str(share_interview_obj.interview_id)+";"
		async_interview = db.engine.execute(query_asyc_interview)
		interview_details = AsyncInterviewDetailsSchema().dump(async_interview,many=True).data
		if share_interview_obj.share_video:
			query_async_video = "select at.asyncid,at.asyncqid,at.duration,at.attempted_time,at.recorded_length,at.isanswered,(select aq.question from asyncquestions as aq where aq.asyncqid = at.asyncqid) as question,at.duration,at.videolink,at.recorded_length from asynctest as at where at.asyncid = "+str(share_interview_obj.interview_id)+" and at.isactive = true;"
			async_videos = db.engine.execute(query_async_video)
			question_videos = AsyncInterviewQuestionVideosSchema().dump(async_videos,many=True).data
		else:
			question_videos = False
		details = {'interview_details':interview_details,'question_videos':question_videos, 'orgid': interview.orgid}
		app.logger.info('returning Async interview details')
		return details,True

class ApiGetShareDetails(RestResource):
	def get(self, access_link):
		app.logger.info("Accessing ApiGetShareDetails")
		clientip = request.remote_addr
		app.logger.debug("Clientip - " + str(clientip))
		share_interview_obj = ShareInterviews().query.filter(and_(ShareInterviews.access_link==access_link)).first()
		org_signature = None
		org_logo = None
		contactemail= None
		org_name = None
		if share_interview_obj:
			app.logger.info("Valid access link -" + str(clientip))
			shared_by_user = User().query.filter(User.userid==share_interview_obj.shared_by).first()
			if shared_by_user:
				# logo = getOrgLogo(shared_by_user.orgid)
				org_signature,org_logo,contactemail,org_name = organizationSignatureAndLogo(shared_by_user.orgid)
			else:
				logo = False
				name = False
			if share_interview_obj.is_enabled:
				pass
			else:
				message = 'ACCESS_LINK_EXPIRED'
				app.logger.info("access_link disabled -" +str(clientip))
				app.logger.info("End of ApiGetShareDetails - " +str(clientip))
				return {'status':False,'code':403,'message':message,'org_logo':org_logo, 'org_name': org_name},403
			app.logger.info("End of ApiGetShareDetails - " +str(clientip))
			return {'email_id':share_interview_obj.email_id,'code':200,'status':True,'org_logo':org_logo, 'org_name': org_name},200
		else:
			message = 'APIAUTHSHAREINTERVIEWBYPASSCODE_INVALID_ACCESS_LINK'
			app.logger.info("Invalid Access link -" +str(clientip))
			app.logger.info("End of ApiGetShareDetails - " +str(clientip))
			return {'status':False,'code':404,'message':'APIAUTHSHAREINTERVIEWBYPASSCODE_INVALID_ACCESS_LINK'},404

def getMaskedInterviewDetails(interviewid):
	app.logger.info("Accessing allinterviewDetails")
	interview = Interviews.query.get(interviewid)
	
	#Add rdt to calculate remaining time of a interview .. Used to end the interview based on Server Time exactly
	if not interview:
		app.logger.info("Interview not found - ["+str(interview.orgid)+" ]")
		app.logger.info("End of allinterviewDetails - [" +str(interview.orgid)+" ]")
		return None,{'status': False, 'message': 'COMMON_INTERVIEW_NOT_FOUND', 'code':404},False
	
	logo = getOrgLogo(interview.orgid)
	duration = interview.duration * 60

	
	regular_interviewer=[]
	primary_interviewer =[]
	interviewers_list=[]
	interviewer_details =[]
	candidate_details =[]
	user_details=[]
	interview_interviewer = Interview_Interviewer().query.filter(and_(Interview_Interviewer.interviewid == interview.interviewid,Interview_Interviewer.active_status == True)).all()
	if interview_interviewer:
		for i in interview_interviewer:
			app.logger.info(i)
			interviewers_list = User().query.get(i.interviewerid)
			
			if interviewers_list:
				if i.isprimary == True:
					user_details.append({'primary_interviewerid':i.interviewerid})
					primary_interviewer.append({'firstname':interviewers_list.firstname,'lastname':interviewers_list.lastname,'mobile':interviewers_list.mobile,'email':interviewers_list.email})
				else:
					user_details.append({'interviewerid':i.interviewerid})
					regular_interviewer.append({'firstname':interviewers_list.firstname,'lastname':interviewers_list.lastname,'mobile':interviewers_list.mobile,'email':interviewers_list.email})
	else:
		app.logger.info("Interviewer not found - [" +str(interview.orgid)+" ]")
		app.logger.info("End of allinterviewDetails - [" +str(interview.orgid)+" ]")
		return None,{'status': False, 'message': 'COMMON_INTERVIEWER_NOT_FOUND', 'code':404},False

	interview_candidate = Interview_Candidate().query.filter(and_(Interview_Candidate.interviewid == interview.interviewid,Interview_Candidate.active_status == True)).first()
	candidate_data =[]
	app.logger.info("interview_candidate")
	app.logger.info(interview_candidate)
	if interview_candidate:
		candidate_details = User().query.get(interview_candidate.candidateid)
		app.logger.info("candidate_details in interview_candidate")
		app.logger.info(candidate_details)
		if candidate_details:
			user_details.append({'candidateid':candidate_details.userid})
			candidate_data.append({'firstname':candidate_details.firstname,'lastname':candidate_details.lastname,'mobile':candidate_details.mobile,'email':candidate_details.email})
		else: 
			app.logger.info("Interviewer not found - [" +str(interview.orgid)+" ]")
			app.logger.info("End of allinterviewDetails - [" +str(interview.orgid)+" ]")
			return None,{'status': False, 'message': 'COMMON_CANDIDATE_NOT_FOUND', 'code':404},False				
	
	interview_date ={'date':interview.date}
	date_result = InterviewDetailsSchema().dump(interview_date).data
	interviewDateTime = getInterviewTimeByDateSlotid(interview.date,interview.slot_id)
	interview_list ={'primaryinterviewer':primary_interviewer,'interviewer_list':regular_interviewer,'candidate_data':candidate_data,'user_details':user_details, 'interviewid': interview.interviewid,'interviewtitle': interview.interviewtitle,'ftid':interview.ftid,'interiewstatus':interview.interviewstatus,'date':date_result,'slot_id':interview.slot_id,'duration':interview.duration,'interviewdescription':interview.interviewdescription,'recording':interview.prerecording_enabled,'interviewtime':interviewDateTime}
	
	app.logger.info("End of allinterviewDetails - [" +str(interview.orgid)+" ]")
	return interview_list ,None,True


class ApiAddCustomCategories(RestResource):
	def post(self):
		app.logger.info("Accessing ApiAddCustomCategories")
		clientip = request.remote_addr
		app.logger.debug("Check user token - " + str(clientip))
		status, user = user_auth(admin_access=C.ADMIN)
		if status and user:
			app.logger.debug("User is authorized to access ApiAddCustomCategories service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			# read parameters
			jsonData = request.get_json()
			app.logger.info(jsonData)
			if 'custom_categories' not in jsonData or 'status' not in jsonData:
				app.logger.info("Error while parsing json details - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.info("End of ApiAddCustomCategories - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': False, 'message': 'APIADDCUSTOMCATEGORIES_FIELDS_MISSING', 'code':422},422
			else:
				pass

			if isinstance(jsonData['custom_categories'],list):
				pass
			else:
				app.logger.info("custom_categories not valid")
				app.logger.info("End of ApiAddCustomCategories - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': False, 'message': 'ERROR_RECEIVING_CUSTOM_CATEGORIES', 'code': 422}, 422

			custom_categories = {'custom_categories':json.dumps(jsonData['custom_categories'])}	
			app.logger.info("custom_categories")
			app.logger.info(custom_categories)

			status = jsonData['status']
			orgid = user.orgid

			app.logger.info("status")
			app.logger.info(status)

			message,status = addCustomCategories(orgid,custom_categories,status)
			if status == True:
				app.logger.info("End of ApiAddCustomCategories - " + str(clientip))
				return {'code':201,'status':True,'message':'APIADDCUSTOMCATEGORIES_SUCCESS','cc_status':message.status,'custom_categories':message.custom_categories["custom_categories"]},200
			else:
				app.logger.info("End of ApiAddCustomCategories - " + str(clientip))
				return {'code':422,'status':False,'message':message},422

		elif user:
			app.logger.debug("User is not authorized to access ApiAddCustomCategories service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.info("End of ApiAddCustomCategories - " + str(clientip))
			return {'status': False,  'message': 'COMMON_UNAUTHORIZED', 'code':403}
		else:
			app.logger.info("Invalid token or Session expired - " + str(clientip))
			app.logger.info("End of ApiAddCustomCategories - " + str(clientip))
			return {'status': False, 'message': 'COMMON_INVALID_TOKEN', 'code':401}

def addCustomCategories(orgid,custom_categories,status):
	app.logger.info("In addCustomCategories")
	org_custom_data = OrgCustomCategories().query.filter(OrgCustomCategories.orgid==orgid).first()
	app.logger.info("Check if custom_categories data exist for this orgid:" +str(orgid))
	if org_custom_data is not None:
		app.logger.info("custom_categories data exists")

		if status == C.CC_DRAFT_STATE:
			app.logger.info("user requested to change status to draft state")
			if org_custom_data.status == C.CC_DRAFT_STATE:
				org_custom_data.custom_categories = custom_categories
				org_custom_data.status = status
				app.logger.info("********** status of custom fields "+ str(status))
			else:
				return "STATUS_CHANGE_NOT_POSSIBLE",False
		elif status == C.CC_ACTIVE:
			app.logger.info("user requested to change status to active state")
			org_custom_data.custom_categories = custom_categories
			org_custom_data.status = status
			app.logger.info("********** status of custom fields "+ str(status))
			
		elif status == C.CC_INACTIVE:
			app.logger.info("user requested to change status to inactive state")
			org_custom_data.status = status
			org_custom_data.custom_categories = custom_categories
			app.logger.info("********** status of custom fields "+ str(status))
	else:
		app.logger.info("custom_categories data does not exists")
		org_custom_data = OrgCustomCategories()
		org_custom_data.orgid = orgid
		org_custom_data.custom_categories = custom_categories
		org_custom_data.status = status
		db.session.add(org_custom_data)
		app.logger.info("********** status of custom fields "+ str(status))
	try:
		db.session.commit()
	except exc.SQLAlchemyError:
		app.logger.debug("Error while committing custom_categories row in database")
		app.logger.info("End of addCustomCategories")
		db.session.rollback()
		return 'COMMON_ERROR_IN_ROW_UPDATE',False
	app.logger.info("update custom_categories success")
	return org_custom_data,True

class ApiGetCustomCategories(RestResource):
	def get(self):
		app.logger.info("Accessing ApiGetCustomCategories")
		clientip = request.remote_addr
		app.logger.debug("Check user token - " + str(clientip))
		status, user = user_auth(admin_access=C.ADMIN)
		if status and user:
			app.logger.debug("User is authorized to access ApiGetCustomCategories service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			message,status = getCustomCategoriesByOrgid(user.orgid)
			app.logger.info("End of ApiGetCustomCategories - " + str(clientip))
			if status:
				return {'code':200,'status':True,'custom_categories':message['custom_categories'],'cc_status':message['cc_status']},200
			else:
				return {'code':422,'status':False,'message':message},422

		elif user:
			app.logger.debug("User is not authorized to access ApiAddCustomCategories service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.info("End of ApiAddCustomCategories - " + str(clientip))
			return {'status': False,  'message': 'COMMON_UNAUTHORIZED', 'code':403}
		else:
			app.logger.info("Invalid token or Session expired - " + str(clientip))
			app.logger.info("End of ApiAddCustomCategories - " + str(clientip))
			return {'status': False, 'message': 'COMMON_INVALID_TOKEN', 'code':401}

def getCustomCategoriesByOrgid(orgid):
	app.logger.info("In getCustomCategoriesByOrgid")
	org_custom_data = OrgCustomCategories().query.filter(OrgCustomCategories.orgid==orgid).first()
	if org_custom_data is not None:
		pass
	else:
		app.logger.info("custom categories data not exists for this orgid:" +str(orgid))
		message = 'CC_DATA_NOT_EXISTS'
		return message,False

	custom_categories = org_custom_data.custom_categories["custom_categories"]
	app.logger.info("returning custom categories data")
	ret = {'custom_categories':org_custom_data.custom_categories["custom_categories"],'cc_status':org_custom_data.status}
	return ret,True

def addInterviewCustomCategoriesData(interview_type,interview_id,custom_categories_data):
	app.logger.info("In addInterviewCustomCategoriesData")
	if interview_type == C.INTERVIEW_TYPE_FACETOFACE:
		app.logger.info("user selected face to face interview")
		interview = Interviews().query.filter(Interviews.interviewid==interview_id).first()
	else:
		app.logger.info("user selected async interview")
		interview = AsyncInterviews().query.filter(AsyncInterviews.asyncid==interview_id).first()
	if interview is not None:
		app.logger.info("Interview found")
	else:
		app.logger.info("Interview not found")
		message = 'INTERVIEW_NOT_FOUND',False
		return message,False
	app.logger.info("custom_categories_data")
	app.logger.info(custom_categories_data)
	interview.custom_categories_data = custom_categories_data
	try:
		db.session.commit()
	except exc.SQLAlchemyError:
		app.logger.debug("Error while committing custom_categories_data into interview row in database")
		app.logger.info("End of addInterviewCustomCategoriesData")
		db.session.rollback()
		return 'COMMON_ERROR_IN_ROW_UPDATE',False


def getCcDataByInterviewidByOrgidByInterviewtype(intid,orgid,interview_type):
	app.logger.info("In getCcDataByInterviewidByOrgidByInterviewtype")
	app.logger.info("interview_type: "+ str(interview_type))
	org_custom_data = OrgCustomCategories().query.filter(OrgCustomCategories.orgid==orgid).first()
	if org_custom_data is not None:
		app.logger.info("custom_categories data exists")
		app.logger.info("update query with custom fields")
		custom_categories = json.loads(org_custom_data.custom_categories["custom_categories"])
		app.logger.info(custom_categories)

		cc_select_details = ""
		categories_list = []
		for category in custom_categories:
			# app.logger.info(category)
			categories_list.append(category)
			# i.custom_categories_data -> 'custom_categories_data' ->> 'Section',
			cc_select_details = cc_select_details + " i.custom_categories_data -> '"+category+"' as \""+category+"\","

		cc_select_details = cc_select_details[0:len(cc_select_details)-1]

		if interview_type == C.INTERVIEW_TYPE_FACETOFACE:
			table_name = " interviews "
			interview_field_name = " interviewid "
		else:
			table_name = " asyncinterviews "
			interview_field_name = " asyncid "
		query = "select " + cc_select_details + " from " + str(table_name) + " as i where "+ str(interview_field_name) +" = " + str(intid) + ";"
		app.logger.info(query)
		results = db.engine.execute(query)
		cc_result = [dict (r) for r in results]
		if len(cc_result) > 0:
			cc_result = cc_result[0]
		else:
			cc_result = None
		app.logger.info('end of getCcDataByInterviewidByOrgidByInterviewtype')
		return cc_result,org_custom_data.status,True
	else:
		cc_result = None
		app.logger.info("organization with orgid:" + str(orgid) + "not found")
		app.logger.info('end of getCcDataByInterviewidByOrgidByInterviewtype')
		return cc_result,False,False


class ApiUpcomingInterviewsExport(RestResource):
	def get(self, tempTimeStampVar): 
		app.logger.info("Accessing ApiUpcomingInterviewsExport")
		clientip = request.remote_addr
		status, user = user_auth(admin_access=C.ADMIN)
		if status and user:
			app.logger.debug("User is authorized to access ApiUpcomingInterviewsExport service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			# read parameters
			app.logger.info("Request args")
			app.logger.info(request.args)
			if 'status' in request.args:
				app.logger.info("Status exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			else:
				app.logger.info("Status not exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				raise RequiredFieldsMissing()
			if 'orgid' in request.args:
				app.logger.info("Orgid exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			else:
				app.logger.info("Orgid not exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				raise RequiredFieldsMissing()
			if 'start_date' in request.args:
				app.logger.info("Start date exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			else:
				app.logger.info("Start Date not exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				raise RequiredFieldsMissing()
			if 'end_date' in request.args:
				app.logger.info("End Date exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			else:
				app.logger.info("End Date not exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				raise RequiredFieldsMissing()
			try:
				app.logger.info("Request args")
				app.logger.info(request.args)
				interviewstatus = request.args['status']
				orgid = request.args['orgid']
				start_date = request.args['start_date']
				end_date = request.args['end_date']


				if int(interviewstatus) >=0 :
					app.logger.info("Status is valid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					interviewstatus = int(interviewstatus)
				else:
					app.logger.info("Status is not valid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.info("End of ApiUpcomingInterviewsExport - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {'status': False, 'message': 'APIUPDATEUSER_STATUS_INVALID', 'code':422}

				if int(orgid) > 0:
					app.logger.info("Orgid is valid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				else:
					app.logger.info("Orgid is not valid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.info("End of ApiUpcomingInterviewsExport - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {'status': False, 'message': 'APICREATEUSER_ORGID_INVALID', 'code':422}

				isStartDateString = validateString(start_date)
				if isStartDateString:
					app.logger.info("Start_date is not empty - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					dateValidation = datetime.strptime(start_date, "%Y-%m-%d")
					s_date = date(*map(int, start_date.split('-')))
					if dateValidation:
						app.logger.debug("Date format is valid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					else:
						app.logger.debug("Date format is not valid "+str(start_date)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						app.logger.info("End of ApiUpcomingInterviewsExport - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						return {'status': False, 'message': 'APISCHEDULEINTERVIEW_DATE_INVALID', 'code':422}
				else:
					app.logger.debug("Start_date is empty"+str(start_date)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					raise emptyStringError()

				isEndDateString = validateString(end_date)
				if isEndDateString:
					app.logger.info("Start_date is not empty - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					dateValidation = datetime.strptime(end_date, "%Y-%m-%d")
					e_date = date(*map(int, end_date.split('-')))
					if dateValidation:
						app.logger.debug("Date format is valid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					else:
						app.logger.debug("Date format is not valid "+str(end_date)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						app.logger.info("End of ApiUpcomingInterviewsExport - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						return {'status': False, 'message': 'APISCHEDULEINTERVIEW_DATE_INVALID', 'code':422}
				else:
					app.logger.debug("Start_date is empty"+str(end_date)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					raise emptyStringError()
				
				list_of_interviews=[]
				regular_interviewer_data=[]
				primary_interviewer =[]
				result_data=[]
				candidate_data =[]
				list_of_interviewers=[]
				final_data=[]
				if s_date <= e_date:
					slot_id,cur_date = calculateCurrentSlot()
					select_q = "select "
					cc_select_details = ""
					categories_list = []

					# Check CustomCategories exist or not
					org_custom_data = OrgCustomCategories().query.filter(OrgCustomCategories.orgid==orgid).first()
					app.logger.info("Check if custom_categories data exist for this orgid:" +str(orgid))
					if org_custom_data is not None:
						app.logger.info("custom_categories data exists")
						if org_custom_data.status == C.CC_ACTIVE or org_custom_data.status == C.CC_INACTIVE:
							app.logger.info("update query with custom fields")
							custom_categories = json.loads(org_custom_data.custom_categories["custom_categories"])

							
							for category in custom_categories:
								app.logger.info(category)
								categories_list.append(category.lower())
								# i.custom_categories_data -> 'Section',
								cc_select_details = cc_select_details + " i.custom_categories_data -> '"+category+"' as "+category+","
					else:
						app.logger.info("no custom categories")
					
					upcomingInterviews = select_q + cc_select_details+ " i.interviewid, i.date, i.slot_id, i.duration,i.interviewstatus as interview_status, (select u.email from users as u where u.userid=(select c.interviewerid from interview_interviewer as c where c.interviewid=i.interviewid and c.isprimary=true and active_status = true)) as interviewer_email, (select u.email from users as u where u.userid=(select c.candidateid from interview_candidate as c where c.interviewid=i.interviewid and c.active_status = true)) as candidate_email from interviews as i where (i.date >= '"+str(start_date)+"' and  i.date <= '"+str(end_date)+"') "
					
					
					if interviewstatus > 0:
						upcomingInterviews +=" and i.interviewstatus="+str(interviewstatus)
					upcomingInterviews += " and i.orgid="+str(orgid)+";"
					app.logger.info(upcomingInterviews)
					
					upcoming_interviews = db.engine.execute(upcomingInterviews)
					

					# result = [dict(r) for r in upcoming_interviews]
					result =  upcoming_interviews
					app.logger.info(result)

					app.logger.info("length of categories_list")
					app.logger.info(len(categories_list))
					
					csvsio = StringIO.StringIO()
					cw = csv.writer(csvsio)

					isCustomCategoriesExist = False
					
					columns = ['Interview ID', 'Primary Interviewer Email', 'Candidate Email', 'Interview Status','Interview Time in UTC', 'Duration (in Mins)', 'Overall Rating' ] 
					if len(categories_list) > 0:
						isCustomCategoriesExist = True
						for category in categories_list:
							columns = columns + [str(category)]

					app.logger.info("********* total columns**********")
					app.logger.info(columns)

					# header for document
					cw.writerow(columns)

					# Update Rows in document
					for interview in result:					
						rowData = [interview['interviewid'], interview['interviewer_email'], interview['candidate_email'], getF2FInterviewStatus(interview['interview_status']), getInterviewTimeByDateSlotid(interview['date'], interview['slot_id']), interview['duration']]

						# find overall rating
						if interview['interview_status'] == C.INTERVIEW_SUCCESSFUL:
							overallRating = getOverallRating(interview['interviewid'])
						else:
							overallRating = None
						rowData = rowData + [overallRating]
						
						if isCustomCategoriesExist:
							for category in categories_list:
								app.logger.info(interview[str(category)])
								rowData = rowData + [interview[str(category)]]
						app.logger.info("rowData")
						app.logger.info(rowData)
						cw.writerow(rowData)	
					csvsio.seek(0)
					fn = 'interviews_data.csv'
					app.logger.info("End of ApiUpcomingInterviewsExport - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")

					return send_file(csvsio, mimetype='text/csv', as_attachment=True, attachment_filename=fn)



				else:
					app.logger.debug("Start Date must be less than End Date - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.info("End of ApiUpcomingInterviewsExport - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {'status':False,'code':405,'message':'APIUPCOMINGINTERVIEWS_DATE_COMPARISION'}
			
			except RequiredFieldsMissing as re:
				app.logger.info("One or more required fileds missing - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.exception(re)
				app.logger.info("End of ApiUpcomingInterviewsExport - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_REQ_MORE_FIELD', 'code':422}
			
			except IntegrityError as ie:
				app.logger.info("Error: DB Exceeding limit Error - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.error(ie.message)
				app.logger.info("End of ApiUpcomingInterviewsExport - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_CONTACT_SUPPORT', 'code':409}
			except DataError as de:
				app.logger.info("DataBase field value out of range - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.error(de.message)
				app.logger.info("End of ApiUpcomingInterviewsExport - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_INVALID_DATA', 'code':422}
			except InvalidRequestError as ir:
				app.logger.info("received invalid request - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.exception(ir)
				app.logger.error(ir.message)
				app.logger.info("End of ApiUpcomingInterviewsExport - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_INVALID_REQUSET', 'code':422}
			except ValueError as ve:
				app.logger.info("Date Format is invalid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.error(ve.message)
				app.logger.info("End of ApiUpcomingInterviewsExport - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': str(ve.message), 'code':422}
			except ValueMissing as vm:
				app.logger.info("Received invalid input - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.error(vm.message)
				app.logger.info("End of ApiUpcomingInterviewsExport - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_INVALID_INPUT', 'code':422}
			except invalidEmailError as iee:
				app.logger.info("Email ID is not valid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.exception(iee)
				app.logger.error(iee.message)
				app.logger.info("End of ApiUpcomingInterviewsExport - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_EMAIL_NOT_VALID', 'code':422}
			except emptyStringError as ese:
				app.logger.info("One of the field is Empty - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.exception(ese)
				app.logger.error(ese.message)
				app.logger.info("End of ApiUpcomingInterviewsExport - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_ONE_OF_EMPTY_FIELD', 'code':422}
			except Exception as e:
				app.logger.info("Error while ApiUpcomingInterviewsExport- " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.exception(e.message)
				app.logger.info("End of ApiUpcomingInterviewsExport - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': False, 'message': 'APIUPCOMINGINTERVIEWS_FAIL_INTERVIEWS', 'code':422}

		elif user:
			app.logger.debug("User is not authorized to access ApiUpcomingInterviewsExport service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.info("End of ApiUpcomingInterviewsExport  - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			return {'status': False,  'message': 'COMMON_UNAUTHORIZED', 'code':403}
		else:
			app.logger.info("Invalid token or Session expired - " + str(clientip))
			app.logger.info("End of ApiUpcomingInterviewsExport - " + str(clientip))
			return {'status': False, 'message': 'COMMON_INVALID_TOKEN', 'code':401}


def getF2FInterviewStatus(interiew_status):
	if interiew_status == C.INTERVIEW_BOOKED:
		return "Interview Booked"
	elif interiew_status == C.INTERVIEW_OT_INTERVIEWER_JOINED:
		return "Interviewer Joined"
	elif interiew_status == C.INTERVIEW_OT_CANDIDATE_JOINED:
		return "Candidate Joined"
	elif interiew_status == C.INTERVIEW_IN_PROGRESS:
		return "Interview inprogress"
	elif interiew_status == C.INTERVIEW_INTERVIEWER_JOINED_AND_QUIT:
		return "Interviewer only joined and quit from Interview"
	elif interiew_status == C.INTERVIEW_CANDIDATE_JOINED_AND_QUIT:
		return "Candidate only joined and quit from Interview"
	elif interiew_status == C.INTERVIEW_INTERVIEWER_TERMINATE:
		return "Interviewer quit from Interview"
	elif interiew_status == C.INTERVIEW_CANDIDATE_TERMINATE:
		return "Candidate quit from Interview"
	elif interiew_status == C.INTERVIEW_INTERVIEWER_ONLY_JOINED:
		return "Interviewer only joined and quit from Interview"
	elif interiew_status == C.INTERVIEW_CANDIDATE_ONLY_JOINED:
		return "Candidate only joined and quit from Interview"
	elif interiew_status == C.INTERVIEW_SUCCESSFUL:
		return "Interview Successful"
	elif interiew_status == C.INTERVIEW_FAILED:
		return "Interview Failed"
	elif interiew_status == C.INTERVIEW_CANCELLED:
		return "Interview Cancelled"
		

class ApiAsycInterviewsExport(RestResource):
	def get(self, tempTimeStampVar): 
		app.logger.info("Accessing ApiAsycInterviewsExport")
		clientip = request.remote_addr
		status, user = user_auth(admin_access=C.ADMIN)
		if status and user:
			app.logger.debug("User is authorized to access ApiAsycInterviewsExport service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			if 'status' in request.args:
				app.logger.info("Status exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			else:
				app.logger.info("Status not exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.info("End of ApiAsycInterviewsExport - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': False, 'message': 'APIUPDATEUSER_STATUS_INVALID', 'code':422}
			if 'orgid' in request.args:
				app.logger.info("Orgid exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			else:
				app.logger.info("Orgid not exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.info("End of ApiAsycInterviewsExport - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': False, 'message': 'APIORGUPDATE_ORG_NOT_FOUND', 'code':422}
			if 'start_date' in request.args:
				app.logger.info("Start date exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			else:
				app.logger.info("Start Date not exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.info("End of ApiAsycInterviewsExport - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': False, 'message': 'APIPREVIOUSINTERVIEWS_STDATE_ERROR', 'code':422}
			if 'end_date' in request.args:
				app.logger.info("End Date exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			else:
				app.logger.info("End Date not exists - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.info("End of ApiAsycInterviewsExport - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': False, 'message': 'APIPREVIOUSINTERVIEWS_EDATE_ERROR', 'code':422}
			
			try:
				app.logger.info("Request args")
				app.logger.info(request.args)
				interviewstatus = request.args['status']
				orgid = request.args['orgid']
				start_date = request.args['start_date']
				end_date = request.args['end_date']
			
				current_time = datetime.utcnow()
				if int(interviewstatus) >= 0:
					interviewstatus = int(interviewstatus)
					app.logger.info("Status is valid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				else:
					app.logger.info("Status is not valid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.info("End of ApiAsycInterviewsExport - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {'status': False, 'message': 'APIUPDATEUSER_STATUS_INVALID', 'code':422}

				if int(orgid) > 0:
					app.logger.info("Orgid is valid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				else:
					app.logger.info("Orgid is not valid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.info("End of ApiAsycInterviewsExport - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {'status': False, 'message': 'APICREATEUSER_ORGID_INVALID', 'code':422}

				isStartDateString = validateString(start_date)
				if isStartDateString:
					app.logger.info("Start_date is not empty - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					try:
						startdateValidation = datetime.strptime(start_date, "%Y-%m-%d")
						app.logger.info(startdateValidation)
						start_d = startdateValidation + timedelta(hours=0,minutes=0)  
						app.logger.debug("Start Date format is valid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					except Exception as InvalidDateFormat:
						app.logger.info(InvalidDateFormat)
						app.logger.debug("Start Date format is not valid "+str(start_date)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						app.logger.info("End of ApiAsycInterviewsExport - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						return {'status': False, 'message': 'APISCHEDULEINTERVIEW_DATE_INVALID', 'code':422}
				else:
					app.logger.debug("Start_date is empty"+str(start_date)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					raise emptyStringError()

				isEndDateString = validateString(end_date)
				if isEndDateString:
					app.logger.info("End Date is not empty - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					try:
						enddateValidation = datetime.strptime(end_date, "%Y-%m-%d")
						end_d = enddateValidation + timedelta(hours=23,minutes=59)  
						app.logger.info(enddateValidation)
						app.logger.debug("End Date format is valid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					except Exception as InvalidDateFormat:
						app.logger.info(InvalidDateFormat)
						app.logger.debug("End Date format is not valid "+str(end_date)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						app.logger.info("End of ApiAsycInterviewsExport - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
						return {'status': False, 'message': 'APISCHEDULEINTERVIEW_DATE_INVALID', 'code':422}
				else:
					app.logger.debug("Start_date is empty"+str(end_date)+" - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					raise emptyStringError()
			
				slot_id,cur_date = calculateCurrentSlot()

				app.logger.debug("startdateValidation and enddateValidation")
				app.logger.debug(startdateValidation)
				app.logger.debug(enddateValidation)
				# Fetching all rows based on Database
				if startdateValidation <= enddateValidation:
					select_q = "select "
					cc_select_details = ""
					categories_list = []
					orgid = user.orgid
					# Check CustomCategories exist or not
					org_custom_data = OrgCustomCategories().query.filter(OrgCustomCategories.orgid==orgid).first()
					app.logger.info("Check if custom_categories data exist for this orgid:" +str(orgid))
					if org_custom_data is not None:
						app.logger.info("custom_categories data exists")
						if org_custom_data.status == C.CC_ACTIVE or org_custom_data.status == C.CC_INACTIVE:
							app.logger.info("update query with custom fields")
							custom_categories = json.loads(org_custom_data.custom_categories["custom_categories"])

							
							for category in custom_categories:
								app.logger.info(category)
								categories_list.append(category.lower())
								# i.custom_categories_data -> 'Section',
								cc_select_details = cc_select_details + " i.custom_categories_data -> '"+category+"' as "+category+","
					else:
						app.logger.info("no custom categories")
					
					previousInterviews = select_q + cc_select_details+ "  i.asyncid, i.scheduledtime, i.orgid, i.interviewstatus as interview_status,i.valid_till,i.userid, (select u.email from users as u where u.userid= i.userid) as candidate_email from asyncinterviews as i where  i.scheduledtime >= '"+str(start_d)+"' and i.scheduledtime <= '"+str(end_d)+"'"
					
					if interviewstatus > 0:
						previousInterviews +=" and i.interviewstatus="+str(interviewstatus)+" "
					previousInterviews += " and i.orgid="+str(orgid)+";"
					app.logger.info("final export query")
					app.logger.info(previousInterviews)
					
					export_result = db.engine.execute(previousInterviews)
					
					app.logger.info(type(export_result))

					
					result = export_result
					app.logger.info("result after processing")
					app.logger.info(result)

					app.logger.info("length of categories_list")
					app.logger.info(len(categories_list))
					
					csvsio = StringIO.StringIO()
					cw = csv.writer(csvsio)
					
					
					columns = ['Asycn Interview ID', 'Candidate Email',  'Interview Status', 'Scheduled Time(in UTC)', 'Valid Till'] 
					isCustomCategoriesExist = False
					if len(categories_list) > 0:
						isCustomCategoriesExist = True
						for category in categories_list:
							columns = columns + [str(category)]
						
					app.logger.info("********* total columns**********")
					app.logger.info(columns)

					# header for document
					cw.writerow(columns)
					for interview in result:
						app.logger.info("")						
						rowData = [interview['asyncid'], interview['candidate_email'],getAsyncInterviewStatus(interview['interview_status']), interview['scheduledtime'], interview['valid_till']]
						
						if isCustomCategoriesExist:
							for category in categories_list:
								app.logger.info(interview[str(category)])
								rowData = rowData + [interview[str(category)]]
						
						cw.writerow(rowData)	
					
					csvsio.seek(0)
					fn = 'async_interviews_data.csv'
					app.logger.info("End of ApiAsycInterviewsExport - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")

					return send_file(csvsio, mimetype='text/csv', as_attachment=True, attachment_filename=fn)
				else:
					app.logger.debug("Start Date must be less than End Date - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					app.logger.info("End of ApiAsycInterviewsExport - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
					return {'status': False, 'message': 'APIUPCOMINGINTERVIEWS_DATE_COMPARISION', 'code':422}
		

			except RequiredFieldsMissing as re:
				app.logger.info("One or more required fileds missing - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.exception(re)
				app.logger.info("End of ApiAsycInterviewsExport - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_REQ_MORE_FIELD', 'code':422}
			
			except IntegrityError as ie:
				app.logger.info("Error: DB Exceeding limit Error - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.error(ie.message)
				app.logger.info("End of ApiAsycInterviewsExport - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_CONTACT_SUPPORT', 'code':409}
			except DataError as de:
				app.logger.info("DataBase field value out of range - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.error(de.message)
				app.logger.info("End of ApiAsycInterviewsExport - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_INVALID_DATA', 'code':422}
			except InvalidRequestError as ir:
				app.logger.info("received invalid request - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.exception(ir)
				app.logger.error(ir.message)
				app.logger.info("End of ApiAsycInterviewsExport - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_INVALID_REQUSET', 'code':422}
			except ValueError as ve:
				app.logger.info("Date Format is invalid - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.error(ve.message)
				app.logger.info("End of ApiAsycInterviewsExport - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': str(ve.message), 'code':422}
			except ValueMissing as vm:
				app.logger.info("Received invalid input - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.error(vm.message)
				app.logger.info("End of ApiAsycInterviewsExport - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				db.session.rollback()
				return {'status': False, 'message': 'COMMON_ERROR_INVALID_INPUT', 'code':422}
			except Exception as e:
				app.logger.info("Error while ApiAsycInterviewsExport- " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				app.logger.exception(e.message)
				app.logger.info("End of ApiAsycInterviewsExport - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
				return {'status': False, 'message': 'APIASYNCPREVIOUSINTERVIEWS_LIST_FAILS', 'code':422}
		elif user:
			app.logger.debug("User is not authorized to access ApiAsycInterviewsExport service - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			app.logger.info("End of ApiAsycInterviewsExport  - " + str(clientip) + "- [ " + str(user.userid)+" : "+str(user.orgid)+" ]")
			return {'status': False,  'message': 'COMMON_UNAUTHORIZED', 'code':403}
		else:
			app.logger.info("Invalid token or Session expired - " + str(clientip))
			app.logger.info("End of ApiAsycInterviewsExport - " + str(clientip))
			return {'status': False, 'message': 'COMMON_INVALID_TOKEN', 'code':401}


def getAsyncInterviewStatus(interiew_status):
	if interiew_status == C.ASYNC_INTERVIEW_SCHEDULED:
		return "Interview Booked"
	elif interiew_status == C.ASYNC_INTERVIEW_CANCELLED:
		return "Interview Cancelled"
	elif interiew_status == C.ASYNC_INTERVIEW_IN_PROGRESS:
		return "Interview inprogress"
	elif interiew_status == C.ASYNC_INTERVIEW_SUCCESS:
		return "Interview Successful"
	elif interiew_status == C.ASYNC_INTERVIEW_EXPIRED:
		return "Interview Expired"


def getOverallRating(interview_id):
	app.logger.info("in getOverallRating")
	app.logger.info("get interview overall rating based on interviewid")

	interviewFeedbacks = InterviewFeedback().query.filter(InterviewFeedback.interviewid == interview_id).all()
	# feedback = InterviewFeedback().query.filter(and_(InterviewFeedback.interviewid==interview_interviewer.interviewid,InterviewFeedback.interviewer_id==interview_interviewer.interviewerid)).first()

	if interviewFeedbacks:
		app.logger.debug("found feedbacks for interview "+str(interview_id))
		totalFeedbacks = 0
		totalRating = 0
		haveFeedbacks = False
		for feedback in interviewFeedbacks:
			app.logger.info("feedback.feedback")
			app.logger.info(feedback)
			feedbackInfo = json.loads(feedback.feedback)
			app.logger.info(feedbackInfo)
			if len(feedbackInfo) > 0:
				for individualFeedback in feedbackInfo:
					app.logger.info("individualFeedback")
					app.logger.info(individualFeedback)
					app.logger.info(type(individualFeedback))
					if "rating" not in individualFeedback:
						pass
					else:
						haveFeedbacks = True
						app.logger.info(individualFeedback["rating"])
						totalRating = totalRating + individualFeedback["rating"]
						totalFeedbacks = totalFeedbacks + 1
		if haveFeedbacks:
			app.logger.info("Total Feedbacks and Total rating")
			app.logger.info(totalFeedbacks)
			app.logger.info(totalRating)
			finalRating = totalRating/totalFeedbacks
			app.logger.info("finalRating")
			app.logger.info(finalRating)
			return round(finalRating, 2)
		else:
			return None
	else:
		app.logger.debug("no feedback for interview "+str(interview_id))
		return None

	return None

class GetArchive(RestResource):
	def get(self, archive_id):
		app.logger.info("archive_id")
		app.logger.info(archive_id)
		archive = ot.get_archive(archive_id)
	
		app.logger.info("archive.json()")
		app.logger.info(archive.json())

		return archive.json()
		
import requests
from json2html import *
class ApiSaveSystemLogs(RestResource):
	def post(self):
		app.logger.info("in ApiSaveSystemLogs")
		clientip = request.remote_addr
		app.logger.info("read request data "+str(clientip))
		app.logger.info(request.data)
		jsonData = json.loads(request.data)
		app.logger.info(jsonData)
		app.logger.info(jsonData[0]["Email"])
		systemLogs = SystemTestLogs()
		systemLogs.logs = request.data
		systemLogs.created_on = datetime.utcnow()
		db.session.add(systemLogs)
		db.session.commit()
		app.logger.info("confver json data to html")
		body_template = (json2html.convert(json = jsonData, table_attributes="id=\"info-table\" class=\"table table-bordered table-hover\""))
		app.logger.info("calling send email")
		#Email().em_send_system_logs(systemLogs.logid, "hello@interviewbuddy.app", jsonData[0]["Email"], body_template, cc_email="srgsatishsrg@gmail.com")
		Email().em_send_system_logs(systemLogs.logid, "hello@interviewbuddy.app", jsonData[0]["Email"], body_template, cc_email=app.config['DEFAULT_CC_EMAIL'])
		app.logger.info("end of ApiSaveSystemLogs")
		return {"status": "success"}

		
# import schedule
# import psycopg2
# class dbAutomate(RestResource):

# 	def get(self):
# 		def automate():
# 			conn = psycopg2.connect(
# 			database='defaultdb', user='db-b2b', password=app.config['DATABASE_PASSWORD'], host='b2b-prod-do-user-1875010-0.b.db.ondigitalocean.com', port=25060
# 			)
# 			cur = conn.cursor()
# 			api_new = '46118932'
# 			session_new = str(ot.create_session(record=False))
# 			token_new = str(ot.create_token(session_new, role='p', ot_expires_in=app.config['SELF_RECORD_EXPIRES_IN']))
# 			present = str(datetime.now())
# 			print(type(session_new))
# 			app.logger.info(session_new)
# 			app.logger.info(token_new)
# 			app.logger.info(present)
	
# 			##master table
# 			cur.execute("UPDATE master SET value = '"+session_new +"' WHERE key = 'session_id'")
# 			cur.execute("UPDATE master SET value = '"+token_new +"' WHERE key = 'token_id'")
# 			cur.execute("UPDATE master SET value = '"+api_new +"' WHERE key = 'api_key'")
# 			cur.execute("UPDATE master SET updated_time = '"+present +"' WHERE key = 'session_id'")
# 			# cur.execute("UPDATE master SET updated_time = '"+present +"' WHERE key = 'token_id'")

# 			#master2 table
			
# 			conn.commit()
# 			conn.close()
# 			app.logger.info("inserted session and token successfully")
# 			return None
# 		schedule.every(15).seconds.do(automate)
# 		#schedule.every(15).days.do(automate)
# 		while True:
# 			schedule.run_pending()
# 			#time.sleep(1)


# class ApiCalling(RestResource):
# 	def get(self):
# 		app.logger.info(" started query")
# 		resultset = Master().query.filter(Master.key != None).all()
# 		app.logger.info(resultset)
# 		db.session.commit()
# 		S = str()
		
# 		for x in resultset:
# 			app.logger.info("start")
# 			app.logger.info(str (x.updated_time))
# 			S+=str(x.value)+","	
# 		t = S.split(',')
# 		app.logger.info("resultset")
# 		api_key = t[1]
# 		session_id = t[2]
# 		token_id = t[0]
# 		app.logger.info(api_key)
# 		app.logger.info(token_id)
# 		app.logger.info(session_id)
# 		app.logger.info(type(t))
# 		ret = {"api_key":api_key,"token_id":token_id,"session_id":session_id}
# 		app.logger.info(ret)
# 		return  ret ,200


class ApiFetchTestMySetupOtTokens(RestResource):
	def get(self):
		app.logger.info("In ApiFetchTestMySetupOtTokens")
		resultset = Master().query.filter(Master.key != None).all()
		app.logger.info("resultset")
		app.logger.info(resultset)
		result_json = {resultset[0].key:resultset[0].value,resultset[1].key:resultset[1].value,resultset[2].key:resultset[2].value}
		app.logger.info("returning OT keys")
		app.logger.info("End of ApiFetchTestMySetupOtTokens")
		return result_json,200

class ApiGetInterviewOTdetails(RestResource):
	def get(self, interview_id):
		app.logger.info("getting session id")
		app.logger.info(interview_id)
		interview = Interviews().query.filter(Interviews.interviewid == interview_id).first()
		if interview is not None:
			app.logger.info(interview)
			interview_details = {"OTsessionId": interview.sessionid, "OTarchiveId": interview.archiveid},
			# return {'status': True,'interview_details' : interview_details  ,'code': 200}
			ret = {"sessionID": interview.sessionid, "archiveID": interview.archiveid}
			return ret, 200
		else:
			return {'status': False, 'message': 'COMMON_INTERVIEW_NOT_FOUND', 'code': 404}


# ----------------------------------CRON - CELERY--------------------------

class ApiCronNotifyInterviewerForFeedback(RestResource):
	def get(self):
		app.logger.info("Start ApiCronNotifyInterviewerForFeedback")
		notify_interviewer_for_feedback()
		app.logger.info("End ApiCronNotifyInterviewerForFeedback")

class ApiCronArchiveDetailsUpdate(RestResource):
	def get(self):
		app.logger.info("Start ApiCronArchiveDetailsUpdate")
		ot.archiveDetailsUpdate()
		app.logger.info("End ApiCronArchiveDetailsUpdate")

class ApiCronUpdateInterviews(RestResource):
	def get(self):
		app.logger.info("Start ApiCronUpdateInterviews")
		update_interviews()
		app.logger.info("End ApiCronUpdateInterviews")

class ApiCronUpdateAsyncInterviewStatus(RestResource):
	def get(self):
		app.logger.info("Start ApiCronUpdateAsyncInterviewStatus")
		update_async_interviewstatus()
		app.logger.info("End ApiCronUpdateAsyncInterviewStatus")

class ApiCronSubscribedMinutes(RestResource):
	def get(self):
		app.logger.info("Start ApiCronSubscribedMinutes")
		subscribed_minutes()
		app.logger.info("End ApiCronSubscribedMinutes")

class ApiCronAutomateTestMySetupOtTokensRenewal(RestResource):
	def get(self):
		app.logger.info("Start ApiCronAutomateTestMySetupOtTokensRenewal")
		automateTestMySetupOtTokensRenewal()
		app.logger.info("End ApiCronAutomateTestMySetupOtTokensRenewal")

class HealthCheck(RestResource):
	def get(self):
		return {'status': True, 'message': 'Health Check Passed', 'code': 200}, 200


# ************************* API lists here *********************#
# **************************************************************#

api.add_resource(GetArchive, '/archive/<string:archive_id>')

#calling session and archive ids based on interview id
api.add_resource(ApiGetInterviewOTdetails,'/interview/ot/<int:interview_id>')

# calling values of api,session,tokenid
api.add_resource(ApiFetchTestMySetupOtTokens, '/testsetup/ot/fetch')

#automate key values
# api.add_resource(dbAutomate, '/automate')

# systemlogs
api.add_resource(ApiSaveSystemLogs, '/systemlogs')

#authenticate user here
api.add_resource(ApiAuth, '/auth')

#create organization
api.add_resource(ApiCreateOrganization, '/organization/create')

#update organization
api.add_resource(ApiUpdateOrganization, '/organization/update/<int:orgid>')

#list of organization
api.add_resource(ApiOrganizationList, '/organizations')

#create Organization Admin by "Super Admin" or "Organiation Admin"
api.add_resource(ApiCreateUser, '/user/create')

#update Organization Admin by "Super Admin" or "Organiation Admin"
api.add_resource(ApiUpdateOrganizationUser, '/organization/user/update/<int:uid>')

#list of organizations
api.add_resource(ApiScheduleInterview,'/admin/scheduleInterview')

#list of Admins by Organization
api.add_resource(ApiUserList, '/users')

#validate Sucess Interview for feedback
api.add_resource(ApiValidateSuccessInterview, '/feedback/auth/<joining_link>')

#Store Successful Interview feedback
api.add_resource(ApiStoreInterviewFeedback,'/feedback/<joining_link>')

#Interview Join using joining link
api.add_resource(ApiValidateJoiningLink, '/join_interview/<joining_link>')

#validate interview authentication with user
api.add_resource(ApiInterviewAuth, '/interview/auth/<int:interviewid>')

# get remaining time details
api.add_resource(ApiJoinLiveInterview, '/join/interview/<int:interviewid>')

# Update Interview Status
api.add_resource(ApiInterviewStatusUpdate, '/interview/status/update/<string:input>')

#Upcoming Interviews
api.add_resource(ApiUpcomingInterviewsList,'/admin/upcoming/interviews')


#Previous Interviews
api.add_resource(ApiPreviousInterviewsList,'/admin/previous/interviews')

#Opentok Session for Test System
api.add_resource(ApiTestSystem,'/test/system')

#Interviewers list based on orgid
api.add_resource(ApiOrganizationInterviewers,'/organization/admin/interviewers/<int:orgid>')

#Candidates list based on orgid
api.add_resource(ApiOrganizationCandidates,'/organization/admin/candidates/<int:orgid>')


#Interview Details based on interviewid
api.add_resource(ApiInterviewDetails,'/interview/details/<int:interviewid>')

#Candidates based on orgid and name
api.add_resource(ApiSearchCandidates,'/admin/candidates/search/<int:orgid>')

#Interviewers based on orgid and name
api.add_resource(ApiSearchInterviewers,'/admin/interviewers/search/<int:orgid>')

#update archive id to interview
api.add_resource(ApiUpdateArchiveId,'/archive/update/<int:interviewid>')

#dashboard
api.add_resource(ApiDashboard,'/dashboard')

#Organizations list
api.add_resource(ApiOrganizationsNames,'/superadmin/organizations/list')

#User Details
api.add_resource(ApiUser,'/superadmin/user/details/<int:userid>')

#upload resume
api.add_resource(ApiUploadOrgLogo, '/organization/uploadOrgLogo/<int:orgid>')

#Get usage details
api.add_resource(ApiGetOrganizationUsageDetails, '/organization/usage/<int:orgid>')

# #reset password via email
api.add_resource(ApiUserResetPassword, '/users/reset_password')

# # reset password via email 
# api.add_resource(ApiUserResetAccountPassword, '/users/reset_account_password')

# # send reset password email
api.add_resource(ApiSendResetPasswordLink, '/users/reset_password_email')


#update upcoming interview details
api.add_resource(ApiUpdateUpcomingInterview, '/admin/upcoming/interviews/update/<int:interviewid>')

#create feedback template with categories list
api.add_resource(ApiCreateFeedbackTemplate,'/feedback/template/create')

#update feedback template with categories list
api.add_resource(ApiUpdateFeedbackTemplate,'/feedback/template/update/<int:ftid>')

#get feedback template list
api.add_resource(ApiFeedbackTemplateList,'/feedback/templates')

#get interview details
api.add_resource(ApiGetInterviewDetails,'/interview_details/<int:interviewid>')

#download feedback in pdf
api.add_resource(ApiInterviewFeedbackDownload,'/interview_feedback/download/<int:interviewid>/<string:tempTimeStampVar>')

#Start Recording
api.add_resource(ApiInterviewStartRecording,'/interview/<string:sessionid>/start_recording')

#Stop Recording
api.add_resource(ApiInterviewStopRecording,'/interview/<string:archiveid>/stop_recording')

#Schedule Async Video
api.add_resource(ApiAsyncInterviewSchedule,'/async/interview/schedule')

#validate async interview joining link
api.add_resource(ApiValidateAsyncJoiningLink, '/join_async_interview/<joining_link>')

#start async interview
api.add_resource(ApiStartAsyncInterview, '/start/async_interview/<int:interviewid>')

#Async Previous Interviews 
api.add_resource(ApiAsyncPreviousInterviewsList,'/async/previous/interviews')

#Async Questions 
api.add_resource(ApiAsyncQuestionsList,'/async/questions/list')

#Get Async Question 
api.add_resource(ApiGetAsyncQuestion,'/async/test/<int:asynctestid>/question/<int:asyncqid>')

#CameraTag Webhook handling
api.add_resource(ApiCameraTagWebhookhandling,'/cameratag/weebhook')

#EndAsyncInterview
api.add_resource(ApiEndAsyncInterview,'/stop/async_interview/<int:interviewid>')

#View Async Interview Details
api.add_resource(ApiViewAsyncInterviewDetails,'/async/interview/view/<int:asyncid>')

#update answered flag in asynctest table
api.add_resource(ApiUpdateIsAnsweredFlag,'/async/test/<int:asynctestid>/question/<int:asyncqid>/update')

# cancel Live Interview
api.add_resource(ApiCancelLiveInterview, '/interview/cancel/<int:interviewid>')

# cancel Async Interview
api.add_resource(ApiCancelAsyncInterview, '/asyncinterview/cancel/<int:asyncid>')


#Update Async interview
api.add_resource(ApiUpdateAsyncInterview,'/async/interview/update/<int:asyncid>')


#Share interviews
api.add_resource(ApiShareInterview,'/share/interview/<int:interview_id>')

# #Auth share interview & get data
api.add_resource(AuthShareInterviewByPasscode,'/access/interview/auth/<access_link>')

# Get interview sharing details by access link
api.add_resource(ApiGetShareDetails,'/access/interview/<access_link>')

#Create custom category details for organization
api.add_resource(ApiAddCustomCategories,'/custom/categories/add')

#Get custom category details for organization
api.add_resource(ApiGetCustomCategories,'/custom/categories/get')

#Export F2F interviews
api.add_resource(ApiUpcomingInterviewsExport,'/export/interviews/<string:tempTimeStampVar>')


#Export Async interviews
api.add_resource(ApiAsycInterviewsExport,'/export/asycinterviews/<string:tempTimeStampVar>')


# ----------------------------------CRON - CELERY--------------------------


# CRON - notify_interviewer_for_feedback
api.add_resource(ApiCronNotifyInterviewerForFeedback,'/cron/notify_interviewer_for_feedback')

# CRON -  archiveDetailsUpdate
api.add_resource(ApiCronArchiveDetailsUpdate, '/cron/archiveDetailsUpdate')

# CRON -  update_interviews
api.add_resource(ApiCronUpdateInterviews,'/cron/update_interviews')

# CRON -  update_async_interviewstatus
api.add_resource(ApiCronUpdateAsyncInterviewStatus,'/cron/update_async_interviewstatus')

# CRON -  subscribed_minutes
api.add_resource(ApiCronSubscribedMinutes,'/cron/subscribed_minutes')

# CRON -  automateTestMySetupOtTokensRenewal
api.add_resource(ApiCronAutomateTestMySetupOtTokensRenewal,'/cron/automateTestMySetupOtTokensRenewal')

# Health check
api.add_resource(HealthCheck,'/healthCheck')
