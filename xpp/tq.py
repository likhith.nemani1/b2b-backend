# -*- coding: utf-8 -*-

from celery import Celery
from celery.schedules import crontab
from xpp import app
from xpp.act import notify_schedule,notify_interviewer_for_feedback,update_interviews,subscribed_minutes,update_async_interviewstatus, automateTestMySetupOtTokensRenewal
from xpp.ot import archiveDetailsUpdate
from xpp.msg import send_email, send_emails
import pytz


def make_celery(ai):
	app.logger.info("from make_celery")
	app.logger.info(ai.import_name)
	ci = Celery('xpp.tq', broker=ai.config['CELERY_BROKER_URL'])
	ci.conf.update(ai.config)
	taskbase = ci.Task

	class ContextTask(taskbase):
		abstract = True

		def __call__(self, *args, **kwargs):
			with ai.app_context():
				return taskbase.__call__(self, *args, **kwargs)

	ci.Task = ContextTask
	return ci

def cron_notify_interviewer_for_feedback():
	app.logger.info('In cron_notify_interviewer_for_feedback')
	return '{}'.format(30)


def cron_notify_remind_archive_details_update_task():
	app.logger.info('In cron_notify_remind_archive_details_update_task')
	return '{},{}'.format(03,33)

def notify_update_interviewstatus_list():
	app.logger.info('In notify_update_interviewstatus_list')
	return '{},{}'.format(02,32)


def cron_notify_interview_subscribedminutes():
	app.logger.info('In cron_notify_interview_subscribedminutes')
	return '{},{}'.format(04,34)

def cron_notify_update_async_interviewstatus():
	app.logger.info('In cron_notify_interview_subscribedminutes')
	return '{}'.format(05)

def cron_ot_tokens_renewal():
	app.logger.info("from cron_ot_tokens_renewal")
	return '{},{}'.format(5,25)

class Scheduler:
	CELERYBEAT_SCHEDULE = {
		'remind_interviewer_for_feedback_task': {
			'task': 'xpp.tq.remind_interviewer_for_feedback_task',
			'schedule': crontab(minute=cron_notify_interviewer_for_feedback(), hour='19'),
			'args': None
		},
		'remind_archive_details_update_task': {
			'task': 'xpp.tq.remind_archive_details_update_task',
			'schedule': crontab(minute=cron_notify_remind_archive_details_update_task(), hour='*'),
			'args': None
		},
		'update_interviewstatus_task': {
			'task': 'xpp.tq.notify_update_interviewstatus_task',
			'schedule': crontab(minute=notify_update_interviewstatus_list(), hour='*'),
			'args': None
		},
		'update_async_interview_status_task': {
			'task': 'xpp.tq.notify_update_async_interview_status_task',
			'schedule': crontab(minute=cron_notify_update_async_interviewstatus(), hour='2'),
			'args': None
		},
		'update_interview_subscribeminutes_task': {
			'task': 'xpp.tq.notify_update_interview_subscribeminutes_task',
			'schedule': crontab(minute=cron_notify_interview_subscribedminutes(), hour='*'),
			'args': None
		},
		# cron for ot tokens renewal
		#https://docs.celeryproject.org/en/stable/reference/celery.schedules.html
		'cron_ot_tokens_renewal_test_my_setup': {
			'task': 'xpp.tq.cron_automate_test_my_setup_ot_tokens_renewal',
			# 'schedule': crontab(minute='25', hour='0', day_of_month=cron_ot_tokens_renewal()), # Daily cron need to fix
			'schedule': crontab(minute=cron_notify_interview_subscribedminutes(), hour='*'),
			# 'schedule': crontab(minute='*', hour='*'),
			'args': None
		}
		}


celery = make_celery(app)
celery.config_from_object(Scheduler)
# celery.timezone = 'Asia/Kolkata'
celery.timezone = pytz.timezone('Asia/Kolkata')


@celery.task()
def notify_schedule_task(*args, **kwargs):
	app.logger.info("calling notify_scheduler")
	notify_schedule(*args, **kwargs)	


@celery.task()
def remind_interviewer_for_feedback_task():
	app.logger.info("calling notify_interviewer_for_feedback")
	#notify_interviewer_for_feedback()

@celery.task()
def remind_archive_details_update_task():
	app.logger.info("calling cron_notify_remind_archive_details_update_task")
	archiveDetailsUpdate()

@celery.task()
def notify_update_interviewstatus_task():
	app.logger.info("calling notify_update_interviewstatus_task")
	update_interviews()

@celery.task()
def notify_update_interview_subscribeminutes_task():
	app.logger.info("calling notify_update_interview_subscribeminutes_task")
	subscribed_minutes()


@celery.task()
def notify_update_async_interview_status_task():
	app.logger.info("calling notify_update_async_interview_status_task")
	update_async_interviewstatus()

@celery.task
def send_email_task(*args, **kwargs):
	app.logger.info('In send_email_task')
	send_email(*args, **kwargs)
	
@celery.task
def cron_automate_test_my_setup_ot_tokens_renewal():
	app.logger.info("tq.py: calling act.automateTestMySetupOtTokensRenewal")
	automateTestMySetupOtTokensRenewal()