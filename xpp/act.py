from xpp import app
from xpp import db
from xpp import slot
from xpp.cst import C
from xpp.url import L
from xpp.em import Email
from xpp.msg import send_email, send_emails
from xpp.sms import send_sms
from flask import json
from xpp.dbtables import *
from xpp.mainschemas import *
from datetime import time, datetime, timedelta,date
from sqlalchemy.sql import table, column, select, update, insert
from sqlalchemy import and_,or_
import math
from xpp import ot


def organizationSignatureAndLogo(orgid):
	app.logger.info("Accessing orgSignature")
	org = Organization().query.get(orgid)
	app.logger.info("org.signature")
	app.logger.info(org.signature)
	signature = str(org.signature)
	without_line_breaks = signature.replace("\n", " ")
	contactemail = str(org.contactemail)
	logo = app.config['APP_URL']+'/logos/'+str(org.logo)
	org_name = org.name
	return without_line_breaks, logo, contactemail, org_name

def send_feedback_email(user,interview,interview_links):
	app.logger.info("Accessing send_feedback_email")
	app.logger.info(user)
	app.logger.info(interview)
	app.logger.info(interview_links)
	app.logger.info(user.firstname)
	app.logger.info(interview.interviewid)
	if user.role == C.INTERVIEWER:
		interview_feedback_link = interview_links.joinurl
		app.logger.info('Send feedback to '+user.firstname+'  with Interview ID  '+str(interview.interviewid))
		url = app.config['APP_URL'] + L.interview_feedback.format(feedback_link=interview_feedback_link)
		join_url = app.config['APP_URL'] + L.join_interview.format(join_link=interview_feedback_link)
		meta = {'app_name': app.config['APP_NAME']}
		dateandtime = slot.get_date_and_time_format_email(interview.date,interview.slot_id,interview.timezone)
		org_signature,org_logo,contactemail,org_name = organizationSignatureAndLogo(interview.orgid)
		candidate_details = Interview_Candidate().query.filter(Interview_Candidate.interviewid== interview.interviewid,Interview_Candidate.active_status == True).first()
		app.logger.info("Candidate Details in send_interview_joinlink")
		app.logger.info(candidate_details)
		candidate_data = User().query.get(candidate_details.candidateid)
		content = {'url': url,'datetime':dateandtime,'signature':org_signature,'logo':org_logo,'contactemail':contactemail,'candidate_name':candidate_data.firstname, 'join_url': join_url}
		app.logger.info("content")
		app.logger.info(content)
		em = Email().em_email_feedback(meta,user,interview,content)
		# send_email(em)
		# app.logger.info("Sent Feedback Email after Interview Successful ")	
		app.logger.info("End of api send_feedback_email")

def send_cancel_interview(user,interview):
	app.logger.info("Accessing send_cancel_interview")
	app.logger.info(user)
	app.logger.info(interview)
	app.logger.info(user.firstname)
	app.logger.info(interview.interviewid)
	app.logger.info('Send cancellation interview notification to '+user.firstname+'  with Interview ID  '+str(interview.interviewid))
	meta = {'app_name': app.config['APP_NAME']}
	dateandtime = slot.get_date_and_time_format_email(interview.date,interview.slot_id,interview.timezone)
	org_signature,org_logo,contactemail,org_name = organizationSignatureAndLogo(interview.orgid)
	candidate_details = Interview_Candidate().query.filter(Interview_Candidate.interviewid== interview.interviewid,Interview_Candidate.active_status == True).first()
	candidate_data = User().query.get(candidate_details.candidateid)
	content = {'datetime':dateandtime,'signature':org_signature,'logo':org_logo,'contactemail':contactemail,'candidate_name':candidate_data.firstname}
	app.logger.info("content")
	app.logger.info(content)
	em = Email().em_email_cancel_interview(meta,user,interview,content)
	# send_email(em)
	app.logger.info("End of send_cancel_interview")

def send_async_cancel_interview(user,interview):
	app.logger.info("Accessing send_async_cancel_interview")
	app.logger.info(user)
	app.logger.info(interview)
	app.logger.info(user.firstname)
	app.logger.info(interview.asyncid)
	app.logger.info('Send cancellation async interview notification to '+user.firstname+'  with Interview ID  '+str(interview.asyncid))
	meta = {'app_name': app.config['APP_NAME']}
	dateandtime = slot.get_local_date_and_time(interview.valid_till,interview.timezone)
	org_signature,org_logo,contactemail,org_name = organizationSignatureAndLogo(interview.orgid)
	candidate_data = User().query.get(interview.userid)
	content = {'datetime':dateandtime,'signature':org_signature,'logo':org_logo,'contactemail':contactemail,'candidate_name':candidate_data.firstname}
	app.logger.info("content")
	app.logger.info(content)
	em = Email().em_email_cancel_async_interview(meta,user,interview,content)
	# send_email(em)
	app.logger.info("End of send_cancel_interview")

def send_interview_joinlink(user,interview,interview_links):
	app.logger.info("Accessing send_interview_joinlink")
	app.logger.info(user)
	app.logger.info(interview)
	app.logger.info(interview.date)
	app.logger.info(interview.slot_id)	
	interview_join_link = interview_links.joinurl
	app.logger.info(interview.interviewid)
	#url = app.config['APP_URL'] + L.join_interview.format(join_link=interview_join_link)
	url = "http://localhost:4200/waiting-room?meet_user_id=" + interview_links.joinurl
	feedback_link = app.config['APP_URL'] + L.interview_feedback.format(feedback_link=interview_join_link)
	#system_test_url = app.config['APP_URL'] + L.system_test
	system_test_url = "http://localhost:4200/waiting-room?meet_user_id=" + interview_links.joinurl
	meta = {'app_name': app.config['APP_NAME']}
	dateandtime = slot.get_date_and_time_format_email(interview.date,interview.slot_id,interview.timezone)
	org_signature,org_logo,contactemail,org_name = organizationSignatureAndLogo(interview.orgid)
	candidate_details = Interview_Candidate().query.filter(and_(Interview_Candidate.interviewid== interview.interviewid,Interview_Candidate.active_status == True)).first()
	app.logger.info("Candidate Details in send_interview_joinlink")
	app.logger.info(candidate_details)
	candidate_data = User().query.get(candidate_details.candidateid)
	content = {'url': url,'datetime':dateandtime,'system_test': system_test_url,'signature':org_signature,'logo':org_logo,'contactemail':contactemail,'candidate_name':candidate_data.firstname, 'feedback_link': feedback_link}
	app.logger.info("content")
	app.logger.info(content)
	em = Email().em_join_interview_link(meta,user,interview,content)
	# send_email(em)
	# app.logger.info("Sent Join Interview Email after Interview Successful ")	
	app.logger.info("End of api send_interview_joinlink")	
	
def send_async_interview(user,async_interview):
	app.logger.info("Accessing send_interview_joinlink")
	app.logger.info(user)
	app.logger.info(async_interview)
	app.logger.info(async_interview.scheduledtime)
	interview_join_link = async_interview.joiningurl
	url = app.config['APP_URL'] + L.async_interview_url.format(async_interview_link=interview_join_link)
	meta = {'app_name': app.config['APP_NAME']}
	dateandtime = slot.get_local_date_and_time(async_interview.valid_till,async_interview.timezone)
	org_signature,org_logo,contactemail,org_name = organizationSignatureAndLogo(async_interview.orgid)
	candidate_data = User().query.get(user.userid)
	content = {'url': url,'dateandtime':dateandtime,'signature':org_signature,'logo':org_logo,'contactemail':contactemail}
	em = Email().em_async_interview_link(meta,user,async_interview,content)
	# send_email(em)
	app.logger.info("End of api send_interview_joinlink")

def notify_schedule(usrs,interview_data,n_type,grp=C.INTERVIEWER, url=None, bnd=False, eml=False, mbl=False):
	app.logger.info("Accessing notify")
	app.logger.info(usrs)
	app.logger.info(interview_data)
	if n_type == C.NOTIFICATION_ASYNC_INTERVIEW_SCHEDULE or n_type == C.NOTIFICATION_ASYNC_INTERVIEW_CANCELLATION:
		
		interview = AsyncInterviews().query.get(interview_data.asyncid) 
		dateandtime = slot.get_local_date_and_time(interview_data.scheduledtime,interview_data.timezone)
	else:
		
		interview = Interviews().query.get(interview_data.interviewid)
		dateandtime = slot.get_date_and_time_format_email(interview.date,interview.slot_id,interview.timezone)
	meta = {'app_name': app.config['APP_NAME']}
	sss=[]
	for usr in usrs:
		if usr:
			if eml:
				if n_type == C.NOTIFICATION_JOININTERVIEWURL:
					send_interview_joinlink(usr,interview,interview_data)
				if n_type == C.NOTIFICATION_INTERVIEWFEEDBACKURL:
					send_feedback_email(usr,interview,interview_data)
				if n_type == C.NOTIFICATION_INTERVIEW_CANCELLATION:
					send_cancel_interview(usr,interview)
				if n_type == C.NOTIFICATION_ASYNC_INTERVIEW_SCHEDULE:
					send_async_interview(usr,interview)
				if n_type == C.NOTIFICATION_ASYNC_INTERVIEW_CANCELLATION:
					send_async_cancel_interview(usr,interview)
			if mbl:
				app.logger.info("In sms sending")
				if n_type == C.NOTIFICATION_JOININTERVIEWURL or n_type ==  C.NOTIFICATION_ASYNC_INTERVIEW_SCHEDULE:
					app.logger.info("joiningurl")
					if n_type == C.NOTIFICATION_ASYNC_INTERVIEW_SCHEDULE:
						# msg='Dear '+usr.firstname+', Your async (one-way) video interview is ready. Please check your email for details.'
						msg = 'Dear {firstname}, Your async (one-way) video interview is ready. Please check your email for details. InterviewBuddy Pro'.format(firstname=usr.firstname)
						DLT_TE_ID = C.DLT_ASYNC_INTERVIEW_SCHEDULE
					else:
						# msg='Dear '+usr.firstname+', Your interview scheduled on '+str(dateandtime)+'.'
						msg = 'Dear {firstname}, Your interview is scheduled for {dateandtime}. Please ensure you join on time. (Service enabled by InterviewBuddy Pro)'.format(firstname = usr.firstname,dateandtime=dateandtime)
						DLT_TE_ID = C.DLT_INTERVIEW_SCHEDULE
					sss.append(usr.mobile)
					send_sms(sss, common_message=msg, DLT_TE_ID = DLT_TE_ID)

				elif n_type == C.NOTIFICATION_INTERVIEW_CANCELLATION or n_type ==  C.NOTIFICATION_ASYNC_INTERVIEW_CANCELLATION:
					app.logger.info("Interview Cancelled")
					# msg='Dear '+usr.firstname+', Your interview scheduled on '+str(dateandtime)+' has been cancelled'
					msg='Dear {firstname}, Your interview scheduled for {dateandtime} has been cancelled. (Service enabled by InterviewBuddy Pro)'.format(firstname=usr.firstname,dateandtime=dateandtime)
					sss.append(usr.mobile)
					send_sms(sss, common_message=msg,DLT_TE_ID = C.DLT_CANCEL_INTERVIEW)

def notify_interviewer_for_feedback():
	app.logger.info("In notify_interviewer_for_feedback")
	beforeDate = str(datetime.utcnow().date() - timedelta(days=2))
	app.logger.info("beforeDate")
	app.logger.info(beforeDate)
	interviewers = db.session.query(Interview_Interviewer).join(Interviews).filter(and_(Interviews.date<=beforeDate,Interviews.interviewstatus==C.INTERVIEW_SUCCESSFUL,Interview_Interviewer.is_joined == True,Interview_Interviewer.feedback_available == False)).all()
	app.logger.info(interviewers)
	for interviewer in interviewers:
		interview_feedback_link = interviewer.joinurl
		url = app.config['APP_URL'] + L.interview_feedback.format(feedback_link=interview_feedback_link)
		user = User().query.filter(User.userid == interviewer.interviewerid).first()
		interviewer_name = user.firstname
		interviewer_email = user.email
		app.logger.info("interviewer_email")
		app.logger.info(interviewer_email)

		app.logger.info("find candidate")
		interviewCandidate = Interview_Candidate().query.filter(Interview_Candidate.interviewid == interviewer.interviewid).first()
		if interviewCandidate:
			app.logger.info("candidate")
			candidate = User().query.filter(User.userid == interviewCandidate.candidateid).first()

			interview = Interviews().query.filter(Interviews.interviewid == interviewer.interviewid).first()
			candidate_name = candidate.firstname
			dateandtime = slot.get_date_and_time_format_email(interview.date,interview.slot_id,interview.timezone)
			org_signature,org_logo,contactemail,org_name = organizationSignatureAndLogo(interview.orgid)
			interview_details = {'url':url,'interviewer_name':interviewer_name,'candidate_name':candidate_name,'dateandtime':dateandtime,'email':interviewer_email,'signature':org_signature,'logo':org_logo,'contactemail':contactemail}
			em = Email().em_notify_interviewer_about_feedback(interview_details,interview)
			# app.logger.info("before email send: em")
			# app.logger.info(em)
			# send_email(em)
			# app.logger.info("email sent")
	app.logger.info("end of notify_interviewer_for_feedback")

def update_interviews():
	app.logger.info("Accessing update_interviews")
	cs, td = slot.completed_slot()
	dt = datetime.utcnow().date() + td
	app.logger.info("Updating successful interviews")
	app.logger.info("cs,td")
	app.logger.info(cs)
	app.logger.info(td)
	app.logger.info("dt")
	app.logger.info(dt)
	interviews_successful = Interviews().query.filter(and_(or_(and_(Interviews.date == dt, Interviews.slot_id <= cs), Interviews.date < dt),or_(Interviews.interviewstatus == C.INTERVIEW_IN_PROGRESS,Interviews.interviewstatus == C.INTERVIEW_INTERVIEWER_JOINED_AND_QUIT,Interviews.interviewstatus == C.INTERVIEW_CANDIDATE_JOINED_AND_QUIT ))).all()
	app.logger.info("interviews_successful")
	app.logger.info(interviews_successful)
	if interviews_successful:
		for interview_success in interviews_successful:
			app.logger.info("Call update_sucessful_interviews for following object")
			app.logger.info(interview_success)
			app.logger.info(interview_success.interviewid)
			app.logger.info(interview_success.interviewstatus)
			if dt == interview_success.date:
				app.logger.info("Date matches")
				if  cs == interview_success.slot_id:
					app.logger.info("Slot matches")
					if interview_success.duration == C.ONEHOUR and interview_success.interviewstatus == C.INTERVIEW_IN_PROGRESS:
						app.logger.info("Skip the loop for this interview:"+str(interview_success.interviewid)+" since it is an 60mins session")
						continue
					else:
						app.logger.info("In progress to successful")
						interview_success.interviewstatus = C.INTERVIEW_SUCCESSFUL
						db.session.commit()
						db.session.flush(interview_success)
				else:
					app.logger.info("Date match but slot_id not matched")
					interview_success.interviewstatus = C.INTERVIEW_SUCCESSFUL
					db.session.commit()
					db.session.flush(interview_success)
			else:
				app.logger.info("Some other day status updated")
				interview_success.interviewstatus = C.INTERVIEW_SUCCESSFUL
				db.session.commit()
				db.session.flush(interview_success)
		app.logger.info("Exit from interviews_successful")
	
	
	interviews_failed = Interviews().query.filter(and_(or_(and_(Interviews.date == dt, Interviews.slot_id <= cs),Interviews.date < dt),Interviews.interviewstatus == C.INTERVIEW_BOOKED)).all()
	app.logger.info("interviews_failed")
	app.logger.info(interviews_failed)
	if interviews_failed:
		for interview_fail in interviews_failed:
			if dt == interview_fail.date:
				app.logger.info("Date matches")
				if  cs == interview_fail.slot_id:
					app.logger.info("Slot matches")
					if interview_fail.duration == C.ONEHOUR and interview_fail.interviewstatus == C.INTERVIEW_BOOKED:
						pass
					else:
						app.logger.info(" Before updating failed interviews")
						interview_fail.interviewstatus = C.INTERVIEW_FAILED
						app.logger.info(" After updating failed interviews")
						db.session.commit()
						db.session.flush(interview_fail)
				else:
					app.logger.info("Slot id not matches ")
					app.logger.info(" Before updating failed interviews")
					interview_fail.interviewstatus = C.INTERVIEW_FAILED
					app.logger.info(" After updating failed interviews")
					db.session.commit()
					db.session.flush(interview_fail)
			else:
				app.logger.info("Date not matches ")
				interview_fail.interviewstatus = C.INTERVIEW_FAILED
				app.logger.info(" After updating failed interviews")
				db.session.commit()
				db.session.flush(interview_fail)
			
	interviews_single_joined = Interviews().query.filter(and_(or_(and_(Interviews.date == dt, Interviews.slot_id <= cs),Interviews.date < dt),or_(Interviews.interviewstatus == C.INTERVIEW_INTERVIEWER_TERMINATE,Interviews.interviewstatus == C.INTERVIEW_OT_CANDIDATE_JOINED,Interviews.interviewstatus == C.INTERVIEW_OT_INTERVIEWER_JOINED,Interviews.interviewstatus == C.INTERVIEW_CANDIDATE_TERMINATE))).all()
	app.logger.info("interviews_single_joined")
	app.logger.info(interviews_single_joined)
	if interviews_single_joined:
		for interview_single in interviews_single_joined:
			# app.logger.info("Slot matches")
			app.logger.info(interview_single)
			app.logger.info(interview_single.interviewstatus)
			# interview_single.interviewstatus = C.INTERVIEW_INTERVIEWER_ONLY_JOINED
			if interview_single.interviewstatus == C.INTERVIEW_OT_INTERVIEWER_JOINED or interview_single.interviewstatus == C.INTERVIEW_INTERVIEWER_TERMINATE:
				app.logger.info(" single joined interviews interviewer only joined")
				interview_single.interviewstatus = C.INTERVIEW_INTERVIEWER_ONLY_JOINED

			if interview_single.interviewstatus == C.INTERVIEW_OT_CANDIDATE_JOINED or interview_single.interviewstatus == C.INTERVIEW_CANDIDATE_TERMINATE:
				app.logger.info(" single joined interviews candidate only joined")
				interview_single.interviewstatus = C.INTERVIEW_CANDIDATE_ONLY_JOINED
				
			app.logger.info(" After updating single joined interviews")
			db.session.commit()
			db.session.flush(interview_single)
	app.logger.debug("End of update_interviews")

def convertDatetime(activitytime):
	join_time_string=activitytime
	app.logger.info("activitytime")
	app.logger.info(activitytime)
	join_time_split = join_time_string.split('T')
	join_time_split_date = join_time_split[0]
	join_time_split_time = join_time_split[1]
	app.logger.info("join_time_split_date")
	app.logger.info(join_time_split_date)
	app.logger.debug("join_time_split_time")
	app.logger.debug(join_time_split_time)
	join_time_split_time_split = join_time_split_time.split(':')
	app.logger.info("join_time_split_time_split")
	app.logger.info(join_time_split_time_split)
	join_time_split_time_hours = join_time_split_time_split[0]
	app.logger.info("join_time_split_time_hours")
	app.logger.info(join_time_split_time_hours)
	join_time_split_time_minutes = join_time_split_time_split[1]
	app.logger.info("join_time_split_time_minutes")
	app.logger.info(join_time_split_time_minutes)
	final_string = join_time_split[0]+' '+join_time_split_time_split[0]+':'+join_time_split_time_split[1]
	app.logger.info("final_string")
	app.logger.info(final_string)
	return final_string
def subscribed_minutes():
	app.logger.info("subscribed_minutes")
	cs, td = slot.completed_slot()
	dt = datetime.utcnow().date() + td

	interviews = Interviews().query.filter(and_(or_(and_(Interviews.date == dt, Interviews.slot_id <= cs), Interviews.date < dt),or_(Interviews.interviewstatus == C.INTERVIEW_IN_PROGRESS,Interviews.interviewstatus == C.INTERVIEW_INTERVIEWER_JOINED_AND_QUIT,Interviews.interviewstatus == C.INTERVIEW_CANDIDATE_JOINED_AND_QUIT ,Interviews.interviewstatus == C.INTERVIEW_SUCCESSFUL))).all()
	app.logger.info("interviews")	
	app.logger.info(interviews)	

	for interview in interviews:
		if interview.duration == C.ONEHOUR and interview.interviewstatus == C.INTERVIEW_IN_PROGRESS:
			break
		else:
			try:
				app.logger.debug("Either interview is success or both joined then after single person quit")
				time_slot = slot.sd[interview.slot_id]
				tm = time(time_slot[0], time_slot[1])
				minutes = 0
				list_of_interviewers_query = 'select interviewerid,min(trackingid) as min_id,max(trackingid) as max_id from interviewtracking where interviewid='+str(interview.interviewid)+' and interviewerid is not null  group by interviewerid;'
				app.logger.info("list_of_interviewers_query")
				app.logger.info(list_of_interviewers_query)
				list_of_interviewers = db.engine.execute(list_of_interviewers_query)
				interviewers = InterviewersTrackingSchema().dump(list_of_interviewers,many=True).data
				minutes_i = 0
				if interviewers:
					for interviewer in interviewers:
						app.logger.info("interviewer")
						app.logger.info(interviewer)
						app.logger.info(interviewer['min_id'])
						app.logger.info(interviewer['max_id'])
						min_id = interviewer['min_id']
						max_id = interviewer['max_id']
						interview_i_query = 'select activity,activitytime from interviewtracking where trackingid in('+str(min_id)+','+str(max_id)+');'
						app.logger.info("interview_i_query")
						app.logger.info(interview_i_query)
						interview_i_execute = db.engine.execute(interview_i_query)
						interview_i = InterviewActivitySchema().dump(interview_i_execute,many=True).data
						app.logger.debug("interview_i")
						app.logger.debug(interview_i)
						interview_list_i = interview_i
						if interview_list_i:
							app.logger.info("interview_list_i")
							if interview_list_i[0]:
								if interview_list_i[0]['activity'] =='JOIN':
									app.logger.debug("interview_list_i[0]['activitytime']")
									app.logger.debug(interview_list_i[0]['activitytime'])
									join_time_i_string = convertDatetime(interview_list_i[0]['activitytime'])
									join_time_i = datetime.strptime(join_time_i_string,"%Y-%m-%d %H:%M")
									app.logger.debug("join_time_i")
									app.logger.debug(join_time_i)
							if len(interview_list_i) == 2:
								if interview_list_i[1]:
									if interview_list_i[1]['activity'] == 'END':
										end_time_i_string = convertDatetime(interview_list_i[1]['activitytime'])
										end_time_i = datetime.strptime(end_time_i_string,"%Y-%m-%d %H:%M")
										app.logger.debug("end_time_i")
										app.logger.debug(end_time_i)
									else:
										app.logger.info("End time not exists for interviewer")
										end_time_combine = datetime.combine(interview.date, tm) + timedelta(minutes=interview.duration)
										end_time_strftime = datetime.strftime(end_time_combine,"%Y-%m-%d %H:%M")
										end_time_i = datetime.strptime(end_time_strftime,"%Y-%m-%d %H:%M")
										app.logger.debug("end_time_i")
										app.logger.debug(end_time_i)
								else:
									end_time_combine = datetime.combine(interview.date, tm) + timedelta(minutes=interview.duration)
									end_time_strftime = datetime.strftime(end_time_combine,"%Y-%m-%d %H:%M")
									end_time_i = datetime.strptime(end_time_strftime,"%Y-%m-%d %H:%M")
									app.logger.debug("end_time_i")
									app.logger.debug(end_time_i)	
							else:
								end_time_combine = datetime.combine(interview.date, tm) + timedelta(minutes=interview.duration)
								end_time_strftime = datetime.strftime(end_time_combine,"%Y-%m-%d %H:%M")
								end_time_i = datetime.strptime(end_time_strftime,"%Y-%m-%d %H:%M")
								app.logger.debug("end_time_i")
								app.logger.debug(end_time_i)	
							overall_time_i = end_time_i - join_time_i
							app.logger.info("overall_time_i interviewers")
							app.logger.info(overall_time_i)
							seconds_i = overall_time_i.seconds
							app.logger.debug("seconds_i interviewers")
							app.logger.debug(seconds_i)
							minutes_i += math.ceil(float(seconds_i/60))
							app.logger.info("minutes_i interviewers")
							app.logger.info(minutes_i)
				minutes_c =0
				list_of_candidate_query = 'select candidateid,min(trackingid) as min_id,max(trackingid) as max_id  from interviewtracking where interviewid='+str(interview.interviewid)+' and candidateid is not null  group by candidateid;'
				app.logger.info("list_of_candidate_query")
				app.logger.info(list_of_candidate_query)
				list_of_candidates = db.engine.execute(list_of_candidate_query)
				candidates = CandidatesTrackingSchema().dump(list_of_candidates,many=True).data
				if candidates:
					for candidate in candidates:
						min_cid = candidate['min_id']
						max_cid = candidate['max_id']
						interview_c_query = 'select activity,activitytime from interviewtracking where trackingid in('+str(min_cid)+','+str(max_cid)+');'
						interview_c_execute = db.engine.execute(interview_c_query)
						interview_c = InterviewActivitySchema().dump(interview_c_execute,many=True).data
						interview_list_c = interview_c
						if interview_list_c:
							app.logger.debug("interview_list_c")
							app.logger.debug(interview_list_c)

							if interview_list_c[0]:
								if interview_list_c[0]['activity'] =='JOIN':
									join_time_c_string = convertDatetime(interview_list_c[0]['activitytime'])
									join_time_c = datetime.strptime(join_time_c_string,"%Y-%m-%d %H:%M")
									app.logger.debug("join_time_c")
									app.logger.debug(join_time_c)
							if len(interview_list_c) == 2:
								if interview_list_c[1]:
									if interview_list_c[1]['activity'] == 'END':
										end_time_c_string = convertDatetime(interview_list_c[1]['activitytime'])
										end_time_c = datetime.strptime(end_time_c_string,"%Y-%m-%d %H:%M")
										app.logger.debug("end_time_c")
										app.logger.debug(end_time_c)
									else:
										app.logger.debug("End time not exists for candidate")
										end_timec_combine = datetime.combine(interview.date, tm) + timedelta(minutes=interview.duration)
										end_time_c_strftime = datetime.strftime(end_timec_combine,"%Y-%m-%d %H:%M")
										end_time_c = datetime.strptime(end_time_c_strftime,"%Y-%m-%d %H:%M")
										app.logger.debug("end_time_c")
										app.logger.debug(end_time_c)
								else:
									app.logger.debug("End time not exists for candidate")
									end_timec_combine = datetime.combine(interview.date, tm) + timedelta(minutes=interview.duration)
									end_time_c_strftime = datetime.strftime(end_timec_combine,"%Y-%m-%d %H:%M")
									end_time_c = datetime.strptime(end_time_c_strftime,"%Y-%m-%d %H:%M")
									app.logger.debug("end_time_c")
									app.logger.debug(end_time_c)
							else:
								app.logger.debug("End time not exists for candidate")
								end_timec_combine = datetime.combine(interview.date, tm) + timedelta(minutes=interview.duration)
								end_time_c_strftime = datetime.strftime(end_timec_combine,"%Y-%m-%d %H:%M")
								end_time_c = datetime.strptime(end_time_c_strftime,"%Y-%m-%d %H:%M")
								app.logger.debug("end_time_c")
								app.logger.debug(end_time_c)
							overall_time_c = end_time_c - join_time_c
							app.logger.info("overall_time_c for candidate")
							app.logger.info(overall_time_c)
							seconds_c = overall_time_c.seconds
							app.logger.debug("seconds_c for candidate")
							app.logger.debug(seconds_c)
							minutes_c += math.ceil(float(seconds_c/60))
							app.logger.info("minutes_c for candidate")
							app.logger.info(minutes_c)
				
				interview.subscribeminutes = int(minutes_i)+int(minutes_c)
				app.logger.info("interview.subscribeminutes update to db")
				app.logger.info(interview.subscribeminutes)
				db.session.commit()
				app.logger.debug("End of Interviews subscribed minutes")
			except Exception as e:
				app.logger.info("Exception in adding subscribeminutes")
				app.logger.info(e)
				app.logger.info("End of Interviews subscribe minutes")


def update_async_interviewstatus():
	dt = datetime.now()
	beforeDate = dt - timedelta(days=1)
	asyncinterviews = AsyncInterviews().query.filter(AsyncInterviews.valid_till <= beforeDate).all()
	app.logger.info("Accessing update_async_interviewstatus")
	if asyncinterviews:
		for asyncinterview in asyncinterviews:
			app.logger.info("asyncinterview")
			app.logger.info(asyncinterview)
			app.logger.info(asyncinterview.asyncid)
			app.logger.info(asyncinterview.interviewstatus)
			if asyncinterview.interviewstatus == C.ASYNC_INTERVIEW_SCHEDULED:
				app.logger.info("asyncinterview.interviewstatus in expired")
				app.logger.info(asyncinterview.interviewstatus)
				asyncinterview.interviewstatus = C.ASYNC_INTERVIEW_EXPIRED
			if asyncinterview.interviewstatus == C.ASYNC_INTERVIEW_IN_PROGRESS:
				app.logger.info("asyncinterview.interviewstatus  in success")
				app.logger.info(asyncinterview.interviewstatus)
				asyncinterview.interviewstatus = C.ASYNC_INTERVIEW_SUCCESS

			db.session.commit()
			db.session.flush()
	app.logger.info("End of update_async_interviewstatus")

def automateTestMySetupOtTokensRenewal():
	app.logger.info("In automateTestMySetupOtTokensRenewal")
	api_new = app.config['OT_API_KEY']

	#generating OT <<satrt>>
	session_new = str(ot.create_session(record=False))
	token_new = str(ot.create_token(session_new, role='p', ot_expires_in=app.config['SELF_RECORD_EXPIRES_IN']))
	present = str(datetime.now())
	print(type(session_new))
	app.logger.info(session_new)
	app.logger.info(token_new)
	app.logger.info(present)
	#generating OT <<End>>

	#updating session_id
	update_session = Master().query.filter(Master.key=='session_id').first()
	if update_session is not None:
		update_session.value = session_new
		update_session.updated_time = present
	else:
		app.logger.info("Session row not found in db")

	#updating token_id
	update_token = Master().query.filter(Master.key=='token_id').first()
	if update_token is not None:
		update_token.value = token_new
		update_token.updated_time = present
	else:
		app.logger.info("token row not found in db")

	try:
		db.session.commit()
	except Exception as e:
		app.logger.info("Excetion raised at db commit")
		app.logger.info(e)
		return None

	app.logger.info("inserted session and token successfully")
	app.logger.info("End automateTestMySetupOtTokensRenewal")
	return None