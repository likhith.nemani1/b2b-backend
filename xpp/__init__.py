# -*- coding: utf-8 -*-

import sys
import decimal
import simplejson
sys.modules['decimal'] = decimal
sys.modules['json'] = simplejson

from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.cors import CORS

from cfg import default_config
from xpp.logg import logg


app = Flask(__name__)
app.config.from_object(default_config)
app._static_folder = app.config['STATIC_DIRECTORY']
logg(app.config['LOG_FILE'])

db = SQLAlchemy(app)

if app.config['CORS']:
	CORS(app)


from xpp import mainapi
