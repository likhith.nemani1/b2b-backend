#this is for archive related file

#load libraries 
from xpp import app
from xpp.dbtables import Interviews
import boto3
import requests
from xpp.cst import C
from datetime import datetime 
from env import Environment

	

def getArchiveURL(session,user):
	# update archive id here 
	# https://s3-ap-southeast-1.amazonaws.com/openttokarchives/<api-key>/<archiveid>/archive.mp4
	app.logger.info("info: in archive.geArchiveURL(session)")

	#first check.. for period of time 
	#visible video up to 7 days for provider, 30 days for user, and forver for admin
	app.logger.info(session)
	
	archiveid=session.archiveid
	
	if archiveid is None:
		app.logger.info("archive id not found..  no video url")
		return ''
	else:
		videoURL=generateSignedURL(archiveid)
		
		#videoURL = "https://s3-ap-southeast-1.amazonaws.com/openttokarchives/"+app.config['OT_API_KEY']+"/"+session.archiveid+"/archive.mp4"
		  
		app.logger.info("location of video is "+videoURL)

		return videoURL;
		

def getArchiveURLByArchiveID(archiveid):
	# update archive id here 
	# https://s3-ap-southeast-1.amazonaws.com/openttokarchives/<api-key>/<archiveid>/archive.mp4
	app.logger.info("in archive.ByArchiveID(archiveid)")
	
	if archiveid is None:
		app.logger.info("archive id not found..  no video url")
		return ''
	else:
		videoURL=generateSignedURL(archiveid)
		
		#videoURL = "https://s3-ap-southeast-1.amazonaws.com/openttokarchives/"+app.config['OT_API_KEY']+"/"+session.archiveid+"/archive.mp4"
		  
		app.logger.info("location of video is "+videoURL)

		return videoURL;
		
def generateSignedURL(archive_id):
	app.logger.info("in generateSignedURL")
	# Get the service client.
	s3 = boto3.client('s3')

	# Generate the URL to get 'key-name' from 'bucket-name'
	url = s3.generate_presigned_url(
		ClientMethod='get_object',
		Params={
			'Bucket': 'b2brecords',
			'Key':Environment.OT_API_KEY+'/'+archive_id+'/archive.mp4', #file location
		},
		ExpiresIn=2400,
		HttpMethod='GET'
	)
	app.logger.info("url is "+url)
	return url



def removeVideo(archive_id):
	app.logger.info("in removeVideo")
	s3 = boto3.client('s3')
	try:
		response = s3.delete_object(
			Bucket='b2brecords',
			Key= Environment.OT_API_KEY+'/'+archive_id+'/archive.mp4', #file location

			# Key= Environment.OT_API_KEY+'/'+archive_id+'/archive.mp4', #file location
		)
		app.logger.info("response from AWS when delete resume")
		app.logger.info(response)
		
		return True
	except Exception as exp:
		app.logger.info("error while uploading")
		app.logger.info(exp)
		return False

	

def user_is_admin(g):
	if g >= C.ADMIN:
		return True
	else:
		return False


def user_is_superadmin(g):
	if g == C.SUPERADMIN:
		return True
	else:
		return False

		

def user_is_provider(g):
	if g == C.PROVIDER:
		return True
	else:
		return False

def user_is_user(g):
	if g == C.USER:
		return True
	else:
		return False
