# # -*- coding: utf-8 -*-

# from rauth import OAuth2Service
# from xpp import app
# from xpp.cst import C
# import json
# from itsdangerous import (JSONWebSignatureSerializer as Serializer, BadSignature)


# oauth_names = {C.OAUTH_LINKEDIN: 'linkedin'}


# def deserialize_token(oauth_token):
# 	s = Serializer(app.config['SECRET_KEY'])
# 	try:
# 		data = s.loads(oauth_token)
# 		if data.get('access_token') and data.get('oauth_name'):
# 			return data
# 	except BadSignature:
# 		return None


# class OA(object):
# 	oap = None
# 	oauth = None
# 	redirect_url = None
# 	auth_url_params = None
# 	profile_api_path = None
# 	profile_api_fields = None

# 	def get_url(self):
# 		url = self.oauth.get_authorize_url(**self.auth_url_params)
# 		return url

# 	def x_get_token_from_code(self, params):
# 		try:
# 			session = self.oauth.get_auth_session(decoder=json.loads, data=params)
# 		except KeyError:
# 			return
# 		access_token = session.access_token
# 		return access_token

# 	def get_profile(self, access_token):
# 		session = self.oauth.get_session(access_token)
# 		resp = session.get(self.profile_api_path, params={'format': 'json'}).json()
# 		# print resp
# 		try:
# 			ret = {x: resp[self.profile_api_fields[x]] for x in self.profile_api_fields.iterkeys()}
# 		except KeyError:
# 			return
# 		return ret

# 	def serialize_token(self, access_token):
# 		s = Serializer(app.config['SECRET_KEY'])
# 		return s.dumps({'access_token': access_token, 'oauth_name': oauth_names[self.oap]})


# class LinkedIn(OA):
# 	oap = C.OAUTH_LINKEDIN

# 	oauth = OAuth2Service(
# 		client_id=app.config['LINKEDIN_CLIENT_ID'],
# 		client_secret=app.config['LINKEDIN_CLIENT_SECRET'],
# 		name=oauth_names[oap],
# 		authorize_url='https://www.linkedin.com/uas/oauth2/authorization',
# 		access_token_url='https://www.linkedin.com/uas/oauth2/accessToken',
# 		base_url='https://api.linkedin.com/v1/')

# 	redirect_url = app.config['APP_URL'] + '/redirect/from_linkedin'

# 	auth_url_params = {
# 		'response_type': 'code',
# 		'redirect_uri': redirect_url,
# 		'state': '_',
# 		'scope': 'r_basicprofile r_emailaddress'}

# 	profile_api_path = 'people/~:(email-address,public-profile-url,id,formatted-name,picture-url)'

# 	profile_api_fields = {'email': 'emailAddress', 'oaid': 'id', 'name': 'formattedName', 'profile_url': 'publicProfileUrl', 'image_url': 'pictureUrl'}

# 	def get_token_from_code(self, auth_code):
# 		params = {
# 			'grant_type': 'authorization_code',
# 			'code': auth_code,
# 			'redirect_uri': self.redirect_url}
# 		return super(LinkedIn, self).x_get_token_from_code(params)


# oauth_classes = [LinkedIn, ]
# oauth_classes_by_name = {oauth_names[oac.oap]: oac for oac in oauth_classes}
