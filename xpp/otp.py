# -*- coding: utf-8 -*-

import base64
from datetime import datetime

import onetimepass
from xpp import app


def get_intervals_number(tm):
    secs = int((tm - app.config['SECRET_EPOCH']).total_seconds())
    n = int(secs / app.config['MOBILE_VC_EXPIRES_IN'])
    return n


def make_mobile_secret(x):
    s = app.config['SECRET_KEY'] + str(x)  # Can alter in any-way to make it unique. Later onetimepass library uses creates HMAC digest using SHA1
    return base64.b32encode(s).decode('utf-8')


def gen_otp(x, tm=None):
	app.logger.info("in gen_otp")
	if not tm:
		tm = datetime.utcnow()
	
	token = onetimepass.get_hotp(make_mobile_secret(x), get_intervals_number(tm), as_string=True, token_length=app.config['MOBILE_VC_LENGTH'])
	app.logger.info("otp token is "+str(token))
	return token


def vfy_otp(t, x, tm=None):
    if not tm:
        app.logger.info("tm")
        app.logger.info(tm)
        tm = datetime.utcnow()
    app.logger.info("In vfy_otp ")
    app.logger.info(t)
    app.logger.info(x)
    app.logger.info(tm)
    app.logger.info(onetimepass.get_hotp(make_mobile_secret(x), get_intervals_number(tm), token_length=app.config['MOBILE_VC_LENGTH']))
    if int(t) == onetimepass.get_hotp(make_mobile_secret(x), get_intervals_number(tm), token_length=app.config['MOBILE_VC_LENGTH']):
        app.logger.info("verify otp is " +str(t))
        return True
    else:
        return False
