# -*- coding: utf-8 -*-

import time
import sys, traceback
from opentok import OpenTok, MediaModes, ArchiveModes, Roles
from opentok.exceptions import OpenTokException, RequestError, AuthError, NotFoundError, ArchiveError
from xpp import app
from xpp.dbtables import Interviews 
from xpp.cst import C
from xpp import db
from sqlalchemy import exc, func, or_, not_,and_
import math

#create new session here
def create_session(record=True):
	opentok = OpenTok(app.config['OT_API_KEY'], app.config['OT_API_SECRET'])
	try:
		if record:
			#ot_session = opentok.create_session(media_mode=MediaModes.routed, archive_mode=ArchiveModes.manual)
			app.logger.info("Archive is true")
			ot_session = opentok.create_session(media_mode=MediaModes.routed, archive_mode=ArchiveModes.always)
		else:
			app.logger.info("Archive is false")
			ot_session = opentok.create_session(media_mode=MediaModes.routed, archive_mode=ArchiveModes.always)
		
		return ot_session.session_id
	except:
		return None

#create new token
def create_token(ot_session_id, role='p', ot_expires_in=None):
	if ot_expires_in is None:
		ot_expires_in = app.config['SESSION_LENGTH']
	opentok = OpenTok(app.config['OT_API_KEY'], app.config['OT_API_SECRET'])
	if role == 'p':
		rc = Roles.publisher
	else:
		rc = Roles.subscriber
	try:
		token = opentok.generate_token(ot_session_id, rc, int(time.time()) + ot_expires_in, 'x')
		return token
	except:
		return None


#this funtion enable recording (archive) 
def startRecording(sessionId):
	app.logger.info("archive.py: in startRecording")
	app.logger.info("session id is")
	app.logger.info(sessionId)
	try:
		if sessionId:
			pass
		else:
			app.logger.info("sessionId is null")
			return false
		opentok = OpenTok(app.config['OT_API_KEY'], app.config['OT_API_SECRET'])
		app.logger.info('opentok')
		app.logger.info(opentok)
		archive = opentok.start_archive(sessionId)
		
		if(archive):
			app.logger.info("archive started and archive id is ")
			app.logger.info(archive.id)
			return archive.id
	except ArchiveError as ar:
		app.logger.info("ArchiveError raised while start recording")
		app.logger.info(ar)
		
	except Exception as e:
		app.logger.info("Exception in Start recording")
		app.logger.info(e)
	else:
		app.logger.info("error while start recording in ot.py")
		app.logger.info(traceback.print_last())
		app.logger.info(traceback.format_exc())
		app.logger.info(traceback.print_exc())
		app.logger.info(traceback.format_stack())
		app.logger.info(traceback.last_traceback())
		


#this funtion disable recording (archive) 
def stopRecording(archiveid):
	app.logger.info("ot.py: in stopRecording")
	app.logger.info("archiveid is")
	app.logger.info(archiveid)
	try:
		opentok = OpenTok(app.config['OT_API_KEY'], app.config['OT_API_SECRET'])
		app.logger.info("After opentok initialization")
		app.logger.info(opentok)
		archive = opentok.stop_archive(archiveid)
		app.logger.info("After opentok stop archieve")
		
		if(archive):
			app.logger.info("archive stoped and archive id is ")
			app.logger.info(archive.id)
			return archive.id
	# except ArchiveError as er:
	# 	app.logger.info(er)
	# 	app.logger.info("error while stop recording in ot.py")

	except Exception as e:
		app.logger.info(e)
		app.logger.info("Error in Stop recording")


#save archive id in session
def update_archive_informaiton():
	app.logger.info("invoked ot.firstTimeUpdate")
	opentok = OpenTok(app.config['OT_API_KEY'], app.config['OT_API_SECRET'])
	
	#get all archives and save into db based on session id
	archive_list = opentok.get_archives() 
	
	totalArchives = 0

	session=Session.query.filter(Session.ot_id == sessionID).first()
	
	
	for archive in iter(archive_list):
		totalArchives = totalArchives + 1
		app.logger.info("session id "+archive.session_id+"   archive id "+archive.id)
		
		sessionID=archive.session_id
		
		if(session):
			app.logger.info("session exists")
			arch = session.archiveid;
			#app.logger.info("session.archiveid  "+arch)
			if(arch is None):
				app.logger.info("archive update here")
				session.archiveid=archive.id;
				db.session.commit()
				app.logger.info("sid :"+Session.sid+"  archive id  "+ archive.id)
			else:
				app.logger.info("archive already exists")
		else:
				app.logger.info("no session")
				
		app.logger.info("no of archives "+ str(totalArchives))
	

#call every hour, for updating archive list
def archiveDetailsUpdate():
	app.logger.info("starting of archiveDetailsUpdate()")
	
	try:
		#get all session whose status success and have session and archive not updated
		interviews = Interviews.query.filter(and_(or_(Interviews.interviewstatus != C.INTERVIEW_BOOKED,Interviews.interviewstatus != C.INTERVIEW_FAILED), Interviews.prerecording_enabled.is_(True),or_(Interviews.recordingsize.is_(None),Interviews.recordingtime.is_(None))))
		#no.of session with out archives
		app.logger.info("interviews "+str(interviews.count()))
		#create instance for opentok
		opentok = OpenTok(app.config['OT_API_KEY'], app.config['OT_API_SECRET'])
		
		
		if(interviews.count() > 0):
			#get all archives and save into db based on session id
			archive_list = opentok.get_archives() 
			
			for archive in iter(archive_list):
				app.logger.debug("to find session using archive.session_id in opentok")

				for interview in interviews:
					# interview_detail = interview.items()
					if(archive.status == 'uploaded' or archive.status == 'available'):
						if interview.sessionid is not None:
							app.logger.info("Session id is not none")
							#update archive id to session row
							if interview.sessionid == archive.session_id:
								app.logger.debug("interview.sessionid")
								app.logger.debug(interview.sessionid)
								app.logger.debug("archive.session_id")
								app.logger.debug(archive.session_id)
								interview.archiveid=archive.id
								interview.recordingtime= math.ceil(float(archive.duration/60))
								interview.recordingsize=archive.size
								#commit session
								db.session.commit()
								break
			
			app.logger.info("loop closed.. try to commit sessions")
		else:
			app.logger.info("New sessions not available")
			app.logger.info("end of archiveDetailsUpdate")

	except Exception as e:
		app.logger.info("Error while  accessing archiveDetailsUpdate ")
		app.logger.info(e)


#delete existing archive from list
def deleteArchive(archiveidd):
	app.logger.info("Starting deleteArchive(archiveidd)")
	opentok = OpenTok(app.config['OT_API_KEY'], app.config['OT_API_SECRET'])
	opentok.delete_archive(archiveidd)
	app.logger.info("end of deleteArchive(archiveidd)")
	
def get_archive(archive_id):
	opentok = OpenTok(app.config['OT_API_KEY'], app.config['OT_API_SECRET'])
	archive = opentok.get_archive(archive_id)
	return archive