
from datetime import datetime

import pytz
from marshmallow import Schema, fields, ValidationError, pre_load, post_load, post_dump, missing
from webargs.flaskparser import parser

from xpp import app
from xpp import oauth
from xpp.cst import C


# ******************** Schemas from HERE ****************


class DefaultSchema(Schema):
	class Meta:
		strict = True


class AuthSchema(DefaultSchema):
	email = fields.Str(load_only=True, required=True)
	password = fields.Str(load_only=True, required=True)


class OAuthSchema(DefaultSchema):
	oauth_token = fields.Str(load_only=True, required=True)

	@post_load
	def validate_oat(self, data):
		oat = oauth.deserialize_token(data.get('oauth_token'))
		if not oat:
			raise ValidationError('Invalid OAuth')
		data.pop('oauth_token', None)
		data['oat'] = oat


class UserBaseSchema(DefaultSchema):
	userid = fields.Int(dump_only=True)  # read-only (won't be parsed by webargs)
	orgid = fields.Int(dump_only = True)
	role = fields.Int(dump_only=True)
	email = fields.Email(required=True)
	firstname = fields.Str()
	lastname=fields.Str()
	mobile = fields.Str()
	status = fields.Boolean(dump_only=True)
	last_login_time = fields.DateTime(dump_only=True)
	
	createdby = fields.Int(dump_only = True)
	createdtime = fields.DateTime(dump_only=True)
	updatedby = fields.Int(dump_only = True)
	updatedtime = fields.DateTime(dump_only=True)

	@pre_load
	def preprocess(self, in_data):
		if in_data['email'] is not missing:
			in_data['email'] = in_data['email'].lower().strip()
		return in_data


# {firstname, lastname, email, mobile, role, orgid, status}
class UserCreateSchema(DefaultSchema):
	orgid = fields.Int(required = True)
	role = fields.Int(required=True)
	email = fields.Email(required=True)
	firstname = fields.Str(required = True)
	lastname = fields.Str()
	mobile = fields.Str(required = True)
	status = fields.Boolean()
	
	@pre_load
	def preprocess(self, in_data):
		if in_data['email'] is not missing:
			in_data['email'] = in_data['email'].lower().strip()
		return in_data


# {firstname, lastname, email, mobile, role, orgid, status, userid}
class UserUpdateSchema(UserBaseSchema):
	userid = fields.Int(required = True)
	orgid = fields.Int()
	role = fields.Int()
	email = fields.Email()
	firstname = fields.Str()
	lastname=fields.Str()
	mobile = fields.Str()
	status = fields.Boolean()
	
	@pre_load
	def preprocess(self, in_data):
		if in_data['email'] is not missing:
			in_data['email'] = in_data['email'].lower().strip()
		return in_data


class OrganizationSchema(DefaultSchema):
	orgid = fields.Int(dump_only = True)
	name = fields.Str(dump_only = True)
	preferredname = fields.Str(dump_only = True)
	url = fields.Str(dump_only = True)
	address = fields.Str(dump_only = True)
	location = fields.Str(dump_only = True)
	country = fields.Str(dump_only = True)
	contactperson = fields.Str(dump_only = True)
	contactemail = fields.Str(dump_only = True)
	mobile = fields.Str(dump_only = True)
	phone = fields.Str(dump_only = True)
	alternative = fields.Str(dump_only = True)
	status = fields.Boolean(dump_only = True)
	logo = fields.Str(dump_only = True)
	signature = fields.Str(dump_only = True)
	createdby = fields.Int(dump_only = True)
	createdtime = fields.DateTime(dump_only=True)
	updatedby = fields.Int(dump_only = True)
	updatedtime = fields.DateTime(dump_only=True)


	@pre_load
	def preprocess(self, in_data):
		if in_data['contactemail'] is not missing:
			in_data['contactemail'] = in_data['contactemail'].lower().strip()
		return in_data



class OrganizationUpdateSchema(DefaultSchema):
	# id = fields.Str(required = True)
	name = fields.Str(required = True)
	preferredname = fields.Str(required = True)
	url = fields.Str(required = True)
	address = fields.Str(required = True)
	location = fields.Str(required = True)
	country = fields.Str(required = True)
	contactperson = fields.Str(required = True)
	contactemail = fields.Str(required = True)
	mobile = fields.Str(required = True)
	phone = fields.Str(required = True)
	alternative = fields.Str(required = True)
	status = fields.Boolean(required = True)
	signature = fields.Str(required = True)
	status = fields.Boolean()

	@pre_load
	def preprocess(self, in_data):
		if in_data['contactemail'] is not missing:
			in_data['contactemail'] = in_data['contactemail'].lower().strip()
		return in_data
		

class OrganizationCreateSchema(DefaultSchema):
	name = fields.Str(required = True)
	preferredname = fields.Str(required = True)
	url = fields.Str(required = True)
	address = fields.Str(required = True)
	location = fields.Str(required = True)
	country = fields.Str(required = True)
	contactperson = fields.Str(required = True)
	contactemail = fields.Str(required = True)
	mobile = fields.Str(required = True)
	phone = fields.Str(required = True)
	alternative = fields.Str(required = True)
	signature = fields.Str(required = True)
	status = fields.Boolean(required = True)

	@pre_load
	def preprocess(self, in_data):
		if in_data['contactemail'] is not missing:
			in_data['contactemail'] = in_data['contactemail'].lower().strip()
		return in_data

class OrganizationUserUpdateSchema(DefaultSchema):
	# id = fields.Str(required = True)
	orgid = fields.Int(required = True)
	role = fields.Int(required=True)
	email = fields.Email(required=True)
	firstname = fields.Str(required = True)
	lastname = fields.Str()
	mobile = fields.Str(required = True)
	status = fields.Boolean()
	
	@pre_load
	def preprocess(self, in_data):
		if in_data['email'] is not missing:
			in_data['email'] = in_data['email'].lower().strip()
		return in_data



class ScheduleInterviewSchema(DefaultSchema):
	date = fields.Str(required = True)
	slot_id = fields.Int(required=True)
	candidate_email= fields.Str(required=True) 
	# interviewer_email = fields.Str(required=True) 
	candidate_fname = fields.Str(required=True) 
	# interviewer_fname = fields.Str(required=True) 
	candidate_lname = fields.Str(required=True) 
	# interviewer_lname= fields.Str(required=True) 
	candidate_mobile = fields.Str(required=True) 
	# interviewer_mobile = fields.Str(required=True) 
	interviewdescription= fields.Str(required=True) 
	orgid = fields.Int(required=True) 
	interviewtitle = fields.Str(required=True) 
	duration= fields.Int(required=True) 
	localtimezone = fields.Str(required=True)
	interviewers_list = fields.Str(required= True)
	feedback_template_id = fields.Int(required=True)
	prerecording_enabled = fields.Boolean()
	custom_categories_data = fields.Str(required=True)


class InterviewsSchema(DefaultSchema):
	interviewid = fields.Int(required=True)
	date = fields.Str(required = True)
	slot_id = fields.Int(required=True)
	candidateid = fields.Int(required=True)
	interviewerid = fields.Int(required=True)
	interviewstatus = fields.Int(required=True)
	sessionid = fields.Str(required = True)
	archiveid = fields.Str(required = True)
	feedback = fields.Str(required = True)
	interviewer_join_time = fields.DateTime(dump_only=True)
	candidate_join_time = fields.DateTime(dump_only=True)
	interviewer_end_time = fields.DateTime(dump_only=True)
	candidate_end_time = fields.DateTime(dump_only=True)
	orgid = fields.Int(required=True)
	prerecording_enabled = fields.Boolean()
	interviewtitle = fields.Str(required = True)
	result = fields.Int(required=True)
	interviewdescription = fields.Str(required = True)
	interviewer_joinurl = fields.Str(required = True)
	candidate_joinurl = fields.Str(required = True)
	duration = fields.Int(required=True)
	recordingtime=fields.Int(dump_only=True)
	subscribeminutes=fields.Int(dump_only=True)
	timezone=fields.Str(dump_only=True)
	ftid=fields.Str(dump_only=True)

class InterviewCandidateSchema(InterviewsSchema):
	interviewerid = fields.Int(dump_only=True)
	candidateid = fields.Int(dump_only=True)


class InterviewInterviewerSchema(InterviewsSchema):
	interviewerid = fields.Int(dump_only=True)
	candidateid = fields.Int(dump_only=True)
	

class InterviewAdminSchema(InterviewsSchema):
	date = fields.Date(dump_only=True)
	slot_id = fields.Int(dump_only=True)
	interviewerid = fields.Int(dump_only = True)
	candidateid = fields.Int(dump_only = True)
	status = fields.Int(dump_only = True)

#Upcoming Interviews Schema
class UpcomingInterviewsSchema(DefaultSchema):
	status = fields.Int(required=True)  
	orgid = fields.Int(required = True)
	start_date = fields.Str(required=True)
	end_date = fields.Str(required=True)

#Previous Interviews Schema
class PreviousInterviewsSchema(DefaultSchema):
	status = fields.Int(required=True) 
	orgid = fields.Int(required = True)
	start_date = fields.Str(required=True)
	end_date = fields.Str(required=True)

# Get records information based on the UpcomingInterviews
class UpcomingInterviewsListSchema(DefaultSchema):
	interviewid = fields.Int(required=True)
	orgid = fields.Int(required = True)
	date = fields.Str(required=True)
	candidate_email = fields.Str(required=True)
	interviewer_email = fields.Str(required=True)
	slot_id = fields.Int(required=True)
	interviewstatus= fields.Int(dump_only=True)
	duration = fields.Int(dump_only=True)
	interviewer_joinurl = fields.Str(dump_only=True)
	candidate_joinurl = fields.Str(dump_only=True)



class InterviewCandidateDetailsSchema(DefaultSchema):
	firstname = fields.Str(dump_only=True)
	lastname = fields.Str(dump_only=True)
	email = fields.Str(dump_only=True)
	mobile = fields.Str(dump_only=True)
	interviewid =fields.Int(dump_only=True)
	candidateid = fields.Int(dump_only =True)
	active_status = fields.Boolean(dump_only= True)

class InterviewInterviewerDetailsSchema(DefaultSchema):
	firstname = fields.Str(dump_only=True)
	lastname = fields.Str(dump_only=True)
	email = fields.Str(dump_only=True)
	mobile = fields.Str(dump_only=True)
	interviewerid =fields.Int(dump_only=True)
	interviewid =fields.Int(dump_only=True)
	isprimary = fields.Int(dump_only =True)
	active_status = fields.Boolean(dump_only= True)


# Get records information based on the PreviousInterviews
class PreviousInterviewsListSchema(DefaultSchema):
	interviewid = fields.Int(required=True)
	orgid = fields.Int(required = True)
	date = fields.Str(required=True)
	candidate_email = fields.Str(required=True)
	interviewer_email = fields.Str(required=True)
	slot_id = fields.Int(required=True)
	interviewstatus= fields.Int(dump_only=True)
	duration = fields.Int(dump_only=True)

# Interview Details
class InterviewDetailsSchema(DefaultSchema):
	interviewid = fields.Int(required=True)
	date = fields.Str(dump_only = True)
	slot_id = fields.Int(dump_only=True)
	interviewstatus = fields.Int(dump_only=True)
	orgid = fields.Int(dump_only=True)
	interviewtitle = fields.Str(dump_only = True)
	interviewdescription = fields.Str(dump_only = True)
	duration = fields.Int(dump_only=True)
	primaryinterviewer = fields.Str(dump_only=True)
	interviewer_list= fields.Str(dump_only=True)
	regular_interviewer =fields.Str(dump_only=True)
	candidate_data = fields.Str(dump_only=True)
	interviewdescription =fields.Str(dump_only=True)
	user_details = fields.Str(dump_only=True)
	ftid = fields.Str(dump_only = True)


class InterviewTrackingSchema(DefaultSchema):
	trackingid = fields.Int(required=True)
	interviewid = fields.Str(dump_only = True)
	candidateid = fields.Int(dump_only=True)
	interviewerid = fields.Int(dump_only=True)
	orgid = fields.Int(dump_only=True)
	activity = fields.Str(dump_only= True)
	activitytime = fields.DateTime(dump_only= True)

class MaxActivityTimeSchema(DefaultSchema):
	max_time = fields.DateTime(dump_only = True)

class MinActivityTimeSchema(DefaultSchema):
	min_time = fields.DateTime(dump_only = True)

class MaxTimeSchema(DefaultSchema):
	max_time = fields.Str(dump_only = True)
	
class OrganizationUsageSchema(DefaultSchema):
	totalinterviews = fields.Float(dump_only = True)
	subscribeminutes = fields.Float(dump_only = True)
	recordingtime = fields.Float(dump_only = True)
	recordingsize = fields.Float(dump_only = True)

class OrganizationAsyncUsageSchema(DefaultSchema):
	interviewid = fields.Int(dump_only = True)
	recorded_length = fields.Float(dump_only = True)

class ResetPasswordEmail(DefaultSchema):
	email = fields.Email(required=True, load_only=True)

class ResetPassword(DefaultSchema):
	reset_token = fields.Str(required=True, load_only=True)
	password = fields.Str(required=True, load_only=True)

class InterviewUpdateSchema(DefaultSchema):
	date = fields.Str(required = True)
	slot_id = fields.Int(required=True)
	prerecording_enabled = fields.Boolean(required=True)
	interviewtitle = fields.Str(required = True)
	interviewdescription = fields.Str(required = True)
	duration = fields.Int(required=True)
	candidate_email = fields.Str(required = True)
	candidate_fname = fields.Str(required = True)
	candidate_lname = fields.Str(required = True)
	candidate_mobile = fields.Str(required=True)
	localTimeZone = fields.Str(required=True)
	interviewers_list = fields.Str(required= True)
	feedback_template_id = fields.Int(required=True)
	custom_categories_data = fields.Str(required=True)


class ExistingInterviewersInInterviewSchema(DefaultSchema):
	# interviewid = fields.Int(dump_only = True)
	interviewerid = fields.Int(dump_only = True)


class InterviewIDShema(DefaultSchema):
	interviewid = fields.Int(required = True)


class ExistingCandidateInInterviewSchema(DefaultSchema):
	# interviewid = fields.Int(dump_only = True)
	candidateid = fields.Int(dump_only = True)

class InterviewFeedbackSchema(DefaultSchema):
	fid= fields.Int(required=True)
	interviewid=fields.Int(required=True)
	interviewerid=fields.Int(required=True)
	isprimary=fields.Boolean(required=True)
	feedback=fields.Str(required=True)
	rating=fields.Int(required=True)
	orgid=fields.Int(required=True)
	created_time=fields.Date(required=True)

class InterviewFeedbackCreateSchema(DefaultSchema):
	interviewid=fields.Int(required=True)
	interviewerid=fields.Int(required=True)
	isprimary=fields.Boolean(required=True)
	feedback=fields.Str(required=True)
	rating=fields.Int(required=True)
	orgid=fields.Int(required=True)


class FeedbackTemplateSchema(DefaultSchema):
	ftid= fields.Int(required=True)
	title=fields.Str(required=True)
	categories=fields.Str(required=True)
	max_rating=fields.Int(required=True)
	orgid=fields.Int(required=True)
	created_by=fields.Int(required=True)
	created_time=fields.Date(required=True)
	updated_by=fields.Int(required=True)
	updated_time=fields.Date(required=True)
	status=fields.Boolean(required=True)

class FeedbackTemplateCreateSchema(DefaultSchema):
	title=fields.Str(required=True)
	categories=fields.Str(required=True)
	max_rating=fields.Int(required=True)
	status=fields.Boolean(required=True)
	# orgid=fields.Int(required=True)

class FeedbackTemplateUpdateSchema(DefaultSchema):
	ftid= fields.Int(required=True)
	title=fields.Str(required=True)
	categories=fields.Str(required=True)
	max_rating=fields.Int(required=True)
	orgid=fields.Int(required=True)
	status=fields.Boolean(required=True)

class StoreInterviewFeedbackSchema(DefaultSchema):
	feedback = fields.Str(required=True)
	rating = fields.Int(required=True)

class InterviewerDetailsWithFeedbackSchema(DefaultSchema):
	feedback = fields.Str(required=True)
	email = fields.Email(required=True)
	firstname = fields.Str(required = True)
	lastname = fields.Str()
	interviewer_id = fields.Int(required=True) 
	interviewid = fields.Int(required=True)
	isprimary = fields.Boolean(dump_only = True)

class FeedbacksSchema(DefaultSchema):
	feedback = fields.Str(required=True)


class AsyncInterviewScheduleSchema(DefaultSchema):
	validtilldate = fields.Str(required=True)
	candidate_firstname = fields.Str(required=True)
	candidate_lastname = fields.Str(required=True)
	candidate_email=fields.Email(required=True)
	candidate_mobile=fields.Str(required=True)
	questions_list = fields.Str(required=True)
	timezone = fields.Str(required=True)
	custom_categories_data = fields.Str(required=True)

class AsyncInterviewUpdateSchema(DefaultSchema):
	# asyncid = fields.Int(required=True)
	validtilldate = fields.Str(required=True)
	candidate_firstname = fields.Str(required=True)
	candidate_lastname = fields.Str(required=True)
	candidate_email=fields.Email(required=True)
	candidate_mobile=fields.Str(required=True)
	questions_list = fields.Str(required=True)
	timezone = fields.Str(required=True)
	custom_categories_data = fields.Str(required=True)

class AsyncInterviewSchema(DefaultSchema):
	asyncid = fields.Int(required=True)
	userid = fields.Int(required=True)
	orgid = fields.Int(required=True)
	scheduledtime = fields.DateTime(dump_only=True)
	status = fields.Int(required=True)
	joiningurl = fields.Str(required=True)
	valid_till = fields.DateTime(dump_only=True)

class AsyncQuestionsSchema(DefaultSchema):
	asyncqid = fields.Int(required=True)
	question = fields.Str(required=True)
	orgid = fields.Int(required=True)

class AsyncTestSchema(DefaultSchema):
	asynctestid = fields.Int(required=True)
	asyncid = fields.Int(required=True)
	asyncqid = fields.Int(required=True)
	duration = fields.Int(required=True)
	videolink = fields.Str(required=True)

class AsyncTestQuestionsSchema(DefaultSchema):
	asynctestid = fields.Int(required=True)
	asyncid = fields.Int(required=True)
	asyncqid = fields.Int(required=True)
	duration = fields.Int(required=True)
	question = fields.Str(dump_only=True)
	videolink = fields.Str(dump_only=True)

class AsyncPreviousInterviewsListSchema(DefaultSchema):
	asyncid = fields.Int(required=True)
	userid = fields.Int(dump_only=True)
	orgid = fields.Int(dump_only=True)
	scheduledtime = fields.DateTime(dump_only=True)
	interviewstatus = fields.Int(dump_only=True)
	valid_till = fields.DateTime(dump_only=True)
	email = fields.Str(dump_only=True)

class AsyncQuestionsListSchema(DefaultSchema):
	asyncqid = fields.Int(required=True) 
	question = fields.String(required=True)

class JoiningUrlSchema(DefaultSchema):
	joiningurl = fields.Str(required=True)

class AsyncInterviewDetailsSchema(DefaultSchema):
	asyncid = fields.Int(required=True) 
	scheduledtime =fields.DateTime(dump_only=True)
	valid_till=fields.DateTime(dump_only=True)
	interviewstatus =fields.Int(required=True) 
	email =fields.Email(dump_only=True)
	createdtime = fields.DateTime(dump_only=True)
	mobile=fields.Str(dump_only=True)
	firstname=fields.Str(dump_only=True)
	lastname =fields.Str(dump_only=True)

class AsyncInterviewQuestionVideosSchema(DefaultSchema):
	asyncid=fields.Int(required=True) 
	asyncqid=fields.Int(required=True) 
	duration=fields.Int(dump_only=True) 
	videolink= fields.Str(dump_only=True)
	recorded_length = fields.Int(dump_only=True)
	question = fields.Str(dump_only=True)
	duration= fields.Int(dump_only=True)
	attempted_time= fields.DateTime(dump_only=True)
	isanswered =  fields.Boolean(dump_only=True)


class AsyncInterviewTrackingSchema(DefaultSchema):
	trackingid = fields.Int(required=True)
	interviewid = fields.Str(dump_only = True)
	candidateid = fields.Int(dump_only=True)
	orgid = fields.Int(dump_only=True)
	activity = fields.Str(dump_only= True)
	activitytime = fields.DateTime(dump_only= True)

class InterviewersTrackingSchema(DefaultSchema):
	interviewerid =fields.Int(dump_only=True)
	min_id = fields.Int(dump_only=True)
	max_id =fields.Int(dump_only=True)

class CandidatesTrackingSchema(DefaultSchema):
	candidateid =fields.Int(dump_only=True)
	min_id = fields.Int(dump_only=True)
	max_id =fields.Int(dump_only=True)

class InterviewActivitySchema(DefaultSchema):
	activity = fields.Str(dump_only=True)
	activitytime = fields.DateTime(dump_only=True)

class RecordingTimeOnlySchema(DefaultSchema):
	recorded_length = fields.Int(dump_only=True)
