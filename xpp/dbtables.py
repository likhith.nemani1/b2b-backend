import binascii
import hashlib
import os
from datetime import datetime
from urllib import quote_plus
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer, BadSignature, SignatureExpired
from passlib.apps import custom_app_context as pwd_context
from sqlalchemy.ext.hybrid import hybrid_property
from xpp import app, db
from xpp.cst import C

class User(db.Model):
    __tablename__ = 'users'
    userid = db.Column(db.Integer, primary_key=True)
    firstname = db.Column(db.String(64))
    lastname = db.Column(db.String(48))
    email = db.Column(db.String(256))
    mobile = db.Column(db.String(16))
    role = db.Column(db.Integer)
    orgid = db.Column(db.Integer)
    password = db.Column(db.String(128))
    status = db.Column(db.Integer, default=C.ENABLED)
    lastlogintime = db.Column(db.DateTime)
    reset_token = db.Column(db.String(512))    
    createdby = db.Column(db.Integer)
    createdtime = db.Column(db.DateTime, default=datetime.utcnow)
    updatedby = db.Column(db.Integer)
    updatedtime = db.Column(db.DateTime, default=datetime.utcnow)

    oa = db.relationship('UserOAuth', back_populates='user', lazy='dynamic')

    def __repr__(self):
        return '<User(userid={}, email={}, role={})>'.format(self.userid, self.email, self.role)

    def hash_p(self, password_d):
        self.password = pwd_context.encrypt(password_d)

    def verify_p(self, password_d):
        try:
            return pwd_context.verify(password_d, self.password)
        except ValueError:
            return None
        except:
            return None

    def generate_t(self, expiration = None):
        if not expiration:
            expiration = app.config['TOKEN_EXPIRES_IN']
        s = Serializer(app.config['SECRET_KEY'], expires_in=expiration)
        return s.dumps({'userid': self.userid})

    @staticmethod
    def verify_t(token):
        s = Serializer(app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except SignatureExpired:
            app.logger.info('signature expired')
            return None
        except BadSignature:
            app.logger.info('bad signature')
            return None

        user = User.query.get(data['userid'])
        app.logger.info('successfully verified')
        return user    
    
    def generate_reset_token(self):
        rt = self.generate_email_vc(C.VC_RESET_PASSWORD, expiration=app.config['RESET_PASSWORD_EXPIRES_IN'])
        self.reset_token = rt
        return rt

    @classmethod
    def reset_password(cls, rt, password):
        reset_user = cls.verify_email_vc(rt, C.VC_RESET_PASSWORD)
        if not reset_user:
            app.logger.info("verify email None")
            return None
        if reset_user.reset_token != rt:
            app.logger.info("reset_user token not matched")
            return None
        reset_user.hash_p(password)
        reset_user.reset_token = None
        return reset_user


    def generate_email_vc(self, purpose=C.VC_VERIFY_EMAIL, expiration=None):
        if not expiration:
            expiration = app.config['EMAIL_VC_EXPIRES_IN']
        s = Serializer(app.config['SECRET_KEY'], expires_in=expiration)
        return s.dumps({'email': self.email, 'purpose': purpose})

    @staticmethod
    def verify_email_vc(vc, purpose=C.VC_VERIFY_EMAIL):
        s = Serializer(app.config['SECRET_KEY'])
        try:
            data = s.loads(vc)
        except SignatureExpired:
            app.logger.info("Valid token but expired")
            return None    # valid token, but expired
        except BadSignature:
            app.logger.info("signature bad")
            return None    # invalid token
        if data['purpose'] != purpose:
            app.logger.info("purpose not matched")
            app.logger.info(data['purpose'])
            app.logger.info(purpose)
            return None
        user = User.query.filter_by(email=data['email']).first()
        return user




class UserOAuth(db.Model):
    __tablename__ = 'user_oauth'
    oaid = db.Column(db.String(256), primary_key=True)
    userid = db.Column(db.Integer, db.ForeignKey('users.userid'))
    access_token = db.Column(db.String(512))
    user = db.relationship('User', back_populates='oa', uselist=False)

    def __repr__(self):
        return '<UserOAuth(oaid={}, userid={})>'.format(self.oaid, self.userid)


class Organization(db.Model):
    __tablename__ = "organization"
    orgid = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String)
    preferredname = db.Column(db.String)
    url = db.Column(db.String)
    address = db.Column(db.String)
    location = db.Column(db.String)
    country = db.Column(db.String)
    contactperson = db.Column(db.String)
    contactemail = db.Column(db.String)
    mobile = db.Column(db.String)
    phone = db.Column(db.String)
    alternative = db.Column(db.String)
    signature = db.Column(db.String)
    status = db.Column(db.String)
    logo = db.Column(db.String)
    
    createdby = db.Column(db.Integer)
    createdtime = db.Column(db.DateTime, default=datetime.utcnow)
    updatedby = db.Column(db.Integer)
    updatedtime = db.Column(db.DateTime, default=datetime.utcnow)


class Interviews(db.Model):
    __tablename__ = "interviews"
    interviewid = db.Column(db.Integer, primary_key = True)
    date = db.Column(db.DateTime)
    slot_id= db.Column(db.Integer)
    candidateid = db.Column(db.Integer,db.ForeignKey('users.userid'))
    interviewerid = db.Column(db.Integer,db.ForeignKey('users.userid'))
    interviewstatus = db.Column(db.Integer)
    sessionid = db.Column(db.String)
    archiveid= db.Column(db.String)
    feedback = db.Column(db.String)
    interviewer_join_time = db.Column(db.DateTime)
    candidate_join_time = db.Column(db.DateTime)
    interviewer_end_time = db.Column(db.DateTime)
    candidate_end_time = db.Column(db.DateTime)
    orgid = db.Column(db.Integer,db.ForeignKey('users.userid'), nullable=False)
    prerecording_enabled = db.Column(db.Boolean, default=False)
    interviewtitle = db.Column(db.String)
    interviewdescription = db.Column(db.String)
    subscribeadmin= db.Column(db.Integer)
    result = db.Column(db.Integer)
    recordingsize = db.Column(db.Integer)
    recordingtime = db.Column(db.Integer)
    createdtime= db.Column(db.DateTime)
    createdby = db.Column(db.Integer,db.ForeignKey('users.userid'))
    updatedby = db.Column(db.Integer,db.ForeignKey('users.userid'))
    updatedtime = db.Column(db.DateTime)
    scheduleadmin = db.Column(db.Integer)
    duration = db.Column(db.Integer)
    interviewer_joinurl = db.Column(db.Text)
    candidate_joinurl = db.Column(db.Text)
    subscribeminutes = db.Column(db.Integer)
    timezone = db.Column(db.String)
    ftid= db.Column(db.Integer,db.ForeignKey('feedback_template.ftid'),nullable=False)
    custom_categories_data = db.Column(db.JSON)


class InterviewTracking(db.Model):
    __tablename__ = "interviewtracking"
    trackingid = db.Column(db.Integer, primary_key = True)
    interviewid = db.Column(db.Integer,db.ForeignKey('interviews.interviewid'))
    candidateid = db.Column(db.Integer,db.ForeignKey('users.userid'))
    interviewerid = db.Column(db.Integer,db.ForeignKey('users.userid'))
    orgid = db.Column(db.Integer,db.ForeignKey('organization.orgid'))
    activity = db.Column(db.String)
    activitytime = db.Column(db.DateTime)
    

class Interview_Interviewer(db.Model):
    __tablename__ = "interview_interviewer"
    intid = db.Column(db.Integer, primary_key = True)
    interviewid = db.Column(db.Integer,db.ForeignKey('interviews.interviewid'))
    interviewerid = db.Column(db.Integer,db.ForeignKey('users.userid'))
    isprimary = db.Column(db.Boolean,default=False)
    orgid = db.Column(db.Integer,db.ForeignKey('organization.orgid'))
    joinurl= db.Column(db.Text)
    joining_time = db.Column(db.DateTime)
    end_time =db.Column(db.DateTime)
    active_status = db.Column(db.Boolean,default=True)
    is_joined = db.Column(db.Boolean,default=False)
    feedback_available = db.Column(db.Boolean,default=False)


class Interview_Candidate(db.Model):
    __tablename__ = "interview_candidate"
    cid = db.Column(db.Integer, primary_key = True)
    interviewid = db.Column(db.Integer,db.ForeignKey('interviews.interviewid'))
    candidateid = db.Column(db.Integer,db.ForeignKey('users.userid'))
    orgid = db.Column(db.Integer,db.ForeignKey('organization.orgid'))
    joinurl= db.Column(db.Text)
    joining_time = db.Column(db.DateTime)
    end_time =db.Column(db.DateTime)
    active_status = db.Column(db.Boolean,default=True)

class InterviewFeedback(db.Model):
    __tablename__ = "feedback"
    fid = db.Column(db.Integer, primary_key = True)
    interviewid = db.Column(db.Integer,db.ForeignKey('interviews.interviewid'))
    interviewer_id = db.Column(db.Integer,db.ForeignKey('users.userid'))
    orgid = db.Column(db.Integer,db.ForeignKey('organization.orgid'))
    isprimary=db.Column(db.Boolean, default=False)
    feedback = db.Column(db.Text)
    rating=db.Column(db.Integer)
    created_time = db.Column(db.DateTime)
    updated_time = db.Column(db.DateTime)


class FeedbackTemplate(db.Model):
    __tablename__ = "feedback_template"
    ftid = db.Column(db.Integer, primary_key = True)
    title = db.Column(db.Integer,db.ForeignKey('interviews.interviewid'))
    categories = db.Column(db.Integer,db.ForeignKey('users.userid'))
    max_rating = db.Column(db.Integer)
    orgid = db.Column(db.Integer,db.ForeignKey('organization.orgid'))
    created_by = db.Column(db.DateTime)
    created_time = db.Column(db.DateTime)
    updated_by = db.Column(db.DateTime)
    updated_time = db.Column(db.DateTime)
    status = db.Column(db.Boolean, default=False)


class AsyncInterviews(db.Model):
    __tablename__ = "asyncinterviews"
    asyncid = db.Column(db.Integer, primary_key = True)
    userid = db.Column(db.BigInteger,db.ForeignKey('users.userid'))
    orgid = db.Column(db.BigInteger,db.ForeignKey('organization.orgid'))
    scheduledtime = db.Column(db.DateTime,nullable=False)
    interviewstatus = db.Column(db.Integer,nullable=False)
    joiningurl = db.Column(db.Text,nullable=False)
    createdby = db.Column(db.Integer,db.ForeignKey('users.userid'),nullable=False)
    updatedby = db.Column(db.Integer,db.ForeignKey('users.userid'))
    createdtime = db.Column(db.DateTime,nullable=False)
    updatedtime = db.Column(db.DateTime)
    valid_till = db.Column(db.DateTime)
    timezone = db.Column(db.String)
    custom_categories_data = db.Column(db.JSON)


class AsyncQuestions(db.Model):
    __tablename__ = "asyncquestions"
    asyncqid = db.Column(db.Integer, primary_key = True)
    question = db.Column(db.Text,db.ForeignKey('users.userid'))
    orgid = db.Column(db.BigInteger,db.ForeignKey('organization.orgid'))
    createdby = db.Column(db.Integer,db.ForeignKey('users.userid'))
    updatedby = db.Column(db.Integer,db.ForeignKey('users.userid'))
    createdtime = db.Column(db.DateTime,nullable=False)
    updatedtime = db.Column(db.DateTime)


class AsyncTest(db.Model):
    __tablename__ = "asynctest"
    asynctestid = db.Column(db.Integer, primary_key = True)
    asyncid = db.Column(db.BigInteger,db.ForeignKey('asyncinterviews.asyncid'))
    asyncqid = db.Column(db.BigInteger,db.ForeignKey('asyncquestions.asyncqid'))
    duration = db.Column(db.Integer,nullable=False)
    videolink = db.Column(db.Text)
    createdby = db.Column(db.Integer,db.ForeignKey('users.userid'))
    updatedby = db.Column(db.Integer,db.ForeignKey('users.userid'))
    createdtime = db.Column(db.DateTime,nullable=False)
    updatedtime = db.Column(db.DateTime)
    isanswered = db.Column(db.Boolean, default=False)
    attempted_time = db.Column(db.DateTime)
    recorded_length = db.Column(db.Integer)
    isactive = db.Column(db.Boolean, default=False)

class AsyncInterviewTracking(db.Model):
    __tablename__ = "async_interviewtracking"
    trackingid = db.Column(db.Integer, primary_key = True)
    interviewid = db.Column(db.Integer,db.ForeignKey('asyncinterviews.asyncid'))
    candidateid = db.Column(db.Integer,db.ForeignKey('users.userid'))
    orgid = db.Column(db.Integer,db.ForeignKey('organization.orgid'))
    activity = db.Column(db.String)
    activitytime = db.Column(db.DateTime)

class ShareInterviews(db.Model):
    __tablename__ = 'share_interviews'
    sid = db.Column(db.Integer, primary_key = True)
    interview_type = db.Column(db.Integer)
    interview_id = db.Column(db.Integer)
    email_id = db.Column(db.String(256))
    passcode = db.Column(db.String(256))
    access_link = db.Column(db.String(256))
    shared_by = db.Column(db.Integer,db.ForeignKey('users.userid'))
    created_time = db.Column(db.DateTime)
    is_enabled = db.Column(db.Boolean, default=True)
    valid_till = db.Column(db.DateTime)
    share_video = db.Column(db.Boolean, default=True)
    share_feedback = db.Column(db.Boolean, default=True)

class OrgCustomCategories(db.Model):
    __tablename__ = 'org_custom_categories'
    id = db.Column(db.Integer, primary_key = True)
    orgid = db.Column(db.Integer,db.ForeignKey('organization.orgid'))
    custom_categories = db.Column(db.JSON)
    status = db.Column(db.Integer)

class SystemTestLogs(db.Model):
    __tablename__ = 'systemtest_logs'
    logid = db.Column(db.Integer, primary_key = True)
    userid = db.Column(db.Integer, db.ForeignKey('users.userid'))
    logs = db.Column(db.JSON)
    created_on = db.Column(db.DateTime)

class Master(db.Model):
	__tablename__ = 'master'
	id = db.Column(db.Integer, primary_key=True)
	key = db.Column(db.Text)
	value = db.Column(db.Text)
	updated_time = db.Column(db.Date)



