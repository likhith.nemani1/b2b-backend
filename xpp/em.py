# -*- coding: utf-8 -*-
from flask import render_template
from flask_mail import Mail, Message
from xpp import app
from xpp.newemail import *
from json2html import *

# mail= Mail(app)
class Email:	
	def em_create_password(self, meta, user, content):
		with app.app_context():
			em = Message('Welcome to {}! Create your Account Password'.format(meta['app_name']),sender=content['contactemail'], recipients=[user.email])
			em.html = render_template('email_create_password.html', meta=meta, user=user, content=content)
			app.logger.info(type(em))
			app.logger.info(em.html)
			emailViaAPIRequest(em.subject, app.config['DEFAULT_EMAIL_SENDER'], [user.email], em.html.replace('"','\'' ), from_name="InterviewBuddy Pro")
			app.logger.info("em_create_password email sent")
			return em


	
	def em_email_feedback(self, meta, user, interview , content):
		with app.app_context():
			em = Message('Share Your Interview Feedback - ID: '+str(interview.interviewid)+'  Time: '+str(content['datetime']),sender=content['contactemail'], recipients=[user.email])
			em.html = render_template('email_feedback.html', meta=meta, user=user,interview = interview, content=content)
			app.logger.info("content from em_email_feedback")
			# app.logger.info(content)
			emailViaAPIRequest(em.subject, app.config['SHARE_FEEDBACK_SENDER'], [user.email], em.html.replace('"','\'' ), from_name="InterviewBuddy Pro Share")
			app.logger.info("em_email_feedback email sent")
			return em

	def em_email_cancel_interview(self, meta, user, interview , content):
		with app.app_context():
			em = Message('Interview Cancelled- ID: '+str(interview.interviewid)+'  Time: '+str(content['datetime']),sender=content['contactemail'], recipients=[user.email])
			em.html = render_template('email_cancel_interview.html', meta=meta, user=user,interview = interview, content=content)
			app.logger.info("content from em_email_cancel_interview")
			emailViaAPIRequest(em.subject, app.config['DEFAULT_EMAIL_SENDER'], [user.email], em.html.replace('"','\'' ), from_name="InterviewBuddy Pro Cancellation", cc_email=app.config['DEFAULT_CC_EMAIL'])
			app.logger.info("em_email_cancel_interview email sent")
			return em

	def em_email_cancel_async_interview(self, meta, user, interview , content):
		with app.app_context():
			em = Message('Interview Cancelled- ID: '+str(interview.asyncid)+'  Time: '+str(content['datetime']),sender=content['contactemail'], recipients=[user.email])
			em.html = render_template('email_cancel_async_interview.html', meta=meta, user=user,interview = interview, content=content)
			app.logger.info("content from em_email_cancel_async_interview")
			emailViaAPIRequest(em.subject, app.config['DEFAULT_EMAIL_SENDER'], [user.email], em.html.replace('"','\'' ), from_name = "InterviewBuddy Pro Cancellation", cc_email=app.config['DEFAULT_CC_EMAIL'])
			app.logger.info("em_email_cancel_async_interview email sent")
			return em
	
	def em_reset_password(self, meta, user, content):
		with app.app_context():
			em = Message('Reset Your {} Account Password'.format(meta['app_name']),sender=content['contactemail'], recipients=[user.email])
			em.html = render_template('email_reset_password.html', meta=meta, user=user, content=content)
			emailViaAPIRequest(em.subject, app.config['DEFAULT_EMAIL_SENDER'], [user.email], em.html.replace('"','\'' ), from_name = "InterviewBuddy Pro Reset")
			app.logger.info("em_reset_password email sent")
			return em

	
	def em_join_interview_link(self, meta, user, interview , content):
		with app.app_context():
			app.logger.info("content")
			app.logger.info(content)
			subject = 'Join '+interview.interviewtitle + ' Interview - ID: '
			em = Message(subject+str(interview.interviewid)+'  Time: '+str(content['datetime']),sender=content['contactemail'], recipients=[user.email])
			em.html = render_template('join_interview.html', meta=meta, user=user,interview = interview, content=content)
			app.logger.info("content from em_join_interview_link")
			emailViaAPIRequest(em.subject, app.config['DEFAULT_EMAIL_SENDER'], [user.email], em.html.replace('"','\'' ), from_name = "InterviewBuddy Pro Join", cc_email=app.config['DEFAULT_CC_EMAIL'])
			app.logger.info("em_join_interview_link email sent")
			return em
	
	def em_notify_interviewer_about_feedback(self,interview_details,interview):
		with app.app_context():
			app.logger.info("In em_notify_interviewer_about_feedback")
			em = Message('Feedback reminder '+ str(interview_details['dateandtime']),sender=interview_details['contactemail'], recipients=[interview_details['email']])
			em.html = render_template('email_notify_interviewer_for_feedback.html', interview_details=interview_details,interview=interview)
			app.logger.info(em)
			app.logger.info("content from em_notify_interviewer_about_feedback")
			emailViaAPIRequest(em.subject, app.config['DEFAULT_EMAIL_SENDER'], [interview_details['email']], em.html.replace('"','\'' ), from_name = "InterviewBuddy Pro Reminder")
			app.logger.info("em_notify_interviewer_about_feedback email sent")
			return em

	def em_async_interview_link(self,meta,user,interview,content):
		with app.app_context():
			app.logger.info("In em_async_interview_link")
			em = Message('Async Interview - ID: AS'+str(interview.asyncid)+' | Valid until '+str(content['dateandtime']),sender=content['contactemail'], recipients=[user.email])
			#em = Message('Async Interview- ID: '+str(interview.asyncid)+' Time: '+str(content['dateandtime']),sender=content['contactemail'], recipients=[user.email])
			em.html = render_template('email_notify_async_interview_link.html',meta=meta,user=user,content=content)
			# app.logger.info(em)
			app.logger.info("content from em_async_interview_link")
			emailViaAPIRequest(em.subject, app.config['DEFAULT_EMAIL_SENDER'], [user.email], em.html.replace('"','\'' ), from_name = "InterviewBuddy Pro Join")
			app.logger.info("em_async_interview_link email sent")
			return 	em		

	def em_share_interview(self,meta,content):
		with app.app_context():
			app.logger.info("In em_share_interview")
			em = Message('Interview Access', sender=content['contactemail'], recipients=[content['email_id']])
			em.html = render_template('email_share_interview.html',content=content)
			app.logger.info(em)
			app.logger.info("content from em_share_interview")
			emailViaAPIRequest(em.subject, app.config['SHARE_FEEDBACK_SENDER'], [content['email_id']], em.html.replace('"','\'' ), from_name = "InterviewBuddy Pro Share")
			app.logger.info("em_share_interview email sent")
			return em
		
	def em_share_interview_custom(self,meta,content):
		with app.app_context():
			app.logger.info("In em_share_interview_custom")
			em = Message('Interview Access', sender=content['contactemail'], recipients=[content['email_id']])
			em.html = render_template('email_share_interview_custom.html',content=content)
			app.logger.info(em)
			app.logger.info("content from em_share_interview_custom")
			emailViaAPIRequest(em.subject, app.config['SHARE_FEEDBACK_SENDER'],[content['email_id']], em.html.replace('"','\'' ), from_name = "InterviewBuddy Pro Share")
			app.logger.info("em_share_interview_custom email sent")
			return em

	def em_send_system_logs(self,systemLogsID, from_email, to_email, html_logs, cc_email=None, bcc_email = None):
		with app.app_context():
			app.logger.info("In em_send_system_logs")
			em = Message(app.config['APP_NAME']+': System Logs '+str(systemLogsID), sender = 'hello@interviewbuddy.app', recipients = to_email)
			em.html = "<html><body>Dear, <br/>Please check system logs below<br/><br/>"+html_logs+"</body></html>"
			app.logger.info(em)
			app.logger.info("content from em_send_system_logs")
			emailViaAPIRequest(em.subject, app.config['DEFAULT_EMAIL_SENDER'], to_email, em.html.replace('"','\'' ), cc_email=cc_email, bcc_email=bcc_email, from_name = "InterviewBuddy Pro Test  Logs")
			app.logger.info("em_send_system_logs email sent")
			return em
