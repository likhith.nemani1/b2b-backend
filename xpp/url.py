class L:
	join_interview = '/app/#/access/join_interview/{join_link}'
	interview_feedback = '/app/#/access/feedback/{feedback_link}'
	async_interview_url = '/app/#/access/join_async_interview/{async_interview_link}'
	system_test = '/app/#/access/system/test'
	error = '/app/#/access/error/{error_result}'
	reset_password = '/app/#/access/change_password/{rt}'
	view_interview = '/app/#/access/view/interview/{access_link}'