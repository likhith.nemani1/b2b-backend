#!/bin/bash
# service postgresql stop
service nginx stop
cd /root/xase/
ps auxwww | grep 'celery worker'| awk '{print $2}' | xargs kill -9
ps auxwww | grep 'uwsgi --ini'| awk '{print $2}' | xargs kill -9
su - xu << 'EOF'
cd /home/xu/helpy
kill -QUIT `cat /home/xu/helpy/unicorn.pid`
exit
EOF
