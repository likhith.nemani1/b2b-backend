#!/bin/bash
# service postgresql restart
sudo service nginx restart
cd /var/www/interviewbuddy/backend/
ps auxwww | grep 'celery worker'| awk '{print $2}' | xargs kill -9 2>/dev/null
XC='devd' C_FORCE_ROOT='true' celery -A xpp.tq.celery worker --loglevel=INFO --logfile=logs/celery.log --concurrency=1 --beat -s logs/celerybeat-schedule& 2>/dev/null
ps auxwww | grep 'uwsgi --ini'| awk '{print $2}' | xargs kill -9 2>/dev/null
XC='devd' uwsgi --ini /var/www/interviewbuddy/backend/uwsgi.ini 2>/dev/null
#su - ibbusiness << 'EOF'
# cd /home/xu/helpy
# kill -QUIT `cat /home/xu/helpy/unicorn.pid`
# RAILS_ENV="production" RAILS_RELATIVE_URL_ROOT="/help" unicorn_rails -c /home/xu/helpy/config/unicorn.rb -D
exit
EOF
