#!/bin/bash
apt-get update
apt-get upgrade
mkdir /root/xase
apt-get install python-dev python-pip
apt-get install libpq-dev postgresql postgresql-contrib
apt-get install libxml2-dev libxslt1-dev
apt-get install libjpeg-dev
apt-get install p7zip-full
apt-get install gitapt-get install git
apt-get install git
git clone https://github.com/certbot/certbot /root/xase/certbot/
apt-get install nginx
pip install uwsgi
pip install --upgrade pip
apt-get install rabbitmq-server
apt-get install mosh
apt-get install libreadline-dev
