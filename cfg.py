import os
from datetime import datetime

cn = os.getenv('XC', 'devd')
if cn in ['devx', 'devt']:
    from envX import Environment
else:
    from env import Environment

basedir = '/var/www/backend/'

DEBUG = True


class BaseConfig(Environment):
    # Built-in
    DEBUG = True
    MAX_CONTENT_LENGTH = 16 * 1024 * 1024  # MB

    # Flask Extensions
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    CORS = True

    # Enable https://www.google.com/settings/security/lesssecureapps
    # Verify on server https://www.google.com/accounts/DisplayUnlockCaptcha
    MAIL_PORT = 587
    MAIL_USE_TLS = True
    MAIL_DEFAULT_SENDER = Environment.MAIL_USERNAME

    CELERY_BROKER_URL = 'amqp://'
    # CELERY_RESULT_BACKEND = ''
    CELERY_TIMEZONE = 'UTC'

    # Secrets
    SECRET_EPOCH = datetime(2007, 7, 7)

    # xase
    APP_NAME = 'InterviewBuddy Pro'
    LOG_FILE = os.path.join('', 'logs', 'app.log')
    SESSION_LENGTH = 30 * 60  # Seconds
    TOKEN_EXPIRES_IN = 400 * 60  # Seconds
    EMAIL_VC_EXPIRES_IN = 12 * 60 * 60  # Seconds
    RESET_PASSWORD_EXPIRES_IN = 3 * 60 * 60  # Seconds
    MOBILE_VC_EXPIRES_IN = 30 * 60  # Seconds
    #expire case
    SELF_RECORD_EXPIRES_IN = 15 * 1440 * 60  # Seconds (15 days)
    #end expire
    MOBILE_VC_LENGTH = 4
    ITEMS_PER_PAGE = 10
    TEST_SYSTEM_OPENTOK_EXPIRES_IN = 2 * 60  # Seconds (2 min)
    DASHBOARD_LIMITS = {'sessions': 6, 'resources': 4, 'bills': 5, 'graph': 10, 'days': 6}
    ALLOWED_IMAGE_EXTENSIONS={'jpg','png','jpeg','JPEG','JPG','PNG'}
    	
    # Serving
    STATIC_DIRECTORY = os.path.join(basedir, 'static')

	
class DevD(BaseConfig):
    # Built-in
    DEBUG = True if os.getenv('XDEBUG', '').strip().lower() in ['true', '1'] else False

    # Flask Extensions
    # SQLALCHEMY_DATABASE_URI = 'postgresql://db-b2b:{}@b2b-prod-do-user-1875010-0.b.db.ondigitalocean.com:25060/defaultdb'.format(BaseConfig.DATABASE_PASSWORD)
    SQLALCHEMY_DATABASE_URI = 'postgresql://postgres:admin123@localhost:5432/defaultdb'

    # Serving
    RELOADER = True
    HOST = '0.0.0.0'
    PORT = 7000
    SSL_CONTEXT = None
    LOGOS_DIRECTORY = '/var/www/logos/'


class DevX(BaseConfig):
    # Built-in
    DEBUG = True

    # Serving
    RELOADER = True
    HOST = '127.0.0.1'
    # HOST = '0.0.0.0'
    PORT = 7000
    SSL_CONTEXT = None
    
    SQLALCHEMY_DATABASE_URI = 'postgresql://db-b2b:{}@b2b-prod-do-user-1875010-0.b.db.ondigitalocean.com:25060/defaultdb'.format(BaseConfig.DATABASE_PASSWORD)
    
    # xase
    APP_URL = 'http://' + HOST + ':' + str(PORT)
    SESSION_LENGTH = 30 * 60
    TOKEN_EXPIRES_IN = 24000
    ITEMS_PER_PAGE = 3
    LOGOS_DIR = '/var/www/logos/'

class DevT(DevX):
    APP_URL = 'https://t.atx.sx'
    RELOADER = False


configs = {'devx': DevX, 'devd': DevD, 'devt': DevT}
default_config = configs[cn]
print default_config.__name__
