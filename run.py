# -*- coding: utf-8 -*-


from xpp import app
import sys
app.logger.debug(app.config)

reload(sys)
sys.setdefaultencoding('utf8')

if __name__ == '__main__':
	app.run(debug=app.config['DEBUG'], use_reloader=app.config['RELOADER'], host=app.config['HOST'], port=app.config['PORT'], ssl_context=app.config['SSL_CONTEXT'])
